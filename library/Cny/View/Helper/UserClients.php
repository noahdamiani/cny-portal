<?php
class Cny_View_Helper_UserClients
{
	protected $view;
	function setView($view)
	{
		$this->view = $view;
	}

	function userClients()
	{
		$auth = Zend_Auth::getInstance();
		if( $auth->hasIdentity() ){
			$user = $auth->getIdentity();

			$db = Zend_Db_Table::getDefaultAdapter();

			$sql = "SELECT c.id, c.ClientName FROM Clients AS c LEFT JOIN users_client AS uc ON uc.client_id = c.id WHERE uc.user_id = {$user->ID}";
			$clients = $db->fetchPairs($sql);

			if (!$clients || count($clients) == 1)
				return false;
			else {
				return $clients;
			}
		}else{
			return false;
		}
	}
}

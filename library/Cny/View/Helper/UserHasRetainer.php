<?php
class Cny_View_Helper_UserHasRetainer
{
	protected $view;
	function setView($view)
	{
		$this->view = $view;
	}

	function userHasRetainer()
	{
		$auth = Zend_Auth::getInstance();

		if( $auth->hasIdentity() ){
			$db = Zend_Db_Table::getDefaultAdapter();
			$user = $auth->getIdentity();

			$sql = "SELECT id FROM Retainers WHERE ClientID = {$user->ClientID}";
			$retainer = $db->fetchOne($sql);

			if ($retainer > 0)
				return true;
			else
				return false;
		}else{
			return false;
		}
	}
}
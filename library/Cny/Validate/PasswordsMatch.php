<?php
	class Cny_Validate_PasswordsMatch extends Zend_Validate_Abstract
	{
		const ERROR_NOMATCH = 'noMatch';
		protected $_messageTemplates = array(
			'noMatch' => 'Passwords do not match',
		);
		protected $_element;
		public function __construct($options = null)
		{
			if (null === $options) {
				return;
			}
			if (is_string($options)) {
				$this->_setElement($options);
				return;
			}
			if (is_array($options)) {
				if (array_key_exists('element', $options)) {
					$this->_setElement($options['element']);
					return;
				}
			}
			throw new InvalidArgumentException('$options is invalid');
		}

		public function _setElement($element)
		{
			$this->_element = (string) $element;
			return $this;
		}


		public function isValid($value, $context = null)
		{
			if (!is_array($context)) {
				throw new RuntimeException('Context is required to validate passwords');
			}
			if (!isset($context[$this->_element])) {
				$this->_error(self::ERROR_NOMATCH);
				return false;
			}
			if ($context[$this->_element] === $value) {
				return true;
			}
			$this->_error(self::ERROR_NOMATCH);
			return false;
		}
	}

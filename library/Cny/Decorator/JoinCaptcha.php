<?php
	class Cny_Decorator_JoinCaptcha extends Zend_Form_Decorator_Abstract
	{
		public function render($content)
		{
 			$element        =  $this->getElement();
 			$captcha_image  =  $element->getCaptcha()->render();
 			$captcha_id     =  $element->getCaptcha()->getId();
 			$name           =  $element->getFullyQualifiedName();
			$label          =  $element->getLabel();
			$label_id       =  $element->getId()."-input";

			$errors = '';
			if (count($element->getErrors()))
			{
				foreach ($element->getMessages() as $key => $message)
				{
					if ($key == Zend_Validate_NotEmpty::IS_EMPTY || $key == 'missingValue')
					{
						// customize empty message to indicate the problematic field
						$message = "Entering the characters above is required to proove you are human";
					}

					$errors .= '<li>'.$message.'</li>';
				}
				$errors = '<ul class="errors">'.$errors.'</ul>';
			}

			$output = <<<HTML
				<div id="captcha">
					<div id="captcha-top"></div>
					<div id="captcha-mid">
						<div class="top"></div>
						<div id="captcha-images">
							<input type="hidden" name="{$name}[id]" value="$captcha_id" id="captcha-id">
							$captcha_image
							<p><label for="captcha-input">$label</label></p>
							<p><input id="captcha-input" name="{$name}[input]" class="input-join"  type="text"></p>
						</div>
						<div class="bottom"></div>
					</div>
					<div id="captcha-bot"></div>
				</div>
				$errors
HTML;
				return $output;
			}
		}

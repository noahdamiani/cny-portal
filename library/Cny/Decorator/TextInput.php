<?php
	class Cny_Decorator_TextInput extends Zend_Form_Decorator_Abstract
	{

		protected $_format = '
			<label for="%s">%s</label>
			<input id="%s" name="%s" type="text" value="%s"/><br/>
		';

		public function render($content)
		{
			$element = $this->getElement();
			$name    = htmlentities($element->getFullyQualifiedName());
			$label   = htmlentities($element->getLabel());
			$id      = htmlentities($element->getId());
			$value   = htmlentities($element->getValue());

			$markup  = sprintf($this->_format, $name, $label, $id, $name, $value);

			$errors = '';
			if (count($element->getErrors()))
			{
				foreach ($element->getMessages() as $key => $message)
				{
					if ($key == Zend_Validate_NotEmpty::IS_EMPTY)
					{
						// customize empty message to indicate the problematic field
						$message = "$label is required and cannot be empty";
					}

					$errors .=
						'<div style="font-size: 0.9em;">' .
							$message .
						'</div>';
				}
			}

			if (strlen($errors))
			{
				$markup .= $errors;
			}

			$markup = "<p>$markup</p>";

			return $markup;
		}
	}

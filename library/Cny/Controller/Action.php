<?php

class Cny_Controller_Action extends Zend_Controller_Action
{
	public $_id;
	public $_class = '';
	public $_module;
	public $_controller;
	public $_action;
	public $_flashMessenger;

	public function init()
	{
		$this->_id = $this->getRequest()->getParam('id', null);
		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$this->_module = $this->_request->getModuleName();
		$this->_controller = $this->_request->getControllerName();
		$this->_action = $this->_request->getActionName();
		$request->module = $this->_module;
		$request->controller = $this->_controller;
		$request->action = $this->_action;
		$this->view->rqst = $request;
		$this->view->messages = array();
		$this->view->headTitle()->append(ucfirst($this->_module));
		$this->view->headTitle()->append(ucfirst($this->_controller));
		$this->view->headTitle()->append(ucfirst($this->_action));
		$this->_class = 'Admin_Model_'. ucfirst( $this->_controller );
	}

	public function indexAction()
	{
		$this->_redirect($this->_module.'/'.$this->_controller.'/list');
	}

	public function listAction()
	{
		$page = $this->getRequest()->getParam('page', 1);
		$rowsperpage = $this->getRequest()->getParam('rowsperpage', 30);
		$sort = $this->getRequest()->getParam('sort', 'modified');
		$sortdir = $this->getRequest()->getParam('sortdir', 'desc');
		$search = $this->getRequest()->getParam('search', array());

		$model = new $this->_class();
		$select = $model->getMapper()->getDbTable()->select();
		foreach( $search as $key => $val ){
			if( !empty($val) )
				$select->where( $key.' like ?', '%'.$val.'%' );
		}
		$select->order($sort.' '.$sortdir);
		$paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($select));
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage($rowsperpage);

		$this->view->paginator = $paginator;
		$this->view->sort = $sort;
		$this->view->sortdir = $sortdir;
		$this->view->search = $search;
	}

	function createAction()
	{
		$form = new Admin_Form_Element();
		if( $this->getRequest()->isPost() ){
			if( $form->isValid( $this->getRequest()->getParams() ) ){
				$data = $form->getValues();
				$model = new $this->_class();
				$model->setOptions( $data );
				try{
					$model->save();
				}catch(Exception $e){
					print($e->getMessage());
					exit;
				}
				$this->_redirect($this->_module.'/'.$this->_controller.'/list');
			}
		}
		$form->setAction('/'.$this->_module.'/'. $this->_controller .'/create');
		$this->view->form = $form;
	}

	public function updateAction()
	{
		$form = new Admin_Form_Element();
		if( $this->getRequest()->isPost() ){
			if( $form->isValid( $this->getRequest()->getParams() ) ){
				$data = $form->getValues();
				$model = new $this->_class();
				$model->setOptions( $data );
				$model->save();
				$contButton = $form->getElement('savecontinue');
				if( !is_null($contButton) && $contButton->isChecked() ){
					//continue;
				}else{
					$this->_redirect($this->_module.'/'.$this->_controller.'/list');
				}
			}
		}else{
			$id = $this->getRequest()->getParam('id');
			if( !is_null($id) ){
				$model = new $this->_class();
				$model->find($id);
				if( null !== ($model->getId()) ){
					$form->populate($model->toArray());
				}else{
					$this->_redirect($this->_module.'/'.$this->_controller.'/list');
					exit;
				}
			}else{
				$this->_redirect($this->_module.'/'.$this->_controller.'/list');
				exit;
			}
		}
		$this->view->title = $model->getTitle();
		$form->setAction('/'.$this->_module.'/'.$this->_controller.'/update');
		$this->view->form = $form;
	}

	public function deleteAction()
	{
		if( $this->getRequest()->isPost() ){
			//Insert data
		}
	}

	public function disableAction()
	{
		$id = $this->getRequest()->getParam('id');
		$hash = $this->getRequest()->getParam('hash');
		if( !is_null($hash) && 'HASH'.$id == $hash ){
			$model = new $this->_class();
			$model->setOptions( array('id'=>$id, 'status'=>'Disabled') );
			$model->save();
		}
		$this->_redirect($this->_module.'/'.$this->_controller.'/list');
		exit;
	}
}

<?php

class Cny_Controller_LayoutAction extends Zend_Controller_Action
{
	public $layout_view;
	public $layout_instance;

	public function init()
	{
		$this->layout_view     =  $this->_helper->layout->getLayoutInstance()->getView();
		$this->layout_instance =  $this->_helper->layout->getLayoutInstance();

 		$request     = $this->getRequest();
 		$controller  = $request->getControllerName();
 		$action      = $request->getActionName();

		$this->layout_view->nav_template = 'index';
		$this->layout_view->head_include = array(
			'layout_styles' => array(
				'master' => array('type' => 'css', 'file' => 'layout'),
			),
			'page_styles' => array(
				'section' => array('type' => 'css', 'file' => "$controller"),
				'page'    => array('type' => 'css', 'file' => "$controller-$action"),
			),
			'ie_styles' => array(
				'ie8' => array('type' => 'css', 'file' => 'ie8', 'cond' => array('op' => 'lte', 'vs' => '8')),
				'ie7' => array('type' => 'css', 'file' => 'ie7', 'cond' => array('op' => 'lte', 'vs' => '7')),
				'ie6' => array('type' => 'css', 'file' => 'ie6', 'cond' => array('op' => 'lt',  'vs' => '7')),
			),
			'master_scripts' => array(
				'master' => array('type' => 'js',  'file' => 'jquery-1.3.2.min'),
			),
			'jquery_plugins' => array(
				'png' => array('type' => 'js',  'file' => 'jquery.pngFix'),
			),
			'page_scripts' => array(
				'page' => array('type' => 'js',  'file' => "$controller-$action"),
			),
		);
	}
}

<?php
	require_once 'Zend/Captcha/Image.php';

	class Cny_Captcha_Image extends Zend_Captcha_Image
	{
		public function getUseNumbers ()
		{
			return $this->_useNumbers;
		}

		public function setUseNumbers ($useNumbers)
		{
			$this->_useNumbers = $useNumbers;
			return $this;
		}
	}

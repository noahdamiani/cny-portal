﻿// Aurigma Image Uploader Dual 6.x - IUEmbed Script Library
// Ukrainian Localization
// Copyright(c) Aurigma Inc. 2002-2007
// Version 6.5.6.0

//--------------------------------------------------------------------------
//ua_resources class
//--------------------------------------------------------------------------

ua_resources = {
    addParams: IULocalization.addParams,

    Language: "Ukrainian",

    // In different browsers the installation instructions are slightly different. So we provide browser specific instructions in addition to common messages.
    InstallationProgress: {
        // Common message. Shows on the loading progress dialog and on the instructions dialog.
        commonHtml: "<p>Aurigma Image Uploader ActiveX control is necessary to upload your files quickly and easily. You will be able to select multiple images in user-friendly interface instead of clumsy input fields with <strong>Browse</strong> button.</p>",
        // Progress message. Shows while loading Image Uploader ActiveX control.
        progressHtml: "<p><img src=\"{0}\" /><br />Loading Aurigma Image Uploader ActiveX...</p>",
        // Progress message. Shows while loading Image Uploader Java applet.
        javaProgressHtml: "<p><img src=\"{0}\" /><br />Loading Aurigma Image Uploader Java Applet...</p>",

        // IE6 specific progress message. Shows while loading Image Uploader control.
        beforeIE6XPSP2ProgressHtml: "<p>To install Image Uploader, please wait until the control will be loaded and click the <strong>Yes</strong> button when you see the installation dialog.</p>",
        // IE6 specific installation instructions to install Image Uploader ActiveX control.
        beforeIE6XPSP2InstructionsHtml: "<p>To install Image Uploader, please reload the page and click the <strong>Yes</strong> button when you see the control installation dialog. If you don't see installation dialog, please check your security settings.</p>",

        // IE6 on Windows XP SP2 and later specific progress message. Shows while loading Image Uploader control.
        IE6XPSP2ProgressHtml: "<p>Please wait until the control will be loaded.</p>",
        // IE6 on Windows XP SP2 and later specific installation instructions to install Image Uploader ActiveX control.
        IE6XPSP2InstructionsHtml: "<p>To install Image Uploader, please click on the <strong>Information Bar</strong> and select <strong>Install ActiveX Control</strong> from the dropdown menu. After page reload click <strong>Install</strong> when you see the control installation dialog. If you don't see Information Bar, please try to reload the page and/or check your security settings.</p>",

        // IE7 specific progress message. Shows while loading Image Uploader control.
        IE7ProgressHtml: "<p>Please wait until the control will be loaded.</p>",
        // IE7 specific installation instructions to install Image Uploader ActiveX control.
        IE7InstructionsHtml: "<p>To install Image Uploader, please click on the <strong>Information Bar</strong> and select <strong>Install ActiveX Control</strong> or <strong>Run ActiveX Control</strong> from the dropdown menu.</p><p>Then either click <strong>Run</strong> or after the page reload click <strong>Install</strong> when you see the control installation dialog. If you don't see Information Bar, please try to reload the page and/or check your security settings.</p>",

        // IE8 specific progress message. Shows while loading Image Uploader control.
        IE8ProgressHtml: "<p>Please wait until the control will be loaded.</p>",
        // IE8 specific installation instructions to install Image Uploader ActiveX control
        IE8InstructionsHtml: "<p>To install Image Uploader, please click on the <strong>Information Bar</strong> and select <strong>Install This Add-on</strong> or <strong>Run Add-on</strong> from the dropdown menu.</p><p>Then either click <strong>Run</strong> or after the page reload click <strong>Install</strong> when you see the control installation dialog. If you don't see Information Bar, please try to reload the page and/or check your security settings.</p>",

        // Update instructions. Shows while loading newer version of Image Uploader ActiveX control then installed on client machine.
        updateInstructions: "You need to update Image Uploader ActiveX control. Click <strong>Install</strong> or <strong>Run</strong> button when you see the control installation dialog. If you don't see installation dialog, please try to reload the page.",

        // Common install java message
        commonInstallJavaHtml: "<p>You need to install Java for running Image Uploader.</p>",

        // IE6 specific Java installation instructions.
        beforeIE6XPSP2InstallJavaHtml: "<p>To install Java, please reload the page and click the <strong>Yes</strong> button when you see the control installation dialog. If you don't see installation dialog, please check your security settings.</p>",

        // IE6 on Windows XP SP2 and later specific Java installation instructions.
        IE6XPSP2InstallJavaHtml: "<p>To install Java, please click on the <strong>Information Bar</strong> and select <strong>Install ActiveX Control</strong> from the dropdown menu. After page reload click <strong>Install</strong> when you see the control installation dialog. If you don't see Information Bar, please try to reload the page and/or check your security settings.</p>",

        // IE7 specific Java installation instructions.
        IE7InstallJavaHtml: "<p>To install Java, please click on the <strong>Information Bar</strong> and select <strong>Install ActiveX Control</strong> or <strong>Run ActiveX Control</strong> from the dropdown menu.</p><p>Then either click <strong>Run</strong> or after the page reload click <strong>Install</strong> when you see the control installation dialog. If you don't see Information Bar, please try to reload the page and/or check your security settings.</p>",

        // IE8 specific Java installation instructions.
        IE8InstallJavaHtml: "<p>To install Java, please click on the <strong>Information Bar</strong> and select <strong>Install This Add-on</strong> or <strong>Run Add-on</strong> from the dropdown menu.</p><p>Then either click <strong>Run</strong> or after the page reload click <strong>Install</strong> when you see the control installation dialog. If you don't see Information Bar, please try to reload the page and/or check your security settings.</p>",

        // Mac specific Java installation instructions.
        MacInstallJavaHtml: "<p>Use the <a href=\"http://support.apple.com/kb/HT1338\">Software Update</a> feature (available on the Apple menu) to check that you have the most up-to-date version of Java for your Mac.</p>",

        // Java installation instructions for other browsers.
        miscBrowsersInstallJavaHtml: "<p>Please <a href=\"http://www.java.com/getjava\">download</a> and install Java.</p>"
    },

    ImageUploader: {
        AddFolderDialogButtonCancelText: "Відмінити",
        AddFolderDialogButtonSkipAllText: "Пропустити усі",
        AddFolderDialogButtonSkipText: "Пропустити",
        AddFolderDialogTitleText: "Додавання папки...",
        AuthenticationRequestBasicText: "[Name] вимагає ідентифікації.",
        AuthenticationRequestButtonCancelText: "Відмінити",
        //IGNORE
        AuthenticationRequestButtonOkText: "OK",
        AuthenticationRequestDomainText: "Домен:",
        AuthenticationRequestLoginText: "Логін:",
        AuthenticationRequestNtlmText: "[Name] вимагає ідентифікації.",
        AuthenticationRequestPasswordText: "Пароль:",
        ButtonAddAllToUploadListText: "Додати усі",
        ButtonAddFilesText: "Додати файли...",
        ButtonAddFoldersText: "Додати папки...",
        ButtonAddToUploadListText: "Додати",
        ButtonAdvancedDetailsCancelText: "Відмінити",

        ButtonCheckAllText: "Виділити усі",
        ButtonDeleteFilesText: "", //"Видалити файли"
        ButtonDeselectAllText: "Зняти виділення",
        ButtonPasteText: "", //"Вставити"
        ButtonRemoveAllFromUploadListText: "Видалити усі",
        ButtonRemoveFromUploadListText: "Видалити",
        ButtonSelectAllText: "Виділити усі",
        ButtonSendText: "Завантажити",
        ButtonStopText: "", //"Стоп"

        ButtonUncheckAllText: "Зняти виділення",
        CmykImagesAreNotAllowedText: "Файл є CMYK",
        DescriptionEditorButtonCancelText: "Відмінити",
        //IGNORE
        DescriptionEditorButtonOkText: "OK",

        //To be supplied
        DeleteFilesDialogTitleText: "Confirm File Delete",
        //To be supplied
        DeleteSelectedFilesDialogMessageText: "Are you sure you want to permanently delete selected items?",
        //To be supplied
        DeleteUploadedFilesDialogMessageText: "Are you sure you want to permanently delete uploaded items?",
        DimensionsAreTooLargeText: "Зображення надто велике",
        DimensionsAreTooSmallText: "Зображення надто мале",
        DropFilesHereText: "Перетягніть файл(и) сюди",
        EditDescriptionText: "Додати опис...",

        //To be supplied
        ErrorDeletingFilesDialogMessageText: "Could not delete [Name]",
        FileIsTooLargeText: "Файл надто великий",
        FileIsTooSmallText: "Файл надто малий",
        HoursText: "години",
        IncludeSubfoldersText: "Включити усі підпапки",
        KilobytesText: "КБ",
        LargePreviewGeneratingPreviewText: "Створювання превью...",
        LargePreviewIconTooltipText: "Превью",
        LargePreviewNoPreviewAvailableText: "Превью недоступне.",
        ListColumnFileNameText: "Ім'я",
        ListColumnFileSizeText: "Размір",
        ListColumnFileTypeText: "Тип",
        ListColumnLastModifiedText: "Змінений",
        ListKilobytesText: "КБ",
        LoadingFilesText: "Сканування файлів...",
        MegabytesText: "МБ",
        MenuAddAllToUploadListText: "Додати усі до списку загрузки",
        MenuAddToUploadListText: "Додати до списку загрузки",
        MenuArrangeByModifiedText: "Змінений",
        MenuArrangeByNameText: "Ім'я",
        MenuArrangeByPathText: "Путь",
        MenuArrangeBySizeText: "Розмір",
        MenuArrangeByText: "Упорядкувати значки",
        MenuArrangeByTypeText: "Тип",
        MenuArrangeByUnsortedText: "Непосортовані",
        MenuDeselectAllText: "Зняти виділення",
        MenuDetailsText: "Таблиця",
        MenuIconsText: "Значки",
        MenuInvertSelectionText: "Зворотнє виділення",
        MenuListText: "Список",
        MenuRefreshText: "Оновити",
        MenuRemoveAllFromUploadListText: "Видалити усі з списку загрузок",
        MenuRemoveFromUploadListText: "Видалити з списку загрузок",
        MenuSelectAllText: "Виділити усі",
        MenuThumbnailsText: "Эскізи",
        MessageBoxTitleText: "Загрузка зображень",
        MessageCannotConnectToInternetText: "підключення до Інтернету не відбулось.",
        MessageCmykImagesAreNotAllowedText: "Зображення CMYK не дозволено",
        MessageDimensionsAreTooLargeText: "Зображення [Name] не може бути вибрано. Розмір зображення ([OriginalImageWidth]x[OriginalImageHeight]) мусить бути менше [MaxImageWidth]x[MaxImageHeight] пікселів.",
        MessageDimensionsAreTooSmallText: "Зображення [Name] не може бути вибрано. Розмір зображення ([OriginalImageWidth]x[OriginalImageHeight]) мусить бути більше [MinImageWidth]x[MinImageHeight] пікселів.",
        MessageFileSizeIsTooSmallText: "Файл [Name] не може бути вибран. Розмір файла менше допустимого значення ([Limit] КБ).",
        MessageMaxFileCountExceededText: "Файл [Name] не може бути вибран. Ви не можете вибрати больш ніж [Limit] файлів.",
        MessageMaxFileSizeExceededText: "Файл [Name] не може бути вибран для завантаження. Розмір файла перевищує [Limit] КБ.",
        MessageMaxTotalFileSizeExceededText: "Файл [Name] не може бути вибран для завантаження. Загальний розмір файлів перевищує [Limit] КБ.",
        MessageNoInternetSessionWasEstablishedText: "Не встановлена Інтернет-сесія.",
        MessageNoResponseFromServerText: "Нема відповіді від сервера.",
        MessageRetryOpenFolderText: "Остання відвідувана папка недоступна. Можливо вона розміщена на зйомному носії. Встановiть носій та натисніть кнопку Повторити. Щоб продовжити дію, натисніть кнопку Відминити.",
        MessageServerNotFoundText: "Сервер чи проксі-сервер [Name] не знайден.",
        MessageSwitchAnotherFolderWarningText: "Ви намагаєтесь перейти в іншу папку. Це приведе до зняття виділення вибраних файлов.\n\nЩоб продовжити і зняти виділення, натисніть OK.\nЩоб сохранити виділення и залишитись в чинній папці, натисніть Відминити.",
        MessageUnexpectedErrorText: "Відбулась помилка під час загрузки. Якщо Ви бачите це повідомлення, зверніться до веб мастера.",
        MessageUploadCancelledText: "Завантаження відмінено.",
        MessageUploadCompleteText: "Завантаження закінчено.",
        MessageUploadFailedText: "Завантаження не відбулось (зв'язок було розірвано).",
        MessageUserSpecifiedTimeoutHasExpiredText: "Час очікувіння, визначений користувачем, вичерпано.",
        MinutesText: "мінут",
        ProgressDialogCancelButtonText: "Відмінити",
        ProgressDialogCloseButtonText: "Закрити",
        ProgressDialogCloseWhenUploadCompletesText: "Закрити діалогове вікно післе завантаження.",
        ProgressDialogEstimatedTimeText: "Залишилось часу: [Current] з [Total]",
        ProgressDialogPreparingDataText: "Підготовка даних...",
        ProgressDialogSentText: "Завантаження: [Current] з [Total]",
        ProgressDialogTitleText: "Завантаження файлів",
        ProgressDialogWaitingForResponseFromServerText: "Очікування відповіді від сервера...",
        ProgressDialogWaitingForRetryText: "Очікування наступної спроби...",
        RemoveIconTooltipText: "Видалити",
        RotateIconClockwiseTooltipText: "Повернути за часовою стрілкою",
        RotateIconCounterclockwiseTooltipText: "Повернути проти часової стрілки",
        SecondsText: "секунд",
        UnixFileSystemRootText: "Файлова система",
        UnixHomeDirectoryText: "Домашній каталог"
    }
}
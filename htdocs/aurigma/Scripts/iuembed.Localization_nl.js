﻿// Aurigma Image Uploader Dual 6.x - IUEmbed Script Library
// Dutch Localization
// www.mmibasisschool.be
// Copyright(c) Aurigma Inc. 2002-2007
// Version 6.5.6.0

//--------------------------------------------------------------------------
//nl_resources class
//--------------------------------------------------------------------------

nl_resources = {
    addParams: IULocalization.addParams,

    Language: "Dutch",

    // In different browsers the installation instructions are slightly different. So we provide browser specific instructions in addition to common messages.
    InstallationProgress: {
        // Common message. Shows on the loading progress dialog and on the instructions dialog.
        commonHtml: "<p>Aurigma Image Uploader ActiveX control is necessary to upload your files quickly and easily. You will be able to select multiple images in user-friendly interface instead of clumsy input fields with <strong>Browse</strong> button.</p>",
        // Progress message. Shows while loading Image Uploader ActiveX control.
        progressHtml: "<p><img src=\"{0}\" /><br />Loading Aurigma Image Uploader ActiveX...</p>",
        // Progress message. Shows while loading Image Uploader Java applet.
        javaProgressHtml: "<p><img src=\"{0}\" /><br />Loading Aurigma Image Uploader Java Applet...</p>",

        // IE6 specific progress message. Shows while loading Image Uploader control.
        beforeIE6XPSP2ProgressHtml: "<p>To install Image Uploader, please wait until the control will be loaded and click the <strong>Yes</strong> button when you see the installation dialog.</p>",
        // IE6 specific installation instructions to install Image Uploader ActiveX control.
        beforeIE6XPSP2InstructionsHtml: "<p>To install Image Uploader, please reload the page and click the <strong>Yes</strong> button when you see the control installation dialog. If you don't see installation dialog, please check your security settings.</p>",

        // IE6 on Windows XP SP2 and later specific progress message. Shows while loading Image Uploader control.
        IE6XPSP2ProgressHtml: "<p>Please wait until the control will be loaded.</p>",
        // IE6 on Windows XP SP2 and later specific installation instructions to install Image Uploader ActiveX control.
        IE6XPSP2InstructionsHtml: "<p>To install Image Uploader, please click on the <strong>Information Bar</strong> and select <strong>Install ActiveX Control</strong> from the dropdown menu. After page reload click <strong>Install</strong> when you see the control installation dialog. If you don't see Information Bar, please try to reload the page and/or check your security settings.</p>",

        // IE7 specific progress message. Shows while loading Image Uploader control.
        IE7ProgressHtml: "<p>Please wait until the control will be loaded.</p>",
        // IE7 specific installation instructions to install Image Uploader ActiveX control.
        IE7InstructionsHtml: "<p>To install Image Uploader, please click on the <strong>Information Bar</strong> and select <strong>Install ActiveX Control</strong> or <strong>Run ActiveX Control</strong> from the dropdown menu.</p><p>Then either click <strong>Run</strong> or after the page reload click <strong>Install</strong> when you see the control installation dialog. If you don't see Information Bar, please try to reload the page and/or check your security settings.</p>",

        // IE8 specific progress message. Shows while loading Image Uploader control.
        IE8ProgressHtml: "<p>Please wait until the control will be loaded.</p>",
        // IE8 specific installation instructions to install Image Uploader ActiveX control
        IE8InstructionsHtml: "<p>To install Image Uploader, please click on the <strong>Information Bar</strong> and select <strong>Install This Add-on</strong> or <strong>Run Add-on</strong> from the dropdown menu.</p><p>Then either click <strong>Run</strong> or after the page reload click <strong>Install</strong> when you see the control installation dialog. If you don't see Information Bar, please try to reload the page and/or check your security settings.</p>",

        // Update instructions. Shows while loading newer version of Image Uploader ActiveX control then installed on client machine.
        updateInstructions: "You need to update Image Uploader ActiveX control. Click <strong>Install</strong> or <strong>Run</strong> button when you see the control installation dialog. If you don't see installation dialog, please try to reload the page.",

        // Common install java message
        commonInstallJavaHtml: "<p>You need to install Java for running Image Uploader.</p>",

        // IE6 specific Java installation instructions.
        beforeIE6XPSP2InstallJavaHtml: "<p>To install Java, please reload the page and click the <strong>Yes</strong> button when you see the control installation dialog. If you don't see installation dialog, please check your security settings.</p>",

        // IE6 on Windows XP SP2 and later specific Java installation instructions.
        IE6XPSP2InstallJavaHtml: "<p>To install Java, please click on the <strong>Information Bar</strong> and select <strong>Install ActiveX Control</strong> from the dropdown menu. After page reload click <strong>Install</strong> when you see the control installation dialog. If you don't see Information Bar, please try to reload the page and/or check your security settings.</p>",

        // IE7 specific Java installation instructions.
        IE7InstallJavaHtml: "<p>To install Java, please click on the <strong>Information Bar</strong> and select <strong>Install ActiveX Control</strong> or <strong>Run ActiveX Control</strong> from the dropdown menu.</p><p>Then either click <strong>Run</strong> or after the page reload click <strong>Install</strong> when you see the control installation dialog. If you don't see Information Bar, please try to reload the page and/or check your security settings.</p>",

        // IE8 specific Java installation instructions.
        IE8InstallJavaHtml: "<p>To install Java, please click on the <strong>Information Bar</strong> and select <strong>Install This Add-on</strong> or <strong>Run Add-on</strong> from the dropdown menu.</p><p>Then either click <strong>Run</strong> or after the page reload click <strong>Install</strong> when you see the control installation dialog. If you don't see Information Bar, please try to reload the page and/or check your security settings.</p>",

        // Mac specific Java installation instructions.
        MacInstallJavaHtml: "<p>Use the <a href=\"http://support.apple.com/kb/HT1338\">Software Update</a> feature (available on the Apple menu) to check that you have the most up-to-date version of Java for your Mac.</p>",

        // Java installation instructions for other browsers.
        miscBrowsersInstallJavaHtml: "<p>Please <a href=\"http://www.java.com/getjava\">download</a> and install Java.</p>"
    },

    ImageUploader: {
        AddFolderDialogButtonCancelText: "Annuleer",
        AddFolderDialogButtonSkipAllText: "Sla alles over",
        AddFolderDialogButtonSkipText: "Sla over",
        AddFolderDialogTitleText: "Folder toevoegen...",
        AuthenticationRequestBasicText: "[Name] vereist een paswoord.",
        AuthenticationRequestButtonCancelText: "Annuleer",
        //IGNORE
        AuthenticationRequestButtonOkText: "OK",
        AuthenticationRequestDomainText: "Domein:",
        //IGNORE
        AuthenticationRequestLoginText: "Login:",
        AuthenticationRequestNtlmText: "[Name] vereist een paswoord.",
        AuthenticationRequestPasswordText: "Paswoord:",
        ButtonAddAllToUploadListText: "Alles toevoegen",
        ButtonAddFilesText: "Bestanden toevoegen...",
        ButtonAddFoldersText: "Mappen toevoegen...",
        ButtonAddToUploadListText: "Toevoegen",
        ButtonAdvancedDetailsCancelText: "Annuleer",

        ButtonCheckAllText: "Alles selecteren",
        ButtonDeleteFilesText: "", //"Delete Files"
        ButtonDeselectAllText: "Alles deselecteren",
        ButtonPasteText: "", //"Paste"
        ButtonRemoveAllFromUploadListText: "Alles verwijderen",
        ButtonRemoveFromUploadListText: "Verwijderen",
        ButtonSelectAllText: "Alles selecteren",
        ButtonSendText: "Verstuur",
        ButtonStopText: "", //"Stop"

        ButtonUncheckAllText: "Alles deselecteren",
        CmykImagesAreNotAllowedText: "Het bestand is  CMYK alleen RGB is toegestaan",
        DescriptionEditorButtonCancelText: "Annuleer",
        //IGNORE
        DescriptionEditorButtonOkText: "OK",

        //To be supplied
        DeleteFilesDialogTitleText: "Confirm File Delete",
        //To be supplied
        DeleteSelectedFilesDialogMessageText: "Are you sure you want to permanently delete selected items?",
        //To be supplied
        DeleteUploadedFilesDialogMessageText: "Are you sure you want to permanently delete uploaded items?",
        DimensionsAreTooLargeText: "Afbeelding is te groot",
        DimensionsAreTooSmallText: "Afbeelding is te klein",
        DropFilesHereText: "Plaats de bestanden hier",
        EditDescriptionText: "Wijzig beschrijving...",

        //To be supplied
        ErrorDeletingFilesDialogMessageText: "Could not delete [Name]",
        FileIsTooLargeText: "Bestand is te groot",
        FileIsTooSmallText: "Bestand is te klein",
        HoursText: "uur",
        IncludeSubfoldersText: "Inclusief submappen",
        //IGNORE
        KilobytesText: "kilobytes",
        LargePreviewGeneratingPreviewText: "Genereer preview",
        //REVIEW
        LargePreviewIconTooltipText: "Preview Thumbnail",
        LargePreviewNoPreviewAvailableText: "Geen preview beschikbaar.",
        ListColumnFileNameText: "Naam",
        ListColumnFileSizeText: "Grootte",
        //IGNORE
        ListColumnFileTypeText: "Type",
        ListColumnLastModifiedText: "Gewijzigd",
        //IGNORE
        ListKilobytesText: "KB",
        LoadingFilesText: "Bestanden oploaden...",
        //IGNORE
        MegabytesText: "megabytes",
        MenuAddAllToUploadListText: "Alles toevoegen aan verzend-lijst",
        MenuAddToUploadListText: "Voeg toe aan verzend-lijst",
        MenuArrangeByModifiedText: "Gewijzigd",
        MenuArrangeByNameText: "Naam",
        MenuArrangeByPathText: "Pad",
        MenuArrangeBySizeText: "Grootte",
        MenuArrangeByText: "Pictogrammen schikken op",
        //IGNORE
        MenuArrangeByTypeText: "Type",
        MenuArrangeByUnsortedText: "Niet gesorteerd",
        MenuDeselectAllText: "Alles deselecteren",
        //REVIEW
        MenuDetailsText: "Details",
        MenuIconsText: "Pictogrammen",
        MenuInvertSelectionText: "Selectie omkeren",
        MenuListText: "Lijst",
        MenuRefreshText: "Vernieuwen",
        MenuRemoveAllFromUploadListText: "Verwijder alles van verzend-lijst",
        MenuRemoveFromUploadListText: "Verwijder van verzend-lijst",
        MenuSelectAllText: "Selecter alles",
        MenuThumbnailsText: "Miniaturen",
        //IGNORE
        MessageBoxTitleText: "Image Uploader",
        MessageCannotConnectToInternetText: "De poging om met internet te verbinden is mislukt.",
        MessageCmykImagesAreNotAllowedText: "CMYK bestanden zijn niet toegestaan",
        MessageDimensionsAreTooLargeText: "De afbeelding [Name] kan niet geselecteerd worden. De afmetingen ([OriginalImageWidth]x[OriginalImageHeight]) zijn te groot. De afbeelding moet kleiner zijn dan [MaxImageWidth]x[MaxImageHeight].",
        MessageDimensionsAreTooSmallText: "De afbeelding [Name] kan niet geselecteerd worden. De afmetingen ([OriginalImageWidth]x[OriginalImageHeight]) zijn te klein. De afbeelding moet groter zijn dan  [MinImageWidth]x[MinImageHeight].",
        MessageFileSizeIsTooSmallText: "Het bestand [Name] kan niet geselecteerd worden. Bestandsgrootte is kleiner dan de limiet van ([Limit] KB).",
        MessageMaxFileCountExceededText: "Het bestand [Name] kan niet geselecteerd worden. Aantal bestanden is hoger dan de limiet van ([Limit] bestanden).",
        MessageMaxFileSizeExceededText: "Het bestand [Name] kan niet geselecteerd worden. Bestandsgrootte is groter dan de limiet van ([Limit] KB).",
        MessageMaxTotalFileSizeExceededText: "Het bestand [Name] kan niet geselecteerd worden. Totale hoeveelheid data is hoger dan de limiet van ([Limit] KB).",
        MessageNoInternetSessionWasEstablishedText: "Er kon geen Internet sessie gemaakt worden.",
        MessageNoResponseFromServerText: "Geen antwoord van server.",
        MessageRetryOpenFolderText: "Laatst bezochte map is niet beschikbaar. Mogelijks is bevindt deze zich op een verwisselbare schijf. Voeg de schijf in en klik op Opnieuw of Annuleer om verder te gaan.",
        MessageServerNotFoundText: "De server of proxy [Name] werd niet gevonden.",
        MessageSwitchAnotherFolderWarningText: "Je staat op het punt te veranderen van map, waardoor de geselecteerde bestanden niet langer geselecteerd zullen zijn.\n\nOm verder te gaan en de selectie te verlizen, klik OK.\nOm de selectie te houden en in de huidige map te blijven, klik op Annuleer.",
        MessageUnexpectedErrorText: "Image Uploader heeft problemen. Contacteer de webmaster.",
        MessageUploadCancelledText: "Versturen is geannuleerd.",
        MessageUploadCompleteText: "Alles verstuurd.",
        MessageUploadFailedText: "Versturen is mislukt (de verbinding werd onderbroken).",
        MessageUserSpecifiedTimeoutHasExpiredText: "De door gebruiker ingegeven maximum tijd is verlopen.",
        MinutesText: "minuten",
        ProgressDialogCancelButtonText: "Annuleer",
        ProgressDialogCloseButtonText: "Sluit",
        ProgressDialogCloseWhenUploadCompletesText: "Sluit dit venster als het versturen klaar is",
        ProgressDialogEstimatedTimeText: "Resterende tijd: [Current] van [Total]",
        ProgressDialogPreparingDataText: "Gegevens voorbereiden...",
        ProgressDialogSentText: "Verstuurd: [Current] van [Total]",
        ProgressDialogTitleText: "Bestanden versturen",
        ProgressDialogWaitingForResponseFromServerText: "Wacht op antwoord van server...",
        ProgressDialogWaitingForRetryText: "Wacht op nieuwe poging...",
        RemoveIconTooltipText: "Verwijderen",
        RotateIconClockwiseTooltipText: "Rechtsom draaien",
        RotateIconCounterclockwiseTooltipText: "Linksom draaien",
        SecondsText: "seconden",
        //REVIEW
        UnixFileSystemRootText: "Filesystem",
        //REVIEW
        UnixHomeDirectoryText: "Home directory"
    }
}
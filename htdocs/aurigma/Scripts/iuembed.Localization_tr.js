﻿// Aurigma Image Uploader Dual 6.x - IUEmbed Script Library
// Turkish Localization
// Copyright(c) Aurigma Inc. 2002-2009
// Version 6.5.6.0

//--------------------------------------------------------------------------
//tr_resources class
//--------------------------------------------------------------------------

tr_resources = {
    addParams: IULocalization.addParams,

    Language: "Turkish",

    // In different browsers the installation instructions are slightly different. So we provide browser specific instructions in addition to common messages.
    InstallationProgress: {
        // Common message. Shows on the loading progress dialog and on the instructions dialog.
        commonHtml: "<p>Aurigma Image Uploader ActiveX control is necessary to upload your files quickly and easily. You will be able to select multiple images in user-friendly interface instead of clumsy input fields with <strong>Browse</strong> button.</p>",
        // Progress message. Shows while loading Image Uploader ActiveX control.
        progressHtml: "<p><img src=\"{0}\" /><br />Loading Aurigma Image Uploader ActiveX...</p>",
        // Progress message. Shows while loading Image Uploader Java applet.
        javaProgressHtml: "<p><img src=\"{0}\" /><br />Loading Aurigma Image Uploader Java Applet...</p>",

        // IE6 specific progress message. Shows while loading Image Uploader control.
        beforeIE6XPSP2ProgressHtml: "<p>To install Image Uploader, please wait until the control will be loaded and click the <strong>Yes</strong> button when you see the installation dialog.</p>",
        // IE6 specific installation instructions to install Image Uploader ActiveX control.
        beforeIE6XPSP2InstructionsHtml: "<p>To install Image Uploader, please reload the page and click the <strong>Yes</strong> button when you see the control installation dialog. If you don't see installation dialog, please check your security settings.</p>",

        // IE6 on Windows XP SP2 and later specific progress message. Shows while loading Image Uploader control.
        IE6XPSP2ProgressHtml: "<p>Please wait until the control will be loaded.</p>",
        // IE6 on Windows XP SP2 and later specific installation instructions to install Image Uploader ActiveX control.
        IE6XPSP2InstructionsHtml: "<p>To install Image Uploader, please click on the <strong>Information Bar</strong> and select <strong>Install ActiveX Control</strong> from the dropdown menu. After page reload click <strong>Install</strong> when you see the control installation dialog. If you don't see Information Bar, please try to reload the page and/or check your security settings.</p>",

        // IE7 specific progress message. Shows while loading Image Uploader control.
        IE7ProgressHtml: "<p>Please wait until the control will be loaded.</p>",
        // IE7 specific installation instructions to install Image Uploader ActiveX control.
        IE7InstructionsHtml: "<p>To install Image Uploader, please click on the <strong>Information Bar</strong> and select <strong>Install ActiveX Control</strong> or <strong>Run ActiveX Control</strong> from the dropdown menu.</p><p>Then either click <strong>Run</strong> or after the page reload click <strong>Install</strong> when you see the control installation dialog. If you don't see Information Bar, please try to reload the page and/or check your security settings.</p>",

        // IE8 specific progress message. Shows while loading Image Uploader control.
        IE8ProgressHtml: "<p>Please wait until the control will be loaded.</p>",
        // IE8 specific installation instructions to install Image Uploader ActiveX control
        IE8InstructionsHtml: "<p>To install Image Uploader, please click on the <strong>Information Bar</strong> and select <strong>Install This Add-on</strong> or <strong>Run Add-on</strong> from the dropdown menu.</p><p>Then either click <strong>Run</strong> or after the page reload click <strong>Install</strong> when you see the control installation dialog. If you don't see Information Bar, please try to reload the page and/or check your security settings.</p>",

        // Update instructions. Shows while loading newer version of Image Uploader ActiveX control then installed on client machine.
        updateInstructions: "You need to update Image Uploader ActiveX control. Click <strong>Install</strong> or <strong>Run</strong> button when you see the control installation dialog. If you don't see installation dialog, please try to reload the page.",

        // Common install java message
        commonInstallJavaHtml: "<p>You need to install Java for running Image Uploader.</p>",

        // IE6 specific Java installation instructions.
        beforeIE6XPSP2InstallJavaHtml: "<p>To install Java, please reload the page and click the <strong>Yes</strong> button when you see the control installation dialog. If you don't see installation dialog, please check your security settings.</p>",

        // IE6 on Windows XP SP2 and later specific Java installation instructions.
        IE6XPSP2InstallJavaHtml: "<p>To install Java, please click on the <strong>Information Bar</strong> and select <strong>Install ActiveX Control</strong> from the dropdown menu. After page reload click <strong>Install</strong> when you see the control installation dialog. If you don't see Information Bar, please try to reload the page and/or check your security settings.</p>",

        // IE7 specific Java installation instructions.
        IE7InstallJavaHtml: "<p>To install Java, please click on the <strong>Information Bar</strong> and select <strong>Install ActiveX Control</strong> or <strong>Run ActiveX Control</strong> from the dropdown menu.</p><p>Then either click <strong>Run</strong> or after the page reload click <strong>Install</strong> when you see the control installation dialog. If you don't see Information Bar, please try to reload the page and/or check your security settings.</p>",

        // IE8 specific Java installation instructions.
        IE8InstallJavaHtml: "<p>To install Java, please click on the <strong>Information Bar</strong> and select <strong>Install This Add-on</strong> or <strong>Run Add-on</strong> from the dropdown menu.</p><p>Then either click <strong>Run</strong> or after the page reload click <strong>Install</strong> when you see the control installation dialog. If you don't see Information Bar, please try to reload the page and/or check your security settings.</p>",

        // Mac specific Java installation instructions.
        MacInstallJavaHtml: "<p>Use the <a href=\"http://support.apple.com/kb/HT1338\">Software Update</a> feature (available on the Apple menu) to check that you have the most up-to-date version of Java for your Mac.</p>",

        // Java installation instructions for other browsers.
        miscBrowsersInstallJavaHtml: "<p>Please <a href=\"http://www.java.com/getjava\">download</a> and install Java.</p>"
    },

    ImageUploader: {
        AddFolderDialogButtonCancelText: "İptal",
        AddFolderDialogButtonSkipAllText: "Tümünü Atla",
        AddFolderDialogButtonSkipText: "Atla",
        AddFolderDialogTitleText: "Klasör Ekleniyor...",
        AuthenticationRequestBasicText: "[Name] girişi gereklidir..",
        AuthenticationRequestButtonCancelText: "Iptal",
        AuthenticationRequestButtonOkText: "Tamam",
        //IGNORE
        AuthenticationRequestDomainText: "Domain:",
        //IGNORE
        AuthenticationRequestLoginText: "Login:",
        AuthenticationRequestNtlmText: "[Name] girişi gereklidir.",
        AuthenticationRequestPasswordText: "Parola:",
        ButtonAddAllToUploadListText: "Tümünü Ekle",
        ButtonAddFilesText: "Dosya Ekle...",
        ButtonAddFoldersText: "Klasör Ekle...",
        ButtonAddToUploadListText: "Ekle",
        ButtonAdvancedDetailsCancelText: "İptal",

        ButtonCheckAllText: "Tümünü Seç",
        ButtonDeleteFilesText: "", //"Dosyaları Sil"
        ButtonDeselectAllText: "Seçimi Kaldır",
        ButtonPasteText: "", //"Yapıştır"
        ButtonRemoveAllFromUploadListText: "Tümünü Sil",
        ButtonRemoveFromUploadListText: "Sil",
        ButtonSelectAllText: "Tümünü Seç",
        ButtonSendText: "Yükle",
        ButtonStopText: "",

        ButtonUncheckAllText: "Seçimi Kaldır",
        CmykImagesAreNotAllowedText: "Dosya CMYK",
        DescriptionEditorButtonCancelText: "İptal",
        DescriptionEditorButtonOkText: "Tamam",

        //To be supplied
        DeleteFilesDialogTitleText: "Confirm File Delete",
        //To be supplied
        DeleteSelectedFilesDialogMessageText: "Are you sure you want to permanently delete selected items?",
        //To be supplied
        DeleteUploadedFilesDialogMessageText: "Are you sure you want to permanently delete uploaded items?",
        DimensionsAreTooLargeText: "Resim Boyutu Çok Büyük.",
        DimensionsAreTooSmallText: "Resim Boyutu Çok Küçük",
        DropFilesHereText: "Dosyaları Buraya Sürükleyin",
        EditDescriptionText: "Açıklamayı Düzenle...",

        //To be supplied
        ErrorDeletingFilesDialogMessageText: "Could not delete [Name]",
        FileIsTooLargeText: "Dosya Çok Büyük",
        FileIsTooSmallText: "Dosya Çok Küçük",
        HoursText: "saat",
        IncludeSubfoldersText: "Alt klasörlerle birlikte",
        KilobytesText: "kilobayt",
        LargePreviewGeneratingPreviewText: "Önizleme oluşturuluyor...",
        LargePreviewIconTooltipText: "Önizleme",
        LargePreviewNoPreviewAvailableText: "Önizleme yok.",
        ListColumnFileNameText: "İsim",
        ListColumnFileSizeText: "Boyut",
        ListColumnFileTypeText: "Tür",
        ListColumnLastModifiedText: "Değiştirme",
        //IGNORE
        ListKilobytesText: "KB",
        LoadingFilesText: "Dosyalar Yukleniyor...",
        MegabytesText: "megabayt",
        MenuAddAllToUploadListText: "Tümünü Yükleme Listesine Ekle",
        MenuAddToUploadListText: "Yükleme Listesine Ekle",
        MenuArrangeByModifiedText: "Değiştirme",
        MenuArrangeByNameText: "İsim",
        MenuArrangeByPathText: "Yol",
        MenuArrangeBySizeText: "Boyut",
        MenuArrangeByText: "İkonları Düzenle",
        MenuArrangeByTypeText: "Tür",
        MenuArrangeByUnsortedText: "Sırasız",
        MenuDeselectAllText: "Seçimi KAldır",
        MenuDetailsText: "Detaylar",
        MenuIconsText: "İkonlar",
        MenuInvertSelectionText: "Seçimi ters çevir",
        MenuListText: "Liste",
        MenuRefreshText: "Yenile",
        MenuRemoveAllFromUploadListText: "Yükleme Listesini Temizle",
        MenuRemoveFromUploadListText: "Yükleme Listesinden Sil",
        MenuSelectAllText: "Tümünü Seç",
        MenuThumbnailsText: "Küçük Resimler",
        MessageBoxTitleText: "Resim Yükleme",
        MessageCannotConnectToInternetText: "İnternete Bağlantı Başarısız.",
        MessageCmykImagesAreNotAllowedText: "CMYK resimler kabul edilemez",
        MessageDimensionsAreTooLargeText: "Resim [Name] seçilemez. Resim boyutları ([OriginalImageWidth]x[OriginalImageHeight]) çok büyük. Resim boyutları [MaxImageWidth]x[MaxImageHeight] değerlerinden küçük olmalıdır..",
        MessageDimensionsAreTooSmallText: "Resim [Name] seçilemez. Resim boyutları ([OriginalImageWidth]x[OriginalImageHeight]) çok küçük. Resim boyutları [MinImageWidth]x[MinImageHeight] değerlerinden büyük olmalıdır.",
        MessageFileSizeIsTooSmallText: "Dosya [Name] seçilemez. ([Limit] KB)dan büyük olmalıdır.",
        MessageMaxFileCountExceededText: "Dosya [Name] seçilemez. Bir seferde en fazla ([Limit] adet) dosya seçebilirsiniz.",
        MessageMaxFileSizeExceededText: "Dosya [Name] seçilemez. En yüksek dosya boyutu ([Limit] KB).",
        MessageMaxTotalFileSizeExceededText: "Dosya [Name] seçilemez. Toplam yükleme boyutu ([Limit] KB).",
        MessageNoInternetSessionWasEstablishedText: "İnternet bağlantısı bulunamadı",
        MessageNoResponseFromServerText: "Sunucudan cevap yok.",
        MessageRetryOpenFolderText: "Son gezdiğiniz klasör bulunamadı. Büyük ihtimal klasör taşınabilir bir aygıtta. Aygıtı bağlayıp Tekrar Dene butonuna basınız veya devam etmek için İptal butonuna basınız.",
        MessageServerNotFoundText: "Sunucu veya Proxy [Name] bulunamadı.",
        MessageSwitchAnotherFolderWarningText: "Başka bir klasöre geçmek üzeresiniz. Bu klasörde seçtiğiniz dosyaların seçimi kalkacaktır.\n\nSeçimi kaldırmak ve devam etmek için Tamam butonuna basınız.\nSeçime devam etmek ve bu klasörde kalmak için İptal butonuna basınız.",
        MessageUnexpectedErrorText: "Resim Yükleyici bir sorunla karşılaştı. Lütfen bu sorunu yazılım geliştiricinize bildiriniz",
        MessageUploadCancelledText: "Yükleme İptal Edildi.",
        MessageUploadCompleteText: "Yükleme Tamamlandı.",
        MessageUploadFailedText: "Yükleme başarısız (bağlantı kesildi).",
        MessageUserSpecifiedTimeoutHasExpiredText: "Kullanıcı tanımlı zaman aşımı.",
        MinutesText: "dakika",
        ProgressDialogCancelButtonText: "İptal",
        ProgressDialogCloseButtonText: "Kapat",
        ProgressDialogCloseWhenUploadCompletesText: "Yükleme işlemi bitince bu pencereyi kapat",
        ProgressDialogEstimatedTimeText: "Tahmini Süre: [Current] / [Total]",
        ProgressDialogPreparingDataText: "Veri Hazırlanıyor...",
        ProgressDialogSentText: "Gönderilen: [Current] / [Total]",
        ProgressDialogTitleText: "Dosyalar Yükleniyor",
        ProgressDialogWaitingForResponseFromServerText: "Sunucudan cevap bekleniyor...",
        ProgressDialogWaitingForRetryText: "Tekrar deniyor...",
        RemoveIconTooltipText: "Sil",
        RotateIconClockwiseTooltipText: "Saat yönünde çevir",
        RotateIconCounterclockwiseTooltipText: "Saat yönünün tersine çevir",
        SecondsText: "saniye",
        UnixFileSystemRootText: "Dosya Sistemi",
        UnixHomeDirectoryText: "Ana Klasör"
    }
}
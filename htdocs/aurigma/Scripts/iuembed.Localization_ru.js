﻿// Aurigma Image Uploader Dual 6.x - IUEmbed Script Library
// Russian Localization
// Copyright(c) Aurigma Inc. 2002-2007
// Version 6.5.6.0

//--------------------------------------------------------------------------
//ru_resources class
//--------------------------------------------------------------------------

ru_resources = {
    addParams: IULocalization.addParams,

    Language: "Russian",

    // In different browsers the installation instructions are slightly different. So we provide browser specific instructions in addition to common messages.
    InstallationProgress: {
        // Common message. Shows on the loading progress dialog and on the instructions dialog.
        commonHtml: "<p>Aurigma Image Uploader - управляющий элемент ActiveX, позволяющий легко и быстро загружать файлы. Вы сможете выбирать изображения для загрузки с помощью понятного и простого интерфейса вместо неудобных полей с кнопкой <strong>Обзор</strong>.</p>",
        // Progress message. Shows while loading Image Uploader ActiveX control.
        progressHtml: "<p><img src=\"{0}\" /><br />Загружается Aurigma Image Uploader ActiveX...</p>",
        // Progress message. Shows while loading Image Uploader Java applet.
        javaProgressHtml: "<p><img src=\"{0}\" /><br />Загружается Aurigma Image Uploader Java Applet...</p>",

        // IE6 specific progress message. Shows while loading Image Uploader control.
        beforeIE6XPSP2ProgressHtml: "<p>Чтобы установить Image Uploader, щелкните по информационной строке вверху (Information Bar). После перезагрузки страницы нажмите <strong>Да</strong> в диалоге установки элемента управления.</p>",
        // IE6 specific installation instructions to install Image Uploader ActiveX control.
        beforeIE6XPSP2InstructionsHtml: "<p>Чтобы установить Image Uploader, обновите страницу и нажмите кнопку \"Да\" в диалоге установки элемента управления. Если диалоговое окно не появилось проверьте настройки безопасности.</p>",

        // IE6 on Windows XP SP2 and later specific progress message. Shows while loading Image Uploader control.
        IE6XPSP2ProgressHtml: "<p>Пожалуйста, дождитесь загрузки контрола.</p>",
        // IE6 on Windows XP SP2 and later specific installation instructions to install Image Uploader ActiveX control.
        IE6XPSP2InstructionsHtml: "<p>Чтобы установить Image Uploader, щелкните по информационной строке вверху (Information Bar) и выберете пункт <strong>Install ActiveX Control</strong> из выпадающего меню. После перезагрузки страницы нажмите <strong>Install</strong> в диалоге установки элемента управления.</p>",

        // IE7 specific progress message. Shows while loading Image Uploader control.
        IE7ProgressHtml: "<p>Пожалуйста, дождитесь загрузки контрола.</p>",
        // IE7 specific installation instructions to install Image Uploader ActiveX control.
        IE7InstructionsHtml: "Чтобы установить Image Uploader, щелкните по информационной строке вверху (Information Bar) и выберете пункт <strong>Install ActiveX Control</strong> или <strong>Run ActiveX Control</strong> из выпадающего меню. Затем нажмите <strong>Run</strong> или после перезагрузки страницы нажмите <strong>Install</strong>.",

        // IE8 specific progress message. Shows while loading Image Uploader control.
        IE8ProgressHtml: "<p>Пожалуйста, дождитесь загрузки контрола.</p>",
        // IE8 specific installation instructions to install Image Uploader ActiveX control
        IE8InstructionsHtml: "<p>Чтобы установить Image Uploader, щелкните по информационной строке вверу (Information Bar) и выберите <strong>Install This Add-on</strong> или <strong>Run Add-on</strong> из выпадающего меню.</p><p>Затем нажмите <strong>Run</strong> или после перезагрузки страницы нажмите <strong>Install</strong>.</p>",

        // Update instructions. Shows while loading newer version of Image Uploader ActiveX control then installed on client machine.
        updateInstructions: "Необходимо обновить Image Uploader ActiveX компонент. Щелкните <strong>Install</strong> или <strong>Run</strong> в появивщемся диалоге установки элемента управления. Если диалоговое окно не появилось попробуйте обновить страницу.",

        // Common install java message
        commonInstallJavaHtml: "<p>Необходимо установить Java для запуска Image Uploader.</p>",

        // IE6 specific Java installation instructions.
        beforeIE6XPSP2InstallJavaHtml: "<p>Чтобы установить Java, обновите страницу и нажмите кнопку \"Да\" в диалоге установки элемента управления. Если диалоговое окно не появилось проверьте настройки безопасности.</p>",

        // IE6 on Windows XP SP2 and later specific Java installation instructions.
        IE6XPSP2InstallJavaHtml: "<p>Чтобы установить Java, щелкните по информационной строке вверху (Information Bar) и выберете пункт <strong>Install ActiveX Control</strong> из выпадающего меню. После перезагрузки страницы нажмите <strong>Install</strong> в диалоге установки элемента управления.</p>",

        // IE7 specific Java installation instructions.
        IE7InstallJavaHtml: "<p>Чтобы установить Java, щелкните по информационной строке вверу (Information Bar) и выберите <strong>Install ActiveX Control</strong> или <strong>Run ActiveX Control</strong> из выпадающего меню.</p><p>Затем нажмите <strong>Run</strong> или после перезагрузки страницы нажмите <strong>Install</strong>.</p>",

        // IE8 specific Java installation instructions.
        IE8InstallJavaHtml: "<p>Чтобы установить Java, щелкните по информационной строке вверу (Information Bar) и выберите <strong>Install This Add-on</strong> или <strong>Run Add-on</strong> из выпадающего меню.</p><p>Затем нажмите <strong>Run</strong> или после перезагрузки страницы нажмите <strong>Install</strong>.</p>",

        // Mac specific Java installation instructions.
        MacInstallJavaHtml: "<p>Используйте <a href=\"http://support.apple.com/kb/HT1338\">Software Update</a> (доступно из Apple меню), чтобы убедиться, что на вашем компьютере установлена последняя версия Java.</p>",

        // Java installation instructions for other browsers.
        miscBrowsersInstallJavaHtml: "<p>Пожалуйста, <a href=\"http://java.com/getjava\">загрузите</a> последнюю версию Java.</p>"
    },

    ImageUploader: {
        AddFolderDialogButtonCancelText: "Отмена",
        AddFolderDialogButtonSkipAllText: "Пропустить все",
        AddFolderDialogButtonSkipText: "Пропустить",
        AddFolderDialogTitleText: "Добавление папки...",
        AuthenticationRequestBasicText: "[Name] требует идентификации.",
        AuthenticationRequestButtonCancelText: "Отмена",
        AuthenticationRequestButtonOkText: "ОК",
        AuthenticationRequestDomainText: "Домен:",
        AuthenticationRequestLoginText: "Логин:",
        AuthenticationRequestNtlmText: "[Name] требует идентификации.",
        AuthenticationRequestPasswordText: "Пароль:",
        ButtonAddAllToUploadListText: "Добавить все",
        ButtonAddFilesText: "Добавить файлы...",
        ButtonAddFoldersText: "Добавить папки...",
        ButtonAddToUploadListText: "Добавить",
        ButtonAdvancedDetailsCancelText: "Отмена",

        ButtonCheckAllText: "Выделить все",
        ButtonDeleteFilesText: "", //"Удалить файлы"
        ButtonDeselectAllText: "Снять выделение",
        ButtonPasteText: "", //"Вставить"
        ButtonRemoveAllFromUploadListText: "Удалить все",
        ButtonRemoveFromUploadListText: "Удалить",
        ButtonSelectAllText: "Выделить все",
        ButtonSendText: "Отправить",
        ButtonStopText: "", //"Стоп"

        ButtonUncheckAllText: "Снять выделение",
        CmykImagesAreNotAllowedText: "CMYK-файл",
        DescriptionEditorButtonCancelText: "Отмена",
        DescriptionEditorButtonOkText: "ОК",

        //To be supplied
        DeleteFilesDialogTitleText: "Confirm File Delete",
        //To be supplied
        DeleteSelectedFilesDialogMessageText: "Are you sure you want to permanently delete selected items?",
        //To be supplied
        DeleteUploadedFilesDialogMessageText: "Are you sure you want to permanently delete uploaded items?",
        DimensionsAreTooLargeText: "Разрешение слишком велико",
        DimensionsAreTooSmallText: "Разрешение слишком мало",
        DropFilesHereText: "Перенесите файл(ы) сюда",
        EditDescriptionText: "Редактировать описание...",

        //To be supplied
        ErrorDeletingFilesDialogMessageText: "Could not delete [Name]",
        FileIsTooLargeText: "Файл слишком большой",
        FileIsTooSmallText: "Файл слишком мал",
        HoursText: "ч",
        IncludeSubfoldersText: "Включить все подпапки",
        KilobytesText: "КБ",
        LargePreviewGeneratingPreviewText: "Создание эскиза...",
        LargePreviewIconTooltipText: "Просмотр большого эскиза",
        LargePreviewNoPreviewAvailableText: "Эскиз недоступен.",
        ListColumnFileNameText: "Название",
        ListColumnFileSizeText: "Размер",
        ListColumnFileTypeText: "Тип",
        ListColumnLastModifiedText: "Изменен",
        ListKilobytesText: "КБ",
        LoadingFilesText: "Чтение файлов...",
        MegabytesText: "МБ",
        MenuAddAllToUploadListText: "Добавить все в список загрузки",
        MenuAddToUploadListText: "Добавить в список загрузки",
        MenuArrangeByModifiedText: "Изменен",
        MenuArrangeByNameText: "Имя",
        MenuArrangeByPathText: "Путь",
        MenuArrangeBySizeText: "Размер",
        MenuArrangeByText: "Упорядочить значки",
        MenuArrangeByTypeText: "Тип",
        MenuArrangeByUnsortedText: "Нет сортировки",
        MenuDeselectAllText: "Снять выделение",
        MenuDetailsText: "Таблица",
        MenuIconsText: "Значки",
        MenuInvertSelectionText: "Обратить выделение",
        MenuListText: "Список",
        MenuRefreshText: "Обновить",
        MenuRemoveAllFromUploadListText: "Удалить все из списка загрузки",
        MenuRemoveFromUploadListText: "Удалить из списка загрузки",
        MenuSelectAllText: "Выделить все",
        MenuThumbnailsText: "Эскизы",
        MessageBoxTitleText: "Загрузка изображений",
        MessageCannotConnectToInternetText: "Попытка соединиться с Интернетом не удалась.",
        MessageCmykImagesAreNotAllowedText: "CMYK-изображения не могут быть выбраны для загрузки.",
        MessageDimensionsAreTooLargeText: "Изображение [Name] не может быть выбрано для загрузки. Разрешение изображения ([OriginalImageWidth]x[OriginalImageHeight]) должно быть меньше [MaxImageWidth]x[MaxImageHeight] пикселей.",
        MessageDimensionsAreTooSmallText: "Изображение [Name] не может быть выбрано для загрузки. Разрешение изображения ([OriginalImageWidth]x[OriginalImageHeight]) должно быть больше [MinImageWidth]x[MinImageHeight] пикселей.",
        MessageFileSizeIsTooSmallText: "Файл [Name] не может быть выбран для загрузки. Размер файла меньше допустимого значения ([Limit] КБ).",
        MessageMaxFileCountExceededText: "Файл [Name] не может быть выбран для загрузки. Вы не можете выбрать больше чем [Limit] файлов.",
        MessageMaxFileSizeExceededText: "Файл [Name] не может быть выбран для загрузки. Размер файла первышает [Limit] КБ.",
        MessageMaxTotalFileSizeExceededText: "Файл [Name] не может быть выбран для загрузки. Общий размер файлов превышает [Limit] КБ.",
        MessageNoInternetSessionWasEstablishedText: "Не установлена Интернет-сессия.",
        MessageNoResponseFromServerText: "Нет ответа от сервера.",
        MessageRetryOpenFolderText: "Последняя посещенная папка недоступна. Возможно она размещена на съемном носителе. Вставьте носитель и нажмите кнопку Повторить. Чтобы продолжить действие, нажмите кнопку Отмена.",
        MessageServerNotFoundText: "Сервер или прокси-сервер [Name] не найден.",
        MessageSwitchAnotherFolderWarningText: "Вы собираетесь перейти в другую папку. Это приведет к снятию выделения выбранных файлов.\n\nЧтобы продолжить и отменить выделение, нажмите OK.\nЧтобы сохранить выделение и остаться в текущей папке, нажмите Отменить.",
        MessageUnexpectedErrorText: "Произошла ошибка во время загрузки. Если вы видите это сообщение, обратитесь к веб мастеру.",
        MessageUploadCancelledText: "Загрузка отменена.",
        MessageUploadCompleteText: "Загрузка завершена.",
        MessageUploadFailedText: "Загрузка не удалась (соединение было разорвано).",
        MessageUserSpecifiedTimeoutHasExpiredText: "Время ожидания, определённое пользователем, истекло.",
        MinutesText: "минут",
        ProgressDialogCancelButtonText: "Отмена",
        ProgressDialogCloseButtonText: "Закрыть",
        ProgressDialogCloseWhenUploadCompletesText: "Закрыть диалоговое окно после завершения загрузки.",
        ProgressDialogEstimatedTimeText: "Осталось времени: [Current] из [Total]",
        ProgressDialogPreparingDataText: "Подготовка данных...",
        ProgressDialogSentText: "Загружено: [Current] из [Total]",
        ProgressDialogTitleText: "Загрузка файлов",
        ProgressDialogWaitingForResponseFromServerText: "Ожидание ответа от сервера...",
        ProgressDialogWaitingForRetryText: "Ожидание повторной попытки...",
        RemoveIconTooltipText: "Удалить",
        RotateIconClockwiseTooltipText: "Повернуть по часовой стрелке",
        RotateIconCounterclockwiseTooltipText: "Повернуть против часовой стрелки",
        SecondsText: "секунд",
        UnixFileSystemRootText: "Файловая система",
        UnixHomeDirectoryText: "Домашний каталог"
    }
}
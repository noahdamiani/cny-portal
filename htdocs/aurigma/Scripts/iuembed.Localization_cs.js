﻿// Aurigma Image Uploader Dual 6.x - IUEmbed Script Library
// Copyright(c) Aurigma Inc. 2002-2009
// Version 6.5.6.0

//--------------------------------------------------------------------------
//cs_resources class
//--------------------------------------------------------------------------

cs_resources = {
    addParams: IULocalization.addParams,

    Language: "Czech",

    // In different browsers the installation instructions are slightly different. So we provide browser specific instructions in addition to common messages.
    InstallationProgress: {
        // Common message. Shows on the loading progress dialog and on the instructions dialog.
        commonHtml: "<p>Aurigma Image Uploader ActiveX control is necessary to upload your files quickly and easily. You will be able to select multiple images in user-friendly interface instead of clumsy input fields with <strong>Browse</strong> button.</p>",
        // Progress message. Shows while loading Image Uploader ActiveX control.
        progressHtml: "<p><img src=\"{0}\" /><br />Loading Aurigma Image Uploader ActiveX...</p>",
        // Progress message. Shows while loading Image Uploader Java applet.
        javaProgressHtml: "<p><img src=\"{0}\" /><br />Loading Aurigma Image Uploader Java Applet...</p>",

        // IE6 specific progress message. Shows while loading Image Uploader control.
        beforeIE6XPSP2ProgressHtml: "<p>To install Image Uploader, please wait until the control will be loaded and click the <strong>Yes</strong> button when you see the installation dialog.</p>",
        // IE6 specific installation instructions to install Image Uploader ActiveX control.
        beforeIE6XPSP2InstructionsHtml: "<p>To install Image Uploader, please reload the page and click the <strong>Yes</strong> button when you see the control installation dialog. If you don't see installation dialog, please check your security settings.</p>",

        // IE6 on Windows XP SP2 and later specific progress message. Shows while loading Image Uploader control.
        IE6XPSP2ProgressHtml: "<p>Please wait until the control will be loaded.</p>",
        // IE6 on Windows XP SP2 and later specific installation instructions to install Image Uploader ActiveX control.
        IE6XPSP2InstructionsHtml: "<p>To install Image Uploader, please click on the <strong>Information Bar</strong> and select <strong>Install ActiveX Control</strong> from the dropdown menu. After page reload click <strong>Install</strong> when you see the control installation dialog. If you don't see Information Bar, please try to reload the page and/or check your security settings.</p>",

        // IE7 specific progress message. Shows while loading Image Uploader control.
        IE7ProgressHtml: "<p>Please wait until the control will be loaded.</p>",
        // IE7 specific installation instructions to install Image Uploader ActiveX control.
        IE7InstructionsHtml: "<p>To install Image Uploader, please click on the <strong>Information Bar</strong> and select <strong>Install ActiveX Control</strong> or <strong>Run ActiveX Control</strong> from the dropdown menu.</p><p>Then either click <strong>Run</strong> or after the page reload click <strong>Install</strong> when you see the control installation dialog. If you don't see Information Bar, please try to reload the page and/or check your security settings.</p>",

        // IE8 specific progress message. Shows while loading Image Uploader control.
        IE8ProgressHtml: "<p>Please wait until the control will be loaded.</p>",
        // IE8 specific installation instructions to install Image Uploader ActiveX control
        IE8InstructionsHtml: "<p>To install Image Uploader, please click on the <strong>Information Bar</strong> and select <strong>Install This Add-on</strong> or <strong>Run Add-on</strong> from the dropdown menu.</p><p>Then either click <strong>Run</strong> or after the page reload click <strong>Install</strong> when you see the control installation dialog. If you don't see Information Bar, please try to reload the page and/or check your security settings.</p>",

        // Update instructions. Shows while loading newer version of Image Uploader ActiveX control then installed on client machine.
        updateInstructions: "You need to update Image Uploader ActiveX control. Click <strong>Install</strong> or <strong>Run</strong> button when you see the control installation dialog. If you don't see installation dialog, please try to reload the page.",

        // Common install java message
        commonInstallJavaHtml: "<p>You need to install Java for running Image Uploader.</p>",

        // IE6 specific Java installation instructions.
        beforeIE6XPSP2InstallJavaHtml: "<p>To install Java, please reload the page and click the <strong>Yes</strong> button when you see the control installation dialog. If you don't see installation dialog, please check your security settings.</p>",

        // IE6 on Windows XP SP2 and later specific Java installation instructions.
        IE6XPSP2InstallJavaHtml: "<p>To install Java, please click on the <strong>Information Bar</strong> and select <strong>Install ActiveX Control</strong> from the dropdown menu. After page reload click <strong>Install</strong> when you see the control installation dialog. If you don't see Information Bar, please try to reload the page and/or check your security settings.</p>",

        // IE7 specific Java installation instructions.
        IE7InstallJavaHtml: "<p>To install Java, please click on the <strong>Information Bar</strong> and select <strong>Install ActiveX Control</strong> or <strong>Run ActiveX Control</strong> from the dropdown menu.</p><p>Then either click <strong>Run</strong> or after the page reload click <strong>Install</strong> when you see the control installation dialog. If you don't see Information Bar, please try to reload the page and/or check your security settings.</p>",

        // IE8 specific Java installation instructions.
        IE8InstallJavaHtml: "<p>To install Java, please click on the <strong>Information Bar</strong> and select <strong>Install This Add-on</strong> or <strong>Run Add-on</strong> from the dropdown menu.</p><p>Then either click <strong>Run</strong> or after the page reload click <strong>Install</strong> when you see the control installation dialog. If you don't see Information Bar, please try to reload the page and/or check your security settings.</p>",

        // Mac specific Java installation instructions.
        MacInstallJavaHtml: "<p>Use the <a href=\"http://support.apple.com/kb/HT1338\">Software Update</a> feature (available on the Apple menu) to check that you have the most up-to-date version of Java for your Mac.</p>",

        // Java installation instructions for other browsers.
        miscBrowsersInstallJavaHtml: "<p>Please <a href=\"http://www.java.com/getjava\">download</a> and install Java.</p>"
    },

    ImageUploader: {
        AddFolderDialogButtonCancelText: "Zrušit",
        AddFolderDialogButtonSkipAllText: "Vynechat vše",
        AddFolderDialogButtonSkipText: "Vynechat",
        AddFolderDialogTitleText: "Přidat složku...",
        AuthenticationRequestBasicText: "[Name] vyžaduje ověření.",
        AuthenticationRequestButtonCancelText: "Zrušit",
        AuthenticationRequestButtonOkText: "Dobře",
        AuthenticationRequestDomainText: "Doména:",
        AuthenticationRequestLoginText: "Přihlásit se:",
        AuthenticationRequestNtlmText: "[Name] vyžaduje ověření.",
        AuthenticationRequestPasswordText: "Heslo:",
        ButtonAddAllToUploadListText: "Přidat vše",
        ButtonAddFilesText: "Přidat soubory...",
        ButtonAddFoldersText: "Přidat složky...",
        ButtonAddToUploadListText: "Přidat",
        ButtonAdvancedDetailsCancelText: "Zrušit",
        ButtonCheckAllText: "Označit vše",
        ButtonDeleteFilesText: "",
        ButtonDeselectAllText: "Zrušit všechno",
        ButtonPasteText: "",
        ButtonRemoveAllFromUploadListText: "Odstranit vše",
        ButtonRemoveFromUploadListText: "Odstranit",
        ButtonSelectAllText: "Označit vše",
        ButtonSendText: "Poslat",
        ButtonStopText: "",
        ButtonUncheckAllText: "Zrušit všechno",
        CmykImagesAreNotAllowedText: "Soubor je CMYK",
        DescriptionEditorButtonCancelText: "Zrušit",
        DescriptionEditorButtonOkText: "Dobře",
        DimensionsAreTooLargeText: "Obrázek je moc velký",
        DimensionsAreTooSmallText: "Obrázek je moc malý",
        DropFilesHereText: "Vložit soubory",
        EditDescriptionText: "Uprvit popis...",
        FileIsTooLargeText: "Soubor je moc velký",
        FileIsTooSmallText: "Soubor je moc malý",
        HoursText: "hodiny",
        IncludeSubfoldersText: "Včetně podsložek",
        KilobytesText: "kB",
        LargePreviewGeneratingPreviewText: "Vygenerovat náhled...",
        LargePreviewIconTooltipText: "Zobrazit mini obrázek",
        LargePreviewNoPreviewAvailableText: "Náhled neni dostupný.",
        ListColumnFileNameText: "Název",
        ListColumnFileSizeText: "Velikost",
        ListColumnFileTypeText: "Typ",
        ListColumnLastModifiedText: "Upraveno",
        ListKilobytesText: "KB",
        LoadingFilesText: "Nahrávaji se soubory...",
        MegabytesText: "megabyty",
        MenuAddAllToUploadListText: "Vložit vše",
        MenuAddToUploadListText: "Vložit",
        MenuArrangeByModifiedText: "Upraveno",
        MenuArrangeByNameText: "Název",
        MenuArrangeByPathText: "Cesta",
        MenuArrangeBySizeText: "Velikost",
        MenuArrangeByText: "Seřadit ikonky podle",
        MenuArrangeByTypeText: "Typ",
        MenuArrangeByUnsortedText: "Neseřazeno",
        MenuDeselectAllText: "Odznačit vše",
        MenuDetailsText: "Detaily",
        MenuIconsText: "Ikonky",
        MenuInvertSelectionText: "Invertovat výběr",
        MenuListText: "Seznam",
        MenuRefreshText: "Obnovit",
        MenuRemoveAllFromUploadListText: "Odebrat vše",
        MenuRemoveFromUploadListText: "Odebrat",
        MenuSelectAllText: "Označit vše",
        MenuThumbnailsText: "Mini obrázky",
        MessageBoxTitleText: "Image Uploader",
        MessageCannotConnectToInternetText: "Pokus o spojení s Internetem selhalo.",
        MessageCmykImagesAreNotAllowedText: "CMYK obrázky nejsou povoleny.",
        MessageDimensionsAreTooLargeText: "Obrázek [Name] nemůže být vybran. Tato velikost ([OriginalImageWidth]x[OriginalImageHeight]) je moc velká. Obrázek musi být menši než [MaxImageWidth]x[MaxImageHeight].",
        MessageDimensionsAreTooSmallText: "Obrázek [Name] nemůže být vybran. Tato velikost ([OriginalImageWidth]x[OriginalImageHeight]) je moc malá. Obrázek musi být větší než [MinImageWidth]x[MinImageHeight].",
        MessageFileSizeIsTooSmallText: "Soubor [Name] nemůže byt vybran. Tento soubor je menší než je povolený limit([Limit] KB).",
        MessageMaxFileCountExceededText: "Soubor [Name] nemůže byt vybran. Počet souboru překročil povolený limit ([Limit] files).",
        MessageMaxFileSizeExceededText: "Soubor [Name] nemůže byt vybran. Tento soubor překročil povolený limit ([Limit] KB).",
        MessageMaxTotalFileSizeExceededText: "Soubor [Name] nemůže byt vybran. Celková velikost nahrávaných dat překročila limit([Limit] KB).",
        MessageNoInternetSessionWasEstablishedText: "Nebylo nalezeno připojení k internetu",
        MessageNoResponseFromServerText: "Žádna odpověď ze strany serveru.",
        MessageRetryOpenFolderText: "Předchozí adresář není dostupný. Je možné že se nachází na nějakém vnějším médiu. Vložte prosím znovu médium a klikněte na tlačítko opakovat.",
        MessageServerNotFoundText: "Proxy server \"[Name]\" nebyl nalezen.",
        MessageSwitchAnotherFolderWarningText: "Přejděte prosím do jiného adresáře. Výběr souborů bude zapomenut.\n\nPro pokračování stiskněte OK.\nPokud chcete zachovat výběr a zůstat v tomto adresáři klikněte na Zrušit",
        MessageUnexpectedErrorText: "Image Uploader narazil na problem. Pokuď vidite tuto zprávu, kontaktujte prosim webového administrátora.",
        MessageUploadCancelledText: "Nahrávani bylo zrušeno.",
        MessageUploadCompleteText: "Nahrávani dokončeno.",
        MessageUploadFailedText: "Nahrávani selhalo (připojeni bylo přerušeno).",
        MessageUserSpecifiedTimeoutHasExpiredText: "Vypršel uživatelem specifikovaný časový limit.",
        MinutesText: "minuty",
        ProgressDialogCancelButtonText: "Zrušit",
        ProgressDialogCloseButtonText: "Zavřit",
        ProgressDialogCloseWhenUploadCompletesText: "Zavřit dialogové okno po dokončeni nahrávani",
        ProgressDialogEstimatedTimeText: "Předpokládaný čas: [Current] z [Total]",
        ProgressDialogPreparingDataText: "Připravit data...",
        ProgressDialogSentText: "Odeslano: [Current] z [Total]",
        ProgressDialogTitleText: "Nahrat soubory",
        ProgressDialogWaitingForResponseFromServerText: "Čeká se na odpověď ze serveru...",
        ProgressDialogWaitingForRetryText: "Čeká se na obnovu...",
        RemoveIconTooltipText: "Odstranit",
        RotateIconClockwiseTooltipText: "Otáčet ve směru hodinových ručiček",
        RotateIconCounterclockwiseTooltipText: "Otáčet proti směru hodinových ručiček",
        SecondsText: "sekundy",
        UnixFileSystemRootText: "System souboru",
        UnixHomeDirectoryText: "Domovský adresář"
    }
}
﻿// Aurigma Image Uploader 
// German Localization
// Copyright(c) Aurigma Inc. 2002-2007
// German Translation (c) Ryan Hood - Album.de
// Version 6.5.6.0

//--------------------------------------------------------------------------
//de_resources class
//--------------------------------------------------------------------------

de_resources = {
    addParams: IULocalization.addParams,

    Language: "German",

    // In different browsers the installation instructions are slightly different. So we provide browser specific instructions in addition to common messages.
    InstallationProgress: {
        // Common message. Shows on the loading progress dialog and on the instructions dialog.
        commonHtml: "<p>Aurigma Image Uploader ActiveX control is necessary to upload your files quickly and easily. You will be able to select multiple images in user-friendly interface instead of clumsy input fields with <strong>Browse</strong> button.</p>",
        // Progress message. Shows while loading Image Uploader ActiveX control.
        progressHtml: "<p><img src=\"{0}\" /><br />Loading Aurigma Image Uploader ActiveX...</p>",
        // Progress message. Shows while loading Image Uploader Java applet.
        javaProgressHtml: "<p><img src=\"{0}\" /><br />Loading Aurigma Image Uploader Java Applet...</p>",

        // IE6 specific progress message. Shows while loading Image Uploader control.
        beforeIE6XPSP2ProgressHtml: "<p>To install Image Uploader, please wait until the control will be loaded and click the <strong>Yes</strong> button when you see the installation dialog.</p>",
        // IE6 specific installation instructions to install Image Uploader ActiveX control.
        beforeIE6XPSP2InstructionsHtml: "<p>To install Image Uploader, please reload the page and click the <strong>Yes</strong> button when you see the control installation dialog. If you don't see installation dialog, please check your security settings.</p>",

        // IE6 on Windows XP SP2 and later specific progress message. Shows while loading Image Uploader control.
        IE6XPSP2ProgressHtml: "<p>Please wait until the control will be loaded.</p>",
        // IE6 on Windows XP SP2 and later specific installation instructions to install Image Uploader ActiveX control.
        IE6XPSP2InstructionsHtml: "<p>To install Image Uploader, please click on the <strong>Information Bar</strong> and select <strong>Install ActiveX Control</strong> from the dropdown menu. After page reload click <strong>Install</strong> when you see the control installation dialog. If you don't see Information Bar, please try to reload the page and/or check your security settings.</p>",

        // IE7 specific progress message. Shows while loading Image Uploader control.
        IE7ProgressHtml: "<p>Please wait until the control will be loaded.</p>",
        // IE7 specific installation instructions to install Image Uploader ActiveX control.
        IE7InstructionsHtml: "<p>To install Image Uploader, please click on the <strong>Information Bar</strong> and select <strong>Install ActiveX Control</strong> or <strong>Run ActiveX Control</strong> from the dropdown menu.</p><p>Then either click <strong>Run</strong> or after the page reload click <strong>Install</strong> when you see the control installation dialog. If you don't see Information Bar, please try to reload the page and/or check your security settings.</p>",

        // IE8 specific progress message. Shows while loading Image Uploader control.
        IE8ProgressHtml: "<p>Please wait until the control will be loaded.</p>",
        // IE8 specific installation instructions to install Image Uploader ActiveX control
        IE8InstructionsHtml: "<p>To install Image Uploader, please click on the <strong>Information Bar</strong> and select <strong>Install This Add-on</strong> or <strong>Run Add-on</strong> from the dropdown menu.</p><p>Then either click <strong>Run</strong> or after the page reload click <strong>Install</strong> when you see the control installation dialog. If you don't see Information Bar, please try to reload the page and/or check your security settings.</p>",

        // Update instructions. Shows while loading newer version of Image Uploader ActiveX control then installed on client machine.
        updateInstructions: "You need to update Image Uploader ActiveX control. Click <strong>Install</strong> or <strong>Run</strong> button when you see the control installation dialog. If you don't see installation dialog, please try to reload the page.",

        // Common install java message
        commonInstallJavaHtml: "<p>You need to install Java for running Image Uploader.</p>",

        // IE6 specific Java installation instructions.
        beforeIE6XPSP2InstallJavaHtml: "<p>To install Java, please reload the page and click the <strong>Yes</strong> button when you see the control installation dialog. If you don't see installation dialog, please check your security settings.</p>",

        // IE6 on Windows XP SP2 and later specific Java installation instructions.
        IE6XPSP2InstallJavaHtml: "<p>To install Java, please click on the <strong>Information Bar</strong> and select <strong>Install ActiveX Control</strong> from the dropdown menu. After page reload click <strong>Install</strong> when you see the control installation dialog. If you don't see Information Bar, please try to reload the page and/or check your security settings.</p>",

        // IE7 specific Java installation instructions.
        IE7InstallJavaHtml: "<p>To install Java, please click on the <strong>Information Bar</strong> and select <strong>Install ActiveX Control</strong> or <strong>Run ActiveX Control</strong> from the dropdown menu.</p><p>Then either click <strong>Run</strong> or after the page reload click <strong>Install</strong> when you see the control installation dialog. If you don't see Information Bar, please try to reload the page and/or check your security settings.</p>",

        // IE8 specific Java installation instructions.
        IE8InstallJavaHtml: "<p>To install Java, please click on the <strong>Information Bar</strong> and select <strong>Install This Add-on</strong> or <strong>Run Add-on</strong> from the dropdown menu.</p><p>Then either click <strong>Run</strong> or after the page reload click <strong>Install</strong> when you see the control installation dialog. If you don't see Information Bar, please try to reload the page and/or check your security settings.</p>",

        // Mac specific Java installation instructions.
        MacInstallJavaHtml: "<p>Use the <a href=\"http://support.apple.com/kb/HT1338\">Software Update</a> feature (available on the Apple menu) to check that you have the most up-to-date version of Java for your Mac.</p>",

        // Java installation instructions for other browsers.
        miscBrowsersInstallJavaHtml: "<p>Please <a href=\"http://www.java.com/getjava\">download</a> and install Java.</p>"
    },

    ImageUploader: {
        AddFolderDialogButtonCancelText: "Abbrechen",
        AddFolderDialogButtonSkipAllText: "Alle überspringen",
        AddFolderDialogButtonSkipText: "Überspringen",
        AddFolderDialogTitleText: "Füge Ordner hinzu...",
        AuthenticationRequestBasicText: "[Name] benötigt Authentifizierung.",
        AuthenticationRequestButtonCancelText: "Abbrechen",
        //IGNORE
        AuthenticationRequestButtonOkText: "OK",
        //IGNORE
        AuthenticationRequestDomainText: "Domain:",
        //IGNORE
        AuthenticationRequestLoginText: "Login:",
        AuthenticationRequestNtlmText: "[Name] benötigt Authentifizierung.",
        AuthenticationRequestPasswordText: "Passwort:",
        ButtonAddAllToUploadListText: "Alle hinzufügen",
        ButtonAddFilesText: "Dateien hinzufügen...",
        ButtonAddFoldersText: "Verzeichnisse hinzufügen...",
        ButtonAddToUploadListText: "Hinzufügen",
        ButtonAdvancedDetailsCancelText: "Abbrechen",

        ButtonCheckAllText: "Alle auswählen",
        ButtonDeleteFilesText: "", //"Dateien löschen"
        ButtonDeselectAllText: "Markierung aufheben",
        ButtonPasteText: "", //"Einfügen"
        ButtonRemoveAllFromUploadListText: "Alle entfernen",
        ButtonRemoveFromUploadListText: "Entfernen",
        ButtonSelectAllText: "Alle auswählen",
        ButtonSendText: "Speichern",
        ButtonStopText: "", //"Anhalten"

        ButtonUncheckAllText: "Markierung aufheben",
        CmykImagesAreNotAllowedText: "Datei ist CMYK",
        DescriptionEditorButtonCancelText: "Abbrechen",
        //IGNORE
        DescriptionEditorButtonOkText: "OK",

        DeleteFilesDialogTitleText: "Löschen von Dateien bestätigen",
        DeleteSelectedFilesDialogMessageText: "Möchten Sie die ausgewählten Elemente wirklich löschen?",
        DeleteUploadedFilesDialogMessageText: "Möchten Sie die hochgeladenen Elemente wirklich löschen?",
        DimensionsAreTooLargeText: "Die Abmessungen sind zu groß",
        DimensionsAreTooSmallText: "Die Abmessungen sind zu klein",
        DropFilesHereText: "Bilder hier ablegen",
        EditDescriptionText: "Beschreibung ändern...",

        ErrorDeletingFilesDialogMessageText: "Löschen fehlgeschlagen: [Name]",
        FileIsTooLargeText: "Datei zu groß",
        FileIsTooSmallText: "Datei ist zu klein",
        HoursText: "Stunden",
        IncludeSubfoldersText: "Unterverzeichnisse einbezihen",
        KilobytesText: "Kilobytes",
        LargePreviewGeneratingPreviewText: "Vorschau wird erstellt...",
        LargePreviewIconTooltipText: "Vorschaubild",
        LargePreviewNoPreviewAvailableText: "Keine Vorschau vorhanden.",
        //IGNORE
        ListColumnFileNameText: "Name",
        ListColumnFileSizeText: "Größe",
        ListColumnFileTypeText: "Typ",
        ListColumnLastModifiedText: "Geändert am",
        //IGNORE
        ListKilobytesText: "KB",
        LoadingFilesText: "Lade Dateien...",
        MegabytesText: "Megabytes",
        MenuAddAllToUploadListText: "Alle zur Übertragungsliste hinzufügen",
        MenuAddToUploadListText: "Zur Übertragungsliste hinzufügen",
        MenuArrangeByModifiedText: "Geändert am",
        //IGNORE
        MenuArrangeByNameText: "Name",
        MenuArrangeByPathText: "Pfad",
        MenuArrangeBySizeText: "Größe",
        MenuArrangeByText: "Sortieren nach",
        MenuArrangeByTypeText: "Typ",
        MenuArrangeByUnsortedText: "Nicht sortieren",
        MenuDeselectAllText: "Markierung aufheben",
        //IGNORE
        MenuDetailsText: "Details",
        MenuIconsText: "Symbole",
        MenuInvertSelectionText: "Auswahl umkehren",
        MenuListText: "Liste",
        MenuRefreshText: "Aktualisieren",
        MenuRemoveAllFromUploadListText: "Alle aus Übertragungsliste löschen",
        MenuRemoveFromUploadListText: "Aus Übertragungsliste löschen",
        MenuSelectAllText: "Alle auswählen",
        MenuThumbnailsText: "Miniaturansicht",
        //IGNORE
        MessageBoxTitleText: "Image Uploader",
        MessageCannotConnectToInternetText: "Die Internetverbindung kann nicht hergestellt werden.",
        MessageCmykImagesAreNotAllowedText: "CMYK-Bilder sind nicht erlaubt",
        MessageDimensionsAreTooLargeText: "Das Bild [Name] kann nicht markiert werden. Die Pixelgröße dieses Bildes ([OriginalImageWidth]x[OriginalImageHeight]) ist zu Groß. Das Bild muss kleiner als [MaxImageWidth]x[MaxImageHeight] sein.",
        MessageDimensionsAreTooSmallText: "Das Bild [Name] kann nicht markiert werden. Die Pixelgröße dieses Bildes ([OriginalImageWidth]x[OriginalImageHeight]) ist zu klein. Das Bild muss größer als [MinImageWidth]x[MinImageHeight] sein.",
        MessageFileSizeIsTooSmallText: "Die Datei [Name] kann nicht selektiert werden. Die Dateigröße ist kleiner als das Limit ([Limit] kb).", //"Die maximale Dateianzahl wurde überschritten."
        MessageMaxFileCountExceededText: "Die Datei [Name] kann nicht selektiert werden. Die Dateigröße aller Dateien übersteigt das Limit ([Limit] files).", //"Die maximale Dateigröße wurde überschritten."
        MessageMaxFileSizeExceededText: "Die Datei [Name] kann nicht selektiert werden. Die Dateigrößer übersteigt das Limit ([Limit] kb).", //"Die maximale Gesamtdateigröße wurde überschritten."
        MessageMaxTotalFileSizeExceededText: "Die Datei [Name] kann nicht selektiert werden. Die Dateigröße aller Dateien übersteigt das Limit. ([Limit] kb).",
        MessageNoInternetSessionWasEstablishedText: "Es konnte keine Internetverbindung hergestellt werden.",
        MessageNoResponseFromServerText: "Es ist ein Fehler bei der Übertragung aufgetreten.",
        MessageRetryOpenFolderText: "Der zuletzt besuchte Ordner ist nicht verfügbar. Wahrscheinlich befindet er sich auf einem Wechseldatenträger (z.B. Speicherkarte der Digitalkamera). Bitte stecken Sie den Wechseldatenträger in Ihrem Computer und drücken Sie den \"Wiederholen\" Button oder klicken Sie \"Abbrechen\" um fortzufahren.",
        MessageServerNotFoundText: "Der Server [Name] wurde nicht gefunden.",
        MessageSwitchAnotherFolderWarningText: "Sie möchten auf einen anderen ordner Wechseln. Dies wird die Markierung aller bisher ausgewählten Dateien in diesem Ordner aufheben. \n\nUm fortzufahen und die Auswahl zu verlieren, klicken Sie bitte OK.\nUm die Auswahl beizubehalten und im aktuellen Ordner zu bleiben, klicken Sie bitte ABBRECHEN.",
        MessageUnexpectedErrorText: "Es ist ein Problem mit dem Image Uploader aufgetreten. Wenn Sie diese Nachricht sehen, kontaktieren Sie bitte den Webmaster.",
        MessageUploadCancelledText: "Die Übertragung wurde abgebrochen.",
        MessageUploadCompleteText: "Die Übertragung wurde abgeschlossen.",
        MessageUploadFailedText: "Die Übertragung schlug fehl.",
        MessageUserSpecifiedTimeoutHasExpiredText: "Die von dem Benutzer festgelegte Zeit wurde überschritten.",
        MinutesText: "Minuten",
        ProgressDialogCancelButtonText: "Abbrechen",
        ProgressDialogCloseButtonText: "Schließen",
        ProgressDialogCloseWhenUploadCompletesText: "Nach Übertragung schließen",
        ProgressDialogEstimatedTimeText: "Geschätzte Dauer: [Current] von [Total]",
        ProgressDialogPreparingDataText: "Vorbereitung der Daten",
        ProgressDialogSentText: "Bytes übertragen: [Current] von [Total]	",
        ProgressDialogTitleText: "Dateiübertragung",
        ProgressDialogWaitingForResponseFromServerText: "Warten auf Serverantwort",
        ProgressDialogWaitingForRetryText: "Warte auf Wiederholung...",
        RemoveIconTooltipText: "entfernen",
        RotateIconClockwiseTooltipText: "Im Uhrzeigersinn drehen",
        RotateIconCounterclockwiseTooltipText: "Gegen den Uhrzeigersinn drehen",
        SecondsText: "Sekunden",
        UnixFileSystemRootText: "Dateisystem",
        UnixHomeDirectoryText: "Persönliches Verzeichnis"
    }
}
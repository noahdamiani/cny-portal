﻿// Aurigma Image Uploader Dual 6.x - IUEmbed Script Library
// Japanese Localization
// Copyright(c) Aurigma Inc. 2002-2007
// Version 6.5.6.0

//--------------------------------------------------------------------------
//ja_resources class
//--------------------------------------------------------------------------

ja_resources = {
    addParams: IULocalization.addParams,

    Language: "Japanese",

    // In different browsers the installation instructions are slightly different. So we provide browser specific instructions in addition to common messages.
    InstallationProgress: {
        // Common message. Shows on the loading progress dialog and on the instructions dialog.
        commonHtml: "<p>Aurigma Image Uploader ActiveX control is necessary to upload your files quickly and easily. You will be able to select multiple images in user-friendly interface instead of clumsy input fields with <strong>Browse</strong> button.</p>",
        // Progress message. Shows while loading Image Uploader ActiveX control.
        progressHtml: "<p><img src=\"{0}\" /><br />Loading Aurigma Image Uploader ActiveX...</p>",
        // Progress message. Shows while loading Image Uploader Java applet.
        javaProgressHtml: "<p><img src=\"{0}\" /><br />Loading Aurigma Image Uploader Java Applet...</p>",

        // IE6 specific progress message. Shows while loading Image Uploader control.
        beforeIE6XPSP2ProgressHtml: "<p>To install Image Uploader, please wait until the control will be loaded and click the <strong>Yes</strong> button when you see the installation dialog.</p>",
        // IE6 specific installation instructions to install Image Uploader ActiveX control.
        beforeIE6XPSP2InstructionsHtml: "<p>To install Image Uploader, please reload the page and click the <strong>Yes</strong> button when you see the control installation dialog. If you don't see installation dialog, please check your security settings.</p>",

        // IE6 on Windows XP SP2 and later specific progress message. Shows while loading Image Uploader control.
        IE6XPSP2ProgressHtml: "<p>Please wait until the control will be loaded.</p>",
        // IE6 on Windows XP SP2 and later specific installation instructions to install Image Uploader ActiveX control.
        IE6XPSP2InstructionsHtml: "<p>To install Image Uploader, please click on the <strong>Information Bar</strong> and select <strong>Install ActiveX Control</strong> from the dropdown menu. After page reload click <strong>Install</strong> when you see the control installation dialog. If you don't see Information Bar, please try to reload the page and/or check your security settings.</p>",

        // IE7 specific progress message. Shows while loading Image Uploader control.
        IE7ProgressHtml: "<p>Please wait until the control will be loaded.</p>",
        // IE7 specific installation instructions to install Image Uploader ActiveX control.
        IE7InstructionsHtml: "<p>To install Image Uploader, please click on the <strong>Information Bar</strong> and select <strong>Install ActiveX Control</strong> or <strong>Run ActiveX Control</strong> from the dropdown menu.</p><p>Then either click <strong>Run</strong> or after the page reload click <strong>Install</strong> when you see the control installation dialog. If you don't see Information Bar, please try to reload the page and/or check your security settings.</p>",

        // IE8 specific progress message. Shows while loading Image Uploader control.
        IE8ProgressHtml: "<p>Please wait until the control will be loaded.</p>",
        // IE8 specific installation instructions to install Image Uploader ActiveX control
        IE8InstructionsHtml: "<p>To install Image Uploader, please click on the <strong>Information Bar</strong> and select <strong>Install This Add-on</strong> or <strong>Run Add-on</strong> from the dropdown menu.</p><p>Then either click <strong>Run</strong> or after the page reload click <strong>Install</strong> when you see the control installation dialog. If you don't see Information Bar, please try to reload the page and/or check your security settings.</p>",

        // Update instructions. Shows while loading newer version of Image Uploader ActiveX control then installed on client machine.
        updateInstructions: "You need to update Image Uploader ActiveX control. Click <strong>Install</strong> or <strong>Run</strong> button when you see the control installation dialog. If you don't see installation dialog, please try to reload the page.",

        // Common install java message
        commonInstallJavaHtml: "<p>You need to install Java for running Image Uploader.</p>",

        // IE6 specific Java installation instructions.
        beforeIE6XPSP2InstallJavaHtml: "<p>To install Java, please reload the page and click the <strong>Yes</strong> button when you see the control installation dialog. If you don't see installation dialog, please check your security settings.</p>",

        // IE6 on Windows XP SP2 and later specific Java installation instructions.
        IE6XPSP2InstallJavaHtml: "<p>To install Java, please click on the <strong>Information Bar</strong> and select <strong>Install ActiveX Control</strong> from the dropdown menu. After page reload click <strong>Install</strong> when you see the control installation dialog. If you don't see Information Bar, please try to reload the page and/or check your security settings.</p>",

        // IE7 specific Java installation instructions.
        IE7InstallJavaHtml: "<p>To install Java, please click on the <strong>Information Bar</strong> and select <strong>Install ActiveX Control</strong> or <strong>Run ActiveX Control</strong> from the dropdown menu.</p><p>Then either click <strong>Run</strong> or after the page reload click <strong>Install</strong> when you see the control installation dialog. If you don't see Information Bar, please try to reload the page and/or check your security settings.</p>",

        // IE8 specific Java installation instructions.
        IE8InstallJavaHtml: "<p>To install Java, please click on the <strong>Information Bar</strong> and select <strong>Install This Add-on</strong> or <strong>Run Add-on</strong> from the dropdown menu.</p><p>Then either click <strong>Run</strong> or after the page reload click <strong>Install</strong> when you see the control installation dialog. If you don't see Information Bar, please try to reload the page and/or check your security settings.</p>",

        // Mac specific Java installation instructions.
        MacInstallJavaHtml: "<p>Use the <a href=\"http://support.apple.com/kb/HT1338\">Software Update</a> feature (available on the Apple menu) to check that you have the most up-to-date version of Java for your Mac.</p>",

        // Java installation instructions for other browsers.
        miscBrowsersInstallJavaHtml: "<p>Please <a href=\"http://www.java.com/getjava\">download</a> and install Java.</p>"
    },

    ImageUploader: {
        AddFolderDialogButtonCancelText: "キャンセル",
        AddFolderDialogButtonSkipAllText: "すべてスキップ",
        AddFolderDialogButtonSkipText: "スキップ",
        AddFolderDialogTitleText: "フォルダ追加中...",
        AuthenticationRequestBasicText: "[Name]は証明が必要です。",
        AuthenticationRequestButtonCancelText: "キャンセル",
        AuthenticationRequestButtonOkText: "OK",
        AuthenticationRequestDomainText: "ドメイン:",
        AuthenticationRequestLoginText: "ログイン:",
        AuthenticationRequestNtlmText: "[Name]は証明が必要です。",
        AuthenticationRequestPasswordText: "パスワード:",
        ButtonAddAllToUploadListText: "すべて追加",
        ButtonAddFilesText: "ファイル追加中...",
        ButtonAddFoldersText: "フォルダ追加中...",
        ButtonAddToUploadListText: "追加",
        ButtonAdvancedDetailsCancelText: "キャンセルl",
        ButtonCheckAllText: "すべて選択",
        ButtonDeleteFilesText: "", //"ファイル削除"
        ButtonDeselectAllText: "すべて非選択",
        ButtonPasteText: "", //"貼り付け"
        ButtonRemoveAllFromUploadListText: "すべて除去",
        ButtonRemoveFromUploadListText: "除去",
        ButtonSelectAllText: "すべて選択",
        ButtonSendText: "送信",
        ButtonStopText: "",
        ButtonUncheckAllText: "すべて非選択",
        CmykImagesAreNotAllowedText: "ファイルはCMYK",
        DescriptionEditorButtonCancelText: "キャンセル",
        DescriptionEditorButtonOkText: "OK",
        DimensionsAreTooLargeText: "画像が大きすぎます。",
        DimensionsAreTooSmallText: "画像が小さすぎます。",
        DropFilesHereText: "ここにファイルをドロップして下さい",
        EditDescriptionText: "説明の編集...",
        FileIsTooLargeText: "ファイルが大きすぎます",
        FileIsTooSmallText: "ファイルが小さすぎます",
        HoursText: "時間",
        IncludeSubfoldersText: "サブ・フォルダを含める",
        KilobytesText: "KB",
        LargePreviewGeneratingPreviewText: "プレビューを作成中...",
        LargePreviewIconTooltipText: "小画像をプレビュー",
        LargePreviewNoPreviewAvailableText: "プレビューはできません。",
        ListColumnFileNameText: "名前",
        ListColumnFileSizeText: "サイズ",
        ListColumnFileTypeText: "タイプ",
        ListColumnLastModifiedText: "変更済",
        ListKilobytesText: "KB",
        LoadingFilesText: "ファイルを読み込み中...",
        MegabytesText: "メガ・バイト",
        MenuAddAllToUploadListText: "すべてアップロード・リストに追加",
        MenuAddToUploadListText: "アップロード・リストに追加",
        MenuArrangeByModifiedText: "変更済",
        MenuArrangeByNameText: "名前",
        MenuArrangeByPathText: "パス",
        MenuArrangeBySizeText: "サイズ",
        MenuArrangeByText: "アイコン並べ替え",
        MenuArrangeByTypeText: "タイプ",
        MenuArrangeByUnsortedText: "順不同",
        MenuDeselectAllText: "すべて非選択",
        MenuDetailsText: "詳細",
        MenuIconsText: "アイコン",
        MenuInvertSelectionText: "選択を反転",
        MenuListText: "リスト",
        MenuRefreshText: "更新",
        MenuRemoveAllFromUploadListText: "すべてをアップロード・リストから除去",
        MenuRemoveFromUploadListText: "アップロード・リストから除去",
        MenuSelectAllText: "すべて選択",
        MenuThumbnailsText: "小画像",
        MessageBoxTitleText: "画像アップローダ",
        MessageCannotConnectToInternetText: "インターネットへの接続ができません。",
        MessageCmykImagesAreNotAllowedText: "CMYK画像は扱えません。",
        MessageDimensionsAreTooLargeText: "[Name]の画像は選択できません。この画像（[OriginalImageWidth]x[OriginalImageHeight]）は大きすぎます。画像は、[MaxImageWidth]x[MaxImageHeight]より小さくなくてはなりません。",
        MessageDimensionsAreTooSmallText: "[Name]の画像は選択できません。この画像（[OriginalImageWidth]x[OriginalImageHeight]）は小さすぎます。画像は、[MinImageWidth]x[MinImageHeight]より大きくなくてはなりません。",
        MessageFileSizeIsTooSmallText: "[Name]の画像は選択できません。このファイルのサイズは、下限 （[Limit] KB）より小さいです。",
        MessageMaxFileCountExceededText: "[Name]のファイルは選択できません。ファイル数が上限（[Limit]）を越えています。",
        MessageMaxFileSizeExceededText: "[Name]のファイルは選択できません。このファイルのサイズは上限（[Limit] KB）を越えています。",
        MessageMaxTotalFileSizeExceededText: "[Name]のファイルは選択できません。アップロードするデータ総量が上限（[Limit] KB）を越えています。",
        MessageNoInternetSessionWasEstablishedText: "インターネットのセッションはありませんでした。",
        MessageNoResponseFromServerText: "サーバからの応答なし。",
        MessageRetryOpenFolderText: "最後に訪問したフォルダを開けません。リムーバブル・メディアにある可能性があります。続けるには、メディアを入れてリトライ・ボタンを押すか、キャンセル・ボタンを押して下さい。",
        MessageServerNotFoundText: "[Name]のサーバあるいはプロキシが見つかりません。",
        MessageSwitchAnotherFolderWarningText: "別のフォルダに変わろうとしています。これは、選ばれているファイルの選択を破棄します。\n\n選択をなしにして進むにはOKを押して下さい。\n選択を保って現在のフォルダに留まるには、キャンセルを押して下さい。",
        MessageUnexpectedErrorText: "画像アップローダがある問題に遭遇しました。このメッセージを見たら、ウェブ管理者に連絡して下さい。",
        MessageUploadCancelledText: "アップロードがキャンセルされています。",
        MessageUploadCompleteText: "アップロード完了",
        MessageUploadFailedText: "アップロード失敗（接続が遮断されました）。",
        MessageUserSpecifiedTimeoutHasExpiredText: "ユーザ指定のタイムアウトが失効しています。",
        MinutesText: "分",
        ProgressDialogCancelButtonText: "キャンセル",
        ProgressDialogCloseButtonText: "閉じる",
        ProgressDialogCloseWhenUploadCompletesText: "アップロード完了後、このダイアログ・ボックスを閉じて下さい。",
        ProgressDialogEstimatedTimeText: "推定時間: [Current]（[Total]中）",
        ProgressDialogPreparingDataText: "データ準備中...",
        ProgressDialogSentText: "送信: [Current] （[Total]中）",
        ProgressDialogTitleText: "ファイル・アップロード",
        ProgressDialogWaitingForResponseFromServerText: "サーバの応答待ち...",
        ProgressDialogWaitingForRetryText: "リトライ待ち...",
        RemoveIconTooltipText: "除去",
        RotateIconClockwiseTooltipText: "時計回り回転",
        RotateIconCounterclockwiseTooltipText: "反時計回り回転",
        SecondsText: "秒",
        UnixFileSystemRootText: "ファイルシステム",
        UnixHomeDirectoryText: "ホーム・フォルダ"
    }
}


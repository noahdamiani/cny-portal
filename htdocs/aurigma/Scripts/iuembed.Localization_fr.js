﻿// Aurigma Image Uploader Dual 6.x - IUEmbed Script Library
// French Localization
// Copyright(c) Jean Numatec/EasyFoto.fr 2004-2007
// Version 6.5.6.0

//--------------------------------------------------------------------------
//fr_resources class
//--------------------------------------------------------------------------

fr_resources = {
    addParams: IULocalization.addParams,

    Language: "French",

    // In different browsers the installation instructions are slightly different. So we provide browser specific instructions in addition to common messages.
    InstallationProgress: {
        // Common message. Shows on the loading progress dialog and on the instructions dialog.
        commonHtml: "<p>Aurigma Image Uploader ActiveX control is necessary to upload your files quickly and easily. You will be able to select multiple images in user-friendly interface instead of clumsy input fields with <strong>Browse</strong> button.</p>",
        // Progress message. Shows while loading Image Uploader ActiveX control.
        progressHtml: "<p><img src=\"{0}\" /><br />Loading Aurigma Image Uploader ActiveX...</p>",
        // Progress message. Shows while loading Image Uploader Java applet.
        javaProgressHtml: "<p><img src=\"{0}\" /><br />Loading Aurigma Image Uploader Java Applet...</p>",

        // IE6 specific progress message. Shows while loading Image Uploader control.
        beforeIE6XPSP2ProgressHtml: "<p>To install Image Uploader, please wait until the control will be loaded and click the <strong>Yes</strong> button when you see the installation dialog.</p>",
        // IE6 specific installation instructions to install Image Uploader ActiveX control.
        beforeIE6XPSP2InstructionsHtml: "<p>To install Image Uploader, please reload the page and click the <strong>Yes</strong> button when you see the control installation dialog. If you don't see installation dialog, please check your security settings.</p>",

        // IE6 on Windows XP SP2 and later specific progress message. Shows while loading Image Uploader control.
        IE6XPSP2ProgressHtml: "<p>Please wait until the control will be loaded.</p>",
        // IE6 on Windows XP SP2 and later specific installation instructions to install Image Uploader ActiveX control.
        IE6XPSP2InstructionsHtml: "<p>To install Image Uploader, please click on the <strong>Information Bar</strong> and select <strong>Install ActiveX Control</strong> from the dropdown menu. After page reload click <strong>Install</strong> when you see the control installation dialog. If you don't see Information Bar, please try to reload the page and/or check your security settings.</p>",

        // IE7 specific progress message. Shows while loading Image Uploader control.
        IE7ProgressHtml: "<p>Please wait until the control will be loaded.</p>",
        // IE7 specific installation instructions to install Image Uploader ActiveX control.
        IE7InstructionsHtml: "<p>To install Image Uploader, please click on the <strong>Information Bar</strong> and select <strong>Install ActiveX Control</strong> or <strong>Run ActiveX Control</strong> from the dropdown menu.</p><p>Then either click <strong>Run</strong> or after the page reload click <strong>Install</strong> when you see the control installation dialog. If you don't see Information Bar, please try to reload the page and/or check your security settings.</p>",

        // IE8 specific progress message. Shows while loading Image Uploader control.
        IE8ProgressHtml: "<p>Please wait until the control will be loaded.</p>",
        // IE8 specific installation instructions to install Image Uploader ActiveX control
        IE8InstructionsHtml: "<p>To install Image Uploader, please click on the <strong>Information Bar</strong> and select <strong>Install This Add-on</strong> or <strong>Run Add-on</strong> from the dropdown menu.</p><p>Then either click <strong>Run</strong> or after the page reload click <strong>Install</strong> when you see the control installation dialog. If you don't see Information Bar, please try to reload the page and/or check your security settings.</p>",

        // Update instructions. Shows while loading newer version of Image Uploader ActiveX control then installed on client machine.
        updateInstructions: "You need to update Image Uploader ActiveX control. Click <strong>Install</strong> or <strong>Run</strong> button when you see the control installation dialog. If you don't see installation dialog, please try to reload the page.",

        // Common install java message
        commonInstallJavaHtml: "<p>You need to install Java for running Image Uploader.</p>",

        // IE6 specific Java installation instructions.
        beforeIE6XPSP2InstallJavaHtml: "<p>To install Java, please reload the page and click the <strong>Yes</strong> button when you see the control installation dialog. If you don't see installation dialog, please check your security settings.</p>",

        // IE6 on Windows XP SP2 and later specific Java installation instructions.
        IE6XPSP2InstallJavaHtml: "<p>To install Java, please click on the <strong>Information Bar</strong> and select <strong>Install ActiveX Control</strong> from the dropdown menu. After page reload click <strong>Install</strong> when you see the control installation dialog. If you don't see Information Bar, please try to reload the page and/or check your security settings.</p>",

        // IE7 specific Java installation instructions.
        IE7InstallJavaHtml: "<p>To install Java, please click on the <strong>Information Bar</strong> and select <strong>Install ActiveX Control</strong> or <strong>Run ActiveX Control</strong> from the dropdown menu.</p><p>Then either click <strong>Run</strong> or after the page reload click <strong>Install</strong> when you see the control installation dialog. If you don't see Information Bar, please try to reload the page and/or check your security settings.</p>",

        // IE8 specific Java installation instructions.
        IE8InstallJavaHtml: "<p>To install Java, please click on the <strong>Information Bar</strong> and select <strong>Install This Add-on</strong> or <strong>Run Add-on</strong> from the dropdown menu.</p><p>Then either click <strong>Run</strong> or after the page reload click <strong>Install</strong> when you see the control installation dialog. If you don't see Information Bar, please try to reload the page and/or check your security settings.</p>",

        // Mac specific Java installation instructions.
        MacInstallJavaHtml: "<p>Use the <a href=\"http://support.apple.com/kb/HT1338\">Software Update</a> feature (available on the Apple menu) to check that you have the most up-to-date version of Java for your Mac.</p>",

        // Java installation instructions for other browsers.
        miscBrowsersInstallJavaHtml: "<p>Please <a href=\"http://www.java.com/getjava\">download</a> and install Java.</p>"
    },

    ImageUploader: {
        AddFolderDialogButtonCancelText: "Annuler",
        //REVIEW
        AddFolderDialogButtonSkipAllText: "Skip All",
        //REVIEW
        AddFolderDialogButtonSkipText: "Skip",
        //REVIEW
        AddFolderDialogTitleText: "Adding folder...",
        //REVIEW
        AuthenticationRequestBasicText: "[Name] requires authentication.",
        AuthenticationRequestButtonCancelText: "Annuler",
        //IGNORE
        AuthenticationRequestButtonOkText: "OK",
        //REVIEW
        AuthenticationRequestDomainText: "Domain:",
        //REVIEW
        AuthenticationRequestLoginText: "Login:",
        //REVIEW
        AuthenticationRequestNtlmText: "[Name] requires authentication.",
        //REVIEW
        AuthenticationRequestPasswordText: "Password:",
        ButtonAddAllToUploadListText: "Tout ajouter",
        ButtonAddFilesText: "Ajouter des fichiers ...",
        ButtonAddFoldersText: "Ajouter des dossiers...",
        ButtonAddToUploadListText: "Ajouter",
        //REVIEW
        ButtonAdvancedDetailsCancelText: "Cancel",

        ButtonCheckAllText: "Sélectionner tout",
        ButtonDeleteFilesText: "", //"Supprimer"
        ButtonDeselectAllText: "Désélectionner tout",
        ButtonPasteText: "", //"Coller"
        ButtonRemoveAllFromUploadListText: "Tout soustraire",
        ButtonRemoveFromUploadListText: "Soustraire",
        ButtonSelectAllText: "Sélectionner tout",
        ButtonSendText: "Transférer",
        ButtonStopText: "", //"Stop"

        ButtonUncheckAllText: "Désélectionner tout",
        //REVIEW
        CmykImagesAreNotAllowedText: "File is CMYK",
        DescriptionEditorButtonCancelText: "Annuler",
        //IGNORE
        DescriptionEditorButtonOkText: "OK",

        //To be supplied
        DeleteFilesDialogTitleText: "Confirm File Delete",
        //To be supplied
        DeleteSelectedFilesDialogMessageText: "Are you sure you want to permanently delete selected items?",
        //To be supplied
        DeleteUploadedFilesDialogMessageText: "Are you sure you want to permanently delete uploaded items?",
        DimensionsAreTooLargeText: "Les dimensions sont trop grandes !",
        DimensionsAreTooSmallText: "Les dimensions sont trop petites !",
        DropFilesHereText: "Ajouter des images ici",
        EditDescriptionText: "Modifier la description ...",

        //To be supplied
        ErrorDeletingFilesDialogMessageText: "Could not delete [Name]",
        FileIsTooLargeText: "Le fichier est trop gros",
        //REVIEW
        FileIsTooSmallText: "File is too small",
        HoursText: "Heures",
        IncludeSubfoldersText: "Inclure sous-dossiers",
        KilobytesText: "Ko",
        //IGNORE
        LargePreviewGeneratingPreviewText: "Generating preview...",
        //IGNORE
        LargePreviewIconTooltipText: "Preview Thumbnail",
        //IGNORE
        LargePreviewNoPreviewAvailableText: "No preview available.",
        ListColumnFileNameText: "Nom",
        ListColumnFileSizeText: "Taille",
        //IGNORE
        ListColumnFileTypeText: "Type",
        ListColumnLastModifiedText: "Date de modification",
        ListKilobytesText: "Ko",
        LoadingFilesText: "Chargement des photos en cours ... Patientez svp ...",
        MegabytesText: "Mo",
        MenuAddAllToUploadListText: "Ajouter tout à la liste de téléchargement",
        MenuAddToUploadListText: "Ajouter à la liste de téléchargement",
        //REVIEW
        MenuArrangeByModifiedText: "Modified",
        MenuArrangeByNameText: "Nom",
        //REVIEW
        MenuArrangeByPathText: "Path",
        //REVIEW
        MenuArrangeBySizeText: "Size",
        //REVIEW
        MenuArrangeByText: "Arrange Icons By",
        //REVIEW
        MenuArrangeByTypeText: "Type",
        //REVIEW
        MenuArrangeByUnsortedText: "Unsorted",
        MenuDeselectAllText: "Désélectionner tout",
        //REVIEW
        MenuDetailsText: "Details",
        MenuIconsText: "Icônes",
        MenuInvertSelectionText: "Inverser la selection",
        MenuListText: "Liste",
        MenuRefreshText: "Actualiser",
        MenuRemoveAllFromUploadListText: "Retirer tout de la liste de téléchargement",
        MenuRemoveFromUploadListText: "Retirer de la liste de téléchargement",
        MenuSelectAllText: "Sélectionner tout",
        MenuThumbnailsText: "Images miniatures",
        //IGNORE
        MessageBoxTitleText: "Image Uploader",
        MessageCannotConnectToInternetText: "Impossible de se connecté à Internet",
        //REVIEW
        MessageCmykImagesAreNotAllowedText: "CMYK images are not allowed",
        MessageDimensionsAreTooLargeText: "L'image [Name] ne peut être sélectionnée. Ca taille ([OriginalImageWidth]x[OriginalImageHeight]) est trop importante. L'image doit être plus petite que [MaxImageWidth]x[MaxImageHeight].",
        MessageDimensionsAreTooSmallText: "L'image [Name] ne peut être sélectionnée. Ca taille ([OriginalImageWidth]x[OriginalImageHeight]) est trop petite.  L'image doit être plus grande que [MinImageWidth]x[MinImageHeight].",
        MessageFileSizeIsTooSmallText: "The file [Name] cannot be selected. This file size is smaller than the limit ([Limit] kb).",
        //REVIEW
        MessageMaxFileCountExceededText: "The file [Name] cannot be selected. Amount of files exceeds the limit ([Limit] files).",
        MessageMaxFileSizeExceededText: "The file [Name] cannot be selected. This file size exceeds the limit ([Limit] kb).",
        MessageMaxTotalFileSizeExceededText: "The file [Name] cannot be selected. Total upload data size exceeds the limit ([Limit] kb).",
        MessageNoInternetSessionWasEstablishedText: "Erreur Session Internet - Contactez nous",
        //REVIEW
        MessageNoResponseFromServerText: "No response from server.",
        //REVIEW
        MessageRetryOpenFolderText: "Last visited folder is not available. It is possible it is located on a removable media. Insert the media and click Retry button or click Cancel button to continue.",
        MessageServerNotFoundText: "Erreur Serveur - Contactez nous",
        //REVIEW
        MessageSwitchAnotherFolderWarningText: "You are about to switch to another folder. This will discard selection from selected files.\n\nTo proceed and lose selection click OK.\nTo keep the selection and stay in the current folder, click Cancel.",
        //REVIEW
        MessageUnexpectedErrorText: "Image Uploader encountered some problem. If you see this message, contact web master.",
        MessageUploadCancelledText: "Téléchargement annulé",
        MessageUploadCompleteText: "Téléchargement terminé",
        MessageUploadFailedText: "Une erreur est survenue - Contactez nous",
        MessageUserSpecifiedTimeoutHasExpiredText: "L'arrêt personnalisé par l'utilisateur a expiré.",
        MinutesText: "Minutes",
        ProgressDialogCancelButtonText: "Annuler",
        ProgressDialogCloseButtonText: "Fermer",
        ProgressDialogCloseWhenUploadCompletesText: "Fermer la fenêtre une fois termine?",
        //REVIEW
        ProgressDialogEstimatedTimeText: "Estimated time: [Current] of [Total]", //"Temps restant estimé :"
        ProgressDialogPreparingDataText: "Préparation des données",
        ProgressDialogSentText: "Sent: [Current] of [Total]", //"[Current] envoyées sur [Total]"
        ProgressDialogTitleText: "Téléchargement en cours",
        ProgressDialogWaitingForResponseFromServerText: "En attente de la réponse du serveur",
        //REVIEW
        ProgressDialogWaitingForRetryText: "Waiting for retry...",
        //REVIEW
        RemoveIconTooltipText: "Remove",
        //REVIEW
        RotateIconClockwiseTooltipText: "Rotate Clockwise",
        //REVIEW
        RotateIconCounterclockwiseTooltipText: "Rotate Counterclockwise",
        SecondsText: "Secondes",
        UnixFileSystemRootText: "Racine système",
        UnixHomeDirectoryText: "Repertoire Local"
    }
}
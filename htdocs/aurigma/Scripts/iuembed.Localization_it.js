﻿// Aurigma Image Uploader Dual 6.x - IUEmbed Script Library
// Italian Localization
// Copyright(c) Aurigma Inc. 2002-2007
// Version 6.5.6.0

//--------------------------------------------------------------------------
//it_resources class
//--------------------------------------------------------------------------

it_resources = {
    addParams: IULocalization.addParams,

    Language: "Italian",

    // In different browsers the installation instructions are slightly different. So we provide browser specific instructions in addition to common messages.
    InstallationProgress: {
        // Common message. Shows on the loading progress dialog and on the instructions dialog.
        commonHtml: "<p>Aurigma Image Uploader ActiveX control is necessary to upload your files quickly and easily. You will be able to select multiple images in user-friendly interface instead of clumsy input fields with <strong>Browse</strong> button.</p>",
        // Progress message. Shows while loading Image Uploader ActiveX control.
        progressHtml: "<p><img src=\"{0}\" /><br />Loading Aurigma Image Uploader ActiveX...</p>",
        // Progress message. Shows while loading Image Uploader Java applet.
        javaProgressHtml: "<p><img src=\"{0}\" /><br />Loading Aurigma Image Uploader Java Applet...</p>",

        // IE6 specific progress message. Shows while loading Image Uploader control.
        beforeIE6XPSP2ProgressHtml: "<p>To install Image Uploader, please wait until the control will be loaded and click the <strong>Yes</strong> button when you see the installation dialog.</p>",
        // IE6 specific installation instructions to install Image Uploader ActiveX control.
        beforeIE6XPSP2InstructionsHtml: "<p>To install Image Uploader, please reload the page and click the <strong>Yes</strong> button when you see the control installation dialog. If you don't see installation dialog, please check your security settings.</p>",

        // IE6 on Windows XP SP2 and later specific progress message. Shows while loading Image Uploader control.
        IE6XPSP2ProgressHtml: "<p>Please wait until the control will be loaded.</p>",
        // IE6 on Windows XP SP2 and later specific installation instructions to install Image Uploader ActiveX control.
        IE6XPSP2InstructionsHtml: "<p>To install Image Uploader, please click on the <strong>Information Bar</strong> and select <strong>Install ActiveX Control</strong> from the dropdown menu. After page reload click <strong>Install</strong> when you see the control installation dialog. If you don't see Information Bar, please try to reload the page and/or check your security settings.</p>",

        // IE7 specific progress message. Shows while loading Image Uploader control.
        IE7ProgressHtml: "<p>Please wait until the control will be loaded.</p>",
        // IE7 specific installation instructions to install Image Uploader ActiveX control.
        IE7InstructionsHtml: "<p>To install Image Uploader, please click on the <strong>Information Bar</strong> and select <strong>Install ActiveX Control</strong> or <strong>Run ActiveX Control</strong> from the dropdown menu.</p><p>Then either click <strong>Run</strong> or after the page reload click <strong>Install</strong> when you see the control installation dialog. If you don't see Information Bar, please try to reload the page and/or check your security settings.</p>",

        // IE8 specific progress message. Shows while loading Image Uploader control.
        IE8ProgressHtml: "<p>Please wait until the control will be loaded.</p>",
        // IE8 specific installation instructions to install Image Uploader ActiveX control
        IE8InstructionsHtml: "<p>To install Image Uploader, please click on the <strong>Information Bar</strong> and select <strong>Install This Add-on</strong> or <strong>Run Add-on</strong> from the dropdown menu.</p><p>Then either click <strong>Run</strong> or after the page reload click <strong>Install</strong> when you see the control installation dialog. If you don't see Information Bar, please try to reload the page and/or check your security settings.</p>",

        // Update instructions. Shows while loading newer version of Image Uploader ActiveX control then installed on client machine.
        updateInstructions: "You need to update Image Uploader ActiveX control. Click <strong>Install</strong> or <strong>Run</strong> button when you see the control installation dialog. If you don't see installation dialog, please try to reload the page.",

        // Common install java message
        commonInstallJavaHtml: "<p>You need to install Java for running Image Uploader.</p>",

        // IE6 specific Java installation instructions.
        beforeIE6XPSP2InstallJavaHtml: "<p>To install Java, please reload the page and click the <strong>Yes</strong> button when you see the control installation dialog. If you don't see installation dialog, please check your security settings.</p>",

        // IE6 on Windows XP SP2 and later specific Java installation instructions.
        IE6XPSP2InstallJavaHtml: "<p>To install Java, please click on the <strong>Information Bar</strong> and select <strong>Install ActiveX Control</strong> from the dropdown menu. After page reload click <strong>Install</strong> when you see the control installation dialog. If you don't see Information Bar, please try to reload the page and/or check your security settings.</p>",

        // IE7 specific Java installation instructions.
        IE7InstallJavaHtml: "<p>To install Java, please click on the <strong>Information Bar</strong> and select <strong>Install ActiveX Control</strong> or <strong>Run ActiveX Control</strong> from the dropdown menu.</p><p>Then either click <strong>Run</strong> or after the page reload click <strong>Install</strong> when you see the control installation dialog. If you don't see Information Bar, please try to reload the page and/or check your security settings.</p>",

        // IE8 specific Java installation instructions.
        IE8InstallJavaHtml: "<p>To install Java, please click on the <strong>Information Bar</strong> and select <strong>Install This Add-on</strong> or <strong>Run Add-on</strong> from the dropdown menu.</p><p>Then either click <strong>Run</strong> or after the page reload click <strong>Install</strong> when you see the control installation dialog. If you don't see Information Bar, please try to reload the page and/or check your security settings.</p>",

        // Mac specific Java installation instructions.
        MacInstallJavaHtml: "<p>Use the <a href=\"http://support.apple.com/kb/HT1338\">Software Update</a> feature (available on the Apple menu) to check that you have the most up-to-date version of Java for your Mac.</p>",

        // Java installation instructions for other browsers.
        miscBrowsersInstallJavaHtml: "<p>Please <a href=\"http://www.java.com/getjava\">download</a> and install Java.</p>"
    },

    ImageUploader: {
        AddFolderDialogButtonCancelText: "Cancella",
        AddFolderDialogButtonSkipAllText: "Ignora tutto",
        AddFolderDialogButtonSkipText: "Ignora",
        AddFolderDialogTitleText: "Inserendo cartella..",
        AuthenticationRequestBasicText: "[Name] richiede autenticazione.",
        AuthenticationRequestButtonCancelText: "Cancella",
        //IGNORE
        AuthenticationRequestButtonOkText: "OK",
        AuthenticationRequestDomainText: "Dominio:",
        //IGNORE
        AuthenticationRequestLoginText: "Login:",
        AuthenticationRequestNtlmText: "[Name] richiede autenticazione.",
        //IGNORE
        AuthenticationRequestPasswordText: "Password:",
        ButtonAddAllToUploadListText: "Inserisci tutti",
        ButtonAddFilesText: "Inserisci Files...",
        ButtonAddFoldersText: "Inserisci Cartelle...",
        ButtonAddToUploadListText: "Inserisci",

        ButtonAdvancedDetailsCancelText: "Cancella",

        ButtonCheckAllText: "Seleziona tutti",
        ButtonDeleteFilesText: "", // "Elimina", //"Delete Files"
        ButtonDeselectAllText: "Elimina Selezioni",
        ButtonPasteText: "", //"Incolla",
        ButtonRemoveAllFromUploadListText: "Rimuovi tutti",
        ButtonRemoveFromUploadListText: "Rimuovi",
        ButtonSelectAllText: "Seleziona tutti",
        ButtonSendText: "Invia",

        ButtonStopText: "",

        ButtonUncheckAllText: "Elimina Selezioni",
        CmykImagesAreNotAllowedText: "Il File è CMYK",
        DescriptionEditorButtonCancelText: "Cancella",
        //IGNORE
        DescriptionEditorButtonOkText: "OK",

        //To be supplied
        DeleteFilesDialogTitleText: "Confirm File Delete",
        //To be supplied
        DeleteSelectedFilesDialogMessageText: "Are you sure you want to permanently delete selected items?",
        //To be supplied
        DeleteUploadedFilesDialogMessageText: "Are you sure you want to permanently delete uploaded items?",
        DimensionsAreTooLargeText: "Immagine troppo grande",
        DimensionsAreTooSmallText: "Immagine troppo piccola",
        DropFilesHereText: "Metti files qui",
        EditDescriptionText: "Modifica descrizione...",

        //To be supplied
        ErrorDeletingFilesDialogMessageText: "Could not delete [Name]",
        FileIsTooLargeText: "File troppo grande",
        FileIsTooSmallText: "File troppo piccolo",
        HoursText: "ore",
        IncludeSubfoldersText: "Includi sottocartella",
        //IGNORE
        KilobytesText: "kilobytes",
        LargePreviewGeneratingPreviewText: "Creazione anteprima...",
        LargePreviewIconTooltipText: "Anteprima",
        LargePreviewNoPreviewAvailableText: "Anteprima non disponibile.",
        ListColumnFileNameText: "Nome",
        ListColumnFileSizeText: "Dimensione",
        ListColumnFileTypeText: "Tipo",
        ListColumnLastModifiedText: "Modificato",
        //IGNORE
        ListKilobytesText: "KB",
        LoadingFilesText: "Caricando files...",
        //IGNORE
        MegabytesText: "megabytes",
        MenuAddAllToUploadListText: "Inseriscili tutti nella lista carica",
        MenuAddToUploadListText: "Inserisci nella lista carica",
        MenuArrangeByModifiedText: "Modificato",
        MenuArrangeByNameText: "Nome",
        MenuArrangeByPathText: "Percorso",
        MenuArrangeBySizeText: "Dimensione",
        MenuArrangeByText: "Ordina Icone per",
        MenuArrangeByTypeText: "Tipo",
        MenuArrangeByUnsortedText: "Non ordinate",
        MenuDeselectAllText: "Elimina Selezioni",
        MenuDetailsText: "Dettagli",
        MenuIconsText: "Icone",
        MenuInvertSelectionText: "Inverti Selezione",
        MenuListText: "Lista",
        MenuRefreshText: "Ripristina",
        MenuRemoveAllFromUploadListText: "Rimuovili tutti dalla lista carica",
        MenuRemoveFromUploadListText: "Rimuovi dalla lista carica",
        MenuSelectAllText: "Selezionali tutti",
        MenuThumbnailsText: "Diapositiva",
        MessageBoxTitleText: "Caricatore Immagini",
        MessageCannotConnectToInternetText: "Il tentativo di connettersi a internet non è riuscito.",
        MessageCmykImagesAreNotAllowedText: "Immagini CMYK non sono permesse",
        MessageDimensionsAreTooLargeText: "L\'immagine [Name] non può essere selezionata. Le dimensioni ([OriginalImageWidth]x[OriginalImageHeight]) sono troppo grandi. L\’immagine non dovrebbe essere più grande di  [MaxImageWidth]x[MaxImageHeight].",
        MessageDimensionsAreTooSmallText: "L\'immagine [Name] non può essere selezionata. Le dimensioni ([OriginalImageWidth]x[OriginalImageHeight]) sono troppo piccole. L\’immagine non dovrebbe essere più piccola di [MinImageWidth]x[MinImageHeight].",
        MessageFileSizeIsTooSmallText: "Il file [Name] non può essere selezionato. Le dimensioni del file sono più piccole del limite di ([Limit] kb).",
        MessageMaxFileCountExceededText: "Il file [Name] non può essere selezionato. Quantità di files eccedenti il limite ([Limit] files).",
        MessageMaxFileSizeExceededText: "Il file [Name] non può essere selezionato. Le dimensioni di questo file eccedono il limite di ([Limit] kb).",
        MessageMaxTotalFileSizeExceededText: "Il file [Name] non può essere selezionato. Il totale delle dimensioni dei dati da caricare eccede il limite di ([Limit] kb).",
        MessageNoInternetSessionWasEstablishedText: "La sessione internet non è stata stabilita.",
        MessageNoResponseFromServerText: "Non c\’è stata risposta dal server.",
        MessageRetryOpenFolderText: "L\’ultima cartella visitata non è disponibile. E’ possibile si trovi su un media rimuovibile. Inserisci il media e clicca sul tasto riprova o clicca sul tasto continua.",
        MessageServerNotFoundText: "Il server o proxy [Name] non è stato trovato.",
        MessageSwitchAnotherFolderWarningText: "Stai per trasferirti su un\'altra cartella. Questo rimuoverà le selezioni effettuate sui file. \n\n. Per procedere e perdere le selezioni, clicca OK.\nPer mantenere le selezioni e rimanere nella cartella attuale, clicca Cancella.",
        MessageUnexpectedErrorText: "Si sono verificati dei problemi con il caricamento delle immagini. Se vedi questo messaggio, contatta il web master.",
        MessageUploadCancelledText: "Caricamento annullato",
        MessageUploadCompleteText: "Caricamento completato",
        MessageUploadFailedText: "Il caricamento è fallito (si è interrotta la connessione).",
        MessageUserSpecifiedTimeoutHasExpiredText: "Il tempo prestabilito per l\’utente è scaduto.",
        MinutesText: "minuti",
        ProgressDialogCancelButtonText: "Cancella",
        ProgressDialogCloseButtonText: "Chiudi",
        ProgressDialogCloseWhenUploadCompletesText: "Chiudi questa finestra quando il caricamento è completato",
        ProgressDialogEstimatedTimeText: "Tempo stimato: [Current] su [Total]",
        ProgressDialogPreparingDataText: "Preparando i dati...",
        ProgressDialogSentText: "Inviati: [Current] su [Total]",
        ProgressDialogTitleText: "Files Caricati",
        ProgressDialogWaitingForResponseFromServerText: "In attesa di una risposta dal server...",
        ProgressDialogWaitingForRetryText: "In attesa per un altro tentativo...",
        RemoveIconTooltipText: "Rimuovi",
        RotateIconClockwiseTooltipText: "Ruota in senso orario",
        RotateIconCounterclockwiseTooltipText: "Ruota in senso antiorario",
        SecondsText: "secondi",
        UnixFileSystemRootText: "Sistema files",
        UnixHomeDirectoryText: "Cartella Home"
    }
}


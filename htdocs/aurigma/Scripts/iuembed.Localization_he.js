﻿// Aurigma Image Uploader Dual 6.x - IUEmbed Script Library
// Copyright(c) Aurigma Inc. 2002-2009
// Version 6.5.6.0

//--------------------------------------------------------------------------
//he_resources class
//--------------------------------------------------------------------------

he_resources = {
    addParams: IULocalization.addParams,

    //Language: "עברית",
    Language: "Hebrew",

    // In different browsers the installation instructions are slightly different. So we provide browser specific instructions in addition to common messages.
    InstallationProgress: {
        // Common message. Shows on the loading progress dialog and on the instructions dialog.
        commonHtml: "<p>Aurigma Image Uploader ActiveX control is necessary to upload your files quickly and easily. You will be able to select multiple images in user-friendly interface instead of clumsy input fields with <strong>Browse</strong> button.</p>",
        // Progress message. Shows while loading Image Uploader ActiveX control.
        progressHtml: "<p><img src=\"{0}\" /><br />Loading Aurigma Image Uploader ActiveX...</p>",
        // Progress message. Shows while loading Image Uploader Java applet.
        javaProgressHtml: "<p><img src=\"{0}\" /><br />Loading Aurigma Image Uploader Java Applet...</p>",

        // IE6 specific progress message. Shows while loading Image Uploader control.
        beforeIE6XPSP2ProgressHtml: "<p>To install Image Uploader, please wait until the control will be loaded and click the <strong>Yes</strong> button when you see the installation dialog.</p>",
        // IE6 specific installation instructions to install Image Uploader ActiveX control.
        beforeIE6XPSP2InstructionsHtml: "<p>To install Image Uploader, please reload the page and click the <strong>Yes</strong> button when you see the control installation dialog. If you don't see installation dialog, please check your security settings.</p>",

        // IE6 on Windows XP SP2 and later specific progress message. Shows while loading Image Uploader control.
        IE6XPSP2ProgressHtml: "<p>Please wait until the control will be loaded.</p>",
        // IE6 on Windows XP SP2 and later specific installation instructions to install Image Uploader ActiveX control.
        IE6XPSP2InstructionsHtml: "<p>To install Image Uploader, please click on the <strong>Information Bar</strong> and select <strong>Install ActiveX Control</strong> from the dropdown menu. After page reload click <strong>Install</strong> when you see the control installation dialog. If you don't see Information Bar, please try to reload the page and/or check your security settings.</p>",

        // IE7 specific progress message. Shows while loading Image Uploader control.
        IE7ProgressHtml: "<p>Please wait until the control will be loaded.</p>",
        // IE7 specific installation instructions to install Image Uploader ActiveX control.
        IE7InstructionsHtml: "<p>To install Image Uploader, please click on the <strong>Information Bar</strong> and select <strong>Install ActiveX Control</strong> or <strong>Run ActiveX Control</strong> from the dropdown menu.</p><p>Then either click <strong>Run</strong> or after the page reload click <strong>Install</strong> when you see the control installation dialog. If you don't see Information Bar, please try to reload the page and/or check your security settings.</p>",

        // IE8 specific progress message. Shows while loading Image Uploader control.
        IE8ProgressHtml: "<p>Please wait until the control will be loaded.</p>",
        // IE8 specific installation instructions to install Image Uploader ActiveX control
        IE8InstructionsHtml: "<p>To install Image Uploader, please click on the <strong>Information Bar</strong> and select <strong>Install This Add-on</strong> or <strong>Run Add-on</strong> from the dropdown menu.</p><p>Then either click <strong>Run</strong> or after the page reload click <strong>Install</strong> when you see the control installation dialog. If you don't see Information Bar, please try to reload the page and/or check your security settings.</p>",

        // Update instructions. Shows while loading newer version of Image Uploader ActiveX control then installed on client machine.
        updateInstructions: "You need to update Image Uploader ActiveX control. Click <strong>Install</strong> or <strong>Run</strong> button when you see the control installation dialog. If you don't see installation dialog, please try to reload the page.",

        // Common install java message
        commonInstallJavaHtml: "<p>You need to install Java for running Image Uploader.</p>",

        // IE6 specific Java installation instructions.
        beforeIE6XPSP2InstallJavaHtml: "<p>To install Java, please reload the page and click the <strong>Yes</strong> button when you see the control installation dialog. If you don't see installation dialog, please check your security settings.</p>",

        // IE6 on Windows XP SP2 and later specific Java installation instructions.
        IE6XPSP2InstallJavaHtml: "<p>To install Java, please click on the <strong>Information Bar</strong> and select <strong>Install ActiveX Control</strong> from the dropdown menu. After page reload click <strong>Install</strong> when you see the control installation dialog. If you don't see Information Bar, please try to reload the page and/or check your security settings.</p>",

        // IE7 specific Java installation instructions.
        IE7InstallJavaHtml: "<p>To install Java, please click on the <strong>Information Bar</strong> and select <strong>Install ActiveX Control</strong> or <strong>Run ActiveX Control</strong> from the dropdown menu.</p><p>Then either click <strong>Run</strong> or after the page reload click <strong>Install</strong> when you see the control installation dialog. If you don't see Information Bar, please try to reload the page and/or check your security settings.</p>",

        // IE8 specific Java installation instructions.
        IE8InstallJavaHtml: "<p>To install Java, please click on the <strong>Information Bar</strong> and select <strong>Install This Add-on</strong> or <strong>Run Add-on</strong> from the dropdown menu.</p><p>Then either click <strong>Run</strong> or after the page reload click <strong>Install</strong> when you see the control installation dialog. If you don't see Information Bar, please try to reload the page and/or check your security settings.</p>",

        // Mac specific Java installation instructions.
        MacInstallJavaHtml: "<p>Use the <a href=\"http://support.apple.com/kb/HT1338\">Software Update</a> feature (available on the Apple menu) to check that you have the most up-to-date version of Java for your Mac.</p>",

        // Java installation instructions for other browsers.
        miscBrowsersInstallJavaHtml: "<p>Please <a href=\"http://www.java.com/getjava\">download</a> and install Java.</p>"
    },

    ImageUploader: {
        AddFolderDialogButtonCancelText: "בטל",
        AddFolderDialogButtonSkipAllText: "דלג על הכל",
        AddFolderDialogButtonSkipText: "דלג",
        AddFolderDialogTitleText: "מוסיף תיקיה...",
        AuthenticationRequestBasicText: "[Name] דורש אישור.",
        AuthenticationRequestButtonCancelText: "בטל",
        AuthenticationRequestButtonOkText: "אשר",
        AuthenticationRequestDomainText: "מתחם:",
        AuthenticationRequestLoginText: "שם משתמש:",
        AuthenticationRequestNtlmText: "[Name] דורש אישור.",
        AuthenticationRequestPasswordText: "סיסמה:",
        ButtonAddAllToUploadListText: "הוסף הכל",
        ButtonAddFilesText: "הוסף קבצים...",
        ButtonAddFoldersText: "מוסיף תיקיות...",
        ButtonAddToUploadListText: "הוסף",

        ButtonAdvancedDetailsCancelText: "בטל",

        ButtonCheckAllText: "בדור הכל",

        ButtonDeleteFilesText: "", //"Delete Files"
        ButtonDeselectAllText: "הסר בחירה",
        ButtonPasteText: "", //"Paste"
        ButtonRemoveAllFromUploadListText: "הסר הכל",
        ButtonRemoveFromUploadListText: "הסר",
        ButtonSelectAllText: "בחר הכל",
        ButtonSendText: "העלה תמונות לאתר",

        ButtonStopText: "",

        ButtonUncheckAllText: "בטל סימון להכל",

        CmykImagesAreNotAllowedText: "קובץ CMYK",
        DescriptionEditorButtonCancelText: "בטל",
        DescriptionEditorButtonOkText: "אשר",

        DeleteFilesDialogTitleText: "אשר ובץ למחיקה",

        DeleteSelectedFilesDialogMessageText: "למחוק קבצים? (לא ניתן לשחזר קבצים אלה מאוחר יותר)",

        DeleteUploadedFilesDialogMessageText: "למחוק קבצים? (לא ניתן לשחזר קבצים אלה מאוחר יותר)",
        DimensionsAreTooLargeText: "הקובץ גדול מידי",
        DimensionsAreTooSmallText: "הקובץ קטן מידי",
        DropFilesHereText: "ניתן לגרור תמונות לחלון זה מתיקיה במחשב שלך או מהחלון למעלה",
        EditDescriptionText: "ערוך תיאור...",

        ErrorDeletingFilesDialogMessageText: "לא יכול למחוק את הקובץ [Name]",
        FileIsTooLargeText: "הקובץ גדול מידי",
        FileIsTooSmallText: "הקובץ קטן מידי",
        HoursText: "שעות",
        IncludeSubfoldersText: "כלול תיקיות משנה",
        KilobytesText: "קילובייט",
        LargePreviewGeneratingPreviewText: "מייצר תצוגה מקדימה...",
        LargePreviewIconTooltipText: "תצוגה מקדימה",
        LargePreviewNoPreviewAvailableText: "תצוגה מקדימה לא זמינה.",
        ListColumnFileNameText: "שם",
        ListColumnFileSizeText: "גודל",
        ListColumnFileTypeText: "סוג",
        ListColumnLastModifiedText: "השתנה",
        ListKilobytesText: "קילובייט",
        LoadingFilesText: "...טוען",
        MegabytesText: "מגהבייט",
        MenuAddAllToUploadListText: "הוסף הכל לרשימת ההעלאה",
        MenuAddToUploadListText: "הוסף לרשימת ההעלאה",
        MenuArrangeByModifiedText: "השתנה",
        MenuArrangeByNameText: "שם",
        MenuArrangeByPathText: "נתיב",
        MenuArrangeBySizeText: "גודל",
        MenuArrangeByText: "סדר סמלים לפי",
        MenuArrangeByTypeText: "סוג",
        MenuArrangeByUnsortedText: "לא ממויין",
        MenuDeselectAllText: "הסר סימון מהכל",
        MenuDetailsText: "פרטים",
        MenuIconsText: "סמלים",
        MenuInvertSelectionText: "הפוך בחירה",
        MenuListText: "רשימה",
        MenuRefreshText: "רענן",
        MenuRemoveAllFromUploadListText: "הסר הכל מרשימת ההעלאה",
        MenuRemoveFromUploadListText: "הסר מרשימת ההעלאה",
        MenuSelectAllText: "בחר הכל",
        MenuThumbnailsText: "תמונה ממוזערת",
        MessageBoxTitleText: "מעלה התמונות",
        MessageCannotConnectToInternetText: "ניסיון החיבור לאינטרנט נכשל.",
        MessageCmykImagesAreNotAllowedText: "לא ניתן להעלות תמונות CMYK",
        MessageDimensionsAreTooLargeText: "התמונה [Name] לא יכולה להבחר. מידות התמונה ([OriginalImageWidth]x[OriginalImageHeight]) גדולות מידי. התמונה צריכה להיות קטנה מ-[MaxImageWidth]x[MaxImageHeight].",
        MessageDimensionsAreTooSmallText: "התמונה [Name] לא יכולה להבחר. מידות התמונה ([OriginalImageWidth]x[OriginalImageHeight]) קטנות מידי. התמונה צריכה להיות גדולה מ-[MinImageWidth]x[MinImageHeight].",
        MessageFileSizeIsTooSmallText: "הקובץ [Name] לא יכול להבחר. גודל הקובץ קטן מההגבלה ([Limit] קילובייט).",
        MessageMaxFileCountExceededText: "הקובץ [Name] לא יכול להבחר. כמות הקבצים עברה את ההגבלה של ([Limit] הקבצים).",
        MessageMaxFileSizeExceededText: "הקובץ [Name] לא יכול להבחר. גודל הקובץ גדול מההגבלה של ([Limit] קילובייט).",
        MessageMaxTotalFileSizeExceededText: "הקובץ [Name] לא יכול להבחר. גודל סך כל הקבצים עבר את ההגבלה של ([Limit] קילובייט).",
        MessageNoInternetSessionWasEstablishedText: "לא בוסס קישור לאינטרנט.",
        MessageNoResponseFromServerText: "אין תגובה מן השרת.",
        MessageRetryOpenFolderText: "התיקיה האחרונה שביקרת בה לא זמינה.ייתכן כי היא ממוקמת במדיה ניידת. הכנס את המדיה ולחץ על נסה שנית או בטל להמשך.",
        MessageServerNotFoundText: "השרת או הפרוקסי [Name] לא נמצאו.",
        MessageSwitchAnotherFolderWarningText: "הנך עומד לעבור לתיקיה אחרת. פעולה זו תסיר את הסימון מהקבצים שבחרת.\n\nלהמשך ולTo proceed and lose selection click OK.\nTo keep the selection and stay in the current folder, click Cancel.",
        MessageUnexpectedErrorText: "מעלה התמונות נתקל בבעיה. צור קשר עם מנהל האינטרנט.",
        MessageUploadCancelledText: "העלאת הקבצים בוטלה.",
        MessageUploadCompleteText: "ההעלאה הסתיימה",
        MessageUploadFailedText: "העלאת הקבצים נכשלה (בעיה בחיבור).",
        MessageUserSpecifiedTimeoutHasExpiredText: "הזמן המוקצה על ידי המשתמש פג.",
        MinutesText: "דקות",
        ProgressDialogCancelButtonText: "בטל",
        ProgressDialogCloseButtonText: "סגור",
        ProgressDialogCloseWhenUploadCompletesText: "סגור חלון זה בעת הסיום",
        ProgressDialogEstimatedTimeText: "זמן לסיום: [Current] מתוך [Total]",
        ProgressDialogPreparingDataText: "אנא המתן...",
        ProgressDialogSentText: "הועבר: [Current] מתוך [Total]",
        ProgressDialogTitleText: "מעלה קבצים לאתר",
        ProgressDialogWaitingForResponseFromServerText: "מחכה לתגובה מן השרת...",
        ProgressDialogWaitingForRetryText: "מחכה לניסיון חוזר...",
        RemoveIconTooltipText: "הסר",
        RotateIconClockwiseTooltipText: "סובב ימינה",
        RotateIconCounterclockwiseTooltipText: "סובב שמאלה",
        SecondsText: "שניות",
        UnixFileSystemRootText: "מערכת הקבצים",
        UnixHomeDirectoryText: "תיקיה ראשית"
    }
}
$(document).ready(function() {
    $('.pop-modal').on('click', function(event) {
    	event.preventDefault();
    	event.stopPropagation();

    	var self = $('.modal-trigger'),
    		popUrl = $(this).data('pop-url'),
    		modalClass = $(this).data('pop-up'),
		    contentType = $(this).data('pop-type');

	    if (typeof(popUrl) == 'undefined') popUrl = $(this).attr('href');
	    if (typeof(contentType) == 'undefined') contentType = 'iframe';

    	self.addClass(modalClass);

    	$('.modal-trigger').bPopup({
    		content: contentType,
    		contentContainer: '.modal-container',
    		loadUrl: popUrl,
    		closeClass: 'close-modal',
    		onOpen: function() {
            	$('.close-modal').fadeIn();
            },
    		onClose: function() {
                self.removeClass(modalClass);
                $('.close-modal').hide();
            }
    	});

    });

	//make sure session still active or log user out
	setInterval(sessionStatus,60000); //1 minute

	$("#changeClientForm #client_id").bind("change",function(){
		$("#changeClientForm").submit();
	});
});

function sessionStatus() {
	$.getJSON("/session/status/", function(data){
		if (data=="0") window.location="/auth/logout";
	});
}

function closeBPopup() {
	$('.modal-trigger').bPopup().close()
}

function in_array (needle, haystack, argStrict) {
	var key = '',
		strict = !! argStrict;

	if (strict) {
		for (key in haystack) {
			if (haystack[key] === needle) {
				return true;
			}
		}
	} else {
		for (key in haystack) {
			if (haystack[key] == needle) {
				return true;
			}
		}
	}

	return false;
}
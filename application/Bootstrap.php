<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
	protected function _initAutoload()
	{
		$autoloader = new Zend_Application_Module_Autoloader(
			array(
				'namespace' => 'Cny_',
				'basePath'  => dirname(__FILE__),
			)
		);
		return $autoloader;
	}

	protected function _initFrontControllerPlugins()
	{
		$this->bootstrap( 'frontController' );
		$fc = $this->getResource( 'frontController' );
		$fc->registerPlugin( new Cny_Controller_Plugin_ModuleLayout() );
	}

	protected function _initViewHelpers()
	{
		$this->bootstrap('layout');
		$layout = $this->getResource('layout');
		$view = $layout->getView();
		$view->doctype('XHTML1_TRANSITIONAL');
		$view->headMeta()->appendHttpEquiv('Content-Type', 'text/html;charset=utf-8');
		$view->headMeta()->appendName("keywords", "");
		$view->headMeta()->appendName("description", "");
		$view->headTitle("Cyber-NY Portal");
		$view->headLink()->headLink(array('rel' => 'icon', 'href' => '/favicon.ico', 'type' => 'image/x-icon'), 'PREPEND');
		$view->headLink()->headLink(array('rel' => 'shortcut icon', 'href' => '/favicon.ico', 'type' => 'image/x-icon'), 'APPEND');
		$view->addHelperPath('Cny/View/Helper', 'Cny_View_Helper');
	}
}
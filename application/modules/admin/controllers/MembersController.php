<?php
class Admin_MembersController extends Zend_Controller_Action
{
	public $_db, $_status, $_user;

	public function indexAction()
	{
		Zend_Session :: namespaceUnset('members_search');

		$this->_redirect('/admin/members/list');
	}

	public function listAction()
	{
		$this->view->placeholder('section')->set("manager");

		$search = new Zend_Session_Namespace('members_search');
    	$mask = $relationship = "";

		$page = $this->getRequest()->getParam('page',1);
		$this->view->dir = $dir = $this->_getParam('dir','ASC');
		$this->view->sort = $sort = $this->_getParam('sort','partner1_last_name');

		if ($search->mask != '') $this->view->mask = $mask = $search->mask;
		if ($search->relationship != '') $this->view->relationship = $relationship = $search->relationship;
		if ($search->date_from != '') $this->view->date_from = $date_from = $search->date_from;
		if ($search->date_to != '') $this->view->date_to = $date_to = $search->date_to;

		if( $this->getRequest()->isPost() ){
			$this->view->mask = $mask = $this->_getParam('mask','');
			$this->view->relationship = $relationship = $this->_getParam('relationship','');
			$this->view->date_from = $date_from = $this->_getParam('date_from','');
			$this->view->date_to = $date_to = $this->_getParam('date_to','');

			if ($mask == '' && $relationship == '' && $date_from == '' && $date_to == '') {
				$search->setExpirationSeconds(1);
			}
		}

		$select = $this->_db->select();
		$select->from('member_profile', '*');
		$select ->order(array("$sort $dir"));

		if ($mask) {
			$search->mask = $mask;
			$select->where("login_username LIKE '%$mask%' OR partner1_first_name = '$mask' OR partner1_last_name LIKE '%$mask%' OR partner2_first_name = '$mask' OR partner2_last_name LIKE '%$mask%'");
		}else {
			$search->mask = '';
		}
		if ($relationship) {
			$search->relationship = $relationship;
			$select->where("partner_relationship = '$relationship'");
		}else {
			$search->relationship = '';
		}
		if ($date_from) {
			if (!$date_to) $date_to = $date_from;

			$search->date_from = $date_from;
			$search->date_to = $date_to;

			$select->where("created >= '".date("Y-m-d",strtotime($date_from))."' AND created <= '".date("Y-m-d",strtotime($date_to))."'");
		}else {
			$search->date_from = '';
			$search->date_to = '';
		}


		if($isCSVExport){
			$this->_helper->layout()->disableLayout();

			$stmt = $this->_db->query($select);
			$this->view->members = $stmt->fetchAll();

			$this->render('csvexport');
		}

		$paginator = Zend_Paginator::factory($select);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(25);
		$this->view->userList = $paginator;
	}

	public function viewAction()
	{
		$user_id = $this->getRequest()->getParam('user_id');
		if(!empty($user_id) && is_numeric($user_id)){
			$sql = 'SELECT * FROM member_profile WHERE id='.(int)$user_id.' LIMIT 1';
			$this->view->user = $this->_db->fetchRow($sql);
		}else{
			$this->_redirect('/admin/members/');
		}
	}

	public function exportAction()
	{
		$id = $this->_getParam('id',0);

		$this->_helper->layout()->disableLayout();

		$sql = "SELECT * FROM member_profile WHERE id = $id";
		$this->view->members = $this->_db->fetchAll($sql);

		$this->render('csvexport');
	}

	public function editAction()
	{
		$user_id = $this->getRequest()->getParam('user_id');
		if(!empty($user_id) && is_numeric($user_id)){
			$sql = 'SELECT * FROM member_profile WHERE id='.(int)$user_id.' LIMIT 1';
			$this->view->user = $this->_db->fetchRow($sql);
		}else{
			$this->_redirect('/admin/members/');
		}
	}

	public function saveAction()
	{
		$user = $this->getRequest()->getParam('data', $this->_user);
		$clean_user = $user;
		$user_id = $clean_user['id'];
		unset($clean_user['id']);
		unset($clean_user['clogin_password']);

		if (trim($clean_user['login_password']) == "" || $clean_user['login_password'] != $clean_user['clogin_password']) unset($clean_user['login_password']);

		$this->_db->update( 'member_profile', $clean_user, 'id='.$user_id );
		$this->_redirect('/admin/members/');
	}

	public function deleteAction()
	{
		$user_id = $this->getRequest()->getParam('user_id', 0);
		if(!empty($user_id) && is_numeric($user_id)){
			$sql = 'DELETE FROM member_profile WHERE id='.(int)$user_id.' LIMIT 1';
			$this->_db->query($sql);
			$this->_redirect('/admin/members/');
		}else{
			$this->_redirect('/admin/members/');
		}
	}

	public function init()
	{
		$this->view->placeholder('section')->set("detailview");

		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('admin'));
		if(!$auth->hasIdentity() || $auth->getIdentity()->role != "admin"){
			$auth->clearIdentity();
			$this->_redirect('/admin/auth');
		}else{
			$this->view->placeholder('logged_in')->set(true);
		}

		//$this->_db = Zend_Registry::get('db');
		$bootstrap = $this->getInvokeArg('bootstrap'); // gets the boostrapper
		$resource = $bootstrap->getPluginResource('multidb'); //multi db support
		$this->_db = $resource->getDefaultDb();

		$this->_relationships = array(
			'Dating' => 'Dating',
			'Engaged' => 'Engaged',
			'Living Together' => 'Living Together',
			'Married' => 'Married'
		);
		$this->view->relationships = $this->_relationships;

		$this->_statuses = array(
			'enabled' => 'Enabled',
			'disabled' => 'Disabled'
		);
		$this->view->statuses = $this->_statuses;
		$this->_user = array(
			'username' => '',
			'password' => '',
			'cuserpassword' => '',
			'firstname' => '',
			'lastname' => '',
			'email' => '',
			'status' => '',
			'role' => ''
		);
		$subSectionMenu = '<li><a href="/admin/members/">View Members</a></li><span>|</span>
							';
		$this->view->placeholder("subSectionMenu")->set($subSectionMenu);
	}
}
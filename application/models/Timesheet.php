<?php
class Cny_Model_Timesheet
{
	public function __construct()
	{
		//
	}

    public function clock($user_id)
    {
		$db = Zend_Db_Table::getDefaultAdapter();
		$sql = $db->quoteInto("SELECT id FROM timesheet WHERE user_id = ? AND clock_out IS NULL",$user_id);
		$id = $db->fetchOne($sql);

    	if( $id ){
			return '<a href="/schedule/timesheet-clock-out/id/'.$id.'"><img src="/images/clockout.gif" border=0 alt="Clock Out"></a>';
		}else {
			return '<a href="/schedule/timesheet-clock-in/"><img src="/images/clockin.gif" border=0 alt="Clock In"></a>';
		}

    }

	public function sec2hm ($sec, $padHours = false) {
		$hms = "";
		$hours = intval(intval($sec) / 3600);

		$hms .= ($padHours) ? str_pad($hours, 2, "0", STR_PAD_LEFT). 'h ' : $hours. 'h ';

		$minutes = intval(($sec / 60) % 60);

		$hms .= str_pad($minutes, 2, "0", STR_PAD_LEFT)."m";

		return $hms;
	}

	public function sec2dec ($sec) {
		$hms = "";
		$hours = intval(intval($sec) / 3600);
		$minutes = intval(($sec / 60) % 60);

		$dec = round($hours + ($minutes/60) ,2);

		return $dec;
	}

	public function weekToDates ($weekNumber, $year) {
		// Count from '0104' because January 4th is always in week 1
		//  (according to ISO 8601).
		$time = strtotime($year . '0104 +' . ($weekNumber - 1) . ' weeks');
		// Get the time of the first day of the week
		$mondayTime = strtotime('-' . (date('w', $time) - 1) . ' days', $time);

		return date("n/j/y",$mondayTime)." - ".date("n/j/y",strtotime('+5 days', $mondayTime));
	}

	public function datesInPayPeriod ($weekNumber, $year) {
		// Count from '0104' because January 4th is always in week 1
		//  (according to ISO 8601).
		$time = strtotime($year . '0104 +' . ($weekNumber - 1) . ' weeks');
		// Get the time of the first day of the week
		$mondayTime = strtotime('-' . (date('w', $time) - 1) . ' days', $time);

		$dayTimes = array ();
		for ($i = 0; $i < 5; ++$i) {
		  $dayTimes[] = date("Y-m-d",strtotime('+' . $i . ' days', $mondayTime));
		}

		return $dayTimes;
	}
}
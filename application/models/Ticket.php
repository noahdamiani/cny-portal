<?php
class Cny_Model_Ticket
{
    public function formatUrl($url)
    {
    	if (stripos($url,"http://") === false && stripos($url,"https://") === false && trim($url) != '') {
    		$url = "http://$url";
    	}
    	return $url;
    }

    public function statusInfo($status)
    {
    	switch ($status) {
			case "Ticket Opened":
				$info['image'] = "open";
				$info['class'] = "green";
				break;
			case "In Progress":
				$info['image'] = "in_progress";
				$info['class'] = "green";
				break;
			case "Attention Required":
				$info['image'] = "attention_required";
				$info['class'] = "red";
				break;
			case "Comments Submitted":
				$info['image'] = "feedback_submitted";
				$info['class'] = "green";
				break;
			case "Completed":
				$info['image'] = "completed";
				$info['class'] = "green";
				break;
			case "Approved & Closed":
				$info['image'] = "closed";
				$info['class'] = "green";
				break;
		}
		return $info;
    }

    public function priorityInfo($priority)
    {
		switch ($priority) {
			case "high":
				$info['image'] = "red";
				$info['class'] = "red";
				break;
			case "medium":
				$info['image'] = "yellow";
				$info['class'] = "yellow";
				break;
			case "low":
				$info['image']= "green";
				$info['class'] = "green";
				break;
		}
		return $info;
    }

	public function emailTicket($ticket_id)
    {
    	$db = Zend_Db_Table::getDefaultAdapter();
    	define( 'newline', "<br/>" );

    	$sql = "SELECT t.*, c.ClientName, CONCAT(u.FirstName, ' ', u.LastName) AS UserName FROM tickets AS t
    			LEFT JOIN Clients AS c ON t.ClientID = c.id
    			LEFT JOIN Users_BugTracker AS u ON t.created_user_id = u.ID WHERE t.id = $ticket_id";
    	$ticket = $db->fetchRow($sql);

		$mail_message  = date('m/d/Y H:i A').newline;
		$mail_message .= 'Client: '.$ticket['ClientName'].newline;
		$mail_message .= 'Ticket: '.$ticket_id.newline;
 		$mail_message .= 'Status: Ticket Opened'.newline;
 		$mail_message .= 'Sender: '.$ticket['UserName'].newline;
		$mail_message .= newline;
		$mail_message .= 'Hello, '.newline;
		$mail_message .= newline;
		$mail_message .= 'The following new ticket has been added: '.newline;
		$mail_message .= newline;
		$mail_message .= '------------------'.newline;
		$mail_message .= 'Ticket Number: '.$ticket_id.newline;
		$mail_message .= 'Ticket Name: '.$ticket['summary'].newline;
		$mail_message .= 'Ticket Description: '.nl2br($ticket['description']).newline;
		$mail_message .= '------------------'.newline;
		$mail_message .= newline;
		//$mail_message .= 'You may view this response and comment from: http://login.cyber-ny.com/'.newline;
		$mail_message .= newline;
		$mail_message .= 'Best Regards,'.newline;
		$mail_message .= 'Cyber-NY Solutions Manager'.newline;
		$mail_message .= 'portal.cyber-ny.com'.newline;
		$mail_message .= '212-475-2721'.newline;

		$sql = "SELECT u.Email, u.FirstName, u.LastName FROM Tickets_Notification_Emails AS t
				LEFT JOIN Users AS u ON t.userID = u.ID WHERE t.clientID = ".$ticket['clientID'];
		$emails = $db->fetchAssoc($sql);

		$mail = new Zend_Mail();
		$mail->setBodyHtml($mail_message);
		$mail->setFrom('do-not-reply@cyber-ny.com', $ticket['ClientName']);
		//$mail->setFrom('contact@cnydevzone3.com', $ticket['ClientName']);

		//if not production only email support@cyber-ny.com
		if(APPLICATION_ENV != 'production') {
			$emails = array(array("Email"=>"support@cyber-ny.com","FirstName"=>"cyber support","LastName"=>""));
		}
		foreach ($emails as $user) {
			$mail->addTo($user['Email'], $user['FirstName']." ".$user['LastName']);
		}
		$mail->setSubject("Cyber-NY:: ".$ticket['ClientName']." - New Ticket #".$ticket_id);

		if ($emails)
			$mail->send();

		return;
    }

    public function emailNote($note_id)
    {
    	$db = Zend_Db_Table::getDefaultAdapter();
    	$emails = array();
    	define( 'newline', "<br/>" );

    	$sql = "SELECT n.*, t.*, u.FirstName, u.LastName, u.Email, u.Roles, cyber.FirstName AS cyber, cyber.LastName AS cyber_last, cyber.Email AS cyber_email, c.ClientName
    			FROM ticket_notes AS n, Users_BugTracker AS u, tickets AS t
    			LEFT JOIN Users AS cyber ON cyber.ID = t.assigned_user_id LEFT JOIN Clients AS c ON t.clientID = c.id
    			WHERE n.created_user_id = u.ID AND n.ticket_id = t.id AND n.id = $note_id";
    	$note = $db->fetchRow($sql);

    	$mail = new Zend_Mail();

    	//send to staff
    	$sql = "SELECT u.Email, u.FirstName, u.LastName FROM Tickets_Notification_Emails AS t
			LEFT JOIN Users AS u ON t.userID = u.ID WHERE u.id <> '".$note['assigned_user_id']."' AND t.clientID = '".$note['clientID']."'";
		$emails = $db->fetchAssoc($sql);

		array_push($emails, array("Email"=>$note['cyber_email'], "FirstName"=>$note['cyber'], "LastName"=>$note['cyber_last']));

    	if ($note['Roles'] == "client") {
    		$UserName = $note['cyber'];
    		$from = $note['ClientName'];
    	}else {
    		$sql = "SELECT u.Email, u.FirstName, u.LastName FROM tickets AS t, Users_BugTracker AS u
					WHERE t.created_user_id = u.id AND t.id = '".$note['ticket_id']."'";
    		$client = $db->fetchRow($sql);

			array_push($emails,$client);

			$UserName = $client['FirstName'];
			$from = "Cyber-NY";
    	}

    	$mail_message  = date('m/d/Y H:i A').newline;
    	$mail_message .= 'Client: '.$note['ClientName'].newline;
		$mail_message .= 'Ticket: '.$note['summary'].newline;
 		$mail_message .= 'Status: '.$note['status'].newline;
 		$mail_message .= 'Sender: '.$note['FirstName'].' '.$note['LastName'].newline;
		$mail_message .= newline;
		$mail_message .= 'Hello, '.$UserName.newline;
		$mail_message .= newline;
		$mail_message .= 'The following response has been added to your ticket entry: '.newline;
		$mail_message .= newline;
		$mail_message .= '------------------'.newline;
		$mail_message .= nl2br($note['message']).newline;
		$mail_message .= '------------------'.newline;
		$mail_message .= newline;
		$mail_message .= 'You may view this response and comment from: http://portal.cyber-ny.com/tickets/view/clientID/'.$note['clientID'].'/id/'.$note['ticket_id'].newline;
		$mail_message .= newline;
		$mail_message .= 'Best Regards,'.newline;
		$mail_message .= 'Cyber-NY Solutions Manager'.newline;
		$mail_message .= 'portal.cyber-ny.com'.newline;
		$mail_message .= '212-475-2721'.newline;

		$mail->setBodyHtml($mail_message);
		$mail->setFrom('do-not-reply@cyber-ny.com', $from);
		//$mail->setFrom('contact@cnydevzone3.com', $from);

		//add attachment if one exists for this note
		$attachment_path = $_SERVER{'DOCUMENT_ROOT'}."/attachments/";
		if ($note['attachment'] && file_exists($attachment_path.$note['attachment'])) {
			$filename = str_replace($note_id."_","",$note['attachment']);

			$fileContents = file_get_contents($attachment_path.$note['attachment']);
			$file = $mail->createAttachment($fileContents);
			$file->filename = $filename;
		}

		//if not production only email support@cyber-ny.com
		if(APPLICATION_ENV != 'production') {
			$emails = array(array("Email"=>"support@cyber-ny.com","FirstName"=>"cyber support","LastName"=>""));
		}
		foreach ($emails as $user) {
			$mail->addTo($user['Email'], $user['FirstName']." ".$user['LastName']);
		}

		//add CC if any in list
		if ($note['cc_list']) {
			$cc_list = explode(",",$note['cc_list']);

			foreach ($cc_list as $cc) {
				$mail->addCc($cc);
			}
		}

		$mail->setSubject("Cyber-NY:: ".$note['ClientName']." - Response to Ticket #".$note['ticket_id']);
		$mail->send();
    }

    public function emailUpdate($note_id)
    {
    	$db = Zend_Db_Table::getDefaultAdapter();
    	define( 'newline', "<br/>" );

    	$sql = "SELECT n.*, t.*, u.FirstName, u.LastName, u.Email, c.ClientName
    			FROM ticket_notes AS n, Users_BugTracker AS u, tickets AS t	LEFT JOIN Clients AS c ON t.clientID = c.id
    			WHERE n.created_user_id = u.ID AND n.ticket_id = t.id AND n.id = $note_id";
    	$ticket = $db->fetchRow($sql);

		$mail_message  = date('m/d/Y H:i A').newline;
		$mail_message .= 'Client: '.$ticket['ClientName'].newline;
		$mail_message .= 'Ticket: '.$ticket['ticket_id'].newline;
 		$mail_message .= 'Status: '.$ticket['status'].newline;
 		$mail_message .= 'Sender: '.$ticket['FirstName'].' '.$ticket['LastName'].newline;
		$mail_message .= newline;
		$mail_message .= 'Hello, '.newline;
		$mail_message .= newline;
		$mail_message .= $ticket['FirstName'].' requested an update for the following ticket: '.newline;
		$mail_message .= newline;
		$mail_message .= '------------------'.newline;
		$mail_message .= 'Ticket Number: '.$ticket['ticket_id'].newline;
		$mail_message .= 'Ticket Name: '.$ticket['summary'].newline;
		$mail_message .= 'Ticket Description: '.$ticket['description'].newline;
		$mail_message .= '------------------'.newline;
		$mail_message .= newline;
		//$mail_message .= 'You may view this response and comment from: http://login.cyber-ny.com/'.newline;
		$mail_message .= newline;
		$mail_message .= 'Best Regards,'.newline;
		$mail_message .= 'Cyber-NY Solutions Manager'.newline;
		$mail_message .= 'portal.cyber-ny.com'.newline;
		$mail_message .= '212-475-2721'.newline;

		$sql = "SELECT DISTINCT u.Email, u.FirstName, u.LastName FROM Users AS u LEFT JOIN Tickets_Notification_Emails AS t ON t.userID = u.ID
				WHERE (t.clientID = {$ticket['clientID']} OR u.id = '{$ticket['assigned_user_id']}')";
		$emails = $db->fetchAssoc($sql);

		array_push($emails,$client);

		$mail = new Zend_Mail();
		$mail->setBodyHtml($mail_message);
		$mail->setFrom('do-not-reply@cyber-ny.com', $ticket['ClientName']);
		//$mail->setFrom('contact@cnydevzone3.com', $ticket['ClientName']);

		//if not production only email support@cyber-ny.com
		if(APPLICATION_ENV != 'production') {
			$emails = array(array("Email"=>"support@cyber-ny.com","FirstName"=>"cyber support","LastName"=>""));
		}
		foreach ($emails as $user) {
			$mail->addTo($user['Email'], $user['FirstName']." ".$user['LastName']);
		}
		$mail->setSubject("Cyber-NY:: ".$ticket['ClientName']." - Update Request for Ticket #".$ticket['id']);
		$mail->send();

		return;
    }

    public function emailRequestQuote($ticket_id)
    {
    	$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('portal'));
		if($auth->hasIdentity()){
			$person = $auth->getIdentity();
		}

    	$db = Zend_Db_Table::getDefaultAdapter();
    	define( 'newline', "<br/>" );

    	$sql = "SELECT t.*, c.ClientName
    			FROM WishTickets AS t LEFT JOIN Clients AS c ON t.clientID = c.id
    			WHERE t.id = $ticket_id";
    	$ticket = $db->fetchRow($sql);

		$mail_message  = date('m/d/Y H:i A').newline;
		$mail_message .= 'Client: '.$ticket['ClientName'].newline;
		$mail_message .= 'Wish Ticket: '.$ticket['id'].newline;
 		$mail_message .= 'Sender: '.$person->FirstName.' '.$person->LastName.newline;
		$mail_message .= newline;
		$mail_message .= 'Hello, '.newline;
		$mail_message .= newline;
		$mail_message .= $person->FirstName.' has requested a quote for the following wish list ticket: '.newline;
		$mail_message .= newline;
		$mail_message .= '------------------'.newline;
		$mail_message .= 'Wish List Ticket Number: '.$ticket['id'].newline;
		$mail_message .= 'Ticket Name: '.$ticket['summary'].newline;
		$mail_message .= 'Ticket Description: '.$ticket['description'].newline;
		$mail_message .= '------------------'.newline;
		$mail_message .= newline;
		//$mail_message .= 'You may view this response and comment from: http://login.cyber-ny.com/'.newline;
		$mail_message .= newline;
		$mail_message .= 'Best Regards,'.newline;
		$mail_message .= 'Cyber-NY Solutions Manager'.newline;
		$mail_message .= 'portal.cyber-ny.com'.newline;
		$mail_message .= '212-475-2721'.newline;

		$sql = "SELECT u.Email, u.FirstName, u.LastName FROM Tickets_Notification_Emails AS t
				LEFT JOIN Users AS u ON t.userID = u.ID WHERE t.clientID = ".$ticket['clientID'];
		$emails = $db->fetchAssoc($sql);

		$mail = new Zend_Mail();
		$mail->setBodyHtml($mail_message);
		$mail->setFrom('do-not-reply@cyber-ny.com', $ticket['ClientName']);
		//$mail->setFrom('contact@cnydevzone3.com', $ticket['ClientName']);

		//if not production only email support@cyber-ny.com
		if(APPLICATION_ENV != 'production') {
			$emails = array(array("Email"=>"support@cyber-ny.com","FirstName"=>"cyber support","LastName"=>""));
		}
		foreach ($emails as $user) {
			$mail->addTo($user['Email'], $user['FirstName']." ".$user['LastName']);
		}
		$mail->setSubject("Cyber-NY:: ".$ticket['ClientName']." - Quote Request for Wish List Ticket #".$ticket_id);

		if ($emails)
			$mail->send();

		return;
    }
}

<?php
class Cny_Model_Project
{
	public function __construct($data)
	{
		$this->data = $data;
	}

    public function formatUrl($url)
    {
    	if (stripos($url,"http://") === false && stripos($url,"https://") === false && trim($url) != '') {
    		$url = "http://$url";
    	}
    	return $url;
    }

    public function statusInfo()
    {
    	$info = array();
    	switch (strtolower($this->data['ProjectStatusName'])) {
    		case "open":
				$info['image'] = "open";
				$info['class'] = "green";

				if ($this->data['ExpectedEndDate'] <= mktime(23,59,59)) {
					$info['image'] = "attention_required";
					$info['class'] = "red";
				}
				if ($this->data['ExpectedEndDate'] <= (mktime(23,59,59) + 24*3600) && $this->data['ExpectedEndDate'] > mktime(23,59,59) ) {
					$info['image'] = "alert";
					$info['class'] = "yellow";
				}

				break;
			case "on client":
				$info['image'] = "on_client";
				$info['class'] = "yellow";
				break;
			case "closed":
				$info['image'] = "closed";
				$info['class'] = "green";
				break;
		}

		return $info;
    }

    public function priorityInfo()
    {
    	$info = array();
		switch (strtolower($this->data['PriorityName'])) {
			case "high":
				$info['image'] = "red";
				$info['class'] = "red";
				break;
			case "48 hours":
				$info['image'] = "yellow";
				$info['class'] = "yellow";
				break;
			default:
				$info['image']= "green";
				$info['class'] = "green";
				break;
		}
		return $info;
    }

    public function titleIcons()
    {
    	$d = "";
    	if ($this->data['RetainerStatus'] == 'y') {
    		$d .= ' <div id="task_titleIcon_retainer"></div>';
    	}
    	if (time()-$this->data['EntryDate'] < (24*3600)) {
    		$d .= ' <div id="task_titleIcon_new" title="Entry Date[ '.date("n/d/Y",$this->data['EntryDate']).' ]"></div>';
    	}
    	if ($this->data['ticket_id'] > 0) {
    		$d .= ' <div id="task_titleIcon_ticket"></div>';
    	}

    	return $d;
    }

    public function clock($user_id)
    {
    	if( $this->data['ClockStatus'] == 1 ){
			if( $user_id == $this->data['UserID'] ){
				return '<a href="/pm/clock/id/'.$this->data['id'].'"><img src="/images/clockin.gif" border=0 alt="Clock In"></a>';
			}else{
				return '<img src="/images/clockin.gif" border=0 alt="Clock In">';
			}
		}else{
			if( $user_id == $this->data['UserID'] ){
				return '<a href="/pm/clock/id/'.$this->data['id'].'"><img src="/images/clockout.gif" border=0 alt="Clock Out"></a>';
			}else{
				return '<img src="/images/clockout.gif" border=0 alt="Clock Out">';
			}
		}
    }

	public function sec2hm ($sec, $padHours = false) {
		$hms = "";
		$hours = intval(intval($sec) / 3600);

		$hms .= ($padHours) ? str_pad($hours, 2, "0", STR_PAD_LEFT). 'h ' : $hours. 'h ';

		$minutes = intval(($sec / 60) % 60);

		$hms .= str_pad($minutes, 2, "0", STR_PAD_LEFT)."m";

		return $hms;
	}

	public function sec2dec ($sec) {
		$hms = "";
		$hours = intval(intval($sec) / 3600);
		$minutes = intval(($sec / 60) % 60);

		$dec = round($hours + ($minutes/60) ,2);

		return $dec;
	}

	public function emailTask($task_id,$cc)
    {
    	$db = Zend_Db_Table::getDefaultAdapter();
    	define( 'newline', "<br/>" );

    	$sql = "SELECT p.*, c.ClientName, u.FirstName, u.LastName, u.Email FROM Projects AS p
    			LEFT JOIN Users AS u ON p.assigner = u.ID
    			LEFT JOIN Clients AS c ON p.ClientID = c.id
    			WHERE p.id = $task_id";
    	$task = $db->fetchRow($sql);

		$mail_message  = date('n/j/Y g:i A').newline;
		$mail_message .= newline;
		$mail_message .= 'Hello, '.newline;
		$mail_message .= newline;
		$mail_message .= 'The following new task has been assigned to you: '.newline;
		$mail_message .= newline;
		$mail_message .= '------------------'.newline;
		$mail_message .= 'Task Number: '.$task_id.newline;
		$mail_message .= 'Task Name: '.$task['Title'].newline;
		$mail_message .= 'Task Due: '.date("n/j/Y",$task['ExpectedEndDate']).newline;
		$mail_message .= 'Task Description: '.$task['Description'].newline;
		$mail_message .= '------------------'.newline;
		$mail_message .= newline;
		//$mail_message .= 'You may view this response and comment from: http://login.cyber-ny.com/'.newline;
		$mail_message .= newline;
		$mail_message .= 'Best Regards,'.newline;
		$mail_message .= 'Cyber-NY Solutions Manager'.newline;
		$mail_message .= 'portal.cyber-ny.com'.newline;
		$mail_message .= '212-475-2721'.newline;

		$sql =" SELECT u.Email, u.FirstName, u.LastName FROM Users AS u, Projects AS p
				WHERE p.UserID = u.ID AND p.id = ".$task_id;
		$emails = $db->fetchAssoc($sql);

		//if CC add them to list
		if ($cc) {
			$cc_list = str_replace(";",",",$cc);
			$cc = explode(",",$cc_list);
			foreach ($cc as $user) {
				$emails[] = array("Email"=>trim($user),"FirstName"=>"","LastName"=>"");
			}
		}

		$mail = new Zend_Mail();
		$mail->setBodyHtml($mail_message);
		$mail->setFrom('do-not-reply@cyber-ny.com', $task['FirstName']." ".$task['LastName']);
		//$mail->setFrom('contact@cnydevzone3.com', $task['FirstName']." ".$task['LastName']);
		$mail->setReturnPath($task['Email']);

		//if not production only email support@cyber-ny.com
		if(APPLICATION_ENV != 'production') {
			$emails = array(array("Email"=>"support@cyber-ny.com","FirstName"=>"cyber support","LastName"=>""));
		}
		foreach ($emails as $user) {
			$mail->addTo($user['Email'], $user['FirstName']." ".$user['LastName']);
		}
		$mail->setSubject($task['ClientName']." :: ".$task['Title']);
		$mail->send();

		return;
    }
	
}
<?php
class Cny_Model_Estimate
{
	public function formatUrl($url)
	{
		if (stripos($url,"http://") === false && stripos($url,"https://") === false && trim($url) != '') {
			$url = "http://$url";
		}
		return $url;
	}

	public function emailTicket($ticket_id)
	{
		$db = Zend_Db_Table::getDefaultAdapter();
		define( 'newline', "<br/>" );

		$sql = "SELECT t.*, c.ClientName, CONCAT(u.FirstName, ' ', u.LastName) AS UserName FROM estimate_requests AS t
    			LEFT JOIN Clients AS c ON t.client_id = c.id
    			LEFT JOIN Users_BugTracker AS u ON t.created_user_id = u.ID WHERE t.id = $ticket_id";
		$ticket = $db->fetchRow($sql);

		$mail_message  = date('m/d/Y H:i A').newline;
		$mail_message .= 'Client: '.$ticket['ClientName'].newline;
		$mail_message .= 'Estimate Request: '.$ticket_id.newline;
		$mail_message .= 'Status: Ticket Opened'.newline;
		$mail_message .= 'Sender: '.$ticket['UserName'].newline;
		$mail_message .= newline;
		$mail_message .= 'Hello, '.newline;
		$mail_message .= newline;
		$mail_message .= 'The following new ticket has been added: '.newline;
		$mail_message .= newline;
		$mail_message .= '------------------'.newline;
		$mail_message .= 'Ticket Number: '.$ticket_id.newline;
		$mail_message .= 'Ticket Name: '.$ticket['summary'].newline;
		$mail_message .= 'Ticket Description: '.nl2br($ticket['description']).newline;
		$mail_message .= '------------------'.newline;
		$mail_message .= newline;
		//$mail_message .= 'You may view this response and comment from: http://login.cyber-ny.com/'.newline;
		$mail_message .= newline;
		$mail_message .= 'Best Regards,'.newline;
		$mail_message .= 'Cyber-NY Solutions Manager'.newline;
		$mail_message .= 'portal.cyber-ny.com'.newline;
		$mail_message .= '212-475-2721'.newline;

		/*
		$sql = "SELECT u.Email, u.FirstName, u.LastName FROM Tickets_Notification_Emails AS t
				LEFT JOIN Users AS u ON t.userID = u.ID WHERE t.clientID = ".$ticket['clientID'];
		$emails = $db->fetchAssoc($sql);
		*/

		$emails = array(
			array("Email"=>"mike@cyber-ny.com","FirstName"=>"Mike","LastName"=>"Brown"),
			array("Email"=>"frank@cyber-ny.com","FirstName"=>"Frank","LastName"=>"Hill")
		);

		$mail = new Zend_Mail();
		$mail->setBodyHtml($mail_message);
		$mail->setFrom('do-not-reply@cyber-ny.com', $ticket['ClientName']);
		//$mail->setFrom('contact@cnydevzone3.com', $ticket['ClientName']);

		//if not production only email support@cyber-ny.com
		if(APPLICATION_ENV != 'production') {
			$emails = array(array("Email"=>"support@cyber-ny.com","FirstName"=>"cyber support","LastName"=>""));
		}
		foreach ($emails as $user) {
			$mail->addTo($user['Email'], $user['FirstName']." ".$user['LastName']);
		}
		$mail->setSubject("Cyber-NY:: ".$ticket['ClientName']." - New Estimate Request #".$ticket_id);

		if ($emails)
			$mail->send();

		return;
	}

	public function emailNote($note_id)
	{
		$db = Zend_Db_Table::getDefaultAdapter();
		$emails = array();
		define( 'newline', "<br/>" );

		$sql = "SELECT n.*, t.*, u.FirstName, u.LastName, u.Email, u.Roles, c.ClientName
    			FROM estimate_request_notes AS n, Users_BugTracker AS u, estimate_requests AS t
    			LEFT JOIN Clients AS c ON t.client_id = c.id
    			WHERE n.created_user_id = u.ID AND n.estimate_request_id = t.id AND n.id = $note_id";
		$note = $db->fetchRow($sql);

		$mail = new Zend_Mail();

		//send to staff
		/*
		$sql = "SELECT u.Email, u.FirstName, u.LastName FROM Tickets_Notification_Emails AS t
			LEFT JOIN Users AS u ON t.userID = u.ID WHERE u.id <> '".$note['assigned_user_id']."' AND t.clientID = '".$note['clientID']."'";
		$emails = $db->fetchAssoc($sql);
		*/
		$emails = array(
			array("Email"=>"mike@cyber-ny.com","FirstName"=>"Mike","LastName"=>"Brown"),
			array("Email"=>"cameron@cyber-ny.com","FirstName"=>"Cameron","LastName"=>"Smith")
		);

		array_push($emails, array("Email"=>$note['cyber_email'], "FirstName"=>$note['cyber'], "LastName"=>$note['cyber_last']));

		if ($note['Roles'] == "client") {
			$from = $note['ClientName'];
		}else {
			$sql = "SELECT u.Email, u.FirstName, u.LastName FROM estimate_requests AS t, Users_BugTracker AS u
					WHERE t.created_user_id = u.id AND t.id = '".$note['estimate_request_id']."'";
			$client = $db->fetchRow($sql);

			array_push($emails,$client);

			$UserName = $client['FirstName'];
			$from = "Cyber-NY";
		}

		$mail_message  = date('m/d/Y H:i A').newline;
		$mail_message .= 'Client: '.$note['ClientName'].newline;
		$mail_message .= 'Estimate Request: '.$note['summary'].newline;
		$mail_message .= 'Status: '.$note['status'].newline;
		$mail_message .= 'Sender: '.$note['FirstName'].' '.$note['LastName'].newline;
		$mail_message .= newline;
		$mail_message .= 'Hello, '.$UserName.newline;
		$mail_message .= newline;
		$mail_message .= 'The following response has been added to your estimate request entry: '.newline;
		$mail_message .= newline;
		$mail_message .= '------------------'.newline;
		$mail_message .= nl2br($note['message']).newline;
		$mail_message .= '------------------'.newline;
		$mail_message .= newline;
		$mail_message .= 'You may view this response and comment from: http://portal.cyber-ny.com/estimates/view/clientID/'.$note['clientID'].'/id/'.$note['estimate_request_id'].newline;
		$mail_message .= newline;
		$mail_message .= 'Best Regards,'.newline;
		$mail_message .= 'Cyber-NY Solutions Manager'.newline;
		$mail_message .= 'portal.cyber-ny.com'.newline;
		$mail_message .= '212-475-2721'.newline;

		$mail->setBodyHtml($mail_message);
		$mail->setFrom('do-not-reply@cyber-ny.com', $from);
		//$mail->setFrom('contact@cnydevzone3.com', $from);

		//add attachment if one exists for this note
		$attachment_path = $_SERVER{'DOCUMENT_ROOT'}."/attachments/";
		if ($note['attachment'] && file_exists($attachment_path.$note['attachment'])) {
			$filename = str_replace($note_id."_","",$note['attachment']);

			$fileContents = file_get_contents($attachment_path.$note['attachment']);
			$file = $mail->createAttachment($fileContents);
			$file->filename = $filename;
		}

		//if not production only email support@cyber-ny.com
		if(APPLICATION_ENV != 'production') {
			$emails = array(array("Email"=>"support@cyber-ny.com","FirstName"=>"cyber support","LastName"=>""));
		}
		foreach ($emails as $user) {
			$mail->addTo($user['Email'], $user['FirstName']." ".$user['LastName']);
		}

		//add CC if any in list
		if ($note['cc_list']) {
			$cc_list = explode(",",$note['cc_list']);

			foreach ($cc_list as $cc) {
				$mail->addCc($cc);
			}
		}

		$mail->setSubject("Cyber-NY:: ".$note['ClientName']." - Response to Estimate Request #".$note['estimate_request_id']);
		//$mail->send();
	}

}

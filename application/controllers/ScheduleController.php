<?php
class ScheduleController extends Cny_Controller_LayoutAction
{
    public function indexAction()
	{
		$this->view->placeholder('sub_section')->set("view");

		$search = new Zend_Session_Namespace('schedule_search');
    	$mask = $relationship = "";

		$page = $this->getRequest()->getParam('page',1);
		$this->view->dir = $dir = $this->_getParam('dir','DESC');
		$this->view->sort = $sort = $this->_getParam('sort','start_date');

		$lastday = date('t');
		$from_time = strtotime(date("Y-m-01"));
		$to_time = strtotime(date("Y-m-$lastday")." 23:59:59");

		$select = $this->_db->select();
		$select->from(array("s"=>"schedule_requests"));
		$select->where("s.user_id = {$this->_user->Cyberny_Users_ID}");
		$select ->order(array("$sort $dir"));

		$paginator = Zend_Paginator::factory($select);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(50);
		$this->view->requests = $paginator;
	}

	public function viewAction()
	{
		$id = $this->_getParam("id",0);

		$this->view->request = $this->_db->fetchRow("SELECT * FROM schedule_requests WHERE id = $id");
	}

	public function requestAction()
	{
		$this->_helper->layout()->setLayout("modal");

		if( $this->getRequest()->isPost() ){
			$data = $this->_getParam('data',array());
			$data['status'] = "pending";
			$data['user_id'] = $this->_user->Cyberny_Users_ID;
			$data['start_date'] = date("Y-m-d",strtotime($data['start_date']));
			$data['end_date'] = date("Y-m-d",strtotime($data['end_date']));
			$data['created'] = new Zend_Db_Expr("NOW()");
			$data['modified'] = new Zend_Db_Expr("NOW()");

			$this->_db->insert("schedule_requests",$data);
			$id = $this->_db->lastInsertId();

			$code = urlencode(base64_encode("approved||$id"));

			define( 'newline', "<br/>" );
			//email Mike to notify
			$mail_message = "Person: ".$this->_user->FirstName." ".$this->_user->LastName.newline;
			$mail_message .= "Request Type: {$data['type']}".newline;
			$mail_message .= "Dates: ".date("F d Y",strtotime($data['start_date']))." - ".date("F d Y",strtotime($data['end_date'])).newline;
			$mail_message .= "Title: {$data['title']}".newline;
			$mail_message .= "Description: {$data['description']}".newline.newline;
			$mail_message .= "To approve this request click the button below:".newline.newline;
			$mail_message .= "<a href='http://$this->_domain/request/schedule/c/$code'><img src='http://$this->_domain/images/btn_email_approve.gif' border='0'></a>";

			$mail = new Zend_Mail();
			$mail->setBodyHtml($mail_message);
			$mail->setFrom('do-not-reply@cyber-ny.com',  $this->_user->FirstName);

			//if not production only email support@cyber-ny.com
			if(APPLICATION_ENV != 'production') {
				$mail->addTo("support@cyber-ny.com", "cyber support");
				//$mail->addTo("craig@cyber-ny.com");
			}else {
				$mail->addTo("mike@cyber-ny.com","Mike Brown");
			}
			$mail->setSubject("Schedule Change Request from {$this->_user->FirstName} {$this->_user->LastName}");
			$mail->send();

			$this->_redirect("/schedule");
		}
	}

	public function deleteAction()
	{
		$id = $this->_getParam("id",0);

		$this->_db->delete("schedule_requests", "id = $id");

		$this->_redirect("/schedule");
	}

	public function editAction()
	{
		$id = $this->_getParam("id",0);

		$this->view->request = $this->_db->fetchRow("SELECT * FROM schedule_requests WHERE id = $id");

		if( $this->getRequest()->isPost() ){
			$data = $this->_getParam('data',array());
			$data['start_date'] = date("Y-m-d",strtotime($data['start_date']));
			$data['end_date'] = date("Y-m-d",strtotime($data['end_date']));
			$data['modified'] = new Zend_Db_Expr("NOW()");

			$this->_db->update("schedule_requests",$data,"id=$id");

			$this->_redirect("/schedule");
		}
	}

	public function timesheetAction()
	{
		$this->view->timesheets = array();

		$sql = $this->_db->quoteInto("SELECT DATE_FORMAT(clock_in,'%x%v') AS year_week, SUM(TIMESTAMPDIFF(SECOND,clock_in,clock_out)) AS seconds FROM timesheet WHERE user_id = ? GROUP BY DATE_FORMAT(clock_in,'%x%v') DESC LIMIT 10",$this->_user->Cyberny_Users_ID);
		$this->view->timesheets = $this->_db->fetchPairs($sql);

		//if current week not exist add it.
		$this_year_week = date("YW");
		if (!array_key_exists($this_year_week,$this->view->timesheets)) {
			$this->view->timesheets[$this_year_week] = 0;
		}

	}

	public function timesheetClockInAction()
	{
		$this->_db->insert("timesheet", array("clock_in"=>new Zend_Db_Expr("NOW()"), "user_id" => $this->_user->Cyberny_Users_ID));

		$this->_redirect("/schedule/timesheet");
	}

	public function timesheetClockOutAction()
	{
		$id = $this->_getParam("id",0);

		$this->_db->update("timesheet", array("clock_out"=>new Zend_Db_Expr("NOW()")),"id = '$id' AND user_id = '{$this->_user->Cyberny_Users_ID}' ");

		$this->_redirect("/schedule/timesheet");
	}

	public function offAction()
	{
		$this->view->layout()->disableLayout();
		$user_id = $this->_getParam("user_id",0);
		$month = str_pad($this->_getParam("month",date("m")),2,"0",STR_PAD_LEFT);
		$year = $this->_getParam("year",date("Y"));

		$sql = $this->_db->quoteInto("SELECT * FROM schedule_requests WHERE user_id = ? AND start_date LIKE '$year-$month-%' AND end_date LIKE '$year-$month-%' AND status='approved' AND type IN ('vacation','sick','off','personal','other') ",$user_id);
		$rows = $this->_db->fetchAssoc($sql);

		$days = array();
		foreach ($rows as $row) {
			$array = GetDays($row['start_date'],$row['end_date']);
			foreach ($array as $k=>$v) {
				$days[] = $v;
			}
		}

		echo json_encode($days);
		exit;
	}

	function init()
	{
		$bootstrap = $this->getInvokeArg('bootstrap'); // gets the boostrapper
		$resource = $bootstrap->getPluginResource('multidb'); //multi db support
		$this->_db = $resource->getDefaultDb();

		$options = $bootstrap->getOptions();
		$this->_domain = $options['site']['domain'];

		$this->view->placeholder('section')->set("schedule");
		$this->view->layout()->setLayout("cyber");

		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('portal'));
		if(!$auth->hasIdentity()){
			$auth->clearIdentity();
			$this->_redirect('/auth');
		}else{
			$this->view->user = $this->_user = $auth->getIdentity();
			$this->view->placeholder('logged_in')->set(true);

			if ($this->_user->cyber_user != "yes") {
				$auth->clearIdentity();
				$this->_redirect('/auth');
			}
		}

		$this->view->types = array(""=>"Select Type") + $this->_types = array("vacation"=>"Vacation","off"=>"Day-Off","sick"=>"Sick Day","personal"=>"Personal Day","remote"=>"Work Remotely","partial"=>"Partial Day Off","appointment"=>"Appointment","other"=>"Other");

		$this->view->statuses = $this->_statuses = array("pending"=>"Pending","approved"=>"Approved","denied"=>"Denied");

		$subSectionMenu = '<li id="subnav-view"><a href="/schedule"><span class="subnav-size">View Schedule</span></a></li>
							<li id="subnav-request"><a href="/schedule/request" class="dialog" title="Request Schedule Change"><span class="subnav-size">Request Schedule Change</span></a></li>
							<!--<li id="subnav-work"><a href="/schedule/work"><span class="subnav-size">My Work</span></a></li>-->
							<li id="subnav-timesheet"><a href="/schedule/timesheet"><span class="subnav-size">Time Sheet</span></a></li>
							';

		$this->view->placeholder("subSectionMenu")->set($subSectionMenu);
	}
}

function GetDays($sStartDate, $sEndDate){
	// Firstly, format the provided dates.
	// This function works best with YYYY-MM-DD
	// but other date formats will work thanks
	// to strtotime().
	$sStartDate = gmdate("Y-m-d", strtotime($sStartDate));
	$sEndDate = gmdate("Y-m-d", strtotime($sEndDate));

	// Start the variable off with the start date
	$aDays[] = $sStartDate;

	// Set a 'temp' variable, sCurrentDate, with
	// the start date - before beginning the loop
	$sCurrentDate = $sStartDate;

	// While the current date is less than the end date
	while($sCurrentDate < $sEndDate){
		// Add a day to the current date
		$sCurrentDate = gmdate("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));

		// Add this new day to the aDays array
		$aDays[] = $sCurrentDate;
	}

	// Once the loop has finished, return the
	// array of days.
	return $aDays;
}
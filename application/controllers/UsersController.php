<?php
class UsersController extends Cny_Controller_LayoutAction
{

	public function indexAction()
	{
		$this->view->placeholder('sub_section')->set("usersview");

		$search = new Zend_Session_Namespace('users_search');
    	$mask = "";

		$page = $this->getRequest()->getParam('page',1);
		$this->view->dir = $dir = $this->_getParam('dir','DESC');
		$this->view->sort = $sort = $this->_getParam('sort','u.UserName');

		$select = $this->_db->select();
		$select->from(array("u"=>"Users_BugTracker"), "*");
		$select->joinLeft(array("uc"=>"Users"), "u.Cyberny_Users_ID = uc.ID", "uc.activity_goal");
		$select->where("u.ClientID = 1");
		$select->where("u.Disabled=?","no");
		$select ->order(array("$sort $dir"));

		$paginator = Zend_Paginator::factory($select);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(50);
		$this->view->users = $paginator;
	}

	public function archiveAction()
	{
		$this->view->placeholder('sub_section')->set("usersarchive");

		$search = new Zend_Session_Namespace('users_archive_search');
		$mask = "";

		$page = $this->getRequest()->getParam('page',1);
		$this->view->dir = $dir = $this->_getParam('dir','DESC');
		$this->view->sort = $sort = $this->_getParam('sort','u.UserName');

		$select = $this->_db->select();
		$select->from(array("u"=>"Users_BugTracker"), "*");
		$select->joinLeft(array("uc"=>"Users"), "u.Cyberny_Users_ID = uc.ID", "uc.activity_goal");
		$select->where("u.ClientID = 1");
		$select->where("u.Disabled=?","yes");
		$select ->order(array("$sort $dir"));

		$paginator = Zend_Paginator::factory($select);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(50);
		$this->view->users = $paginator;
	}

	public function viewAction()
	{
		$id = $this->_getParam("id",0);

		$sql = $this->_db->quoteInto("SELECT u.*, uc.intranet_admin, uc.activity_goal FROM Users_BugTracker AS u LEFT JOIN Users AS uc ON u.Cyberny_Users_ID = uc.ID WHERE u.ID=?",$id);
		$this->view->adminUser = $this->_db->fetchRow($sql);
	}

	public function addAction()
	{
		$this->view->placeholder('sub_section')->set("usersadd");
		$this->view->adminUser = $this->_adminUser;

		if( $this->getRequest()->isPost() ){
			$data = $this->_getParam("data",array());
			$cyber = $this->_getParam("cyber",array());
			$data['ClientID'] = 1;
			$data['Roles'] = "administrator";

			if ($data['UserPassword'] != $data['vUserPassword'] || trim($data['UserPassword']) == '') {
				unset($data['UserPassword']);
			}
			unset($data['vUserPassword']);

			if ($data['UserName']=='') $data['UserName'] = $data['FirstName'];

			$cyber['UserName'] = $data['UserName'];
			$cyber['FirstName'] = $data['FirstName'];
			$cyber['LastName'] = $data['LastName'];
			$cyber['Email'] = $data['Email'];
			$cyber['Disabled'] = $data['Disabled'];

			if ($data['UserPassword']) $cyber['UserPassword'] = $data['UserPassword'];

			$this->_db->insert("Users",$cyber);
			$data['Cyberny_Users_ID'] = $this->_db->lastInsertId();

			$this->_db->insert("Users_BugTracker",$data);

			$this->_redirect("/users/");
		}
	}

	public function editAction()
	{
		$id = $this->_getParam("id",0);

		$sql = $this->_db->quoteInto("SELECT u.*, uc.intranet_admin, uc.activity_goal FROM Users_BugTracker AS u LEFT JOIN Users AS uc ON u.Cyberny_Users_ID = uc.ID WHERE u.ID=?",$id);
		$this->view->adminUser = $admin_user = $this->_db->fetchRow($sql);

		if( $this->getRequest()->isPost() ){
			$data = $this->_getParam("data",array());
			$cyber = $this->_getParam("cyber",array());
			$data['ClientID'] = 1;
			$data['Roles'] = "administrator";

			if ($data['UserPassword'] != $data['vUserPassword'] || trim($data['UserPassword']) == '') {
				unset($data['UserPassword']);
			}
			unset($data['vUserPassword']);

			if ($data['UserName']=='') $data['UserName'] = $data['FirstName'];

			$cyber['UserName'] = $data['UserName'];
			$cyber['FirstName'] = $data['FirstName'];
			$cyber['LastName'] = $data['LastName'];
			$cyber['Email'] = $data['Email'];
			$cyber['Disabled'] = $data['Disabled'];

			if ($data['UserPassword']) $cyber['UserPassword'] = $data['UserPassword'];

			$this->_db->update("Users_BugTracker",$data,"ID=$id");
			$this->_db->update("Users",$cyber,"ID={$admin_user['Cyberny_Users_ID']}");

			$this->_redirect("/users/view/id/$id");
		}
	}

	public function deleteAction()
	{
		$id = $this->_getParam('id',0);
		$user_id = $this->_db->fetchOne("SELECT Cyberny_Users_ID FROM Users_BugTracker WHERE ID=$id");

		$this->_db->delete("Users_BugTracker","ID=$id");
		$this->_db->delete("Users","ID=$user_id");

		$this->_redirect("/users/");
	}

	public function validateAction()
	{
		$type = $this->_getParam('type','');
		$data = $this->_getParam('data',array());
		$id = $this->_getParam('id',0);

		if ($type == 'email') {
			$sql = $this->_db->quoteInto("SELECT ID FROM Users_BugTracker WHERE ID <> $id AND Email = ?", $data['Email']);
			$result = $this->_db->fetchOne($sql);
		}

		if ($result) echo 'false';
		else echo 'true';
		exit();
	}

	function init()
	{
		$bootstrap = $this->getInvokeArg('bootstrap'); // gets the boostrapper
		$resource = $bootstrap->getPluginResource('multidb'); //multi db support
		$this->_db = $resource->getDefaultDb();

		$this->view->placeholder('section')->set("users");
		$this->view->layout()->setLayout("cyber");

		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('portal'));
		if(!$auth->hasIdentity()){
			$auth->clearIdentity();
			$this->_redirect('/auth');
		}else{
			$this->view->user = $this->_user = $auth->getIdentity();
			$this->view->placeholder('logged_in')->set(true);

			if ($this->_user->cyber_user != "yes") {
				$auth->clearIdentity();
				$this->_redirect('/auth');
			}

			if ($this->_user->intranet_admin != 1) {
				$this->_redirect('/');
			}

		}

		$subSectionMenu = '<li id="subnav-usersview"><a href="/users/"><span class="subnav-size">View Users</span></a></li>
							<li id="subnav-usersarchive"><a href="/users/archive"><span class="subnav-size">Users Archive</span></a></li>
							<li id="subnav-usersadd"><a href="/users/add"><span class="subnav-size">Add User</span></a></li>';
		$this->view->placeholder("subSectionMenu")->set($subSectionMenu);

		$this->_adminUser = array(
			"FirstName"=>"",
			"LastName"=>"",
			"UserName"=>"",
			"Email"=>"",
			"Phone"=>"",
			"intranet_admin"=>"",
			"Disabled"=>"",
			"activity_goal"=>""
		);
	}
}



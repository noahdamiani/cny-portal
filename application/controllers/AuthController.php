<?php
class AuthController extends Zend_Controller_Action
{
	protected $_redirectUrl = '/';

	public function indexAction()
	{
		$this->_forward('login');
	}

	public function loginAction()
	{
		$form = new Cny_Form_Login();
		$this->view->loginform = $form;

		$this->view->error = $this->getRequest()->getParam('error', '');
	}

	public function logoutAction()
	{
		//remove DokuWiki Cookie
		setcookie("dk_sso",'',time()-3600,"/wiki/");

		Zend_Session :: forgetMe();

		$auth = Zend_Auth::getInstance();
		$auth->clearIdentity();
		$this->_redirect('/auth');
	}

	public function identifyAction()
	{
		if( $this->_request->isPost() ){
			$formData = $this->_request->getPost();
			if( empty($formData['username']) || empty($formData['password']) ){
				$error = 'Empty email or password.';
			}else{
				// do the authentication
				$authAdapter = $this->_getAuthAdapter($formData);
				$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('portal'));
				$result = $auth->authenticate($authAdapter);
				if( !$result->isValid() ){
					$error = 'Login failed';
				}else{
					$last_login = date("Y-m-d H:i:s");
					$data = $authAdapter->getResultRowObject(null,'UserPassword');
					$db = Zend_Db_Table::getDefaultAdapter();
					$db->update('Users_BugTracker', array('LastLoginDate'=>$last_login), 'ID = '.$data->ID );

					//check if CyberNY ID
					if ($data->Cyberny_Users_ID > 0) {
						$db->update('Users', array('LastLoginDate'=>$last_login, 'LastLoginIP'=>$_SERVER['REMOTE_ADDR']), 'ID = '.$data->Cyberny_Users_ID );
						$data->intranet_admin = $db->fetchOne("SELECT intranet_admin FROM Users WHERE ID = {$data->Cyberny_Users_ID} ");
						$data->cyber_user = "yes";

						//set cookie for DokuWiki
						$sso_string = $data->FirstName."||".$data->LastName."||".$data->Email;
						setcookie("dk_sso",$sso_string,time()+(3600*24*180),"/wiki/");
					}else {
						$data->cyber_user = "no";
						//check if client has an active retainer
						$data->retainer = $db->fetchOne("SELECT id FROM Retainers WHERE ClientID={$data->ClientID} AND Status='enabled'");

						//check if multiple clients for this user
						$sql = "SELECT * FROM users_client WHERE user_id = {$data->ID}";
						$client_ids = $db->fetchAll($sql);

						if (count($client_ids) > 1) {
							$auth->getStorage()->write($data);
							$this->_redirect("/auth/choose");
						}else {
							//set the client_id
							$data->ClientID = $client_ids[0]['client_id'];
						}
					}

					$auth->getStorage()->write($data);
					$url_redirect = ($this->_redirectUrl) ? $this->_redirectUrl : '/';
					$this->_redirect($url_redirect);
					return;
				}
			}
		}
		$this->_redirect('/auth/login/error/'.$error);
	}

	public function chooseAction()
	{
		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('portal'));
		if($auth->hasIdentity()){
			$user = $auth->getIdentity();
		}
		$db = Zend_Db_Table::getDefaultAdapter();

		$sql = "SELECT c.id, c.ClientName FROM Clients AS c LEFT JOIN users_client AS uc ON uc.client_id = c.id WHERE uc.user_id = {$user->ID}";
		$this->view->clients = $db->fetchPairs($sql);

		if( $this->_request->isPost() ){
			$client_id = $this->_getParam("client_id",0);
			$user->ClientID = $client_id;

			$auth->getStorage()->write($user);
			$this->_redirect("/");
		}
	}

	protected function _flashMessage($message)
	{
		$flashMessenger = $this->_helper->FlashMessenger;
		$flashMessenger->setNamespace('actionErrors');
		$flashMessenger->addMessage($message);
	}

	public function forgotAction()
	{
		$bootstrap = $this->getInvokeArg('bootstrap');
		$resource = $bootstrap->getPluginResource('multidb'); //multi db support
		$db = $resource->getDefaultDb();

		if( $this->_request->isPost() ){
			$username = $this->_getParam("email","");

			$sql = "SELECT * FROM Users_BugTracker WHERE Email = '$username'";
			$user = $db->fetchRow($sql);

			if (!$user || !$username) {
				$this->view->error = "Invalid Email Address";
			}else {
				$mail = new Zend_Mail();
				$mail->setBodyHtml("Your password is: {$user['UserPassword']}");
				$mail->setFrom('do-not-reply@cyber-ny.com', 'Cyber-NY Portal');
				$mail->addTo($user['Email'], $user['FirstName']." ".$user['LastName']);
				$mail->setSubject('Password recovery for Cyber-NY Portal');
				$mail->send();

				$error = "Password email sent";
				$this->_redirect('/auth/login/error/'.$error);
			}
		}
	}

	protected function _getAuthAdapter($formData)
	{
		$dbAdapter = Zend_Db_Table::getDefaultAdapter();
		$authAdapter = new Zend_Auth_Adapter_DbTable( $dbAdapter );
		$authAdapter->setTableName('Users_BugTracker')
			->setIdentityColumn('Email')
			->setCredentialColumn('UserPassword')
			->setCredentialTreatment('AND Disabled = "no"');
		// get "salt" for better security
		// $config = Zend_Registry::get('config');
		// $salt = $config->auth->salt;
		// $password = $salt.$formData['password'];
		$password = $formData['password'];
		$authAdapter->setIdentity($formData['username']);
		$authAdapter->setCredential($password);
		return $authAdapter;
	}

	function init()
	{
		$this->view->layout()->setLayout("login");
		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('portal'));
		if( $auth->hasIdentity() && ($_SERVER['REQUEST_URI'] != "/auth/logout" && $_SERVER['REQUEST_URI'] != "/auth/choose")){
			$this->_redirect('/');
		}else{
			$this->view->placeholder('logged_in')->set(false);
			$this->view->placeholder('section')->set("login");
		}
	}
}

<?php
class ServicesController extends Cny_Controller_LayoutAction
{

	public function indexAction()
	{
		//
	}
	
	public function emailAction()
	{
		$this->_helper->layout()->setLayout("blank");
		define( 'newline', "\n" );

		$name = $this->view->name = $this->_user->FirstName." ".$this->_user->LastName;
		$this->view->services = array("Request New Email Account or Pointer","Managed Email Service","Managed Exchange Mail Server","Email Marketing & Database");
		$this->view->description = "Enter a description of the services you require here including any details that would be relevant to the project.";

		if( $this->getRequest()->isPost() ){
			$data = $this->_getParam("data",array());

			$mail_message = "Service Request: {$data['service']}".newline;
			$mail_message .= "From: {$data['name']}".newline;
			$mail_message .= "Website: {$data['website']}".newline;
			$mail_message .= "When Needed: {$data['when']}".newline;
			$mail_message .= "Description: ".newline;
			$mail_message .= $data['description'].newline;
			$mail_message .= newline;
			$mail_message .= 'Thanks,'.newline;
			$mail_message .= 'Mike Brown'.newline;
			$mail_message .= 'Cyber-NY Interactive'.newline;
			$mail_message .= '212-475-2721 Ext. 306'.newline;
			$mail_message .= 'www.cyber-ny.com'.newline;
			$mail_message .= 'mike@cyber-ny.com'.newline;
			$mail_message .= newline.'Follow Us On Twitter'.newline;
			$mail_message .= 'http://www.twitter.com/cyberny'.newline;

			$mail = new Zend_Mail();
			$mail->setBodyText($mail_message);
			$mail->setFrom('mike@cyber-ny.com', $data['name']);
			//$mail->setFrom('contact@cnydevzone3.com', $data['name']);

			$mail->addTo("support@cyber-ny.com", "cyber support");
			$mail->setSubject("Email Service Request");
			$mail->send();

			$this->_redirect("/services/sent");
		}
	}

	public function hostingAction()
	{
		$this->_helper->layout()->setLayout("blank");
		define( 'newline', "\n" );

		$name = $this->view->name = $this->_user->FirstName." ".$this->_user->LastName;
		$this->view->services = array("Virtual & Cloud Hosting", "Dedicated Server", "Amazon S3 Media Hosting");
		$this->view->description = "Enter a description of the services you require here including any details that would be relevant to the project.";

		if( $this->getRequest()->isPost() ){
			$data = $this->_getParam("data",array());

			$mail_message = "Service Request: {$data['service']}".newline;
			$mail_message .= "From: {$data['name']}".newline;
			$mail_message .= "Website: {$data['website']}".newline;
			$mail_message .= "When Needed: {$data['when']}".newline;
			$mail_message .= "Description: ".newline;
			$mail_message .= $data['description'].newline;
			$mail_message .= newline;
			$mail_message .= 'Thanks,'.newline;
			$mail_message .= 'Mike Brown'.newline;
			$mail_message .= 'Cyber-NY Interactive'.newline;
			$mail_message .= '212-475-2721 Ext. 306'.newline;
			$mail_message .= 'www.cyber-ny.com'.newline;
			$mail_message .= 'mike@cyber-ny.com'.newline;
			$mail_message .= newline.'Follow Us On Twitter'.newline;
			$mail_message .= 'http://www.twitter.com/cyberny'.newline;

			$mail = new Zend_Mail();
			$mail->setBodyText($mail_message);
			$mail->setFrom('mike@cyber-ny.com', $data['name']);
			//$mail->setFrom('contact@cnydevzone3.com', $data['name']);

			$mail->addTo("support@cyber-ny.com", "cyber support");
			$mail->setSubject("Hosting Request");
			$mail->send();

			$this->_redirect("/services/sent");
		}
	}

	public function googleAction()
	{
		$this->_helper->layout()->setLayout("blank");
		define( 'newline', "\n" );

		$name = $this->view->name = $this->_user->FirstName." ".$this->_user->LastName;
		$this->view->services = array("Add Google Analytics","Integrate Google Calendar","Google Maps","Google Website Search");
		$this->view->description = "Enter a description of the services you require here including any details that would be relevant to the project.";

		if( $this->getRequest()->isPost() ){
			$data = $this->_getParam("data",array());

			$mail_message = "Service Request: {$data['service']}".newline;
			$mail_message .= "From: {$data['name']}".newline;
			$mail_message .= "Website: {$data['website']}".newline;
			$mail_message .= "When Needed: {$data['when']}".newline;
			$mail_message .= "Description: ".newline;
			$mail_message .= $data['description'].newline;
			$mail_message .= newline;
			$mail_message .= 'Thanks,'.newline;
			$mail_message .= 'Mike Brown'.newline;
			$mail_message .= 'Cyber-NY Interactive'.newline;
			$mail_message .= '212-475-2721 Ext. 306'.newline;
			$mail_message .= 'www.cyber-ny.com'.newline;
			$mail_message .= 'mike@cyber-ny.com'.newline;
			$mail_message .= newline.'Follow Us On Twitter'.newline;
			$mail_message .= 'http://www.twitter.com/cyberny'.newline;

			$mail = new Zend_Mail();
			$mail->setBodyText($mail_message);
			$mail->setFrom('mike@cyber-ny.com', $data['name']);
			//$mail->setFrom('contact@cnydevzone3.com', $data['name']);

			$mail->addTo("support@cyber-ny.com", "cyber support");
			$mail->setSubject("Google Service Request");
			$mail->send();

			$this->_redirect("/services/sent");
		}
	}

	public function socialAction()
	{
		$this->_helper->layout()->setLayout("blank");
		define( 'newline', "\n" );

		$name = $this->view->name = $this->_user->FirstName." ".$this->_user->LastName;
		$this->view->services = array("Facebook - Page Development","Facebook - App Development","Facebook - Facebook Connect",
				"Twitter - Page Design", "YouTube", "WordPress Blog", "Sharing & Referral Features");
		$this->view->description = "Enter a description of the services you require here including any details that would be relevant to the project.";

		if( $this->getRequest()->isPost() ){
			$data = $this->_getParam("data",array());

			$mail_message = "Service Request: {$data['service']}".newline;
			$mail_message .= "From: {$data['name']}".newline;
			$mail_message .= "Website: {$data['website']}".newline;
			$mail_message .= "When Needed: {$data['when']}".newline;
			$mail_message .= "Description: ".newline;
			$mail_message .= $data['description'].newline;
			$mail_message .= newline;
			$mail_message .= 'Thanks,'.newline;
			$mail_message .= 'Mike Brown'.newline;
			$mail_message .= 'Cyber-NY Interactive'.newline;
			$mail_message .= '212-475-2721 Ext. 306'.newline;
			$mail_message .= 'www.cyber-ny.com'.newline;
			$mail_message .= 'mike@cyber-ny.com'.newline;
			$mail_message .= newline.'Follow Us On Twitter'.newline;
			$mail_message .= 'http://www.twitter.com/cyberny'.newline;

			$mail = new Zend_Mail();
			$mail->setBodyText($mail_message);
			$mail->setFrom('mike@cyber-ny.com', $data['name']);
			//$mail->setFrom('contact@cnydevzone3.com', $data['name']);

			$mail->addTo("support@cyber-ny.com", "cyber support");
			$mail->setSubject("Social Services Request");
			$mail->send();

			$this->_redirect("/services/sent");
		}
	}

	public function mobileAction()
	{
		$this->_helper->layout()->setLayout("blank");
		define( 'newline', "\n" );

		$name = $this->view->name = $this->_user->FirstName." ".$this->_user->LastName;
		$this->view->services = array("SMS Text Gateway","Twitter & Facebook API","Mobile Website Development","iPhone & iPad Development");
		$this->view->description = "Enter a description of the services you require here including any details that would be relevant to the project.";

		if( $this->getRequest()->isPost() ){
			$data = $this->_getParam("data",array());

			$mail_message = "Service Request: {$data['service']}".newline;
			$mail_message .= "From: {$data['name']}".newline;
			$mail_message .= "Website: {$data['website']}".newline;
			$mail_message .= "When Needed: {$data['when']}".newline;
			$mail_message .= "Description: ".newline;
			$mail_message .= $data['description'].newline;
			$mail_message .= newline;
			$mail_message .= 'Thanks,'.newline;
			$mail_message .= 'Mike Brown'.newline;
			$mail_message .= 'Cyber-NY Interactive'.newline;
			$mail_message .= '212-475-2721 Ext. 306'.newline;
			$mail_message .= 'www.cyber-ny.com'.newline;
			$mail_message .= 'mike@cyber-ny.com'.newline;
			$mail_message .= newline.'Follow Us On Twitter'.newline;
			$mail_message .= 'http://www.twitter.com/cyberny'.newline;

			$mail = new Zend_Mail();
			$mail->setBodyText($mail_message);
			$mail->setFrom('mike@cyber-ny.com', $data['name']);
			//$mail->setFrom('contact@cnydevzone3.com', $data['name']);

			$mail->addTo("support@cyber-ny.com", "cyber support");
			$mail->setSubject("Mobile Services Request");
			$mail->send();

			$this->_redirect("/services/sent");
		}
	}

	public function marketingAction()
	{
		$this->_helper->layout()->setLayout("blank");
		define( 'newline', "\n" );

		$name = $this->view->name = $this->_user->FirstName." ".$this->_user->LastName;
		$this->view->services = array("Pay Per Click & Search Marketing (SEM)","Search Engine Optimization (SEO)","Email List Management","Online Sweepstakes Promotions");
		$this->view->description = "Enter a description of the services you require here including any details that would be relevant to the project.";

		if( $this->getRequest()->isPost() ){
			$data = $this->_getParam("data",array());

			$mail_message = "Service Request: {$data['service']}".newline;
			$mail_message .= "From: {$data['name']}".newline;
			$mail_message .= "Website: {$data['website']}".newline;
			$mail_message .= "When Needed: {$data['when']}".newline;
			$mail_message .= "Description: ".newline;
			$mail_message .= $data['description'].newline;
			$mail_message .= newline;
			$mail_message .= 'Thanks,'.newline;
			$mail_message .= 'Mike Brown'.newline;
			$mail_message .= 'Cyber-NY Interactive'.newline;
			$mail_message .= '212-475-2721 Ext. 306'.newline;
			$mail_message .= 'www.cyber-ny.com'.newline;
			$mail_message .= 'mike@cyber-ny.com'.newline;
			$mail_message .= newline.'Follow Us On Twitter'.newline;
			$mail_message .= 'http://www.twitter.com/cyberny'.newline;

			$mail = new Zend_Mail();
			$mail->setBodyText($mail_message);
			$mail->setFrom('mike@cyber-ny.com', $data['name']);
			//$mail->setFrom('contact@cnydevzone3.com', $data['name']);

			$mail->addTo("support@cyber-ny.com", "cyber support");
			$mail->setSubject("Online Marketing Service Request");
			$mail->send();

			$this->_redirect("/services/sent");
		}
	}

	public function ecommerceAction()
	{
		$this->_helper->layout()->setLayout("blank");
		define( 'newline', "\n" );

		$name = $this->view->name = $this->_user->FirstName." ".$this->_user->LastName;
		$this->view->services = array("PayPal - Verisign Payflow Pro","PayPal - Web Payment Pro","PayPal - Web Payment Standard",
    			"SSL Certificate","Shopping Cart or Online Purchase Capabilities");
		$this->view->description = "Enter a description of the services you require here including any details that would be relevant to the project.";

		if( $this->getRequest()->isPost() ){
			$data = $this->_getParam("data",array());

			$mail_message = "Service Request: {$data['service']}".newline;
			$mail_message .= "From: {$data['name']}".newline;
			$mail_message .= "Website: {$data['website']}".newline;
			$mail_message .= "When Needed: {$data['when']}".newline;
			$mail_message .= "Description: ".newline;
			$mail_message .= $data['description'].newline;
			$mail_message .= newline;
			$mail_message .= 'Thanks,'.newline;
			$mail_message .= 'Mike Brown'.newline;
			$mail_message .= 'Cyber-NY Interactive'.newline;
			$mail_message .= '212-475-2721 Ext. 306'.newline;
			$mail_message .= 'www.cyber-ny.com'.newline;
			$mail_message .= 'mike@cyber-ny.com'.newline;
			$mail_message .= newline.'Follow Us On Twitter'.newline;
			$mail_message .= 'http://www.twitter.com/cyberny'.newline;

			$mail = new Zend_Mail();
			$mail->setBodyText($mail_message);
			$mail->setFrom('mike@cyber-ny.com', $data['name']);
			//$mail->setFrom('contact@cnydevzone3.com', $data['name']);

			$mail->addTo("support@cyber-ny.com", "cyber support");
			$mail->setSubject("Ecommerce Service Request");
			$mail->send();

			$this->_redirect("/services/sent");
		}
	}

	public function proposalAction()
	{
		$this->_helper->layout()->setLayout("blank");
		define( 'newline', "\n" );

		$name = $this->view->name = $this->_user->FirstName." ".$this->_user->LastName;
		$this->view->services = array("Website Design & Development","Ecommerce Integration","Database Development","Flash Animation & Presentation Graphics",
    				"Intranet or Project Management System","Banner or Email Design");
    	$this->view->description = "Enter a description of the services you require here including any details that would be relevant to the project.";

		if( $this->getRequest()->isPost() ){
			$data = $this->_getParam("data",array());

			$mail_message = "Service Request: {$data['service']}".newline;
			$mail_message .= "From: {$data['name']}".newline;
			$mail_message .= "Website: {$data['website']}".newline;
			$mail_message .= "When Needed: {$data['when']}".newline;
			$mail_message .= "Description: ".newline;
			$mail_message .= $data['description'].newline;
			$mail_message .= newline;
			$mail_message .= 'Thanks,'.newline;
			$mail_message .= 'Mike Brown'.newline;
			$mail_message .= 'Cyber-NY Interactive'.newline;
			$mail_message .= '212-475-2721 Ext. 306'.newline;
			$mail_message .= 'www.cyber-ny.com'.newline;
			$mail_message .= 'mike@cyber-ny.com'.newline;
			$mail_message .= newline.'Follow Us On Twitter'.newline;
			$mail_message .= 'http://www.twitter.com/cyberny'.newline;

			$mail = new Zend_Mail();
			$mail->setBodyText($mail_message);
			$mail->setFrom('mike@cyber-ny.com', $data['name']);
			//$mail->setFrom('contact@cnydevzone3.com', $data['name']);

			$mail->addTo("support@cyber-ny.com", "cyber support");
			$mail->setSubject("Proposal Request");
			$mail->send();

			$this->_redirect("/services/sent");
		}
	}

	public function sentAction()
	{
		//
	}

	function init()
	{
		$bootstrap = $this->getInvokeArg('bootstrap'); // gets the boostrapper
		$resource = $bootstrap->getPluginResource('multidb'); //multi db support
		$this->_db = $resource->getDefaultDb();

		$this->view->placeholder('section')->set("services");

		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('portal'));
		if(!$auth->hasIdentity()){
			$auth->clearIdentity();
			$this->_redirect('/auth');
		}else{
			$this->view->user = $this->_user = $auth->getIdentity();
			$this->view->placeholder('logged_in')->set(true);
		}

		$subSectionMenu = '';
		$this->view->placeholder("subSectionMenu")->set($subSectionMenu);
	}
}



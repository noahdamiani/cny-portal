<?php
class PmController extends Cny_Controller_LayoutAction
{
    public function indexAction()
	{
		$this->view->placeholder('sub_section')->set("pmview");

		//there is an issue where some tasks don't close properly. This query will find and fix them
		$sql = "UPDATE Projects SET ActualEndDate = ExpectedEndDate WHERE ActualEndDate IS NULL AND ProjectStatusID='4'";
		$this->_db->query($sql);


		//$user_id = ($this->_user->intranet_admin == 1 && $this->_getParam("user_id","")) ? $this->_getParam("user_id") : $this->_user->Cyberny_Users_ID;
		$user_id = ($this->_getParam("show_user_id","")) ? $this->_getParam("show_user_id") : $this->_user->Cyberny_Users_ID;

		if ($this->_user->intranet_admin == 1) {
			$sql = "SELECT id, UserName FROM Users AS u WHERE Disabled = 'no' ORDER BY u.UserName ASC";
			$this->view->staff = $this->_db->fetchPairs($sql);
			$this->view->view_user = $user_id;
		}

		//get week to date hours and retainer hours
		$mondaytime = (date('N')==1)?strtotime("today"):strtotime("last Monday");
		$sql = "SELECT SUM( (EndTime-StartTime) ) FROM ProjectActivity WHERE UserID = $user_id AND StartTime > $mondaytime AND EndTime IS NOT NULL";
		$this->view->total_hours_wtd = sec2hm($this->_db->fetchOne($sql));

		$sql = "SELECT SUM( (a.EndTime-a.StartTime) ) FROM ProjectActivity AS a, Projects AS p WHERE a.ProjectID = p.id AND a.UserID = $user_id AND a.StartTime > $mondaytime AND a.EndTime IS NOT NULL AND p.RetainerStatus = 'y'";
		$this->view->total_retainer_wtd = sec2hm($this->_db->fetchOne($sql));


		$search = new Zend_Session_Namespace('pm_search');
    	$mask = $relationship = "";

		$page = $this->getRequest()->getParam('page',1);
		$this->view->dir = $dir = $this->_getParam('dir','ASC');
		$this->view->sort = $sort = $this->_getParam('sort','StatusRank');

		$select = $this->_db->select();
		$select->from(array("p"=>"Projects"),array("id","Title","RetainerStatus","EntryDate","StartDate","ExpectedEndDate","ActualEndDate","LastActivityDate","ticket_id","UserID","Description",
					"TotalTime"=>new Zend_Db_Expr("ABS( SUM(EndTime - StartTime) )")));
		$select->joinLeft(array("c"=>"Clients"), "p.ClientID = c.ID", "ClientName");
		$select->joinLeft(array("u"=>"Users"), "p.UserID = u.id", "UserName");
		$select->joinLeft(array("pri"=>"Priority"), "p.ProjectPriorityID = pri.id", "PriorityName");
		$select->joinLeft(array("ps"=>"ProjectStatus"), "p.ProjectStatusID = ps.id", array("ProjectStatusName",
				"StatusRank"=>new Zend_Db_Expr("(CASE ps.ProjectStatusName WHEN 'Open' THEN CONCAT('1.',p.ExpectedEndDate)
						WHEN 'On Client' THEN 2 WHEN 'Completed' THEN 3 WHEN 'Closed' THEN 4 END)")));
		$select->joinLeft(array("pa"=>"ProjectActivity"), "p.id = pa.ProjectID", array("ClockStatus"=>"( IF( COUNT(pa.StartTime) > COUNT(pa.EndTime), 0, 1 ) )"));

		$select->group(array("p.id", "p.Title", "p.EntryDate", "p.StartDate", "p.ExpectedEndDate", "p.ActualEndDate", "p.LastActivityDate", "p.Description", "pri.PriorityName", "ps.ProjectStatusName", "u.UserName", "c.ClientName"));
		$select->where("p.UserID = $user_id");
		$select->where("ps.ProjectStatusName<>'Closed' OR (ps.ProjectStatusName='Closed' AND ((".time()." - p.ActualEndDate <= ".(24*3600*7).") OR p.ActualEndDate IS NULL) )");
		$select ->order(array("$sort $dir","StatusRank ASC","p.ExpectedEndDate DESC"));

//echo $select->__toString();exit;
		$paginator = Zend_Paginator::factory($select);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(50);
		$this->view->tasks = $paginator;
	}

	public function allAction()
	{
		$this->view->placeholder('sub_section')->set("pmall");

		$search = new Zend_Session_Namespace('pm_all_search');
    	$mask = $relationship = "";
    	$mask = $this->view->mask = $this->_getParam("mask","");

		$page = $this->getRequest()->getParam('page',1);
		$this->view->dir = $dir = $this->_getParam('dir','ASC');
		$this->view->sort = $sort = $this->_getParam('sort','StatusRank');

		$select = $this->_db->select();
		$select->from(array("p"=>"Projects"),array("id","Title","RetainerStatus","EntryDate","StartDate","ExpectedEndDate","ActualEndDate","LastActivityDate","ticket_id","UserID","Description",
					"TotalTime"=>new Zend_Db_Expr("ABS( SUM(EndTime - StartTime) )")));
		$select->joinLeft(array("c"=>"Clients"), "p.ClientID = c.ID", "ClientName");
		$select->joinLeft(array("u"=>"Users"), "p.UserID = u.id", "UserName");
		$select->joinLeft(array("pri"=>"Priority"), "p.ProjectPriorityID = pri.id", "PriorityName");
		$select->joinLeft(array("ps"=>"ProjectStatus"), "p.ProjectStatusID = ps.id", array("ProjectStatusName",
				"StatusRank"=>new Zend_Db_Expr("(CASE ps.ProjectStatusName WHEN 'Open' THEN CONCAT('1.',p.ExpectedEndDate)
						WHEN 'On Client' THEN 2 WHEN 'Completed' THEN 3 WHEN 'Closed' THEN 4 END)")));
		$select->joinLeft(array("pa"=>"ProjectActivity"), "p.id = pa.ProjectID", array("ClockStatus"=>"( IF( COUNT(pa.StartTime) > COUNT(pa.EndTime), 0, 1 ) )"));

		$select->group(array("p.id", "p.Title", "p.EntryDate", "p.StartDate", "p.ExpectedEndDate", "p.ActualEndDate", "p.LastActivityDate", "p.Description", "pri.PriorityName", "ps.ProjectStatusName", "u.UserName", "c.ClientName"));
		$select->where("ps.ProjectStatusName<>'Closed' OR (ps.ProjectStatusName='Closed' AND (".time()." - p.ActualEndDate <= ".(24*3600*7)."))");
		$select ->order(array("$sort $dir","StatusRank ASC","p.ExpectedEndDate ASC"));

		if ($mask) {
			$select->where("p.Title LIKE '%$mask%' OR c.ClientName LIKE '%$mask%' OR u.UserName LIKE '%$mask%' ");
		}

//echo $select->__toString();
		$paginator = Zend_Paginator::factory($select);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(50);
		$this->view->tasks = $paginator;
	}

	public function archiveAction()
	{
		$sql = "SELECT id, ClientName FROM Clients ORDER BY ClientName ASC";
		$this->view->clients = array(""=>"Select Client")+$this->_db->fetchPairs($sql);
		$sql = "SELECT id, UserName FROM Users AS u ORDER BY u.UserName ASC";
		$this->view->staff = array(""=>"Select Owner")+$this->_db->fetchPairs($sql);
		$sql = "SELECT id, PriorityName FROM Priority ORDER BY SortOrder DESC";
		$this->view->priorities = array(""=>"Select Priority")+$this->_db->fetchPairs($sql);

		$this->view->placeholder('sub_section')->set("pmarchive");

		$search = new Zend_Session_Namespace('pm_archive_search');

		$page = $this->getRequest()->getParam('page',1);
		$this->view->dir = $dir = $this->_getParam('dir','DESC');
		$this->view->sort = $sort = $this->_getParam('sort','ExpectedEndDate');

		$select = $this->_db->select();

		//initialize variables
		$keyword = $UserID = $ClientID = $dateStart = $dateEnd = $ProjectPriorityID = "";

		if ($search->keyword != '') $keyword = $search->keyword;
		if ($search->UserID != '') $UserID = $search->UserID;
		if ($search->ClientID != '') $ClientID = $search->ClientID;
		if ($search->dateStart != '') $dateStart = $search->dateStart;
		if ($search->dateEnd != '') $dateEnd = $search->dateEnd;
		if ($search->ProjectPriorityID != '') $ProjectPriorityID = $search->ProjectPriorityID;

		if( $this->getRequest()->isPost() ){
			$this->view->data = $data = $this->_getParam("search",array());
			foreach ($data as $k=>$v) $$k=$v;

			if (empty($data)) {
				$search->setExpirationSeconds(1);
			}
		}

		if ($keyword) {
			$search->keyword = $keyword;
			$select->where("p.Title LIKE '%$keyword%'");
		}else {
			$search->keyword = '';
		}
		if ($UserID) {
			$search->UserID = $UserID;
			$select->where("p.UserID = $UserID");
		}else {
			$search->UserID = '';
		}
		if ($ClientID) {
			$search->ClientID = $ClientID;
			$select->where("p.ClientID = $ClientID");
		}else {
			$search->ClientID = '';
		}
		if ($dateStart) {
			if (!$dateEnd) $dateEnd = $dateStart;

			$search->dateStart = $dateStart;
			$search->dateEnd = $dateEnd;

			$select->where("startDate >= '".strtotime($dateStart)."' AND startDate <= '".strtotime($dateEnd)."'");
		}else {
			$search->dateStart = '';
			$search->dateEnd = '';
		}
		if ($ProjectPriorityID) {
			$search->ProjectPriorityID = $ProjectPriorityID;
			$select->where("p.ProjectPriorityID = $ProjectPriorityID");
		}else {
			$search->ProjectPriorityID = '';
		}

		$select->from(array("p"=>"Projects"),array("id","Title","RetainerStatus","EntryDate","StartDate","ExpectedEndDate","ActualEndDate","LastActivityDate","ticket_id","UserID","Description",
					"TotalTime"=>new Zend_Db_Expr("ABS( SUM(EndTime - StartTime) )")));
		$select->joinLeft(array("c"=>"Clients"), "p.ClientID = c.ID", "ClientName");
		$select->joinLeft(array("u"=>"Users"), "p.UserID = u.id", "UserName");
		$select->joinLeft(array("pri"=>"Priority"), "p.ProjectPriorityID = pri.id", "PriorityName");
		$select->joinLeft(array("ps"=>"ProjectStatus"), "p.ProjectStatusID = ps.id", array("ProjectStatusName"));
		$select->joinLeft(array("pa"=>"ProjectActivity"), "p.id = pa.ProjectID", array(""));

		$select->group(array("p.id", "p.Title", "p.EntryDate", "p.StartDate", "p.ExpectedEndDate", "p.ActualEndDate", "p.LastActivityDate", "p.Description", "pri.PriorityName", "ps.ProjectStatusName", "u.UserName", "c.ClientName"));
		$select->where("ps.ProjectStatusName='Closed' AND (".time()." - p.ActualEndDate > ".(24*3600*7).")");
		$select ->order(array("$sort $dir","p.ExpectedEndDate DESC"));

//echo $select->__toString();
		$paginator = Zend_Paginator::factory($select);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(50);
		$this->view->tasks = $paginator;

		$isCSVExport = $this->_getParam('csvExport','');
		if($isCSVExport){
			$this->_helper->layout()->disableLayout();

			$stmt = $this->_db->query($select);
			$this->view->tasks = $stmt->fetchAll();

			$this->render('csvexport');
		}
	}

	public function viewAction()
	{
		$id = $this->_getParam('id',0);
		$this->view->activity_id = $this->_getParam("activity","");

		$select = $this->_db->select();
		$select->from(array("p"=>"Projects"),array("*",
					"TotalTime"=>new Zend_Db_Expr("ABS( SUM(EndTime - StartTime) )")));
		$select->joinLeft(array("c"=>"Clients"), "p.ClientID = c.ID", "ClientName");
		$select->joinLeft(array("u"=>"Users"), "p.UserID = u.id", "UserName");
		$select->joinLeft(array("au"=>"Users"), "p.assigner = au.id", array("assigner"=>"UserName"));
		$select->joinLeft(array("cp"=>"client_projects"), "p.client_project_id = cp.id", array("ClientProject"=>"name"));
		$select->joinLeft(array("pri"=>"Priority"), "p.ProjectPriorityID = pri.id", "PriorityName");
		$select->joinLeft(array("ps"=>"ProjectStatus"), "p.ProjectStatusID = ps.id", array("ProjectStatusName",
				"StatusRank"=>new Zend_Db_Expr("(CASE ps.ProjectStatusName WHEN 'Open' THEN 1
						WHEN 'On Client' THEN 2 WHEN 'Completed' THEN 3 WHEN 'Closed' THEN 4 END)")));
		$select->joinLeft(array("pa"=>"ProjectActivity"), "p.id = pa.ProjectID", array("ClockStatus"=>"( IF( COUNT(pa.StartTime) > COUNT(pa.EndTime), 0, 1 ) )"));
		$select->joinLeft(array("t"=>"tickets"), "p.ticket_id = t.id", "");
		$select->joinLeft(array("ub"=>"Users_BugTracker"), "t.created_user_id = ub.ID",array("created_by"=>"CONCAT(ub.FirstName, ' ', ub.LastName)"));
		$select->where("p.id=$id");
		$this->view->task = $this->_db->fetchRow($select->__toString());

		if ($this->view->task['ticket_id']) {
			$sql = $this->_db->quoteInto("SELECT tn.*, CONCAT(u.FirstName, ' ', u.LastName) AS name, u.Roles FROM ticket_notes AS tn LEFT JOIN Users_BugTracker AS u ON tn.created_user_id = u.ID
				WHERE tn.ticket_id = ? ORDER BY tn.created DESC",$this->view->task['ticket_id']);
			$this->view->notes = $this->_db->fetchAssoc($sql);
		}

		$sql = "SELECT * FROM Ftps WHERE clientId = {$this->view->task['ClientID']}";
		$this->view->ftps = $this->_db->fetchAssoc($sql);

		$sql = $this->_db->quoteInto("SELECT pa.id, u.UserName, pa.StartTime, pa.EndTime FROM ProjectActivity pa LEFT JOIN Users u ON pa.UserID=u.id WHERE ProjectID=? ORDER BY pa.StartTime",$id);
		$this->view->activity = $activity = $this->_db->fetchAssoc($sql);

		$sql = $this->_db->quoteInto("SELECT tcr.id, u.UserName, tcr.original_date, tcr.new_date, tcr.explenation FROM task_change_requests AS tcr LEFT JOIN Users u ON tcr.user_id=u.id WHERE task_id=? AND tcr.status='approved' ORDER BY created DESC",$id);
		$this->view->movement = $movement = $this->_db->fetchAssoc($sql);

		$sql = $this->_db->quoteInto("SELECT tn.*, CONCAT(u.FirstName, ' ', u.LastName) AS name, u.Roles FROM task_notes AS tn LEFT JOIN Users_BugTracker AS u ON tn.created_user_id = u.ID
				WHERE tn.task_id = ? ORDER BY tn.created DESC",$this->view->task['id']);
		$this->view->task_notes = $this->_db->fetchAssoc($sql);

		$this->view->prev = $this->_getParam("prev","");
	}

	public function editAction()
	{
		$id = $this->_getParam("id",0);

		$this->view->priorities = $this->_db->fetchPairs("SELECT id, PriorityName FROM Priority ORDER BY SortOrder ASC");
		$this->view->statuses = $this->_db->fetchPairs("SELECT id, ProjectStatusName FROM ProjectStatus WHERE Disabled='no' ORDER BY SortOrder ASC");

		$sql = $this->_db->quoteInto("SELECT p.*, u.UserName, au.UserName AS assigner, au.id AS assigner_id, c.ClientName FROM Projects AS p LEFT JOIN Users AS u ON p.UserID = u.ID LEFT JOIN Users as au ON p.assigner = au.ID LEFT JOIN Clients AS c ON p.ClientID = c.id WHERE p.id = ?",$id);
		$this->view->task = $task = $this->_db->fetchRow($sql);

		$sql = "SELECT id, ClientName FROM Clients WHERE Disabled = 'no' ORDER BY ClientName ASC";
		$this->view->clients = array(""=>"Select Client")+$this->_db->fetchPairs($sql);

		$sql = "SELECT id, UserName FROM Users WHERE Disabled = 'no' ORDER BY UserName ASC";
		$this->view->staff = array(""=>"Select Staff")+$this->_db->fetchPairs($sql);

		$sql = "SELECT id, name FROM client_projects WHERE 1=1 ORDER BY name ASC";
		$this->view->projects = array(""=>"Select Project")+$this->_db->fetchPairs($sql);

		if( $this->getRequest()->isPost() && $id ){
			$data = $this->_getParam('data',array());

			if ($data['StartDate']) $data['StartDate'] = strtotime($data['StartDate']);
			else $data['StartDate'] = new Zend_Db_Expr("NULL");

			if ($data['ExpectedEndDate']) $data['ExpectedEndDate'] = strtotime($data['ExpectedEndDate']);
			else $data['ExpectedEndDate'] = new Zend_Db_Expr("NULL");

			//close task and set end date/time
			if ($data['ProjectStatusID'] == 4) { $data['ActualEndDate'] = time(); }
			if ($data['ProjectStatusID'] != $task['ProjectStatusID']) { $data['LastActivityDate'] = time(); }

			//if Expected End Date changed and assigned by someone else request approval
			if (date("Ymd",$data['ExpectedEndDate']) != date("Ymd",$task['ExpectedEndDate']) && $task['assigner_id'] != $this->_user->Cyberny_Users_ID && $this->_user->Cyberny_Users_ID != 2) {
				$this->_db->insert("task_change_requests",array(
					"user_id" => $this->_user->Cyberny_Users_ID,
					"assigner_id" => $task['assigner_id'],
					"task_id" => $task['id'],
					"original_date" => $task['ExpectedEndDate'],
					"new_date" => $data['ExpectedEndDate'],
					"explenation" => $this->_getParam("explenation",""),
					"created" => new Zend_Db_Expr("NOW()"),
					"modified" => new Zend_Db_Expr("NOW()")
				));
				$request_id = $this->_db->lastInsertId();

				$sql = "SELECT * FROM Users WHERE id = {$task['assigner_id']}";
				$assigner = $this->_db->fetchRow($sql);

				$code_approve = urlencode(base64_encode("approved||$request_id"));
				$code_deny = urlencode(base64_encode("denied||$request_id"));

				//send request email
				define( 'newline', "<br/>" );
				$mail_message = "<strong>".$this->_user->FirstName." ".$this->_user->LastName." is requesting to move task: ".$task['Title']."</strong>".newline.newline;
				$mail_message .= 'Client: '.$task['ClientName'].newline;
				$mail_message .= 'Orignal Due Date: '.date("l F d Y",$task['ExpectedEndDate']).newline;
				$mail_message .= 'Requested Due Date: '.date("l F d Y",$data['ExpectedEndDate']).newline.newline;
				$mail_message .= 'Reason: '.$this->_getParam("explenation","").newline.newline;

				$mail_message .= "<a href='http://$this->_domain/request/task/c/$code_approve'><img src='http://$this->_domain/images/btn_email_approve.gif' border='0'></a>";
				$mail_message .= " ";
				$mail_message .= "<a href='http://$this->_domain/request/task/c/$code_deny'><img src='http://$this->_domain/images/btn_email_deny.gif' border='0'></a>";

				$mail = new Zend_Mail();
				$mail->setBodyHtml($mail_message);

				//if not production only email support@cyber-ny.com
				if(APPLICATION_ENV != 'production') {
					$mail->setFrom('do-not-reply@cnydevzone3.com', $task['ClientName']);
					$mail->addTo("support@cyber-ny.com", "cyber support");
					//$mail->addTo("craig@cyber-ny.com");
				}else {
					$mail->setFrom('do-not-reply@cyber-ny.com',  $task['ClientName']);
					$mail->addTo("{$assigner['Email']}");
				}
				$mail->setSubject("Request to move task: {$task['Title']}");
				$mail->send();

				unset($data['ExpectedEndDate']);
			}elseif ($data['ExpectedEndDate'] != $task['ExpectedEndDate']) {
				$this->_db->insert("task_change_requests",array(
					"user_id" => $this->_user->Cyberny_Users_ID,
					"assigner_id" => $task['assigner_id'],
					"task_id" => $task['id'],
					"original_date" => $task['ExpectedEndDate'],
					"new_date" => $data['ExpectedEndDate'],
					"explenation" => $this->_getParam("explenation",""),
					"status" => "approved",
					"created" => new Zend_Db_Expr("NOW()"),
					"modified" => new Zend_Db_Expr("NOW()")
				));
			}

			$this->_db->update("Projects",$data,"id=$id");

			//if client ticket connected make updates to that as well.
			if ($task['ticket_id'] > 0) {
				$tdata = array();

				if ($data['ProjectStatusID'] == 3) {
					//$tdata['status'] = "Attention Required";
					$tdata['status'] = "Comments Submitted";
				}elseif ($data['ProjectStatusID'] == 4) {
					$sql = "SELECT SUM(ProjectStatusID) AS stat, COUNT(*) AS cnt FROM Projects WHERE ticket_id = {$task['ticket_id']} AND id <> $id";
					$result = $this->_db->fetchRow($sql);

					if ($result['stat']/$result['cnt'] == 4 || !$result)
						$tdata['status'] = "Completed";
				}

				if ($data['ExpectedEndDate']) {
					$tdata['duedate'] = date("Y-m-d",$data['ExpectedEndDate']);
				}

				$tdata['assigned_user_id'] = $data['UserID'];

				if ($tdata)
					$this->_db->update("tickets",$tdata,"id={$task['ticket_id']}");

				//send email to client if put on client.
				$ticket_note = new Cny_Model_Ticket();

				if( $data['ProjectStatusID'] == 3 && ($this->_getParam('no_email',0)==0) ){
					$ndata['ticket_id'] = $task['ticket_id'];
					$ndata['message'] = htmlentities($this->_getParam("status_explenation",""));
					$ndata['created_user_id'] = $this->_user->ID;
					$ndata['type'] = 'User';
					$ndata['created'] = new Zend_Db_Expr("NOW()");
					//$ndata['cc_list'] = str_replace(";",",",$this->_getParam("cc_list",""));

					$this->_db->insert("ticket_notes",$ndata);
					$note_id = $this->_db->lastInsertId();

					$ticket_note->emailNote($note_id);
				}
			}

			//check if need to email
			if ($data['UserID'] != $task['UserID'] || $this->_getParam("resend","") == "email") {
				$project = new Cny_Model_Project();
				$project->emailTask($id,$this->_getParam("cc",""));
			}

			//add note and email to assigner
			if (($data['ProjectStatusID'] == 3 || $data['ProjectStatusID'] == 4) && $this->_getParam("status_explenation","")) {
				$data = array("ProjectID"=>$id,"UserID"=>$this->_user->Cyberny_Users_ID,"EntryDate"=>time(),"LastUpdate"=>time(),"Notes"=>$this->_getParam("status_explenation",""));
				$this->_db->insert("ProjectNotes",$data);

				//send email to assigner if different from user
				if ($task['assigner'] != $this->_user->Cyberny_Users_ID) {
					$sql = "SELECT * FROM Users WHERE id = {$task['assigner_id']}";
					$assigner = $this->_db->fetchRow($sql);

					$h_status = ($data['ProjectStatusID'] == 4)?"Closed":"On Client";

					//send email
					define( 'newline', "<br/>" );
					$mail_message = "<strong>".$this->_user->FirstName." ".$this->_user->LastName." has updated task: ".$task['Title']."</strong>".newline.newline;
					$mail_message .= 'Status changed to: '.$h_status.newline;
					$mail_message .= 'Client: '.$task['ClientName'].newline;
					$mail_message .= 'Comment: '.$this->_getParam("status_explenation","").newline.newline;

					$mail = new Zend_Mail();
					$mail->setBodyHtml($mail_message);

					//if not production only email support@cyber-ny.com
					if(APPLICATION_ENV != 'production') {
						$mail->setFrom('do-not-reply@cnydevzone3.com', $this->_user->FirstName." ".$this->_user->LastName);
						$mail->addTo("support@cyber-ny.com", "cyber support");
						//$mail->addTo("craig@cyber-ny.com");
					}else {
						$mail->setFrom('do-not-reply@cyber-ny.com', $this->_user->FirstName." ".$this->_user->LastName);
						$mail->addTo("{$assigner['Email']}");
					}
					$mail->setSubject("Status change for task: {$task['Title']}");
					$mail->send();
				}
			}

			$this->_redirect("/pm/view/id/$id");
		}
	}

	public function addAction()
	{
		//$this->_helper->layout()->setLayout("blank");
		$this->view->url = $url = $this->_getParam("url","");
		$this->view->back = ($url)?base64_decode($url):"/pm/";
		$this->view->ticket_id = $this->_getParam("ticket_id","");

		$this->view->client_project_id = $this->_getParam("client_project_id","");
		$this->view->ClientID = $this->_getParam("ClientID","");

		$this->view->priorities = $this->_db->fetchPairs("SELECT id, PriorityName FROM Priority ORDER BY SortOrder ASC");
		$this->view->statuses = $this->_db->fetchPairs("SELECT id, ProjectStatusName FROM ProjectStatus WHERE Disabled='no' ORDER BY SortOrder ASC");

		$sql = "SELECT id, ClientName FROM Clients WHERE Disabled = 'no' ORDER BY ClientName ASC";
		$this->view->clients = array(""=>"Select Client")+$this->_db->fetchPairs($sql);

		$sql = "SELECT id, UserName FROM Users WHERE Disabled = 'no' ORDER BY UserName ASC";
		$this->view->staff = array(""=>"Select Staff")+$this->_db->fetchPairs($sql);

		$sql = "SELECT id, name FROM client_projects WHERE 1=1 ORDER BY name ASC";
		$this->view->projects = array(""=>"Select Project")+$this->_db->fetchPairs($sql);

		$this->view->job_code = '';
		if ($this->view->client_project_id) {
			$sql = $this->_db->quoteInto("SELECT job_code FROM client_projects WHERE id = ?",$this->view->client_project_id);
			$this->view->job_code = $this->_db->fetchOne($sql);
		}

		$this->view->task = array();
		$this->view->task['ProjectPriorityID'] = "1";
		$this->view->task['ProjectStatusID'] = "1";
		$this->view->task['assigner'] = $this->_user->FirstName;

		if (!$this->view->ClientID && $this->view->ticket_id) {
			$this->view->ClientID = $this->_db->fetchOne("SELECT clientID FROM tickets WHERE id = {$this->view->ticket_id}");
			$this->view->retainer = $this->_db->fetchOne("SELECT id FROM Retainers WHERE ClientID = '{$this->view->ClientID}' AND Status = 'enabled' AND StartDate <= DATE(NOW())");
		}

		if( $this->getRequest()->isPost() ){
			$data = $this->_getParam('data',array());

			if ($data['StartDate']) $data['StartDate'] = strtotime($data['StartDate']);
			if ($data['ExpectedEndDate']) $data['ExpectedEndDate'] = strtotime($data['ExpectedEndDate']);
			$data['assigner'] = $this->_user->Cyberny_Users_ID;

			if ($data['ticket_id'] && stripos($data['title'],$data['ticket_id']) === false)
				$data['Title'] = $data['ticket_id']." - ".$data['Title'];

			$this->_db->insert("Projects",$data);
			$id = $this->_db->lastInsertId();

			if ($data['UserID'] != $this->_user->Cyberny_Users_ID && $this->_user->Cyberny_Users_ID) {
				$project = new Cny_Model_Project();
				$project->emailTask($id,$this->_getParam("cc",""));
			}

			if ($url)
				$this->_redirect(base64_decode($url));

			$this->_redirect("/pm/view/id/$id");
		}
	}

	public function deleteAction()
	{
		$id = $this->_getParam("id",0);

		$this->_db->delete("Projects", "id = $id");

		$this->_redirect("/pm");
	}

	public function addactivityAction()
	{
		$id = $this->_getParam("id",0);
		$start = $this->_getParam("start","now");
		$end = $this->_getParam("end",null);
		if ($start == "") $start = "now";
		if ($end == "") $end = null;

		if ($start !== null && $id) {
			$data['ProjectID'] = $id;
			$data['UserID'] = $this->_user->Cyberny_Users_ID;
			$data['StartTime'] = strtotime($start);

			if ($end !== null && $end != '') {
				$data['EndTime'] = strtotime($end);
			}
			$this->_db->insert("ProjectActivity",$data);
		}

		$this->_redirect("/pm/view/id/$id");
	}

	public function editactivityAction()
	{
		$id = $this->_getParam("id",0);
		$activity_id = $this->_getParam("activity",0);
		$start = $this->_getParam("start",null);
		$end = $this->_getParam("end",null);

		if ($start !== null && $activity_id) {
			$data['StartTime'] = strtotime($start);

			if ($end !== null && $end != '') {
				$data['EndTime'] = strtotime($end);
			}
			$this->_db->update("ProjectActivity",$data,"id=$activity_id");
		}

		$this->_redirect("/pm/view/id/$id");
	}

	public function deleteactivityAction()
	{
		$id = $this->_getParam("id",0);
		$activity_id = $this->_getParam("activity",0);

		$this->_db->delete("ProjectActivity","id=$activity_id");

		$this->_redirect("/pm/view/id/$id");
	}

	public function productionscheduleAction()
	{
		$this->view->placeholder('sub_section')->set("pmschedule");

		$this->view->time = $time = $this->_getParam("timestamp",time());

		$sql = "SELECT
					p.id, p.Title, p.EntryDate, p.StartDate, p.ExpectedEndDate, p.ActualEndDate, u.UserName, p.UserID, c.ClientName, p.Description,
				    DAYOFWEEK(FROM_UNIXTIME(p.ExpectedEndDate)) AS WeekDay, timespan
				FROM
					Projects p LEFT JOIN Clients c ON p.ClientID = c.id
					LEFT JOIN Users u ON p.UserID = u.id
					LEFT JOIN ProjectStatus ps on p.ProjectStatusID = ps.id
				WHERE WEEK(FROM_UNIXTIME(p.ExpectedEndDate)) =  WEEK(FROM_UNIXTIME($time)) AND
				      YEAR(FROM_UNIXTIME(p.ExpectedEndDate)) =  YEAR(FROM_UNIXTIME($time))
				AND ProjectStatusName = 'Open'
				ORDER BY p.ExpectedEndDate DESC, seq_no";
		$this->view->projects = $this->_db->fetchAssoc($sql);
	}

	public function prodscheduleajaxAction()
	{
		$this->view->layout()->disableLayout();
		$tod = $this->_getParam("timeOfDay","");
		$date = $this->_getParam("newDate","");
		$id = $this->_getParam("id",0);

		if( $tod == 'morning' ) $timespan = '1';
		else if( $tod == 'afternoon' ) $timespan = '2';
		else $timespan = '3';

		$endday = strtotime("$date 23:59:59");

		$sql = "UPDATE Projects SET ExpectedEndDate = '$endday', timespan = '$timespan' WHERE id = $id";
		$this->_db->query($sql);

		exit;
	}

	public function dailyscheduleAction()
	{
		$this->view->placeholder('sub_section')->set("dailyschedule");

		$i=1; $this->view->results = array();
		$this->view->start = $start = date('z')+1;
		$this->view->end = $end = date('z')+8;

		for ($c=$start; $c<$end; $c++) {
			$sql = "SELECT
					p.id, p.Title, p.EntryDate, p.StartDate, p.ExpectedEndDate, p.ActualEndDate, u.UserName, p.UserID, c.ClientName, p.Description,
				    DAYOFWEEK(FROM_UNIXTIME(p.ExpectedEndDate)) AS WeekDay, timespan
				FROM
					Projects p LEFT JOIN Clients c ON p.ClientID = c.id
					LEFT JOIN Users u ON p.UserID = u.id
					left join ProjectStatus ps on p.ProjectStatusID = ps.id
				WHERE DAYOFYEAR(FROM_UNIXTIME(p.ExpectedEndDate)) = $c
				AND ProjectStatusName = 'Open'
				ORDER BY u.UserName ASC";
			$this->view->results[$i] = $this->_db->fetchAssoc($sql);

			$i++;
		}

	}

	public function emaildailyscheduleAction()
	{
		//send out email
		if( $this->getRequest()->isPost() ){
			$days = $this->_getParam("days","");
			$intro = $this->_getParam("intro","");
			$mail_message = "";

			define( 'newline', "<br/>" );

			if ($intro) {
				$mail_message .= newline.$intro.newline;
			}

			$mail_message .= '<div style="color:#800080;">Staff Schedule Changes:</div>';

			$startDate = $endDate = date("Y-m-d");
			foreach ($days as $c) {
				$date = date("Y-m-d",mktime(0,0,0,1,$c,date('Y')));
				$startDate = ($startDate > $date) ? $date : $startDate;
				$endDate = ($endDate < $date) ? $date : $endDate;
			}

			//get events
			$sql = "SELECT ce.id, DATE_FORMAT(ce.start,'%c/%e/%y') AS StartDate, DATE_FORMAT(ce.end,'%c/%e/%y') AS EndDate, CONCAT(u.FirstName, ' ', u.LastName) AS name, ce.title
					FROM calendar_entries AS ce LEFT JOIN Users AS u ON ce.user_id = u.ID WHERE u.Disabled = 'no' AND ce.public = 'y'
					AND ((ce.start >= '$startDate' AND ce.end >= '$startDate' AND ce.start <= '$endDate') OR (ce.start < '$startDate' AND ce.end >= '$startDate')) ORDER BY ce.start";
			$events = $this->_db->fetchAssoc($sql);

			if ($events) {
				foreach ($events as $e) {
					$mail_message .= "{$e['name']} - {$e['title']} - {$e['StartDate']} - {$e['EndDate']}".newline;
				}
			}else {
				$mail_message .= "No schedule changes".newline;
			}
			$mail_message .= newline;


			$mail_message .= '<div style="color:red;">================'.newline.'Project Priorities:'.newline.'================</div>';

			foreach ($days as $c) {
				$day_range[] = mktime(0,0,0,1,$c,date('Y'));
			}
			$day_range = implode(",",$day_range);

			$sql = "SELECT
						p.id, p.Title, p.EntryDate, p.StartDate, p.ExpectedEndDate, p.ActualEndDate, u.UserName, p.UserID, c.ClientName, p.Description,
					    DAYOFWEEK(FROM_UNIXTIME(p.ExpectedEndDate)) as WeekDay, timespan, cp.name AS project_name, plu.UserName AS project_lead
					FROM
						Projects p LEFT JOIN Clients c ON p.ClientID = c.id
						LEFT JOIN Users u ON p.UserID = u.id
						left join ProjectStatus ps on p.ProjectStatusID = ps.id
						LEFT JOIN client_projects AS cp ON p.client_project_id = cp.id
						LEFT JOIN Users plu ON cp.project_lead_id = plu.id
					WHERE p.ExpectedEndDate IN ($day_range)
						AND ProjectStatusName = 'Open' AND p.client_project_id <> 0
					ORDER BY cp.name ASC, u.UserName ASC";
			$results = $this->_db->fetchAssoc($sql);

			$project_name = '';
			foreach ($results as $row) {
				if ($row['project_name'] != $project_name) {
					$mail_message .= newline.'<div style="color:#003366;font-weight:bold;">'.$row['project_name'].': Project Lead - '.$row['project_lead'].'</div>';
					$project_name = $row['project_name'];
				}
				$mail_message .= "&bull; {$row['Title']} - {$row['UserName']}".newline;
			}

			$mail_message .= newline.newline;
			$mail_message .= '<span style="color:red;font-weight:bold;">Here\'s the Production Breakdown:</span>'.newline.newline;

			foreach ($days as $c) {
				if ($c != '') {
					$day = strtoupper(date('l',mktime(0,0,0,1,$c,date('Y'))));

					$mail_message .= "<strong>$day ::</strong>".newline;

					$sql = "SELECT
								p.id, p.Title, p.EntryDate, p.StartDate, p.ExpectedEndDate, p.ActualEndDate, u.UserName, p.UserID, c.ClientName, p.Description,
							    DAYOFWEEK(FROM_UNIXTIME(p.ExpectedEndDate)) as WeekDay, timespan
							FROM
								Projects p LEFT JOIN Clients c ON p.ClientID = c.id
								LEFT JOIN Users u ON p.UserID = u.id
								left join ProjectStatus ps on p.ProjectStatusID = ps.id
							WHERE DAYOFYEAR(FROM_UNIXTIME(p.ExpectedEndDate)) = $c
							and ProjectStatusName = 'Open'
							ORDER BY u.UserName ASC";
					$results = $this->_db->fetchAssoc($sql);

					foreach ($results as $row) {
						$mail_message .= "{$row['UserName']} - {$row['ClientName']} - {$row['Title']}".newline;
					}

					$mail_message .= newline;
				}
			}

			//past due items
			$sql = "SELECT
						p.id, p.Title, p.EntryDate, p.StartDate, p.ExpectedEndDate, p.ActualEndDate, u.UserName, p.UserID, c.ClientName, p.Description,
					    DAYOFWEEK(FROM_UNIXTIME(p.ExpectedEndDate)) as WeekDay, timespan
					FROM
						Projects p LEFT JOIN Clients c ON p.ClientID = c.id
						LEFT JOIN Users u ON p.UserID = u.id
						left join ProjectStatus ps on p.ProjectStatusID = ps.id
					WHERE p.ExpectedEndDate < ".mktime(0,0,0,date('n'),date('j'),date('Y'))."
					and ProjectStatusName = 'Open'
					ORDER BY u.UserName ASC";
			$results = $this->_db->fetchAssoc($sql);

			$past_due = '';
			foreach ($results as $row) {
				$past_due .= "<span style='color:red;'>{$row['UserName']} - {$row['ClientName']} - {$row['Title']}</span>".newline;
			}

			if ($past_due) {
				$mail_message .= '<span style="color:red;font-weight:bold;">These Projects Are Past Due:</span>'.newline.newline;
				$mail_message .= '<strong>Contact the Assigner and Reschedule.  Then Update Your Entry.</strong>'.newline;
				$mail_message .= $past_due.newline;
			}

			$mail_message .= newline;
			$mail_message .= 'Thanks,'.newline;
			$mail_message .= $this->_user->FirstName." ".$this->_user->LastName.newline;
			$mail_message .= 'Cyber-NY Interactive'.newline;
			$mail_message .= '212-475-2721'.newline;
			$mail_message .= 'www.cyber-ny.com'.newline;
			$mail_message .= $this->_user->Email.newline;
			$mail_message .= newline.'Follow Us On Twitter'.newline;
			$mail_message .= 'http://www.twitter.com/cyberny'.newline;

			$mail = new Zend_Mail();
			$mail->setBodyHtml($mail_message);
			$mail->setFrom($this->_user->Email,  $this->_user->FirstName." ".$this->_user->LastName);
			//$mail->setFrom('contact@cnydevzone3.com', $ticket['ClientName']);

			//if not production only email support@cyber-ny.com
			if(APPLICATION_ENV != 'production') {
				$mail->addTo("support@cyber-ny.com", "cyber support");
				//$mail->addTo("craig@cyber-ny.com");
			}else {
				$mail->addTo("all-office@cyber-ny.com");
			}
			$mail->setSubject("Cyber-NY :: Production Updates - ".date('l n/d/y'));
			$mail->send();

			$this->_redirect("/pm/sent");

			//echo $mail_message;exit;
		}
	}

	public function sentAction()
	{}

	public function clockAction()
	{
		$id = $this->_getParam("id",0);
		$return = $this->_getParam("return","index");
		$time = time();

		$sql = $this->_db->quoteInto("SELECT RetainerStatus FROM Projects WHERE id = ?",$id);
		$project = $this->_db->fetchRow($sql);

		$sql = $this->_db->quoteInto("SELECT id FROM ProjectActivity WHERE EndTime IS NULL AND ProjectID = ?",$id);
		if (!$this->_db->fetchOne($sql)) {
			//not clocked in already
			$sql = "INSERT ProjectActivity (ProjectID, UserID, StartTime) VALUES ('$id', '{$this->_user->Cyberny_Users_ID}', '$time')";
			$this->_db->query($sql);

			//force the project to be Open if you clock in
			$today = strtotime("today");
			$sql = "UPDATE Projects SET ProjectStatusID = 1, ExpectedEndDate = IF (ExpectedEndDate < $today, $today, ExpectedEndDate) WHERE id=$id";
			$this->_db->query($sql);

			if ($project['ticket_id'] > 0) {
				$sql = "UPDATE tickets SET status = 'In Progress' WHERE id = {$project['ticket_id']}";
				$this->_db->query($sql);
				//add note?
			}
		}else {
			//already clocked in
			//if retainer implement 15 minute minimum
			if ($project['RetainerStatus'] == 'y') {
				$sql = "SELECT * FROM ProjectActivity WHERE EndTime IS NULL AND ProjectID=$id AND UserID={$this->_user->Cyberny_Users_ID}";
				$old_data = $this->_db->fetchRow($sql);

				if( $old_data ){
					if ( $old_data['StartTime'] > (time()-(15*60)) ) $time = $old_data['StartTime'] + (15*60);
				}
			}

			$sql = "UPDATE ProjectActivity SET EndTime='$time' WHERE EndTime IS NULL AND ProjectID=$id AND UserID={$this->_user->Cyberny_Users_ID}";
			$this->_db->query($sql);
		}

		$this->_redirect("/pm/$return");
	}

	public function statusAction()
	{
		$status = $this->_getParam("status","");
		$ids = $this->_getParam("id",0);
		$return = $this->_getParam("return","index");

		if (!is_array($ids)) $ids = array($ids);

		foreach ($ids as $k=>$id) {
			$sql = "SELECT p.*, c.ClientName FROM Projects AS p, Clients AS c WHERE p.ClientID = c.id AND p.id = $id";
			$entry = $this->_db->fetchRow($sql);

			if ($status == "open") {
				$data = array("ProjectStatusID"=>1,"ActualEndDate"=>new Zend_Db_Expr("NULL"),"LastActivityDate"=>time());
				if ($entry['ExpectedEndDate'] < strtotime("today"))
					$data['ExpectedEndDate'] = strtotime("today");
			}elseif ($status == "client") {
				$data = array("ProjectStatusID"=>3,"LastActivityDate"=>time());
				//$tdata = array("status"=>"Attention Required");
				$tdata = array("status"=>"Comments Submitted");
			}elseif ($status == "close") {
				$data = array("ProjectStatusID"=>4,"ActualEndDate"=>time(),"LastActivityDate"=>time());

				if ($entry['ticket_id']) {
					$sql = "SELECT SUM(ProjectStatusID) AS stat, COUNT(*) AS cnt FROM Projects WHERE ticket_id = {$entry['ticket_id']} AND id <> $id";
					$result = $this->_db->fetchRow($sql);

					if ($result['stat']/$result['cnt'] == 4 || !$result)
						$tdata = array("status"=>"Completed");
				}

			}
			if ($id)
				$this->_db->update("Projects",$data,"id=$id");

			if ($entry['ticket_id'] > 0 && $tdata) {
				$this->_db->update("tickets",$tdata,"id={$entry['ticket_id']}");

				//send email to client if put on client.
				$ticket_note = new Cny_Model_Ticket();

				if( $status == "client" && ($this->_getParam('no_email',0)==0) ){
					$ndata['ticket_id'] = $entry['ticket_id'];
					$ndata['message'] = htmlentities($this->_getParam("explenation",""));
					$ndata['created_user_id'] = $this->_user->ID;
					$ndata['type'] = 'User';
					$ndata['created'] = new Zend_Db_Expr("NOW()");
					//$ndata['cc_list'] = str_replace(";",",",$this->_getParam("cc_list",""));

					$this->_db->insert("ticket_notes",$ndata);
					$note_id = $this->_db->lastInsertId();

					$ticket_note->emailNote($note_id);
				}
			}

			//add note and email to assigner
			if (($status == "close" || $status == "client") && $this->_getParam("explenation","")) {
				$data = array("ProjectID"=>$id,"UserID"=>$this->_user->Cyberny_Users_ID,"EntryDate"=>time(),"LastUpdate"=>time(),"Notes"=>$this->_getParam("explenation",""));
				$this->_db->insert("ProjectNotes",$data);

				//send email to assigner if different from user
				if ($entry['assigner'] != $this->_user->Cyberny_Users_ID) {
					$sql = "SELECT * FROM Users WHERE id = {$entry['assigner']}";
					$assigner = $this->_db->fetchRow($sql);

					$h_status = ($status=="close")?"Closed":"On Client";

					//send email
					define( 'newline', "<br/>" );
					$mail_message = "<strong>".$this->_user->FirstName." ".$this->_user->LastName." has updated task: ".$entry['Title']."</strong>".newline.newline;
					$mail_message .= 'Status changed to: '.$h_status.newline;
					$mail_message .= 'Client: '.$entry['ClientName'].newline;
					$mail_message .= 'Comment: '.$this->_getParam("explenation","").newline.newline;

					$mail = new Zend_Mail();
					$mail->setBodyHtml($mail_message);

					//if not production only email support@cyber-ny.com
					if(APPLICATION_ENV != 'production') {
						$mail->setFrom('do-not-reply@cnydevzone3.com', $this->_user->FirstName." ".$this->_user->LastName);
						$mail->addTo("support@cyber-ny.com", "cyber support");
						//$mail->addTo("craig@cyber-ny.com");
					}else {
						$mail->setFrom('do-not-reply@cyber-ny.com', $this->_user->FirstName." ".$this->_user->LastName);
						$mail->addTo("{$assigner['Email']}");
					}
					$mail->setSubject("Status change for task: {$entry['Title']}");
					$mail->send();
				}
			}
		}

		$this->_redirect("/pm/$return");
	}

	public function requestsAction()
	{
		$this->view->placeholder('sub_section')->set("requestview");

		//requests for user to approve/deny
		$sql = "SELECT tcr.*, CONCAT(u.FirstName,' ',u.LastName) AS owner, p.Title FROM task_change_requests AS tcr LEFT JOIN Users AS u ON tcr.user_id = u.id LEFT JOIN Projects AS p ON p.id = tcr.task_id WHERE tcr.assigner_id = {$this->_user->Cyberny_Users_ID} AND tcr.status='pending' ORDER BY tcr.created DESC";
		$this->view->approvals = $this->_db->fetchAssoc($sql);

		//list of outstanding requests
		$sql = "SELECT tcr.*, p.Title FROM task_change_requests AS tcr LEFT JOIN Projects AS p ON p.id = tcr.task_id WHERE tcr.user_id = {$this->_user->Cyberny_Users_ID} AND tcr.status='pending' ORDER BY tcr.created DESC";
		$this->view->requests = $this->_db->fetchAssoc($sql);
	}

	public function requestviewAction()
	{
		$id = $this->_getParam('id',0);
		$request_id = $this->_getParam('request_id',0);

		$select = $this->_db->select();
		$select->from(array("p"=>"Projects"),array("*",
					"TotalTime"=>new Zend_Db_Expr("ABS( SUM(EndTime - StartTime) )")));
		$select->joinLeft(array("c"=>"Clients"), "p.ClientID = c.ID", "ClientName");
		$select->joinLeft(array("u"=>"Users"), "p.UserID = u.id", array("UserName","Email"));
		$select->joinLeft(array("au"=>"Users"), "p.assigner = au.id", array("assigner"=>"UserName"));
		$select->joinLeft(array("cp"=>"client_projects"), "p.client_project_id = cp.id", array("ClientProject"=>"name"));
		$select->joinLeft(array("pri"=>"Priority"), "p.ProjectPriorityID = pri.id", "PriorityName");
		$select->joinLeft(array("ps"=>"ProjectStatus"), "p.ProjectStatusID = ps.id", array("ProjectStatusName",
				"StatusRank"=>new Zend_Db_Expr("(CASE ps.ProjectStatusName WHEN 'Open' THEN 1
						WHEN 'On Client' THEN 2 WHEN 'Completed' THEN 3 WHEN 'Closed' THEN 4 END)")));
		$select->joinLeft(array("pa"=>"ProjectActivity"), "p.id = pa.ProjectID", array("ClockStatus"=>"( IF( COUNT(pa.StartTime) > COUNT(pa.EndTime), 0, 1 ) )"));
		$select->joinLeft(array("t"=>"tickets"), "p.ticket_id = t.id", "");
		$select->joinLeft(array("ub"=>"Users_BugTracker"), "t.created_user_id = ub.ID",array("created_by"=>"CONCAT(ub.FirstName, ' ', ub.LastName)"));
		$select->where("p.id=$id");
		$this->view->task = $this->_db->fetchRow($select->__toString());

		if ($this->view->task['ticket_id']) {
			$sql = $this->_db->quoteInto("SELECT tn.*, CONCAT(u.FirstName, ' ', u.LastName) AS name, u.Roles FROM ticket_notes AS tn LEFT JOIN Users_BugTracker AS u ON tn.created_user_id = u.ID
				WHERE tn.ticket_id = ? ORDER BY tn.created DESC",$this->view->task['ticket_id']);
			$this->view->notes = $this->_db->fetchAssoc($sql);
		}

		$sql = $this->_db->quoteInto("SELECT pa.id, u.UserName, pa.StartTime, pa.EndTime FROM ProjectActivity pa LEFT JOIN Users u ON pa.UserID=u.id WHERE ProjectID=? ORDER BY pa.StartTime",$id);
		$this->view->activity = $this->_db->fetchAssoc($sql);

		$sql = "SELECT * FROM task_change_requests WHERE id = $request_id";
		$this->view->request = $this->_db->fetchRow($sql);

		$this->view->statuses = array("pending"=>"Pending","approved"=>"Approved","denied"=>"Denied");

		if( $this->getRequest()->isPost() ){
			$data = $this->_getParam('data',array());
			$data['modified'] = new Zend_Db_Expr("NOW()");

			$this->_db->update("task_change_requests",$data,"id=$request_id");

			if ($data['status'] != 'pending') {
				define( 'newline', "<br/>" );
				$mail_message = 'Request to move task: '.$task['Title'].' has been '.$data['status'].'.'.newline;
				$mail_message .= 'Due Date: '.date("l F d Y",$data['ExpectedEndDate']).newline.newline;
				if ($data['explenation'])
					$mail_message .= 'Explenation: '.$data['explenation'].newline.newline;

				$mail = new Zend_Mail();
				$mail->setBodyHtml($mail_message);
				//$mail->setFrom('do-not-reply@cyber-ny.com',  $ticket['ClientName']);
				$mail->setFrom('do-not-reply@cnydevzone3.com', $ticket['ClientName']);

				//if not production only email support@cyber-ny.com
				if(APPLICATION_ENV != 'production') {
					$mail->addTo("support@cyber-ny.com", "cyber support");
					//$mail->addTo("craig@cyber-ny.com");
				}else {
					$mail->addTo("{$this->view->task['Email']}");
				}
				$mail->setSubject("Request {$data['status']} for task: {$task['Title']}");
				$mail->send();

				if ($data['status'] == "approved") {
					$this->_db->update("Projects",array("ExpectedEndDate"=>$this->view->request['new_date']),"id=$id");
				}
			}

			$this->_redirect("/pm/requests");
		}
	}

	public function addtasknoteAction()
	{
		$this->_helper->layout()->setLayout("modal");
		$this->view->id = $id = $this->_getParam('id',0);

		if( $this->getRequest()->isPost() ){
			$data['task_id'] = $id;
			$data['message'] = ($this->_getParam("Description",""));
			$data['created_user_id'] = $this->_user->ID;
			$data['type'] = 'User';
			$data['created'] = new Zend_Db_Expr("NOW()");
			$data['cc_list'] = str_replace(";",",",$this->_getParam("cc_list",""));

			$this->_db->insert("task_notes",$data);
			$note_id = $this->_db->lastInsertId();

			//upload attachment
			if ($_FILES['attachment']['name']) {
				$uploadpath = realpath('.').$this->_attachment_path;

				move_uploaded_file($_FILES['attachment']['tmp_name'], $uploadpath.'/task_'.$note_id."_".$_FILES['attachment']['name']);

				$this->_db->query("UPDATE task_notes SET attachment = 'task_".$note_id."_".$_FILES['attachment']['name']."' WHERE id = $note_id");
			}

			$select = $this->_db->select();
			$select->from(array("p"=>"Projects"),array("*"));
			$select->joinLeft(array("c"=>"Clients"), "p.ClientID = c.ID", "ClientName");
			$select->joinLeft(array("u"=>"Users"), "p.UserID = u.id", "UserName");
			$select->joinLeft(array("au"=>"Users"), "p.assigner = au.id", array("assigner"=>"UserName","assigner_id"=>"id"));
			$select->joinLeft(array("cp"=>"client_projects"), "p.client_project_id = cp.id", array("ClientProject"=>"name"));
			$select->where("p.id=$id");
			$task = $this->_db->fetchRow($select->__toString());

			if ($task['UserID'] != $this->_user->Cyberny_Users_ID) {
				$sql = "SELECT * FROM Users WHERE ID = '{$task['UserID']}'";
				$emails = $this->_db->fetchAssoc($sql);
			}else {
				$sql = "SELECT * FROM Users WHERE ID = '{$task['assigner_id']}'";
				$emails = $this->_db->fetchAssoc($sql);
			}

			$sql = "SELECT * FROM task_notes WHERE id = '$note_id'";
			$note = $this->_db->fetchRow($sql);

			define( 'newline', "<br/>" );
			$mail_message  = date('n/j/Y g:i A').newline;
			$mail_message .= 'Client: '.$task['ClientName'].newline;
			$mail_message .= 'Task: '.$task['Title'].newline;
			$mail_message .= 'Sender: '.$this->_user->FirstName.' '.$this->_user->LastName.newline;
			$mail_message .= newline;
			$mail_message .= 'Hello, '.$task['UserName'].newline;
			$mail_message .= newline;
			$mail_message .= 'The following note has been added to your task entry: '.newline;
			$mail_message .= newline;
			$mail_message .= '------------------'.newline;
			$mail_message .= nl2br($data['message']).newline;
			$mail_message .= '------------------'.newline;
			$mail_message .= newline;
			$mail_message .= 'You may view this note from: http://portal.cyber-ny.com/pm/view/id/'.$id.newline;
			$mail_message .= newline;
			$mail_message .= 'Best Regards,'.newline;
			$mail_message .= 'Cyber-NY Solutions Manager'.newline;
			$mail_message .= 'portal.cyber-ny.com'.newline;
			$mail_message .= '212-475-2721'.newline;

			$mail = new Zend_Mail();
			$mail->setBodyHtml($mail_message);
			$mail->setFrom('do-not-reply@cyber-ny.com', $this->_user->Email);
			//$mail->setFrom('contact@cnydevzone3.com', $from);

			//add attachment if one exists for this note
			$attachment_path = $_SERVER{'DOCUMENT_ROOT'}."/attachments/";
			if ($note['attachment'] && file_exists($attachment_path.$note['attachment'])) {
				$fileContents = file_get_contents($attachment_path.$note['attachment']);
				$file = $mail->createAttachment($fileContents);
				$file->filename = $_FILES['attachment']['name'];
			}

			//if not production only email support@cyber-ny.com
			if(APPLICATION_ENV != 'production') {
				$emails = array(array("Email"=>"support@cyber-ny.com","FirstName"=>"cyber support","LastName"=>""));
			}
			foreach ($emails as $user) {
				$mail->addTo($user['Email'], $user['FirstName']." ".$user['LastName']);
			}

			//add CC if any in list
			if ($note['cc_list']) {
				$cc_list = explode(",",$note['cc_list']);

				foreach ($cc_list as $cc) {
					$mail->addCc($cc);
				}
			}

			$mail->setSubject("Cyber-NY:: ".$note['ClientName']." - Note added to Task #".$id);
			$mail->send();

		}

	}

	public function updatetasknotesAction()
	{
		$id = $this->_getParam("task",0);
		$last_note = $this->_getParam("last_note",0);

		$sql = $this->_db->quoteInto("SELECT tn.*, CONCAT(u.FirstName, ' ', u.LastName) AS name, u.Roles FROM task_notes AS tn LEFT JOIN Users_BugTracker AS u ON tn.created_user_id = u.ID
				WHERE tn.task_id = ? AND tn.id > '$last_note' ORDER BY tn.created ASC",$id);
		$notes = $this->_db->fetchAssoc($sql);

		$items = array();
		foreach ($notes as $note) {
			$role_class = ($note['Roles'] == "administrator") ? "2" : "";

			$attachment = ($note['attachment'] && file_exists($_SERVER['DOCUMENT_ROOT'].$this->_attachment_path.$note['attachment']))?$this->_attachment_path.$note['attachment']:"";

			$items[] = array("id"=>$note['id'], "name"=>$note['name'], "created"=>date("n/j/y",strtotime($note['created'])), "time"=>date("g:ia",strtotime($note['created'])), "message"=>nl2br($note['message']), "attachment"=>$attachment, "role"=>$role_class);
		}

		$array['items'] = $items;

		echo Zend_Json_Encoder::encode($array);exit;
	}

	public function deletetasknoteAction()
	{
		$this->_helper->layout()->disableLayout();

		$id = $this->_getParam("note_id",0);

		$this->_db->delete("task_notes","id=$id");

		exit;
	}

	public function quickAction()
	{
		$this->_helper->layout()->setLayout("modal");

		$this->view->titles = array('Support Call'=>'Support Call', 'Client Status Call'=>'Client Status Call', 'Project Planning & Status Call'=>'Project Planning & Status Call', 'Emergency Support Request'=>'Emergency Support Request','Developer Assistance Request'=>'Developer Assistance Request');
		$this->view->times = array('0'=>'Now','-5'=>'5 minutes ago','-10'=>'10 minutes ago','-15'=>'15 minutes ago','15'=>'Log 15 minutes','20'=>'Log 20 minutes','30'=>'Log 30 minutes','45'=>'Log 45 minutes','60'=>'Log 1 hour');

		$sql = "SELECT id, ClientName FROM Clients WHERE Disabled = 'no' ORDER BY ClientName ASC";
		$this->view->clients = array(""=>"Select Client")+$this->_db->fetchPairs($sql);

		if( $this->getRequest()->isPost() ){
			$data = $this->_getParam('data',array());
			$time = (int)$this->_getParam("time",0);
			$time_offset = abs($time)*60;
			$start_time = time() - $time_offset;

			if ($this->_getParam("title","")) {
				$data['Title'] = $this->_getParam("title","");
			}

			$data['ProjectPriorityID'] = "1";
			$data['ProjectStatusID'] = ($time > 0)?4:1;
			$data['EntryDate'] = time();
			$data['StartDate'] = strtotime("today");
			$data['ExpectedEndDate'] = strtotime("today");
			$data['assigner'] = $this->_user->Cyberny_Users_ID;
			$data['UserID'] = $this->_user->Cyberny_Users_ID;

			$this->_db->insert("Projects",$data);
			$id = $this->_db->lastInsertId();

			//close all open tasks
			$open = $this->_db->fetchAssoc("SELECT * FROM ProjectActivity WHERE UserID={$this->_user->Cyberny_Users_ID} AND EndTime IS NULL");
			foreach ($open as $task) {
				$end_time = ($task['StartTime']<$start_time)?$start_time:time();
				$this->_db->update("ProjectActivity",array("EndTime"=>$end_time),"id = {$task['id']}");
			}

			//add activity for the new quick task
			$activity = array("StartTime"=>$start_time,"UserID"=>$this->_user->Cyberny_Users_ID,"ProjectID"=>$id);
			if ($time > 0) {
				$activity['EndTime'] = time();
			}

			$this->_db->insert("ProjectActivity",$activity);

			$this->_redirect("/pm/");
		}
	}

	function init()
	{
		$bootstrap = $this->getInvokeArg('bootstrap'); // gets the boostrapper
		$resource = $bootstrap->getPluginResource('multidb'); //multi db support
		$this->_db = $resource->getDefaultDb();

		$options = $bootstrap->getOptions();
		$this->view->attachment_path = $this->_attachment_path = $options['attachment']['path'];
		$this->_domain = $options['site']['domain'];

		$this->view->placeholder('section')->set("pm");
		$this->view->layout()->setLayout("cyber");

		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$this->_message = new Zend_Session_Namespace('message');

		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('portal'));
		if(!$auth->hasIdentity()){
			$auth->clearIdentity();
			$this->_redirect('/auth');
		}else{
			$this->view->user = $this->_user = $auth->getIdentity();
			$this->view->placeholder('logged_in')->set(true);

			if ($this->_user->cyber_user != "yes") {
				$auth->clearIdentity();
				$this->_redirect('/auth');
			}
		}

		$subSectionMenu = '<li id="subnav-myprojects"><a href="/pm"><span class="subnav-size">My Tasks</span></a></li>
							<li id="subnav-allprojects"><a href="/pm/all"><span class="subnav-size">All Tasks</span></a></li>
							<li id="subnav-quicktask"><a href="/pm/quick" class="dialog" title="Quick Task"><span class="subnav-size">Quick Task</span></a></li>
							<li id="subnav-addproject"><a href="/pm/add"><span class="subnav-size">Add Task</span></a></li>
							<li id="subnav-pmarchive"><a href="/pm/archive"><span class="subnav-size">Archive</span></a></li>
							<li id="subnav-productionschedule"><a href="/pm/productionschedule"><span class="subnav-size">Weekly Planner</span></a></li>
							<li id="subnav-dailyschedule"><a href="/pm/dailyschedule"><span class="subnav-size">Daily Schedule</span></a></li>
							<li id="subnav-viewprojects"><a href="/projects"><span class="subnav-size">Projects</span></a></li>
							<!--<li id="subnav-changerequests"><a href="/pm/requests"><span class="subnav-size">Task Change Requests</span></a></li>-->
							';

		$this->view->placeholder("subSectionMenu")->set($subSectionMenu);
	}
}

function sec2hm ($sec, $padHours = false) {
	$hms = "";
	$hours = intval(intval($sec) / 3600);

	$hms .= ($padHours) ? str_pad($hours, 2, "0", STR_PAD_LEFT). 'h ' : $hours. 'h ';

	$minutes = intval(($sec / 60) % 60);

	$hms .= str_pad($minutes, 2, "0", STR_PAD_LEFT)."m";

	return $hms;
}

function sec2dec ($sec) {
	$hms = "";
	$hours = intval(intval($sec) / 3600);
	$minutes = intval(($sec / 60) % 60);

	$dec = round($hours + ($minutes/60) ,2);

	return $dec;
}
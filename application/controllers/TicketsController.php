<?php
class TicketsController extends Cny_Controller_LayoutAction
{
	public function indexAction()
	{
		if ($this->_user->cyber_user != "yes") {
			$this->_redirect("/tickets/list");
		}

		$this->view->placeholder('sub_section')->set("tickets");

		$search = new Zend_Session_Namespace('ticket_client_search');
    	$mask = $relationship = "";

		$page = $this->getRequest()->getParam('page',1);
		$this->view->dir = $dir = $this->_getParam('dir','ASC');
		$this->view->sort = $sort = $this->_getParam('sort','ClientName');

		$lastday = date('t');
		$from_time = strtotime(date("Y-m-01"));
		$to_time = strtotime(date("Y-m-$lastday")." 23:59:59");

		$select = $this->_db->select();
		$select->from(array("t"=>"tickets"), array('status'));
		$select->joinLeft(array("c"=>"Clients"), "c.id = t.clientID", array('id','ClientName',
						'totalTickets'=>new Zend_Db_Expr("COUNT(c.ClientName)"),
						'opened'=>new Zend_Db_Expr("(SELECT COUNT(*) FROM tickets AS t WHERE t.status <> 'Approved & Closed' AND c.id = t.clientID)"),
						'closed'=>new Zend_Db_Expr("(SELECT COUNT(*) FROM tickets AS t WHERE t.status = 'Approved & Closed' AND TO_DAYS(NOW()) - TO_DAYS(modified)<=7 AND c.id = t.clientID)"),
						'unassigned'=>new Zend_Db_Expr("(SELECT COUNT(*) FROM tickets AS t WHERE t.status <> 'Approved & Closed' AND (assigned_user_id = 0 || assigned_user_id IS NULL ) AND c.id = t.clientID)")
						));
		$select->joinLeft(array("r"=>"Retainers"),"c.id = r.ClientID", array("Hours",
						"currentHours"=>new Zend_Db_Expr("(SELECT ABS(SUM(pa.EndTime - pa.StartTime)) FROM Projects p LEFT JOIN ProjectActivity pa ON p.id = pa.ProjectID WHERE p.RetainerStatus = 'y' AND pa.StartTime >= {$from_time} AND pa.EndTime <= {$to_time} AND p.ClientID = r.ClientID GROUP BY p.ClientID)"),
						"percent"=>new Zend_Db_Expr("( ROUND( ( (SELECT ABS(SUM(pa.EndTime - pa.StartTime)) FROM Projects p LEFT JOIN ProjectActivity pa ON p.id = pa.ProjectID WHERE p.RetainerStatus = 'y' AND pa.StartTime >= {$from_time} AND pa.EndTime <= {$to_time} AND p.ClientID = r.ClientID GROUP BY p.ClientID) / (r.Hours*3600) ) * 100 ) )"))
						);
		$select->where("(t.status <>'Approved & Closed') OR (t.status ='Approved & Closed' AND TO_DAYS(NOW()) - TO_DAYS(modified)<=7)");
		$select->group("c.ClientName");
		$select ->order(array("$sort $dir"));

		$paginator = Zend_Paginator::factory($select);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(50);
		$this->view->clients = $paginator;
	}

    public function listAction()
	{
		$this->view->placeholder('sub_section')->set("tickets");

		$this->view->clientID = $this->_clientID;
		$search = new Zend_Session_Namespace('ticket_search');
    	$mask = $relationship = "";
    	$mask = $this->view->mask = $this->_getParam("mask","");

		$page = $this->getRequest()->getParam('page',1);
		$this->view->dir = $dir = $this->_getParam('dir','ASC');
		$this->view->sort = $sort = $this->_getParam('sort','t.status');

		$select = $this->_db->select();
		$select->from(array("t"=>"tickets"), array('id', 'summary', 'priority', 'status', 'sort', 'ClientID',
					'modified'=>new Zend_Db_Expr("DATE_FORMAT(t.modified, '%m/%d/%Y')"), 'created'=>new Zend_Db_Expr("DATE_FORMAT(t.created, '%m/%d/%Y')"),
					'duedate'=>new Zend_Db_Expr("IF (t.duedate = '1969-12-31 00:00:00','Pending',DATE_FORMAT(t.duedate, '%m/%d/%Y'))")));
		$select->joinLeft(array("u"=>"Users_BugTracker"), "t.created_user_id = u.ID", array("UserName"=>new Zend_Db_Expr("CONCAT(u.FirstName, ' ', SUBSTRING(u.LastName,1,1),'.')")));
		$select->where("(t.clientID =".$this->_clientID." AND t.status <>'Approved & Closed') OR
			 			(created_user_id =".$this->_userID." AND t.status ='Approved & Closed' AND TO_DAYS(NOW()) - TO_DAYS(modified)<=7)");
		$select ->order(array("$sort $dir"));

		if ($mask) {
			$select->where("t.summary LIKE '%$mask%' OR t.id = '$mask' ");
		}

		$paginator = Zend_Paginator::factory($select);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(50);
		$this->view->tickets = $paginator;

		$this->view->clientName = $this->_db->fetchOne($this->_db->quoteInto("SELECT ClientName FROM Clients WHERE id = ?",$this->_clientID));

		$this->view->statuses = array('Ticket Opened'=>"Ticket Opened", 'In Progress'=>"In Progress", 'Comments Submitted'=>'Comments Submitted', 'Approved & Closed'=>'Approved & Closed');

		/* lightbox message */
		if ($this->_message->clientadd == "yes") {
			$this->_message->setExpirationSeconds(1,"clientadd");

			$this->view->clientadd = "display";
		}
	}

	public function archiveAction()
	{
		$this->view->placeholder('sub_section')->set("archive");

		$search = new Zend_Session_Namespace('archive_search');
    	$mask = $relationship = "";

		$page = $this->getRequest()->getParam('page',1);
		$this->view->dir = $dir = $this->_getParam('dir','DESC');
		$this->view->sort = $sort = $this->_getParam('sort','t.created');

		$mask = $date_to = $date_from = "";

		if ($search->mask != '') $this->view->mask = $mask = $search->mask;
		if ($search->date_from != '') $this->view->date_from = $date_from = $search->date_from;
		if ($search->date_to != '') $this->view->date_to = $date_to = $search->date_to;

		if( $this->getRequest()->isPost() ){
			$this->view->mask = $mask = $this->_getParam('mask','');
			$this->view->date_from = $date_from = $this->_getParam('date_from','');
			$this->view->date_to = $date_to = $this->_getParam('date_to','');

			if ($mask == '' && $date_from == '' && $date_to == '') {
				$search->setExpirationSeconds(1);
			}
		}

		$select = $this->_db->select();
		$select->from(array("t"=>"tickets"), array('id', 'summary', 'priority', 'status', 'sort', 'ClientID',
					'modified'=>new Zend_Db_Expr("DATE_FORMAT(t.modified, '%m/%d/%Y')"), 'created'=>new Zend_Db_Expr("DATE_FORMAT(t.created, '%m/%d/%Y')"),
					'duedate'=>new Zend_Db_Expr("DATE_FORMAT(t.duedate, '%m/%d/%Y')")));
		$select->joinLeft(array("u"=>"Users_BugTracker"), "t.created_user_id = u.ID", array("UserName"=>new Zend_Db_Expr("CONCAT(u.FirstName, ' ', SUBSTRING(u.LastName,1,1),'.')")));
		if ($this->_user->cyber_user == "yes") {
			$select->where("t.status = 'Approved & Closed'");
		}else {
			$select->where("(t.clientID =".$this->_clientID." AND t.status = 'Approved & Closed') OR
			 			(created_user_id =".$this->_userID." AND t.status = 'Approved & Closed')");
		}
		$select ->order(array("$sort $dir"));

		if ($mask) {
			$search->mask = $mask;
			$select->where("t.summary LIKE '%$mask%' OR t.id = '$mask' OR u.FirstName LIKE '%$mask%'");
		}else {
			$search->mask = '';
		}
		if ($date_from) {
			if (!$date_to) $date_to = $date_from;

			$search->date_from = $date_from;
			$search->date_to = $date_to;

			$select->where("(t.created >= '".date("Y-m-d",strtotime($date_from))."' AND t.created <= '".date("Y-m-d",strtotime($date_to))."') OR (t.duedate >= '".date("Y-m-d",strtotime($date_from))."' AND t.duedate <= '".date("Y-m-d",strtotime($date_to))."')");
		}else {
			$search->date_from = '';
			$search->date_to = '';
		}

		$paginator = Zend_Paginator::factory($select);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(50);
		$this->view->tickets = $paginator;
	}

	public function viewAction()
	{
		$id = $this->_getParam('id',0);

		$sql = $this->_db->quoteInto("SELECT t.*, CONCAT(u.FirstName, ' ', u.LastName) AS assigned, c.ClientName FROM tickets AS t LEFT JOIN Users AS u ON t.assigned_user_id = u.ID LEFT JOIN Clients AS c ON t.clientID = c.id
				WHERE t.id = ? AND (t.created_user_id = {$this->_userID} OR t.clientID = {$this->_clientID})",$id);
		$this->view->ticket = $this->_db->fetchRow($sql);

		if ($this->view->ticket) {
			$sql = $this->_db->quoteInto("SELECT tn.*, CONCAT(u.FirstName, ' ', u.LastName) AS name, u.Roles FROM ticket_notes AS tn LEFT JOIN Users_BugTracker AS u ON tn.created_user_id = u.ID
				WHERE tn.ticket_id = ? ORDER BY tn.created DESC",$id);
			$this->view->notes = $this->_db->fetchAssoc($sql);

			$sql = $this->_db->quoteInto("SELECT p.id, u.FirstName, u.LastName, p.Title, ps.ProjectStatusName, p.time_estimate FROM Projects AS p, Users AS u, ProjectStatus AS ps WHERE p.UserID = u.ID AND p.ProjectStatusID = ps.id AND p.ticket_id = ?",$id);
			$this->view->tasks = $this->_db->fetchAssoc($sql);
		}

		$select = $this->_db->select();
		$select->from(array("t"=>"tickets"),
			array("id", "currentHours"=>new Zend_Db_Expr("(SELECT ABS(SUM(pa.EndTime - pa.StartTime)) FROM Projects p LEFT JOIN ProjectActivity pa ON p.id = pa.ProjectID WHERE p.ticket_id = t.id GROUP BY p.ticket_id)"))
		);
		$select->where("t.id = {$id}");

		$this->view->activity = $this->_db->fetchRow($select->__toString());
	}


	public function addAction()
	{
		$this->_helper->layout()->setLayout("modal");
		$ticket = new Cny_Model_Ticket();

		$this->view->priorities = $this->_db->fetchPairs("SELECT Priority, Priority FROM BugTicketPriority ORDER BY sort ASC");

		$sql = "SELECT id, ClientName FROM Clients WHERE Disabled = 'no' ORDER BY ClientName ASC";
		$this->view->clients = array(""=>"Select Client")+$this->_db->fetchPairs($sql);

		if( $this->getRequest()->isPost() ){
			$data = $this->_getParam('data',array());
			$data['status'] = "Ticket Opened";
			$data['created_user_id'] = $this->_user->ID;
			if (!$this->_clientID) $this->_clientID = $data['clientID'];
			$data['clientID'] = $this->_clientID;
			$data['created'] = new Zend_Db_Expr("NOW()");
			$data['modified'] = new Zend_Db_Expr("NOW()");

			$this->_db->insert("tickets",$data);
			$ticket_id = $this->_db->lastInsertId();

			$ticket->emailTicket($ticket_id);

			if ($this->_user->cyber_user != "yes")
				$this->_message->clientadd = "yes";

			$this->_redirect("/tickets/sent");
		}
	}

	public function approveAction()
	{
		$id = $this->_getParam("id",0);

		$sql = "UPDATE tickets SET status = 'Approved & Closed', modified = NOW() WHERE id = $id";
		$this->_db->query($sql);

		//close PM tasks for this ticket
		$sql = "UPDATE Projects SET ProjectStatusID = '4', ActualEndDate = ".time()." WHERE ticket_id = '$id'";
		$this->_db->query($sql);

		$data['ticket_id'] = $id;
		$data['message'] = "Ticket# $id has been Approved & Closed.";
		$data['created_user_id'] = $this->_user->ID;
		$data['type'] = 'System';
		$data['created'] = new Zend_Db_Expr("NOW()");

		$this->_db->insert("ticket_notes",$data);
		$note_id = $this->_db->lastInsertId();

		$this->_redirect("/tickets");
	}

	public function deleteAction()
	{
		$id = $this->_getParam("id",0);

		$this->_db->delete("tickets", "id = $id");

		$this->_redirect("/tickets");
	}

	public function editAction()
	{
		$this->_helper->layout()->setLayout("modal");
		$id = $this->_getParam("id",0);

		$this->view->priorities = $this->_db->fetchPairs("SELECT Priority, Priority FROM BugTicketPriority ORDER BY sort ASC");
		$this->view->statuses = array('Ticket Opened'=>"Ticket Opened", 'In Progress'=>"In Progress", 'Comments Submitted'=>'Comments Submitted', 'Approved & Closed'=>'Approved & Closed');

		$sql = $this->_db->quoteInto("SELECT t.*, u.FirstName AS cyber FROM tickets AS t LEFT JOIN Users AS u ON t.assigned_user_id = u.ID WHERE t.id = ? AND t.clientID = {$this->_clientID}",$id);
		$this->view->ticket = $ticket = $this->_db->fetchRow($sql);

		//if cyber-ny user AND they changed the client on the ticket, the ticket can't be found. Lookup just on id
		if (!$ticket && $this->_user->cyber_user == "yes") {
			$sql = $this->_db->quoteInto("SELECT t.*, u.FirstName AS cyber FROM tickets AS t LEFT JOIN Users AS u ON t.assigned_user_id = u.ID WHERE t.id = ?",$id);
			$this->view->ticket = $ticket = $this->_db->fetchRow($sql);
		}

		$sql = "SELECT id, ClientName FROM Clients WHERE Disabled = 'no' ORDER BY ClientName ASC";
		$this->view->clients = array(""=>"Select Client")+$this->_db->fetchPairs($sql);

		$sql = "SELECT id, UserName FROM Users WHERE Disabled = 'no' ORDER BY UserName ASC";
		$this->view->staff = array(""=>"Select Staff")+$this->_db->fetchPairs($sql);

		$sql = "SELECT u.ID, CONCAT(u.FirstName, ' ', u.LastName) AS UserName FROM Users_BugTracker AS u LEFT JOIN users_client AS uc ON uc.user_id = u.ID WHERE u.Disabled = 'no' AND (uc.client_id = {$this->_clientID} OR u.ID = {$ticket['created_user_id']}) ORDER BY CONCAT(u.FirstName, ' ', u.LastName) ASC";
		$this->view->users = $this->_db->fetchPairs($sql);

		$retainerStatus = ($this->_db->fetchOne("SELECT id FROM Retainers WHERE ClientID = {$this->_clientID} AND Status = 'enabled'") > 0)?"y":"n";
		if (!$ticket['retainer']) {
			$this->view->ticket['retainer'] = $ticket['retainer'] = $retainerStatus;
		}

		if( $this->getRequest()->isPost() ){
			$data = $this->_getParam('data',array());
			$data['modified'] = new Zend_Db_Expr("NOW()");
			$data['clientID'] = $this->_clientID;

			if ($data['duedate']) $data['duedate'] = date("Y-m-d",strtotime($data['duedate']));
			else $data['duedate'] = date("Y-m-d",time()); //new Zend_Db_Expr("NULL");

			$this->_db->update("tickets",$data,"id=$id");

			//check if assigned user changed. If so add entry to PM.
			if ($data['assigned_user_id'] != $ticket['assigned_user_id'] && $data['assigned_user_id']) {
				//lets check to make sure the new assigned user doesn't already have this ticket in the task list
				$sql = "SELECT * FROM Projects WHERE ticket_id = {$ticket['id']} AND UserID = {$data['assigned_user_id']} AND ActualEndDate IS NULL";
				$exist = $this->_db->fetchRow($sql);

				if (!$exist) {
					$pm['Title'] = $ticket['id']." - ".$data['summary'];
					$pm['RetainerStatus'] = $data['retainer'];
					$pm['EntryDate'] = time();
					$pm['StartDate'] = time();
					$pm['ExpectedEndDate'] = strtotime($data['duedate']." 23:59:59");
					if ($data['priority'] == "high")
						$pm['ProjectPriorityID'] = "3";
					elseif ($data['priority'] == "medium")
						$pm['ProjectPriorityID'] = "2";
					else $pm['ProjectPriorityID'] = "1";
					$pm['ProjectStatusID'] = "1";
					$pm['UserID'] = $data['assigned_user_id'];
					$pm['ClientID'] = $this->_clientID;
					$pm['url'] = $data['url'];
					$pm['Description'] = $data['description'];
					$pm['timespan'] = "1";
					$pm['assigner'] = $this->_user->Cyberny_Users_ID;
					$pm['ticket_id'] = $ticket['id'];
					$pm['job_code'] = $ticket['job_code'];

					$this->_db->insert("Projects",$pm);
				}
			}

			$this->_redirect("/tickets/view/clientID/{$data['clientID']}/id/$id");
		}
	}

	public function statusAction()
	{
		$status = $this->_getParam("status","");
		$ids = $this->_getParam("id",0);

		if (!is_array($ids)) $ids = array($ids);

		foreach ($ids as $k=>$id) {
			$this->_db->update("tickets",array("status"=>$status),"id={$id}");
		}

		$this->_redirect("/tickets/list/clientID/{$this->_clientID}");
	}

	public function requestAction()
	{
		$this->_helper->layout()->disableLayout();
		$ticket = new Cny_Model_Ticket();
		$ticket_id = $this->_getParam("id",0);

		$data['ticket_id'] = $ticket_id;
		$data['message'] = "An <b>Update Request</b> was made for this ticket.";
		$data['created_user_id'] = $this->_user->ID;
		$data['type'] = 'System';
		$data['created'] = new Zend_Db_Expr("NOW()");

		$this->_db->insert("ticket_notes",$data);
		$note_id = $this->_db->lastInsertId();

		$ticket->emailUpdate($note_id);

		exit;
	}

	public function addnoteAction()
	{
		$this->_helper->layout()->setLayout("modal");
		$ticket = new Cny_Model_Ticket();
		$this->view->id = $id = $this->_getParam('id',0);

		if( $this->getRequest()->isPost() ){
			$data['ticket_id'] = $id;
			$data['message'] = ($this->_getParam("Description",""));
			$data['created_user_id'] = $this->_user->ID;
			$data['type'] = 'User';
			$data['created'] = new Zend_Db_Expr("NOW()");
			$data['cc_list'] = str_replace(";",",",$this->_getParam("cc_list",""));

			$this->_db->insert("ticket_notes",$data);
			$note_id = $this->_db->lastInsertId();

			//upload attachment
			if ($_FILES['attachment']['name']) {
				$uploadpath = realpath('.').$this->_attachment_path;

				move_uploaded_file($_FILES['attachment']['tmp_name'], $uploadpath.'/'.$note_id."_".$_FILES['attachment']['name']);

				$this->_db->query("UPDATE ticket_notes SET attachment = '".$note_id."_".$_FILES['attachment']['name']."' WHERE id = $note_id");
			}

			$ticket->emailNote($note_id);

			//check task related status and open if needed
			if ($this->_user->cyber_user != "yes") {
				$due_date = (date('G') < 17)?strtotime('today'):strtotime('tomorrow');
				if (date('N') > 5 || (date('N') == 5 && date('G') > 16) ) $due_date = strtotime('monday');

				$sql = "SELECT * FROM tickets WHERE id = '$id' LIMIT 1";
				$tic = $this->_db->fetchRow($sql);

				$sql = "SELECT * FROM Projects WHERE ticket_id = '$id' AND UserId = '{$tic['assigned_user_id']}' ORDER BY ExpectedEndDate DESC LIMIT 1";
				$tasks = $this->_db->fetchAssoc($sql);

				$users = array();
				foreach ($tasks as $task) {
					if (!in_array($task['UserID'],$users)) {
						$array = array();
						$array['ProjectStatusID'] = 1;
						if ($task['ExpectedEndDate'] < $due_date) $array['ExpectedEndDate'] = $due_date;

						$this->_db->update("Projects",$array,"id={$task['id']}");

						$users[] = $task['UserID'];
					}
				}
			}
		}
	}

	public function updatenotesAction()
	{
		$id = $this->_getParam("ticket",0);
		$last_note = $this->_getParam("last_note",0);

		$sql = $this->_db->quoteInto("SELECT tn.*, CONCAT(u.FirstName, ' ', u.LastName) AS name, u.Roles FROM ticket_notes AS tn LEFT JOIN Users_BugTracker AS u ON tn.created_user_id = u.ID
				WHERE tn.ticket_id = ? AND tn.id > '$last_note' ORDER BY tn.created ASC",$id);
		$notes = $this->_db->fetchAssoc($sql);

		$items = array();
		foreach ($notes as $note) {
			$role_class = ($note['Roles'] == "administrator") ? "2" : "";

			$attachment = ($note['attachment'] && file_exists($_SERVER['DOCUMENT_ROOT'].$this->_attachment_path.$note['attachment']))?$this->_attachment_path.$note['attachment']:"";

			$items[] = array("id"=>$note['id'], "name"=>$note['name'], "created"=>date("n/j/y",strtotime($note['created'])), "time"=>date("g:ia",strtotime($note['created'])), "message"=>nl2br($note['message']), "attachment"=>$attachment, "role"=>$role_class);
		}

		$array['items'] = $items;

		echo Zend_Json_Encoder::encode($array);exit;
	}

	public function deletenoteAction()
	{
		$this->_helper->layout()->disableLayout();

		$id = $this->_getParam("note_id",0);

		$this->_db->delete("ticket_notes","id=$id");

		exit;
	}

	public function reopenAction()
	{
		$this->_helper->layout()->setLayout("blank");
		$ticket = new Cny_Model_Ticket();
		$this->view->id = $id = $this->_getParam('id',0);

		if( $this->getRequest()->isPost() ){
			$data['ticket_id'] = $id;
			$data['message'] = "Ticket reopened by {$this->_user->FirstName} {$this->_user->LastName} at ".date("g:ia \o\\n n/d/Y");
			$data['created_user_id'] = $this->_user->ID;
			$data['type'] = 'User';
			$data['created'] = new Zend_Db_Expr("NOW()");

			$this->_db->insert("ticket_notes",$data);
			$note_id = $this->_db->lastInsertId();

			$ticket->emailNote($note_id);

			$sql = "UPDATE tickets SET status = 'Ticket Opened', modified = NOW() WHERE id = $id";
			$this->_db->query($sql);

			$this->_redirect("/tickets/view/clientID/{$this->_clientID}/id/$id");
		}
	}

	public function sentAction()
	{
		$this->_helper->layout()->setLayout("modal");
	}

	function init()
	{
		$bootstrap = $this->getInvokeArg('bootstrap'); // gets the boostrapper
		$resource = $bootstrap->getPluginResource('multidb'); //multi db support
		$this->_db = $resource->getDefaultDb();

		$options = $bootstrap->getOptions();
		$this->view->attachment_path = $this->_attachment_path = $options['attachment']['path'];

		$this->view->placeholder('section')->set("tickets");

		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$this->_message = new Zend_Session_Namespace('message');

		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('portal'));
		if(!$auth->hasIdentity()){
			$auth->clearIdentity();
			$this->_redirect('/auth');
		}else{
			$this->view->user = $this->_user = $auth->getIdentity();
			$this->view->placeholder('logged_in')->set(true);
		}

		if ($this->_user->cyber_user == "yes") {
			$this->view->layout()->setLayout("cyber");
			$this->_clientID = $this->_getParam("clientID",0);
			$this->_userID = 0;
		}else {
			$this->_clientID = $this->_user->ClientID;
			$this->_userID = $this->_user->ID;
		}
		$this->view->clientID = $this->_clientID;

		$subSectionMenu = '<li id="subnav-viewtickets"><a href="/tickets"><span class="subnav-size">View Tickets</span></a></li>
							<li id="subnav-addticket"><a href="/tickets/add" id="addticket" title="Add Ticket" class="dialog"><span class="subnav-size">Add Ticket</span></a></li>
							<li id="subnav-wishlist"><a href="/tickets/wishindex"><span class="subnav-size">Wish List</span></a></li>
							<li id="subnav-archive"><a href="/tickets/archive/clientID/'.$this->_clientID.'"><span class="subnav-size">Archive</span></a></li>
							';
		$this->view->placeholder("subSectionMenu")->set($subSectionMenu);
	}

}

function sec2hm ($sec, $padHours = false) {
	$hms = "";
	$hours = intval(intval($sec) / 3600);

	$hms .= ($padHours) ? str_pad($hours, 2, "0", STR_PAD_LEFT). 'h ' : $hours. 'h ';

	$minutes = intval(($sec / 60) % 60);

	$hms .= str_pad($minutes, 2, "0", STR_PAD_LEFT)."m";

	return $hms;
}
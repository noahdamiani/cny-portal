<?php
//error_reporting(E_ALL);
//ini_set("display_errors", 1);

class ConceptController extends Cny_Controller_LayoutAction
{
	public function displayAction()
	{
		$this->view->concept_id = $concept_id = $this->_getParam("concept_id",0);
		$client_id = $this->_getParam("client_id","");

		$sql = $this->_db->quoteInto("SELECT c.*, p.title, p.client_id, Clients.ClientName FROM concepts AS c, concept_projects AS p LEFT JOIN Clients ON p.client_id = Clients.id WHERE c.concept_project_id = p.id AND p.client_id = '$client_id' AND c.id = ?",$concept_id);
		$this->view->concept = $this->_db->fetchRow($sql);

		if ($this->view->concept['concept_width_id']) {
			$sql = "SELECT width FROM concept_widths WHERE id = {$this->view->concept['concept_width_id']}";
			$this->view->width = $this->_db->fetchOne($sql);
		}

		$sql = $this->_db->quoteInto("SELECT * FROM concept_images WHERE concept_id=? ORDER BY zorder ASC",$concept_id);
		$this->view->images = $this->_db->fetchAssoc($sql);

		if (!$this->view->concept) {
			$this->render("unknown");
		}
	}

	public function display2Action()
	{
		$this->view->concept_id = $concept_id = $this->_getParam("concept_id",0);

		$sql = $this->_db->quoteInto("SELECT c.*, p.title, p.client_id, Clients.ClientName FROM concepts AS c, concept_projects AS p LEFT JOIN Clients ON p.client_id = Clients.id WHERE c.concept_project_id = p.id AND c.id = ?",$concept_id);
		$this->view->concept = $this->_db->fetchRow($sql);

		if ($this->view->concept['concept_width_id']) {
			$sql = "SELECT width FROM concept_widths WHERE id = {$this->view->concept['concept_width_id']}";
			$this->view->width = $this->_db->fetchOne($sql);
		}

		$page = $this->getRequest()->getParam('page',1);

		$select = $this->_db->select();
		$select->from(array("i"=>"concept_images"));
		$select->where("i.concept_id = $concept_id");

		$count = $this->_db->fetchOne($this->_db->quoteInto("SELECT COUNT(*) FROM concept_images WHERE concept_id = ?",$concept_id));

		$paginator = Zend_Paginator::factory($select);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(1);
		$this->view->images = $paginator;
		$this->view->next = (($page+1) > $count)?1:($page+1);
	}

	public function addnoteAction()
	{
		if ($this->_user->ID == "") {
			$this->_redirect("/concept/login");
		}

		$this->_helper->layout()->setLayout("blank");
		$this->view->id = $id = $this->_getParam('id',0);
		$this->view->concept_id = $concept_id = $this->_getParam('concept_id',0);

		$sql = $this->_db->quoteInto("SELECT tn.*, CONCAT(u.FirstName, ' ', u.LastName) AS name, u.Roles FROM concept_notes AS tn LEFT JOIN Users_BugTracker AS u ON tn.user_id = u.ID
								WHERE tn.concept_image_id = ? ORDER BY tn.created DESC",$id);
		$this->view->notes = $this->_db->fetchAssoc($sql);

		if( $this->getRequest()->isPost() ){
			$like = $this->_getParam("add_like","");
			$approve = $this->_getParam("add_approval","");

			$data['concept_image_id'] = $id;
			$data['note'] = $this->_getParam("Description","");
			$data['user_id'] = $this->_user->ID;
			$data['created'] = new Zend_Db_Expr("NOW()");

			if (trim($data['note'] != ''))
				$this->_db->insert("concept_notes",$data);

			if ($like == "add") {
				$data['note'] = "Image marked as LIKED";
				$this->_db->insert("concept_notes",$data);

				$like_data = $data;
				unset($like_data['note']);
				$this->_db->insert("concept_likes",$like_data);
			}
			if ($approve == "approve") {
				$data['note'] = "Image marked as APPROVED";
				$this->_db->insert("concept_notes",$data);

				$this->_db->update("concept_images", array("approved"=>"y"), "id=$id");
			}
			exit;
		}
	}

	public function approvedAction()
	{
		$project_id = $this->_getParam("project_id",0);
		$client_id = $this->_getParam("client_id","");

		$sql = $this->_db->quoteInto("SELECT cp.id, cp.name, p.client_id, Clients.ClientName FROM concept_projects AS p LEFT JOIN Clients ON p.client_id = Clients.id LEFT JOIN client_projects AS cp ON p.client_project_id = cp.id WHERE p.client_id = '$client_id' AND p.client_project_id = ?",$project_id);
		$this->view->concept = $this->_db->fetchRow($sql);

		$sql = $this->_db->quoteInto("SELECT ci.* FROM concept_images AS ci, concept_projects AS p, concepts AS c WHERE p.client_project_id = ? AND c.concept_project_id = p.id AND ci.concept_id = c.id AND ci.approved ='y' ORDER BY ci.created ASC",$project_id);
		$this->view->images = $this->_db->fetchAssoc($sql);

		if (!$this->view->concept) {
			$this->render("unknown");
		}
	}

	public function loginAction()
	{
		$form = new Cny_Form_Login();
		$this->view->loginform = $form;

		$this->view->error = $this->getRequest()->getParam('error', '');

		if( $this->_request->isPost() ){
			$formData = $this->_request->getPost();
			if( empty($formData['username']) || empty($formData['password']) ){
				$error = 'Empty email or password.';
			}else{
				// do the authentication
				$authAdapter = $this->_getAuthAdapter($formData);
				$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('portal'));
				$result = $auth->authenticate($authAdapter);
				if( !$result->isValid() ){
					$error = 'Login failed';
				}else{
					$last_login = date("Y-m-d H:i:s");
					$data = $authAdapter->getResultRowObject(null,'UserPassword');
					$db = Zend_Db_Table::getDefaultAdapter();
					$db->update('Users_BugTracker', array('LastLoginDate'=>$last_login), 'ID = '.$data->ID );

					//check if CyberNY ID
					if ($data->Cyberny_Users_ID > 0) {
						$db->update('Users', array('LastLoginDate'=>$last_login, 'LastLoginIP'=>$_SERVER['REMOTE_ADDR']), 'ID = '.$data->Cyberny_Users_ID );
						$data->intranet_admin = $db->fetchOne("SELECT intranet_admin FROM Users WHERE ID = {$data->Cyberny_Users_ID} ");
						$data->cyber_user = "yes";
					}else {
						$data->cyber_user = "no";
					}

					$auth->getStorage()->write($data);
					$url_redirect = ($this->_redirectUrl) ? $this->_redirectUrl : '/';
					$this->_redirect($url_redirect);
					return;
				}
			}
		}
	}

	protected function _getAuthAdapter($formData)
	{
		$dbAdapter = Zend_Db_Table::getDefaultAdapter();
		$authAdapter = new Zend_Auth_Adapter_DbTable( $dbAdapter );
		$authAdapter->setTableName('Users_BugTracker')
			->setIdentityColumn('Email')
			->setCredentialColumn('UserPassword')
			->setCredentialTreatment('AND Disabled = "no"');
		// get "salt" for better security
		// $config = Zend_Registry::get('config');
		// $salt = $config->auth->salt;
		// $password = $salt.$formData['password'];
		$password = $formData['password'];
		$authAdapter->setIdentity($formData['username']);
		$authAdapter->setCredential($password);
		return $authAdapter;
	}

	public function unknownAction()
    {
    	$this->_helper->layout()->setLayout("blank");
    }

	function init()
	{
		$bootstrap = $this->getInvokeArg('bootstrap'); // gets the boostrapper
		$resource = $bootstrap->getPluginResource('multidb'); //multi db support
		$this->_db = $resource->getDefaultDb();

		$options = $bootstrap->getOptions();
		$this->view->concept_path = $this->_concept_path = $options['concept']['path'];

		$this->_helper->layout()->setLayout("slideshow");

		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('portal'));
		if(!$auth->hasIdentity()){
			$auth->clearIdentity();
			$this->_user = new stdClass();
			$this->_user->cyber_user = "";
			$this->_user->ClientID = "";
			$this->_user->ID = "";
		}else{
			$this->view->user = $this->_user = $auth->getIdentity();
			$this->view->placeholder('logged_in')->set(true);
		}

		if ($this->_user->cyber_user == "yes") {
			$this->_clientID = $this->_getParam("clientID",0);
			$this->_userID = 0;
		}else {
			$this->_clientID = $this->_user->ClientID;
			$this->_userID = $this->_user->ID;
		}

	}
}



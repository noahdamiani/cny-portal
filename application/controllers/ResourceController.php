<?php
class ResourceController extends Cny_Controller_LayoutAction
{
	public function activityreportAction()
	{
		$this->view->from_date = $from_date = $this->_getParam("from_date",'');
		$this->view->to_date = $to_date = $this->_getParam("to_date",'');
		$this->view->type = $type = $this->_getParam("type","week");

		if ($from_date && $to_date) {

		}elseif ($type == "week") {
			$from_date = date("Y-m-d", strtotime("last Monday"));
			$to_date = date("Y-m-d");
		}elseif ($type == "month") {
			$from_date = date("Y-m-01");
			$to_date = date("Y-m-d");
		}else {
			$from_date = $to_date = date("Y-m-d");
		}

		$from_time = strtotime($from_date);
		$to_time = strtotime($to_date)+(24*60*60)-1;

		$sql = "SELECT UserID, SUM( (EndTime-StartTime) ) FROM ProjectActivity WHERE StartTime >= $from_time AND EndTime <= $to_time AND EndTime IS NOT NULL GROUP BY UserID";
		$this->view->total_hours_wtd = $this->_db->fetchPairs($sql);

		$sql = "SELECT a.UserID, SUM( (a.EndTime-a.StartTime) ) FROM ProjectActivity AS a, Projects AS p WHERE a.ProjectID = p.id AND StartTime >= $from_time AND EndTime <= $to_time AND a.EndTime IS NOT NULL AND p.RetainerStatus = 'y' GROUP BY a.UserID";
		$this->view->total_retainer_wtd = $this->_db->fetchPairs($sql);

		$sql = "SELECT * FROM Users AS u WHERE id <> 14 ORDER BY u.UserName ASC";
		$this->view->staff = $this->_db->fetchAssoc($sql);

		if($this->_getParam("export","")) {
			$this->view->from_date = $from_date;
			$this->view->to_date = $to_date;

			$this->_helper->layout()->disableLayout();
			$this->render('activityreport-export');
		}
	}

	public function activitydetailAction()
	{
		$id = $this->_getParam("id",0);
		$this->view->from_date = $from_date = $this->_getParam("from_date",'');
		$this->view->to_date = $to_date = $this->_getParam("to_date",'');
		$this->view->type = $type = $this->_getParam("type","today");

		if ($from_date && $to_date) {

		}elseif ($type == "week") {
			$from_date = date("Y-m-d", strtotime("last Monday"));
			$to_date = date("Y-m-d");
		}elseif ($type == "month") {
			$from_date = date("Y-m-01");
			$to_date = date("Y-m-d");
		}else {
			$from_date = $to_date = date("Y-m-d");
		}

		$from_time = strtotime($from_date);
		$to_time = strtotime($to_date)+(24*60*60)-1;

		$sql = "SELECT pa.*, ps.ProjectStatusName, p.Title, c.ClientName FROM ProjectActivity AS pa, Projects AS p, ProjectStatus AS ps, Clients AS c WHERE p.ProjectStatusID = ps.id AND pa.ProjectID = p.id AND p.ClientID = c.id AND pa.StartTime >= $from_time AND pa.EndTime <= $to_time AND pa.EndTime IS NOT NULL AND pa.UserID = '$id' ORDER BY pa.StartTime ASC";
		$this->view->tasks = $this->_db->fetchAssoc($sql);

		//$sql = "SELECT a.UserID, SUM( (a.EndTime-a.StartTime) ) FROM ProjectActivity AS a, Projects AS p WHERE a.ProjectID = p.id AND StartTime >= $from_time AND EndTime <= $to_time AND a.EndTime IS NOT NULL AND p.RetainerStatus = 'y'  AND UserID = '$id'";
		//$this->view->total_retainer_wtd = $this->_db->fetchPairs($sql);

		$sql = "SELECT * FROM Users AS u WHERE id = '$id'";
		$this->view->staff = $this->_db->fetchRow($sql);
	}

	public function scheduleListAction()
	{
		if ($this->_user->intranet_admin != 1) {
			$this->_redirect("/schedule");
		}

		$sql = "SELECT id, UserName FROM Users AS u WHERE Disabled = 'no' ORDER BY u.UserName ASC";
		$this->view->staff = $this->_db->fetchPairs($sql);

		$this->view->placeholder('sub_section')->set("requests");

		$search = new Zend_Session_Namespace('schedule_search');
		$mask = $relationship = "";

		$lastday = date('t');
		$this->view->from_date = $this->_getParam("from_date",date("Y-01-01"));
		$this->view->to_date = $this->_getParam("to_date",date("Y-m-d"));

		$from_time = strtotime($this->view->from_date);
		$to_time = strtotime($this->view->to_date." 23:59:59");

		$this->view->status = $status = $this->_getParam('status','');
		$this->view->user_id = $user_id = $this->_getParam('user_id','');

		$this->view->placeholder('sub_section')->set("retainer");

		$page = $this->getRequest()->getParam('page',1);
		$this->view->dir = $dir = $this->_getParam('dir','ASC');
		$this->view->sort = $sort = $this->_getParam('sort','start_date');

		$select = $this->_db->select();
		$select->from(array("s"=>"schedule_requests"));
		$select->joinLeft(array("u"=>"Users"),"s.user_id = u.ID");
		$select->where("((s.start_date >= '{$this->view->from_date}' AND s.end_date <= '{$this->view->to_date}') OR (s.start_date < '{$this->view->from_date}' AND s.end_date > '{$this->view->to_date}') OR (s.start_date <= '{$this->view->to_date}' AND s.end_date > '{$this->view->to_date}'))");
		$select ->order(array("$sort $dir"));

		if ($status) {
			$select->where("s.status = ?",$status);
		}
		if ($user_id) {
			$select->where("s.user_id = ?",$user_id);
		}

		/*
		$paginator = Zend_Paginator::factory($select);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(50);
		$this->view->requests = $paginator;
		*/

		$sql = $select->__toString();
		$this->view->requests = $this->_db->fetchAssoc($sql);
	}

	public function scheduleApproveAction()
	{
		if ($this->_user->intranet_admin != 1) {
			$this->_redirect("/schedule");
		}

		$id = $this->_getParam("id",0);

		$this->view->request = $request = $this->_db->fetchRow("SELECT * FROM schedule_requests WHERE id = $id");

		if( $this->getRequest()->isPost() ){
			$data = $this->_getParam('data',array());
			$data['modified'] = new Zend_Db_Expr("NOW()");

			$this->_db->update("schedule_requests",$data,"id=$id");

			if ($data['status'] == 'approved') {
				//add to calendar
				$cal = array("user_id"=>$request['user_id'],"start"=>$request['start_date'],"end"=>$request['end_date'],
					"title"=>$request['title'],"type"=>"vacation","created"=>$data['modified'],"modified"=>$data['modified']);
				$this->_db->insert("calendar_entries",$cal);

				$user = $this->_db->fetchRow("SELECT * FROM Users WHERE ID = {$request['user_id']}");

				define( 'newline', "<br/>" );
				//email to notify
				$mail_message = "Your change of schedule request has been {$data['status']}".newline;
				$mail_message .= "Dates: ".date("F d Y",strtotime($request['start_date']))." - ".date("F d Y",strtotime($request['end_date'])).newline;
				$mail_message .= "Title: {$request['title']}".newline;
				$mail_message .= "Description: {$request['description']}".newline.newline;

				$mail = new Zend_Mail();
				$mail->setBodyHtml($mail_message);
				$mail->setFrom('do-not-reply@cyber-ny.com', "Schedule Request Response");

				//if not production only email support@cyber-ny.com
				if(APPLICATION_ENV != 'production') {
					$mail->addTo("support@cyber-ny.com", "cyber support");
					//$mail->addTo("craig@cyber-ny.com");
				}else {
					$mail->addTo($user['Email'],"{$user['FirstName']} {$user['LastName']}");
				}
				$mail->setSubject("Schedule Change Request Response");
				$mail->send();

				if ($request['type'] == 'vacation' || $request['type'] == 'sick' || $request['type'] == 'off' || $request['type'] == 'personal') {
					//send notification for task move
					$sql = $this->_db->quoteInto("SELECT p.*, u.UserName, au.UserName AS assigner, au.id AS assigner_id, c.ClientName FROM Projects AS p LEFT JOIN Users AS u ON p.UserID = u.ID LEFT JOIN Users as au ON p.assigner = au.ID LEFT JOIN Clients AS c ON p.ClientID = c.id WHERE p.UserID = ? AND p.ExpectedEndDate >= '".strtotime($request['start_date'])."' AND p.ExpectedEndDate <= '".strtotime($request['end_date'])."'",$request['user_id']);
					$tasks = $this->_db->fetchAssoc($sql);

					if (date("N",strtotime($request['end_date'])) == 5) {
						$new_date = strtotime($request['end_date']." +3 days");
					}elseif (date("N",strtotime($request['end_date'])) == 6) {
						$new_date = strtotime($request['end_date']." +2 days");
					}else {
						$new_date = strtotime($request['end_date']." +1 days");
					}

					foreach ($tasks as $task) {
						//send email to assigner if different from user
						if ($task['assigner'] != $request['user_id']) {
							$this->_db->insert("task_change_requests",array(
								"user_id" => $request['user_id'],
								"assigner_id" => $task['assigner_id'],
								"task_id" => $task['id'],
								"original_date" => $task['ExpectedEndDate'],
								"new_date" => $new_date,
								"explenation" => $request['description'],
								"created" => new Zend_Db_Expr("NOW()"),
								"modified" => new Zend_Db_Expr("NOW()")
							));
							$request_id = $this->_db->lastInsertId();

							$sql = "SELECT * FROM Users WHERE id = {$task['assigner']}";
							$assigner = $this->_db->fetchRow($sql);

							$code_approve = urlencode(base64_encode("approved||$request_id"));
							$code_deny = urlencode(base64_encode("denied||$request_id"));

							//send request email
							define( 'newline', "<br/>" );
							$mail_message = "<strong>".$this->_user->FirstName." ".$this->_user->LastName." is requesting to move task: ".$task['Title']."</strong>".newline.newline;
							$mail_message .= 'Client: '.$task['ClientName'].newline;
							$mail_message .= 'Orignal Due Date: '.date("l F d Y",$task['ExpectedEndDate']).newline;
							$mail_message .= 'Requested Due Date: '.date("l F d Y",$new_date).newline.newline;
							$mail_message .= 'Reason: '.$request['description'].newline.newline;

							$mail_message .= "<a href='http://$this->_domain/request/task/c/$code_approve'><img src='http://$this->_domain/images/btn_email_approve.gif' border='0'></a>";
							$mail_message .= " ";
							$mail_message .= "<a href='http://$this->_domain/request/task/c/$code_deny'><img src='http://$this->_domain/images/btn_email_deny.gif' border='0'></a>";

							$mail = new Zend_Mail();
							$mail->setBodyHtml($mail_message);

							//if not production only email support@cyber-ny.com
							if(APPLICATION_ENV != 'production') {
								$mail->setFrom('do-not-reply@cnydevzone3.com', $task['ClientName']);
								$mail->addTo("support@cyber-ny.com", "cyber support");
								//$mail->addTo("craig@cyber-ny.com");
							}else {
								$mail->setFrom('do-not-reply@cyber-ny.com',  $task['ClientName']);
								$mail->addTo("{$assigner['Email']}");
							}
							$mail->setSubject("Request to move task: {$task['Title']}");
							$mail->send();
						}
					}
				}

			}

			$this->_redirect("/resource/schedule-list");
		}
	}

	public function timesheetDetailAction()
	{
		$year_week = $this->_getParam("week","");

		$timesheet = new Cny_Model_Timesheet();

		$year = substr($year_week, 0,4);
		$week = substr($year_week, 4,2);

		$this->view->pay_period = $timesheet->weekToDates($week,$year);

		$dates = $timesheet->datesInPayPeriod($week,$year);

		$user = ($this->_user->intranet_admin == 1) ? $this->_getParam("user",$this->_user->Cyberny_Users_ID) : $this->_user->Cyberny_Users_ID;
		$this->view->timesheets = array();
		foreach ($dates as $day) {
			$sql = $this->_db->quoteInto("SELECT SUM(TIMESTAMPDIFF(SECOND,clock_in,clock_out)) AS seconds FROM timesheet WHERE user_id = ? AND clock_in LIKE '$day%' GROUP BY DATE_FORMAT(clock_in,'%Y-%m-%d') ASC",$user);
			$this->view->timesheets[$day] = $this->_db->fetchOne($sql);
		}

	}

	public function timesheetReportAction()
	{
		$sql = "SELECT DISTINCT(t.user_id), CONCAT(u.FirstName,' ',u.LastName) AS name FROM timesheet AS t, Users AS u WHERE t.user_id = u.ID ORDER BY u.LastName, u.FirstName ASC";
		$employees = $this->_db->fetchAssoc($sql);

		$this->view->employees = array();
		foreach ($employees as $person) {
			$this->view->employees[$person['user_id']]['name'] = $person['name'];

			$sql = $this->_db->quoteInto("SELECT DATE_FORMAT(clock_in,'%x%v') AS year_week, SUM(TIMESTAMPDIFF(SECOND,clock_in,clock_out)) AS seconds FROM timesheet WHERE user_id = ? GROUP BY DATE_FORMAT(clock_in,'%x%v') DESC LIMIT 10",$person['user_id']);
			$this->view->employees[$person['user_id']]['timesheets'] = $this->_db->fetchPairs($sql);
		}

	}

	public function performanceAction()
	{
		$this->view->from_date = $from_date = $this->_getParam("from_date",'');
		$this->view->to_date = $to_date = $this->_getParam("to_date",'');
		$this->view->type = $type = $this->_getParam("type","week");

		if ($from_date && $to_date) {

		}elseif ($type == "week") {
			$from_date = date("Y-m-d", strtotime("last Monday"));
			$to_date = date("Y-m-d");
		}elseif ($type == "month") {
			$from_date = date("Y-m-01");
			$to_date = date("Y-m-d");
		}else {
			$from_date = $to_date = date("Y-m-d");
		}

		$from_time = strtotime($from_date);
		$to_time = strtotime($to_date)+(24*60*60)-1;

		$sql = "SELECT UserID, COUNT(*) FROM Projects WHERE ExpectedEndDate >= $from_time AND ExpectedEndDate <= $to_time GROUP BY UserID";
		$this->view->total_tasks = $this->_db->fetchPairs($sql);

		$sql = "SELECT UserID, COUNT(*) FROM Projects WHERE ExpectedEndDate >= $from_time AND ExpectedEndDate <= $to_time AND ProjectStatusID=3 GROUP BY UserID";
		$this->view->on_client = $this->_db->fetchPairs($sql);

		$sql = "SELECT UserID, COUNT(*) FROM Projects WHERE ExpectedEndDate >= $from_time AND ExpectedEndDate <= $to_time AND ProjectStatusID=4 GROUP BY UserID";
		$this->view->closed = $this->_db->fetchPairs($sql);

		$sql = "SELECT UserID, COUNT(*) FROM Projects WHERE ExpectedEndDate <= $from_time AND ProjectStatusID=1 GROUP BY UserID";
		$this->view->past_due = $this->_db->fetchPairs($sql);

		$sql = "SELECT user_id, COUNT(*) FROM task_change_requests WHERE original_date >= $from_time AND original_date <= $to_time GROUP BY user_id";
		$this->view->moves = $this->_db->fetchPairs($sql);
	}

	function init()
	{
		$bootstrap = $this->getInvokeArg('bootstrap'); // gets the boostrapper
		$resource = $bootstrap->getPluginResource('multidb'); //multi db support
		$this->_db = $resource->getDefaultDb();

		$options = $bootstrap->getOptions();
		$this->view->attachment_path = $this->_attachment_path = $options['attachment']['path'];
		$this->_domain = $options['site']['domain'];

		$this->view->placeholder('section')->set("resource");
		$this->view->layout()->setLayout("cyber");

		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$this->_message = new Zend_Session_Namespace('message');

		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('portal'));
		if(!$auth->hasIdentity()){
			$auth->clearIdentity();
			$this->_redirect('/auth');
		}else{
			$this->view->user = $this->_user = $auth->getIdentity();
			$this->view->placeholder('logged_in')->set(true);

			if ($this->_user->cyber_user != "yes") {
				$auth->clearIdentity();
				$this->_redirect('/auth');
			}
		}

		$subSectionMenu = '';
		if ($this->_user->intranet_admin == 1) {
			$subSectionMenu .= '<li id="subnav-activityreport"><a href="/resource/activityreport"><span class="subnav-size">Daily Activity Report</span></a></li>';
			$subSectionMenu .= '<li id="subnav-performance"><a href="/resource/performance"><span class="subnav-size">Performance Report</span></a></li>';
			$subSectionMenu .= '<li id="subnav-retainers"><a href="/resource/schedule-list"><span class="subnav-size">Schedule Requests</span></a></li>';
			$subSectionMenu .= '<li id="subnav-timesheet-report"><a href="/resource/timesheet-report"><span class="subnav-size">Time Sheet Report</span></a></li>';
		}

		$this->view->placeholder("subSectionMenu")->set($subSectionMenu);
	}
}

function sec2hm ($sec, $padHours = false) {
	$hms = "";
	$hours = intval(intval($sec) / 3600);

	$hms .= ($padHours) ? str_pad($hours, 2, "0", STR_PAD_LEFT). 'h ' : $hours. 'h ';

	$minutes = intval(($sec / 60) % 60);

	$hms .= str_pad($minutes, 2, "0", STR_PAD_LEFT)."m";

	return $hms;
}

function sec2dec ($sec) {
	$hms = "";
	$hours = intval(intval($sec) / 3600);
	$minutes = intval(($sec / 60) % 60);

	$dec = round($hours + ($minutes/60) ,2);

	return $dec;
}
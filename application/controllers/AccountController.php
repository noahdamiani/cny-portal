<?php
class AccountController extends Cny_Controller_LayoutAction
{

	public function indexAction()
	{
		$this->view->placeholder('sub_section')->set("account");

		$search = new Zend_Session_Namespace('admin_search');
    	$mask = "";

		$page = $this->getRequest()->getParam('page',1);
		$this->view->dir = $dir = $this->_getParam('dir','DESC');
		$this->view->sort = $sort = $this->_getParam('sort','u.UserName');

		$select = $this->_db->select();
		$select->from(array("u"=>"Users_BugTracker"), "*");
		$select->where("u.ClientID =".$this->_user->ClientID);
		$select ->order(array("$sort $dir"));

		$paginator = Zend_Paginator::factory($select);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(50);
		$this->view->users = $paginator;
	}

	public function viewAction()
	{
		if ($this->_user->cyber_user == "yes") {
			$id = $this->_getParam("id",0);
		}else {
			$id = $this->_user->ID;
		}

		if ($id == $this->_user->ID)
			$this->view->placeholder('sub_section')->set("settings");

		$sql = $this->_db->quoteInto("SELECT u.*, c.ClientName FROM Users_BugTracker AS u LEFT JOIN Clients AS c ON u.clientID = c.ID WHERE u.ID=?",$id);
		$this->view->accountUser = $this->_db->fetchRow($sql);
	}

	public function addAction()
	{
		if ($this->_user->cyber_user == "yes") {
			$id = $this->_getParam("id",0);
		}else {
			$this->_redirect("/account/");
		}

		if( $this->getRequest()->isPost() ){
			$data = $this->_getParam("data",array());
			$data['ClientID'] = $this->_user->ClientID;
			$data['Roles'] = "client";

			//verify email not already used
			$sql = $this->_db->quoteInto("SELECT * FROM Users_BugTracker WHERE Email = ?",$data['Email']);
			$exist = $this->_db->fetchRow($sql);

			if (!$exist) {
				if ($data['UserPassword'] != $data['vUserPassword'] || trim($data['UserPassword']) == '') {
					unset($data['UserPassword']);
				}
				unset($data['vUserPassword']);

				$this->_db->insert("Users_BugTracker",$data);
				$new_user_id = $this->_db->lastInsertId();

				$this->_db->insert("users_client",array("user_id"=>$new_user_id,"client_id"=>$this->_user->ClientID));
			}

			$this->_redirect("/account/");
		}
	}

	public function editAction()
	{
		$this->_helper->layout()->setLayout("modal");

		if ($this->_user->cyber_user == "yes") {
			$id = $this->_getParam("id",0);
		}else {
			$id = $this->_user->ID;
		}

		$sql = $this->_db->quoteInto("SELECT u.*, c.ClientName FROM Users_BugTracker AS u LEFT JOIN Clients AS c ON u.clientID = c.ID WHERE u.ID=?",$id);
		$this->view->accountUser = $this->_db->fetchRow($sql);

		if( $this->getRequest()->isPost() ){
			$data = $this->_getParam("data",array());
			$data['ClientID'] = $this->_user->ClientID;
			$data['Roles'] = "client";

			//verify email not already used
			$sql = $this->_db->quoteInto("SELECT * FROM Users_BugTracker WHERE ID <> $id AND Email = ?",$data['Email']);
			$exist = $this->_db->fetchRow($sql);

			if (!$exist) {
				if ($data['UserPassword'] != $data['vUserPassword'] || trim($data['UserPassword']) == '') {
					unset($data['UserPassword']);
				}
				unset($data['vUserPassword']);

				$this->_db->update("Users_BugTracker",$data,"ID=$id");
			}

			$this->_redirect("/account/view/id/$id");
		}
	}

	public function deleteAction()
	{
		if ($this->_user->cyber_user == "yes") {
			$id = $this->_getParam("id",0);
		}else {
			$this->_redirect("/account/");
		}

		$this->_db->delete("Users_BugTracker","ID=$id");

		$this->_redirect("/account/");
	}

	public function validateAction()
	{
		$type = $this->_getParam('type','');
		$data = $this->_getParam('data',array());
		$id = $this->_getParam('id',0);

		if ($type == 'email') {
			$sql = $this->_db->quoteInto("SELECT ID FROM Users_BugTracker WHERE ID <> $id AND Email = ?", $data['Email']);
			$result = $this->_db->fetchOne($sql);
		}

		if ($result) echo 'false';
		else echo 'true';
		exit();
	}

	function init()
	{
		$bootstrap = $this->getInvokeArg('bootstrap'); // gets the boostrapper
		$resource = $bootstrap->getPluginResource('multidb'); //multi db support
		$this->_db = $resource->getDefaultDb();

		$this->view->placeholder('section')->set("account");

		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('portal'));
		if(!$auth->hasIdentity()){
			$auth->clearIdentity();
			$this->_redirect('/auth');
		}else{
			$this->view->user = $this->_user = $auth->getIdentity();
			$this->view->placeholder('logged_in')->set(true);

			if ($this->_user->cyber_user == "yes") {
				$this->view->layout()->setLayout("cyber");
			}
		}

		$subSectionMenu = '<li id="subnav-settings"><a href="/settings/"><span class="subnav-size">My Account Settings</span></a></li>
							<li id="subnav-viewusers"><a href="/account/"><span class="subnav-size">View Users</span></a></li>
							<li id="subnav-addusers"><a href="/account/add"><span class="subnav-size">Add User</span></a></li>';
		$this->view->placeholder("subSectionMenu")->set($subSectionMenu);
	}
}



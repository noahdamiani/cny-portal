<?php
class ServersController extends Cny_Controller_LayoutAction
{
    public function indexAction()
	{
		$this->view->placeholder('sub_section')->set("server");

		$search = new Zend_Session_Namespace('server_search');
    	$mask = $relationship = "";

		$page = $this->getRequest()->getParam('page',1);
		$this->view->dir = $dir = $this->_getParam('dir','ASC');
		$this->view->sort = $sort = $this->_getParam('sort','name');

		$select = $this->_db->select();
		$select->from(array("s"=>"Servers"), "*");
		$select->joinLeft(array("c"=>"Clients"), "s.clientId = c.id", "ClientName");
		$select ->order(array("$sort $dir"));

		$paginator = Zend_Paginator::factory($select);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(50);
		$this->view->servers = $paginator;

		$this->view->messages = $this->_flashMessenger->getMessages();
	}

	public function viewAction()
	{
		$id = $this->_getParam("id",0);

		$sql = $this->_db->quoteInto("SELECT s.*, c.ClientName FROM Servers AS s LEFT JOIN Clients AS c ON s.clientId = c.id WHERE s.id = ?", $id);
		$this->view->server = $this->_db->fetchRow($sql);
	}

	public function addAction()
	{
		$this->view->placeholder('sub_section')->set("addserver");

		$sql = "SELECT id, ClientName FROM Clients WHERE Disabled = 'no' ORDER BY ClientName ASC";
		$this->view->clients = array(""=>"Select Client")+$this->_db->fetchPairs($sql);

		$sql = "SELECT id, name FROM Servers ORDER BY name ASC";
		$this->view->servers = array(""=>"Select Server")+$this->_db->fetchPairs($sql);

		if( $this->getRequest()->isPost() ){
			$data = $this->_getParam('data',array());
			$data['created'] = new Zend_Db_Expr("NOW()");
			$data['modified'] = new Zend_Db_Expr("NOW()");

			if (!stripos($data['webminUrl'],"http://") && !stripos($data['webminUrl'],"https://") && trim($data['webminUrl']) != '') {
	    		$data['webminUrl'] = "http://{$data['webminUrl']}";
	    	}
	    	if (!stripos($data['databaseUrl'],"http://") && !stripos($data['databaseUrl'],"https://") && trim($data['databaseUrl']) != '') {
	    		$data['databaseUrl'] = "http://{$data['databaseUrl']}";
	    	}

			$this->_db->insert("Servers",$data);

			$this->_redirect("/servers/");
		}
	}

	public function editAction()
	{
		$id = $this->_getParam("id",0);

		$sql = $this->_db->quoteInto("SELECT s.*, c.ClientName FROM Servers AS s LEFT JOIN Clients AS c ON s.clientId = c.id WHERE s.id = ?", $id);
		$this->view->server = $this->_db->fetchRow($sql);

		$sql = "SELECT id, ClientName FROM Clients WHERE Disabled = 'no' ORDER BY ClientName ASC";
		$this->view->clients = array(""=>"Select Client")+$this->_db->fetchPairs($sql);

		if( $this->getRequest()->isPost() ){
			$data = $this->_getParam('data',array());
			$data['modified'] = new Zend_Db_Expr("NOW()");

			if (!stripos($data['webminUrl'],"http://") && !stripos($data['webminUrl'],"https://") && trim($data['webminUrl']) != '') {
	    		$data['webminUrl'] = "http://{$data['webminUrl']}";
	    	}
	    	if (!stripos($data['databaseUrl'],"http://") && !stripos($data['databaseUrl'],"https://") && trim($data['databaseUrl']) != '') {
	    		$data['databaseUrl'] = "http://{$data['databaseUrl']}";
	    	}

			$this->_db->update("Servers",$data, "id=$id");

			$this->_redirect("/servers/view/id/$id");
		}
	}

	public function deleteAction()
	{
		$id = $this->_getParam("id",0);

		$this->_db->delete("Servers", "id = $id");

		$this->_redirect("/servers");
	}

	public function statusAction()
	{
		$sql = "SELECT * FROM server_status WHERE created >= DATE_SUB(NOW(), INTERVAL 5 MINUTE)";
		$status = $this->_db->fetchRow($sql);

		if (!$status) {
			$sql = "SELECT id, IP FROM Servers WHERE IP <> '' AND server_status='enabled'";
			$servers = $this->_db->fetchPairs($sql);

			$status['issue'] = "0";
			foreach ($servers as $id => $IP) {
				$pingDomain = new Cny_View_Helper_PingDomain();
				$ping = $pingDomain->pingDomain($IP);
				if ($ping == -1) $status['issue'] = "1";
			}

			$status['created'] = new Zend_Db_Expr("NOW()");
			$this->_db->insert("server_status",$status);
		}

		echo $status['issue'];exit;
	}

	function init()
	{
		$bootstrap = $this->getInvokeArg('bootstrap'); // gets the boostrapper
		$resource = $bootstrap->getPluginResource('multidb'); //multi db support
		$this->_db = $resource->getDefaultDb();

		$this->view->placeholder('section')->set("server");
		$this->view->layout()->setLayout("cyber");

		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('portal'));
		if(!$auth->hasIdentity()){
			$auth->clearIdentity();
			$this->_redirect('/auth');
		}else{
			$this->view->user = $this->_user = $auth->getIdentity();
			$this->view->placeholder('logged_in')->set(true);

			if ($this->_user->cyber_user != "yes") {
				$auth->clearIdentity();
				$this->_redirect('/auth');
			}
		}

		$subSectionMenu = '<li id="subnav-viewserver"><a href="/servers"><span class="subnav-size">View Servers</span></a></li>
							<li id="subnav-addserver"><a href="/servers/add"><span class="subnav-size">Add Server</span></a></li>
							';
		$this->view->placeholder("subSectionMenu")->set($subSectionMenu);
	}
}

<?php
class FtpController extends Cny_Controller_LayoutAction
{
    public function indexAction()
	{
		$this->view->placeholder('sub_section')->set("ftp");

		$search = new Zend_Session_Namespace('ftp_search');
    	$mask = $relationship = "";
    	$mask = $this->view->mask = $this->_getParam("mask","");

		$page = $this->getRequest()->getParam('page',1);
		$this->view->dir = $dir = $this->_getParam('dir','ASC');
		$this->view->sort = $sort = $this->_getParam('sort','website');

		$select = $this->_db->select();
		$select->from(array("f"=>"Ftps"), "*");
		$select->joinLeft(array("s"=>"Servers"), "f.serverId = s.id", array("server"=>"s.name"));
		$select->joinLeft(array("c"=>"Clients"), "f.clientId = c.id", "ClientName");
		$select ->order(array("$sort $dir"));

		if ($mask) {
			$select->where("c.ClientName LIKE '%$mask%' OR f.website LIKE '%$mask%' OR c.client_code LIKE '%$mask' ");
		}

		$paginator = Zend_Paginator::factory($select);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(50);
		$this->view->ftps = $paginator;

		$this->view->messages = $this->_flashMessenger->getMessages();
	}

	public function viewAction()
	{
		$id = $this->_getParam("id",0);

		$sql = $this->_db->quoteInto("SELECT f.*, s.owned, s.host, s.name AS server, c.ClientName FROM Ftps AS f LEFT JOIN Servers AS s ON f.serverId = s.id LEFT JOIN Clients AS c ON f.clientId = c.id WHERE f.id = ?", $id);
		$this->view->ftp = $this->_db->fetchRow($sql);
	}

	public function addAction()
	{
		$crypt = new Cny_Model_Encryption();
		$this->view->placeholder('sub_section')->set("addftp");

		$sql = "SELECT id, ClientName FROM Clients WHERE Disabled = 'no' ORDER BY ClientName ASC";
		$this->view->clients = array(""=>"Select Client")+$this->_db->fetchPairs($sql);

		$sql = "SELECT id, name FROM Servers ORDER BY name ASC";
		$this->view->servers = array(""=>"Select Server")+$this->_db->fetchPairs($sql);

		if( $this->getRequest()->isPost() ){
			$data = $this->_getParam('data',array());
			$data['created'] = new Zend_Db_Expr("NOW()");
			$data['modified'] = new Zend_Db_Expr("NOW()");

			if ($data['ftp_password'] == $data['vftp_password'] && trim($data['ftp_password']) != '') {
				unset($data['vftp_password']);
				$data['ftp_password'] = $crypt->encrypt($this->_enc_key,$data['ftp_password'],10);
			}else {
				unset($data['ftp_password']);
				unset($data['vftp_password']);
			}

			if ($data['db_password'] == $data['vdb_password'] && trim($data['db_password']) != '') {
				unset($data['vdb_password']);
				$data['db_password'] = $crypt->encrypt($this->_enc_key,$data['db_password'],10);
			}else {
				unset($data['db_password']);
				unset($data['vdb_password']);
			}

			if ($data['enc_notes']) $data['enc_notes'] = $crypt->encrypt($this->_enc_key,str_replace(array("\n","\r","\t"),array("",""," "),nl2br($data['enc_notes'])),10);

			$this->_db->insert("Ftps",$data);

			$this->_redirect("/ftp/");
		}
	}

	public function editAction()
	{
		$crypt = new Cny_Model_Encryption();
		$id = $this->_getParam("id",0);

		$sql = $this->_db->quoteInto("SELECT f.*, s.owned, s.host, s.name AS server, c.ClientName FROM Ftps AS f LEFT JOIN Servers AS s ON f.serverId = s.id LEFT JOIN Clients AS c ON f.clientId = c.id WHERE f.id = ?", $id);
		$this->view->ftp = $this->_db->fetchRow($sql);

		$sql = "SELECT id, ClientName FROM Clients WHERE Disabled = 'no' ORDER BY ClientName ASC";
		$this->view->clients = array(""=>"Select Client")+$this->_db->fetchPairs($sql);

		$sql = "SELECT id, name FROM Servers ORDER BY name ASC";
		$this->view->servers = array(""=>"Select Server")+$this->_db->fetchPairs($sql);

		if( $this->getRequest()->isPost() ){
			$data = $this->_getParam('data',array());
			$data['modified'] = new Zend_Db_Expr("NOW()");

			if ($data['ftp_password'] == $data['vftp_password'] && trim($data['ftp_password']) != '') {
				unset($data['vftp_password']);
				$data['ftp_password'] = $crypt->encrypt($this->_enc_key,$data['ftp_password'],10);
			}else {
				unset($data['ftp_password']);
				unset($data['vftp_password']);
			}

			if ($data['db_password'] == $data['vdb_password'] && trim($data['db_password']) != '') {
				unset($data['vdb_password']);
				$data['db_password'] = $crypt->encrypt($this->_enc_key,$data['db_password'],10);
			}else {
				unset($data['db_password']);
				unset($data['vdb_password']);
			}

			if ($data['enc_notes']) $data['enc_notes'] = $crypt->encrypt($this->_enc_key,$data['enc_notes'],10);

			$this->_db->update("Ftps",$data, "id=$id");

			$this->_redirect("/ftp/view/id/$id");
		}
	}

	public function deleteAction()
	{
		$id = $this->_getParam("id",0);

		$this->_db->delete("Ftps", "id = $id");

		$this->_redirect("/ftp");
	}

	public function passwordAction()
	{
		$crypt = new Cny_Model_Encryption();
		$password = $this->_getParam("password","");
		$id = $this->_getParam("ftp_id",0);

		$sql = $this->_db->quoteInto("SELECT f.$password FROM Ftps AS f WHERE f.id = ? ", $id);
		$password = $this->_db->fetchOne($sql);

		echo "<span onclick='selectNode(this)'>".$crypt->decrypt($this->_enc_key,$password)."</span>";
		exit;
	}

	function init()
	{
		$bootstrap = $this->getInvokeArg('bootstrap'); // gets the boostrapper
		$resource = $bootstrap->getPluginResource('multidb'); //multi db support
		$this->_db = $resource->getDefaultDb();

		$options = $bootstrap->getOptions();
		$this->view->enc_key = $this->_enc_key = $options['key']['encryption'];

		$this->view->placeholder('section')->set("ftp");
		$this->view->layout()->setLayout("cyber");

		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('portal'));
		if(!$auth->hasIdentity()){
			$auth->clearIdentity();
			$this->_redirect('/auth');
		}else{
			$this->view->user = $this->_user = $auth->getIdentity();
			$this->view->placeholder('logged_in')->set(true);

			if ($this->_user->cyber_user != "yes") {
				$auth->clearIdentity();
				$this->_redirect('/auth');
			}
		}

		$subSectionMenu = '<li id="subnav-viewftp"><a href="/ftp"><span class="subnav-size">View FTPs</span></a></li>
							<li id="subnav-addftp"><a href="/ftp/add"><span class="subnav-size">Add FTP</span></a></li>
							';
		$this->view->placeholder("subSectionMenu")->set($subSectionMenu);
	}
}

<?php
class RequestController extends Cny_Controller_LayoutAction
{
	public function scheduleAction()
	{
		$code = base64_decode(urldecode($this->_getParam("c","")));
		list($status,$id) = explode("||",$code);

		$data['status'] = $status;
		$data['modified'] = new Zend_Db_Expr("NOW()");

		$this->_db->update("schedule_requests",$data,"id=$id");

		$request = $this->_db->fetchRow("SELECT * FROM schedule_requests WHERE id = $id");

		if ($status == 'approved') {
			//add to calendar
			$cal = array("user_id"=>$request['user_id'],"start"=>$request['start_date'],"end"=>$request['end_date'],
				"title"=>$request['title'],"type"=>"vacation","created"=>$data['modified'],"modified"=>$data['modified']);
			$this->_db->insert("calendar_entries",$cal);

			$user = $this->_db->fetchRow("SELECT * FROM Users WHERE ID = {$request['user_id']}");

			define( 'newline', "<br/>" );
			//email to notify
			$mail_message = "Your change of schedule request has been $status".newline;
			$mail_message .= "Dates: ".date("F d Y",strtotime($request['start_date']))." - ".date("F d Y",strtotime($request['end_date'])).newline;
			$mail_message .= "Title: {$request['title']}".newline;
			$mail_message .= "Description: {$request['description']}".newline.newline;

			$mail = new Zend_Mail();
			$mail->setBodyHtml($mail_message);
			$mail->setFrom('do-not-reply@cyber-ny.com', "Schedule Request Response");

			//if not production only email support@cyber-ny.com
			if(APPLICATION_ENV != 'production') {
				$mail->addTo("support@cyber-ny.com", "cyber support");
				//$mail->addTo("craig@cyber-ny.com");
			}else {
				$mail->addTo($user['Email'],"{$user['FirstName']} {$user['LastName']}");
			}
			$mail->setSubject("Schedule Change Request Response");
			$mail->send();
		}

		echo "change of schedule request $status";
		exit;
	}

	public function taskAction()
	{
		$code = base64_decode(urldecode($this->_getParam("c","")));
		list($status,$id) = explode("||",$code);

		$data['status'] = $status;
		$data['modified'] = new Zend_Db_Expr("NOW()");

		$sql = "SELECT trc.*, p.Title, u.Email, ua.FirstName, ua.LastName FROM task_change_requests AS trc LEFT JOIN Projects AS p ON trc.task_id = p.id
				LEFT JOIN Users AS ua ON p.assigner = ua.id
				LEFT JOIN Users as u ON p.UserID = u.id WHERE trc.id = $id";
		$request = $this->_db->fetchRow($sql);

		$this->_db->update("task_change_requests",$data,"id=$id");

		define( 'newline', "<br/>" );

		if ($status == "approved") {
			$this->_db->update("Projects",array("ExpectedEndDate"=>$request['new_date']),"id={$request['task_id']}");

			$mail_message = date("g:h A, n/d/y").newline.newline;
			$mail_message .= "<strong>Approval Request for Task <a href='http://{$this->_domain}/pm/view/id/{$request['task_id']}'>{$request['Title']}</a></strong>".newline.newline;
			$mail_message .= "The task assignment date change has been <span style='color:green;font-weight:bold;'>Approved</span>".newline.newline;
			$mail_message .= "Thank You!".newline;
			$mail_message .= "Cyber-NY Project Manager";

			$mail = new Zend_Mail();
			$mail->setBodyHtml($mail_message);

			//if not production only email support@cyber-ny.com
			if(APPLICATION_ENV != 'production') {
				$mail->setFrom('do-not-reply@cnydevzone3.com');
				$mail->addTo("support@cyber-ny.com", "cyber support");
				//$mail->addTo("craig@cyber-ny.com");
			}else {
				$mail->setFrom('do-not-reply@cyber-ny.com');
				$mail->addTo("{$request['Email']}");
			}
			$mail->setSubject("Request approved for task: {$request['Title']}");
			$mail->send();

			echo "Task due date has been changed and user notified";
		}elseif ($status == "denied") {
			$mail_message = date("g:h A, n/d/y").newline.newline;
			$mail_message .= "<strong>Approval Request for Task <a href='http://{$this->_domain}/pm/view/id/{$request['task_id']}'>{$request['Title']}</a></strong>".newline.newline;
			$mail_message .= "<span style='color:red;font-weight:bold;'>Sorry, this task is a priority.</span>".newline.newline;
			$mail_message .= "Please contact {$request['FirstName']} {$request['LastName']} to reschedule or contact another developer for coverage.".newline.newline;
			$mail_message .= "Thank You!".newline;
			$mail_message .= "Cyber-NY Project Manager";

			$mail = new Zend_Mail();
			$mail->setBodyHtml($mail_message);;

			//if not production only email support@cyber-ny.com
			if(APPLICATION_ENV != 'production') {
				$mail->setFrom('do-not-reply@cnydevzone3.com');
				$mail->addTo("support@cyber-ny.com", "cyber support");
				//$mail->addTo("craig@cyber-ny.com");
			}else {
				$mail->setFrom('do-not-reply@cyber-ny.com');
				$mail->addTo("{$request['Email']}");
			}
			$mail->setSubject("Request denied for task: {$request['Title']}");
			$mail->send();

			echo "Task due date change has been denied. Please contact user and explain";
		}

		exit;
	}

	function init()
	{
		$bootstrap = $this->getInvokeArg('bootstrap'); // gets the boostrapper
		$resource = $bootstrap->getPluginResource('multidb'); //multi db support
		$this->_db = $resource->getDefaultDb();

		$options = $bootstrap->getOptions();
		$this->_domain = $options['site']['domain'];

		$this->view->layout()->disableLayout();
	}
}
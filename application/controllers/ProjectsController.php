<?php
class ProjectsController extends Cny_Controller_LayoutAction
{
    public function indexAction()
	{
		$this->_redirect("/projects/list");
		/*
		$this->view->placeholder('sub_section')->set("viewprojects");

		$search = new Zend_Session_Namespace('proejct_client_search');
    	$mask = $relationship = "";
    	$mask = $this->view->mask = $this->_getParam("mask","");

		$page = $this->getRequest()->getParam('page',1);
		$this->view->dir = $dir = $this->_getParam('dir','ASC');
		$this->view->sort = $sort = $this->_getParam('sort','ClientName');

		$select = $this->_db->select();
		$select->from(array("cp"=>"client_projects"), '*');
		$select->joinLeft(array("c"=>"Clients"), "c.id = cp.client_id", array('ClientName'));
		$select->group("c.ClientName");
		$select ->order(array("$sort $dir"));

		if ($mask) {
			$select->where("cp.name LIKE '%$mask%' OR c.ClientName = '$mask' ");
		}

		$paginator = Zend_Paginator::factory($select);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(50);
		$this->view->clients = $paginator;

		$this->view->messages = $this->_flashMessenger->getMessages();
		*/
	}

	public function listAction()
	{
		$this->view->placeholder('sub_section')->set("viewprojects");

		$search = new Zend_Session_Namespace('proejcts_search');
    	$mask = $relationship = "";
    	$mask = $this->view->mask = $this->_getParam("mask","");

		$page = $this->getRequest()->getParam('page',1);
		$this->view->dir = $dir = $this->_getParam('dir','ASC');
		$this->view->sort = $sort = $this->_getParam('sort','ClientName');

		$select = $this->_db->select();
		$select->from(array("cp"=>"client_projects"), '*');
		$select->joinLeft(array("c"=>"Clients"), "c.id = cp.client_id", array('ClientName',
						"hours"=>new Zend_Db_Expr("(SELECT ABS(SUM(pa.EndTime - pa.StartTime)) FROM Projects p LEFT JOIN ProjectActivity pa ON p.id = pa.ProjectID WHERE p.client_project_id = cp.id GROUP BY p.client_project_id)"))
						);
		$select->joinLeft(array("cc"=>"client_project_status"), "cc.id = cp.current_status_id", array('current'=>'name'));
		$select->joinLeft(array("cs"=>"client_project_status"), "cs.id = cp.scheduled_status_id", array('scheduled'=>'name'));
		$select->where("cp.current_status_id <> 9");
		$select ->order(array("$sort $dir"));

		if ($mask) {
			$select->where("cp.name LIKE '%$mask%' OR c.ClientName = '$mask' ");
		}

		$paginator = Zend_Paginator::factory($select);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(50);
		$this->view->clients = $paginator;

		$sql = "SELECT id, name FROM client_project_status ORDER BY zorder ASC";
		$this->view->project_statuses = $this->_db->fetchPairs($sql);

		$this->view->messages = $this->_flashMessenger->getMessages();
	}

	public function archiveAction()
	{
		$this->view->placeholder('sub_section')->set("archiveprojects");

		$search = new Zend_Session_Namespace('proejcts_search');
    	$mask = $relationship = "";
    	$mask = $this->view->mask = $this->_getParam("mask","");

		$page = $this->getRequest()->getParam('page',1);
		$this->view->dir = $dir = $this->_getParam('dir','ASC');
		$this->view->sort = $sort = $this->_getParam('sort','ClientName');

		$select = $this->_db->select();
		$select->from(array("cp"=>"client_projects"), '*');
		$select->joinLeft(array("c"=>"Clients"), "c.id = cp.client_id", array('ClientName',
						"hours"=>new Zend_Db_Expr("(SELECT ABS(SUM(pa.EndTime - pa.StartTime)) FROM Projects p LEFT JOIN ProjectActivity pa ON p.id = pa.ProjectID WHERE p.client_project_id = cp.id GROUP BY p.client_project_id)"))
						);
		$select->joinLeft(array("cc"=>"client_project_status"), "cc.id = cp.current_status_id", array('current'=>'name'));
		$select->joinLeft(array("cs"=>"client_project_status"), "cs.id = cp.scheduled_status_id", array('scheduled'=>'name'));
		$select->where("cp.current_status_id = 9");
		$select ->order(array("$sort $dir"));

		if ($mask) {
			$select->where("cp.name LIKE '%$mask%' OR c.ClientName = '$mask' ");
		}

		$paginator = Zend_Paginator::factory($select);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(50);
		$this->view->clients = $paginator;

		$sql = "SELECT id, name FROM client_project_status ORDER BY zorder ASC";
		$this->view->project_statuses = $this->_db->fetchPairs($sql);

		$this->view->messages = $this->_flashMessenger->getMessages();
	}

	public function statusAction()
	{
		$status = $this->_getParam("status","");
		$ids = $this->_getParam("id",0);
		$return = $this->_getParam("return","index");

		if (!is_array($ids)) $ids = array($ids);

		foreach ($ids as $k=>$id) {
			$data['current_status_id'] = $status;

			$this->_db->update("client_projects",$data,"id=$id");
		}
		$this->_redirect("/projects/$return");
	}

	public function viewAction()
	{
		$id = $this->_getParam("id",0);

		//project details
		$sql = $this->_db->quoteInto("SELECT cp.*, c.ClientName
				FROM  client_projects AS cp
				LEFT JOIN Clients AS c ON cp.client_id = c.id
				WHERE cp.id = ?", $id);
		$this->view->project = $this->_db->fetchRow($sql);
	}

	public function overviewAction()
	{
		$this->view->layout()->disableLayout();
		$id = $this->_getParam("id",0);

		//project details
		$sql = $this->_db->quoteInto("SELECT cp.*, cc.name AS current, cs.name AS scheduled, ct.name AS type, plu.UserName AS project_lead
				FROM  client_projects AS cp
				LEFT JOIN client_project_status AS cc ON cc.id = cp.current_status_id
				LEFT JOIN client_project_status AS cs ON cs.id = cp.scheduled_status_id
				LEFT JOIN client_project_types AS ct ON ct.id = cp.client_project_type_id
				LEFT JOIN Users AS plu ON cp.project_lead_id = plu.id
				WHERE cp.id = ?", $id);
		$this->view->project = $this->_db->fetchRow($sql);
	}

	public function detailsAction()
	{
		$this->view->layout()->disableLayout();
		$id = $this->_getParam("id",0);

		//project details
		$sql = $this->_db->quoteInto("SELECT cp.*
				FROM  client_projects AS cp
				WHERE cp.id = ?", $id);
		$this->view->project = $this->_db->fetchRow($sql);
	}

	public function technicalAction()
	{
		$this->view->layout()->disableLayout();
		$id = $this->_getParam("id",0);

		//project details
		$sql = $this->_db->quoteInto("SELECT cp.*
				FROM  client_projects AS cp
				WHERE cp.id = ?", $id);
		$this->view->project = $this->_db->fetchRow($sql);

		//ftp info
		$sql = $this->_db->quoteInto("SELECT f.*, s.owned, s.host, s.name AS server FROM Ftps AS f LEFT JOIN Servers AS s ON f.serverId = s.id LEFT JOIN client_projects AS cp ON f.id = cp.ftp_id WHERE cp.id = ?", $id);
		$this->view->ftp = $this->_db->fetchRow($sql);

		//servers
		$sql = $this->_db->quoteInto("SELECT s.* FROM Servers AS s LEFT JOIN client_projects AS cp ON s.id = cp.server_id WHERE cp.id = ?", $id);
		$this->view->server = $this->_db->fetchRow($sql);
	}

	public function tasksAction()
	{
		$this->view->layout()->disableLayout();
		$id = $this->_getParam("id",0);

		//get client info
		$sql = $this->_db->quoteInto("SELECT id, client_id FROM client_projects WHERE id = ?",$id);
		$this->view->project = $this->_db->fetchRow($sql);

		//tasks
		$select = $this->_db->select();
		$select->from(array("p"=>"Projects"),array("id","Title","RetainerStatus","EntryDate","StartDate","ExpectedEndDate","ActualEndDate","LastActivityDate","ticket_id","UserID","Description",
					"TotalTime"=>new Zend_Db_Expr("ABS( SUM(EndTime - StartTime) )")));
		$select->joinLeft(array("u"=>"Users"), "p.UserID = u.id", "UserName");
		$select->joinLeft(array("pri"=>"Priority"), "p.ProjectPriorityID = pri.id", "PriorityName");
		$select->joinLeft(array("ps"=>"ProjectStatus"), "p.ProjectStatusID = ps.id", array("ProjectStatusName",
				"StatusRank"=>new Zend_Db_Expr("(CASE ps.ProjectStatusName WHEN 'Open' THEN 1
						WHEN 'On Client' THEN 2 WHEN 'Completed' THEN 3 WHEN 'Closed' THEN 4 END)")));
		$select->joinLeft(array("pa"=>"ProjectActivity"), "p.id = pa.ProjectID", array("ClockStatus"=>"( IF( COUNT(pa.StartTime) > COUNT(pa.EndTime), 0, 1 ) )"));

		$select->group(array("p.id", "p.Title", "p.EntryDate", "p.StartDate", "p.ExpectedEndDate", "p.ActualEndDate", "p.LastActivityDate", "p.Description", "pri.PriorityName", "ps.ProjectStatusName", "u.UserName"));
		$select->where("p.client_project_id = $id");
		$select->order(array("StatusRank ASC","StatusRank ASC","p.ExpectedEndDate ASC"));

		$this->view->tasks = $this->_db->fetchAssoc($select->__toString());
	}

	public function conceptsAction()
	{
		$this->view->layout()->disableLayout();
		$id = $this->_getParam("id",0);

		//concepts
		$select = $this->_db->select();
		$select->from(array("p"=>"concept_projects"));
		$select->joinLeft(array("cp"=>"client_projects"), "p.client_project_id = cp.id","");
		$select->joinLeft(array("c"=>"concepts"), "p.id = c.concept_project_id");
		$select->where("cp.id = $id");

		$this->view->concepts = $this->_db->fetchAssoc($select->__toString());
	}

	public function staffAction()
	{
		$this->view->layout()->disableLayout();
		$this->view->id = $id = $this->_getParam("id",0);

		//staff assigned to project
		$sql = $this->_db->quoteInto("SELECT cpne.id, cpne.role, CONCAT(u.FirstName, ' ', u.LastName) AS name FROM client_project_notification_emails AS cpne, Users AS u WHERE cpne.userID = u.ID AND cpne.client_project_id =?", $id);
		$this->view->staff = $this->_db->fetchAssoc($sql);
	}

	public function addstaffAction()
	{
		$this->_helper->layout()->setLayout('modal');
		$this->view->client_project_id = $client_project_id = $this->_getParam('client_project_id', 0);

		$sql = "SELECT ID, CONCAT(FirstName, ' ', LastName) AS name FROM Users WHERE Disabled = 'no' ORDER BY LastName ASC";
		$this->view->staff = $this->_db->fetchPairs($sql);

		if( $this->getRequest()->isPost() ){
			$data = $this->_getParam('data', array());
			$data['client_project_id'] = $client_project_id;

			$this->_db->insert('client_project_notification_emails', $data);

			$this->_redirect("/projects/view/id/$client_project_id/#staff");
		}
	}

	public function deletestaffAction()
	{
		$id = $this->_getParam('id', 0);
		$client_project_id = $this->_getParam('client_project_id', 0);
		$this->_db->delete('client_project_notification_emails', $this->_db->quoteInto('id=?', $id));
		$this->_redirect("/projects/view/id/$client_project_id/#staff");
	}


	public function addAction()
	{
		$this->view->placeholder('sub_section')->set("addproject");

		$sql = "SELECT id, ClientName FROM Clients WHERE Disabled = 'no' ORDER BY ClientName ASC";
		$this->view->clients = array(""=>"Select Client")+$this->_db->fetchPairs($sql);

		$sql = "SELECT id, name FROM Servers ORDER BY name ASC";
		$this->view->servers = array(""=>"Select Server")+$this->_db->fetchPairs($sql);

		$sql = "SELECT id, website FROM Ftps ORDER BY website ASC";
		$this->view->ftps = array(""=>"Select FTP")+$this->_db->fetchPairs($sql);

		$sql = "SELECT id, name FROM client_project_status ORDER BY zorder ASC";
		$this->view->status = array(""=>"Select Status")+$this->_db->fetchPairs($sql);

		$sql = "SELECT id, name FROM client_project_types ORDER BY zorder ASC";
		$this->view->types = array(""=>"Select Project Type")+$this->_db->fetchPairs($sql);

		$sql = "SELECT id, UserName FROM Users AS u WHERE Disabled = 'no' ORDER BY u.UserName ASC";
		$this->view->staff = array(""=>"Select Project Lead")+$this->_db->fetchPairs($sql);

		if( $this->getRequest()->isPost() ){
			$data = $this->_getParam('data',array());
			$data['created'] = new Zend_Db_Expr("NOW()");
			$data['modified'] = new Zend_Db_Expr("NOW()");

			if ($data['start_date']) $data['start_date'] = date("Y-m-d",strtotime($data['start_date']));
			else $data['start_date'] = new Zend_Db_Expr("NULL");

			if ($data['completion_date']) $data['completion_date'] = date("Y-m-d",strtotime($data['completion_date']));
			else $data['completion_date'] = new Zend_Db_Expr("NULL");

			$this->_db->insert("client_projects",$data);
			$id = $this->_db->lastInsertId();

			$this->_redirect("/projects/autotasks/id/$id");
		}
	}

	public function editAction()
	{
		$id = $this->_getParam("id",0);

		$sql = $this->_db->quoteInto("SELECT * FROM  client_projects WHERE id = ?", $id);
		$this->view->project = $this->_db->fetchRow($sql);

		$sql = "SELECT id, ClientName FROM Clients WHERE Disabled = 'no' ORDER BY ClientName ASC";
		$this->view->clients = array(""=>"Select Client")+$this->_db->fetchPairs($sql);

		$sql = "SELECT id, name FROM Servers ORDER BY name ASC";
		$this->view->servers = array(""=>"Select Server")+$this->_db->fetchPairs($sql);

		$sql = "SELECT id, website FROM Ftps ORDER BY website ASC";
		$this->view->ftps = array(""=>"Select FTP")+$this->_db->fetchPairs($sql);

		$sql = "SELECT id, name FROM client_project_status ORDER BY zorder ASC";
		$this->view->status = array(""=>"Select Status")+$this->_db->fetchPairs($sql);

		$sql = "SELECT id, name FROM client_project_types ORDER BY zorder ASC";
		$this->view->types = array(""=>"Select Project Type")+$this->_db->fetchPairs($sql);

		$sql = "SELECT id, UserName FROM Users AS u WHERE Disabled = 'no' ORDER BY u.UserName ASC";
		$this->view->staff = array(""=>"Select Project Lead")+$this->_db->fetchPairs($sql);

		if( $this->getRequest()->isPost() ){
			$data = $this->_getParam('data',array());
			$data['modified'] = new Zend_Db_Expr("NOW()");

			if ($data['start_date']) $data['start_date'] = date("Y-m-d",strtotime($data['start_date']));
			else $data['start_date'] = new Zend_Db_Expr("NULL");

			if ($data['completion_date']) $data['completion_date'] = date("Y-m-d",strtotime($data['completion_date']));
			else $data['completion_date'] = new Zend_Db_Expr("NULL");

			$this->_db->update("client_projects",$data, "id=$id");

			$this->_redirect("/projects/view/id/$id");
		}
	}

	public function deleteAction()
	{
		$id = $this->_getParam("id",0);

		$this->_db->delete("client_projects", "id = $id");

		$this->_redirect("/projects");
	}

	public function autotasksAction()
	{
		$id = $this->_getParam("id",0);

		$sql = $this->_db->quoteInto("SELECT * FROM client_projects WHERE id = ?",$id);
		$this->view->project = $project = $this->_db->fetchRow($sql);

		$sql = "SELECT id, UserName FROM Users WHERE Disabled = 'no' ORDER BY UserName ASC";
		$this->view->staff = array(""=>"Select Staff")+$this->_db->fetchPairs($sql);

		$sql = "SELECT t.*, s.name AS status_section, s.color AS status_color
				FROM client_project_auto_tasks AS t, client_project_status AS s
				WHERE t.client_project_status_id = s.id AND t.client_project_type_id = {$project['client_project_type_id']}
				ORDER BY s.zorder ASC, t.days_from_start ASC";
		$this->view->tasks = $this->_db->fetchAssoc($sql);

		if (!$this->view->tasks) {
			$this->_redirect("/projects");
		}
	}

	public function autoaddAction()
	{
		if( $this->getRequest()->isPost() ){
			$id = $this->_getParam("id",0);
			$tasks = $this->_getParam('data',array());

			$sql = $this->_db->quoteInto("SELECT * FROM client_projects WHERE id = ?",$id);
			$project = $this->_db->fetchRow($sql);

			$data['RetainerStatus'] = 'n';
			$data['EntryDate'] = time();
			$data['StartDate'] = ($project['start_date'])?strtotime($project['start_date']):time();
			$data['ProjectPriorityID'] = 1;
			$data['ProjectStatusID'] = 1;
			$data['ClientID'] = $project['client_id'];
			$data['assigner'] = $this->_user->Cyberny_Users_ID;
			$data['client_project_id'] = $id;

			foreach ($tasks as $auto_id => $task) {
				if (isset($task['active']) && $task['active'] == "on") {
					$sql = "SELECT * FROM client_project_auto_tasks WHERE id = $auto_id";
					$auto_task = $this->_db->fetchRow($sql);

					$data['Title'] = $auto_task['title'];
					$data = array_merge($data,$task);

					if (isset($data['ExpectedEndDate']) && $data['ExpectedEndDate']) $data['ExpectedEndDate'] = strtotime($data['ExpectedEndDate']);
					if (!$data['UserID']) $data['UserID'] = 14; // "unasigned" user id
					unset($data['active']);

					$this->_db->insert("Projects",$data);
					$id = $this->_db->lastInsertId();

					/*
					if ($data['UserID'] != $this->_user->Cyberny_Users_ID && $this->_user->Cyberny_Users_ID) {
						$project = new Cny_Model_Project();
						$project->emailTask($id);
					}
					*/
				}
			}
		}
		$this->_redirect("/projects");
	}

	public function calAction()
	{
		$this->view->layout()->disableLayout();
		$month = $this->_getParam("month",date("n"));
		$year = $this->_getParam("year",date("Y"));

		// draw table
		$calendar = '<table cellpadding="0" cellspacing="0" class="calendar">';

		// create title row
		$calendar .= '<tr><th colspan="7" class="calendar-title">
						<div id="prev_month" onclick="drawCalendar('.date("n,Y",mktime(0,0,0,$month-1,1,$year)).')">'.date("F",mktime(0,0,0,$month-1,1,$year)).'</div>
						'.date("F Y",mktime(0,0,0,$month,1,$year)).'
						<div id="next_month" onclick="drawCalendar('.date("n,Y",mktime(0,0,0,$month+1,1,$year)).')">'.date("F",mktime(0,0,0,$month+1,1,$year)).'</div>
					</th></tr>';

		// table headings
		$headings = array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
		$calendar.= '<tr class="calendar-row"><td class="calendar-day-head">'.implode('</td><td class="calendar-day-head">',$headings).'</td></tr>';

		// days and weeks vars now ...
		$running_day = date('w',mktime(0,0,0,$month,1,$year));
		$days_in_month = date('t',mktime(0,0,0,$month,1,$year));
		$days_in_this_week = 1;
		$day_counter = 0;
		$dates_array = array();

		// row for week one
		$calendar.= '<tr class="calendar-row">';

		// print "blank" days until the first of the current week
		for($x = 0; $x < $running_day; $x++):
			$calendar.= '<td class="calendar-day-np">&nbsp;</td>';
			$days_in_this_week++;
		endfor;

		// keep going with days....
		for($list_day = 1; $list_day <= $days_in_month; $list_day++):
			$calendar.= '<td class="calendar-day" valign="top">';
				// add in the day number
				$calendar.= '<div class="day-number">'.$list_day.'</div>';

				$calendar.= '<div class="day-info" id="day_'.$list_day.'_'.$month.'_'.$year.'">&nbsp;</div>';

			$calendar.= '</td>';
			if($running_day == 6):
				$calendar.= '</tr>';
				if(($day_counter+1) != $days_in_month):
					$calendar.= '<tr class="calendar-row">';
				endif;
				$running_day = -1;
				$days_in_this_week = 0;
			endif;
			$days_in_this_week++; $running_day++; $day_counter++;
		endfor;

		// finish the rest of the days in the week
		if($days_in_this_week < 8 && $days_in_this_week > 1):
			for($x = 1; $x <= (8 - $days_in_this_week); $x++):
				$calendar.= '<td class="calendar-day-np">&nbsp;</td>';
			endfor;
		endif;

		// final row
		$calendar.= '</tr>';

		// end the table
		$calendar.= '</table>';

		//hidden fields that store current calendar month and year (used for simple JS updates)
		$calendar.= '<input type="hidden" id="cal_month" value="'.$month.'"/><input type="hidden" id="cal_year" value="'.$year.'"/>';

		echo $calendar; exit;
	}

	function init()
	{
		$bootstrap = $this->getInvokeArg('bootstrap'); // gets the boostrapper
		$resource = $bootstrap->getPluginResource('multidb'); //multi db support
		$this->_db = $resource->getDefaultDb();

		$options = $bootstrap->getOptions();
		$this->view->enc_key = $this->_enc_key = $options['key']['encryption'];

		$this->view->placeholder('section')->set("pm");
		$this->view->layout()->setLayout("cyber");

		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('portal'));
		if(!$auth->hasIdentity()){
			$auth->clearIdentity();
			$this->_redirect('/auth');
		}else{
			$this->view->user = $this->_user = $auth->getIdentity();
			$this->view->placeholder('logged_in')->set(true);

			if ($this->_user->cyber_user != "yes") {
				$auth->clearIdentity();
				$this->_redirect('/auth');
			}
		}

		$subSectionMenu = '<li id="subnav-myprojects"><a href="/pm"><span class="subnav-size">My Tasks</span></a></li>
							<li id="subnav-allprojects"><a href="/pm/all"><span class="subnav-size">All Tasks</span></a></li>
							<li id="subnav-addproject"><a href="/pm/add"><span class="subnav-size">Add Task</span></a></li>
							<li id="subnav-pmarchive"><a href="/pm/archive"><span class="subnav-size">Archive</span></a></li>
							<li id="subnav-productionschedule"><a href="/pm/productionschedule"><span class="subnav-size">Production Schedule</span></a></li>
							<li id="subnav-dailyschedule"><a href="/pm/dailyschedule"><span class="subnav-size">Daily Schedule</span></a></li>
							<li id="subnav-viewprojects"><a href="/projects"><span class="subnav-size">View Projects</span></a></li>
							<li id="subnav-archiveprojects"><a href="/projects/archive"><span class="subnav-size">Project Archive</span></a></li>
							<li id="subnav-addprojects"><a href="/projects/add"><span class="subnav-size">Add Project</span></a></li>
							';
		$this->view->placeholder("subSectionMenu")->set($subSectionMenu);

		$this->view->languages = array(""=>"None","PHP"=>"PHP",".NET"=>".NET","ASP"=>"ASP","Other"=>"Other");
		$this->view->databases = array(""=>"None","MySQL"=>"MySQL","MS SQL"=>"MS SQL","Access"=>"Access","Other"=>"Other");
		$this->view->frameworks = array(""=>"None","Zend"=>"Zend",".NET"=>".NET","Cake"=>"Cake","Magento"=>"Magento","Commerce Server"=>"Commerce Server","Other"=>"Other");
		$this->view->cmses = array(""=>"None","Drupal"=>"Drupal","Joomla"=>"Joomla","Wordpress"=>"Wordpress","CNY Content Manager"=>"CNY Content Manager","Other"=>"Other");
		$this->view->cc_processing = array(""=>"None","Paypal Basic"=>"Paypal Basic","Paypal Pro"=>"Paypal Pro","Payflow Pro"=>"Payflow Pro","Authorize.net"=>"Authorize.net","Other"=>"Other");
		$this->view->hosting = array(""=>"None","Shared"=>"Shared","Dedicated"=>"Dedicated","Cloud"=>"Cloud");
	}
}

DEFINE ('ONEDAY', 60*60*24);
function dueDate($days_from_start, $startDate) {
	// ESTABLISH THE HOLIDAYS
	$holidays = array
	(
		//New Years
		(date("w",strtotime("January 1"))==0)?"January 2":"January 1",
		//MLK Day
		date("F d",strtotime("2 weeks monday",mktime(0,0,0,1,1,date("Y")))),
		//Memorial Day
		date("F d",strtotime("last monday",mktime(0,0,0,6,1,date("Y")))),
		//July 4
		(date("w",strtotime("July 4"))==0)?"July 5":(date("w",strtotime("July 4"))==6)?"July 3":"July 4",
		//Labor Day
		date("F d",strtotime("monday",mktime(0,0,0,9,1,date("Y")))),
		//Thanksgiving
		date("F d",strtotime("3 weeks thursday",mktime(0,0,0,11,1,date("Y")))),
		date("F d",strtotime("3 weeks thursday",mktime(0,0,0,11,1,date("Y"))+ONEDAY)),
		//Christmas
		(date("w",strtotime("December 25"))==0)?"December 26":(date("w",strtotime("December 25"))==6)?"December 24":"December 25",
		//Check if New Years celebrated in this calendar year
		(date("w",strtotime("January 1 ".date("Y")+1))==6)?"December 31":""
	);

	// CONVERT HOLIDAYS TO ISO DATES
	foreach ($holidays as $x => $holiday) {
		$holidays[$x] = date('Y-m-d', strtotime($holiday));
	}

	// GET NUMBER OF WEEKEND DAYS/HOLIDAYS BETWEEN START AND DATE
	$t_start = strtotime($startDate);
	$t_end = strtotime($startDate)+(ONEDAY * $days_from_start);
	for($day_val = $t_start; $day_val <= $t_end; $day_val+=ONEDAY){
		$iso_date = date('Y-m-d', $day_val);

		if (date("N", $day_val) > 5) $days_from_start++;
		if (in_array($iso_date, $holidays)) $days_from_start++;
	}

	// ITERATE OVER THE DAYS
	$result = false;
	while (!$result) {
		$continue = 0;
		$start = strtotime($startDate)+(ONEDAY * $days_from_start);

		// ELIMINATE WEEKENDS - SAT AND SUN
		$weekday = date('D', $start);
		if (substr($weekday,0,1) == 'S') $continue = 1;

		// ELIMINATE HOLIDAYS
		$iso_date = date('Y-m-d', $start);
		if (in_array($iso_date, $holidays)) $continue = 1;

		if ($continue == 1) {
			$days_from_start++;
		}else {
			$result = true;
		}
	}

	return date("n/d/Y",$start);
}

function sec2hm ($sec, $padHours = false) {
	$hms = "";
	$hours = intval(intval($sec) / 3600);

	$hms .= ($padHours) ? str_pad($hours, 2, "0", STR_PAD_LEFT). 'h ' : $hours. 'h ';

	$minutes = intval(($sec / 60) % 60);

	$hms .= str_pad($minutes, 2, "0", STR_PAD_LEFT)."m";

	return $hms;
}
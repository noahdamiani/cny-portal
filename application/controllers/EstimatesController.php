<?php
class EstimatesController extends Cny_Controller_LayoutAction
{
	public function indexAction()
	{
		if ($this->_user->cyber_user != "yes") {
			$this->_redirect("/estimates/list");
		}

		$this->view->placeholder('sub_section')->set("estimates");

		$search = new Zend_Session_Namespace('estimate_client_search');
		$mask = $relationship = "";

		$page = $this->getRequest()->getParam('page',1);
		$this->view->dir = $dir = $this->_getParam('dir','ASC');
		$this->view->sort = $sort = $this->_getParam('sort','ClientName');

		$lastday = date('t');
		$from_time = strtotime(date("Y-m-01"));
		$to_time = strtotime(date("Y-m-$lastday")." 23:59:59");

		$select = $this->_db->select();
		$select->from(array("er"=>"estimate_requests"), array('status'));
		$select->joinLeft(array("c"=>"Clients"), "c.id = er.client_id", array('id','ClientName'));
		$select->where("(er.status <>'approved') ");
		$select->group("c.ClientName");
		$select ->order(array("$sort $dir"));

		$paginator = Zend_Paginator::factory($select);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(50);
		$this->view->clients = $paginator;
	}

	public function listAction()
	{
		$this->view->placeholder('sub_section')->set("estimates");

		$this->view->clientID = $this->_clientID;
		$search = new Zend_Session_Namespace('estimate_search');
		$mask = $relationship = "";
		$mask = $this->view->mask = $this->_getParam("mask","");

		$page = $this->getRequest()->getParam('page',1);
		$this->view->dir = $dir = $this->_getParam('dir','ASC');
		$this->view->sort = $sort = $this->_getParam('sort','er.status');

		$select = $this->_db->select();
		$select->from(array("er"=>"estimate_requests"), array('id', 'summary', 'status', 'client_id','modified','created'));
		$select->joinLeft(array("u"=>"Users_BugTracker"), "er.created_user_id = u.ID", array("UserName"=>new Zend_Db_Expr("CONCAT(u.FirstName, ' ', SUBSTRING(u.LastName,1,1),'.')")));
		$select->where("(er.client_id =".$this->_clientID." AND er.status <>'approved')");
		$select ->order(array("$sort $dir"));

		if ($mask) {
			$select->where("er.summary LIKE '%$mask%' OR er.id = '$mask' ");
		}

		$paginator = Zend_Paginator::factory($select);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(50);
		$this->view->estimates = $paginator;

		$this->view->clientName = $this->_db->fetchOne($this->_db->quoteInto("SELECT ClientName FROM Clients WHERE id = ?",$this->_clientID));

		$this->view->statuses = array('requested'=>"Estimate Requested", 'submitted'=>"Submitted to Client", 'approved'=>'Approved');
	}

	public function archiveAction()
	{
		$this->view->placeholder('sub_section')->set("estimates");

		$search = new Zend_Session_Namespace('estimate_archive_search');
		$mask = $relationship = "";

		$page = $this->getRequest()->getParam('page',1);
		$this->view->dir = $dir = $this->_getParam('dir','DESC');
		$this->view->sort = $sort = $this->_getParam('sort','er.created');

		$mask = $date_to = $date_from = "";

		if ($search->mask != '') $this->view->mask = $mask = $search->mask;
		if ($search->date_from != '') $this->view->date_from = $date_from = $search->date_from;
		if ($search->date_to != '') $this->view->date_to = $date_to = $search->date_to;

		if( $this->getRequest()->isPost() ){
			$this->view->mask = $mask = $this->_getParam('mask','');
			$this->view->date_from = $date_from = $this->_getParam('date_from','');
			$this->view->date_to = $date_to = $this->_getParam('date_to','');

			if ($mask == '' && $date_from == '' && $date_to == '') {
				$search->setExpirationSeconds(1);
			}
		}

		$select = $this->_db->select();
		$select->from(array("er"=>"estimate_requests"), array('id', 'summary', 'status', 'client_id','modified','created'));
		$select->joinLeft(array("u"=>"Users_BugTracker"), "er.created_user_id = u.ID", array("UserName"=>new Zend_Db_Expr("CONCAT(u.FirstName, ' ', SUBSTRING(u.LastName,1,1),'.')")));
		$select->where("(er.client_id =".$this->_clientID." AND er.status = 'approved')");
		$select ->order(array("$sort $dir"));

		if ($mask) {
			$search->mask = $mask;
			$select->where("er.summary LIKE '%$mask%' OR er.id = '$mask' OR u.FirstName LIKE '%$mask%'");
		}else {
			$search->mask = '';
		}
		if ($date_from) {
			if (!$date_to) $date_to = $date_from;

			$search->date_from = $date_from;
			$search->date_to = $date_to;

			$select->where("(er.created >= '".date("Y-m-d",strtotime($date_from))."' AND er.created <= '".date("Y-m-d",strtotime($date_to))."')");
		}else {
			$search->date_from = '';
			$search->date_to = '';
		}

		$paginator = Zend_Paginator::factory($select);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(50);
		$this->view->estimates = $paginator;
	}

    public function addAction()
    {
	    $this->_helper->layout()->setLayout("modal");

	    if( $this->getRequest()->isPost() ){
		    $data = $this->_getParam('data',array());
		    $data['status'] = "requested";
		    $data['created_user_id'] = $this->_user->ID;
		    if (!$this->_clientID) $this->_clientID = $data['client_id'];
		    $data['client_id'] = $this->_clientID;
		    $data['created'] = new Zend_Db_Expr("NOW()");
		    $data['modified'] = new Zend_Db_Expr("NOW()");

		    $this->_db->insert("estimate_requests",$data);
		    $id = $this->_db->lastInsertId();

		    $this->_redirect("/estimates/sent");
	    }
    }

	public function sentAction()
	{
		$this->_helper->layout()->setLayout("modal");
	}
	public function deleteAction()
	{
		$id = $this->_getParam("id",0);

		$this->_db->delete("estimate_requests", "id = $id");

		$this->_redirect("/estimates");
	}

	public function viewAction()
	{
		$id = $this->_getParam('id',0);

		$sql = $this->_db->quoteInto("SELECT t.*, c.ClientName FROM estimate_requests AS t LEFT JOIN Clients AS c ON t.client_id = c.id
				WHERE t.id = ? AND t.client_id = {$this->_clientID}",$id);
		$this->view->estimate = $this->_db->fetchRow($sql);

		$sql = $this->_db->quoteInto("SELECT tn.*, CONCAT(u.FirstName, ' ', u.LastName) AS name, u.Roles FROM estimate_request_notes AS tn LEFT JOIN Users_BugTracker AS u ON tn.created_user_id = u.ID
				WHERE tn.estimate_request_id = ? ORDER BY tn.created DESC",$id);
		$this->view->notes = $this->_db->fetchAssoc($sql);
	}

	public function editAction()
	{
		$this->_helper->layout()->setLayout("modal");
		$id = $this->_getParam("id",0);

		$sql = $this->_db->quoteInto("SELECT er.*, c.ClientName FROM estimate_requests AS er LEFT JOIN Clients AS c ON er.client_id = c.id WHERE er.id = ? AND er.client_id = {$this->_clientID}",$id);
		$this->view->estimate = $ticket = $this->_db->fetchRow($sql);

		$sql = "SELECT id, ClientName FROM Clients WHERE Disabled = 'no' ORDER BY ClientName ASC";
		$this->view->clients = array(""=>"Select Client")+$this->_db->fetchPairs($sql);

		if( $this->getRequest()->isPost() ){
			$data = $this->_getParam('data',array());
			$data['modified'] = new Zend_Db_Expr("NOW()");
			$data['clientID'] = $this->_clientID;

			$this->_db->update("estimate_requests",$data,"id=$id");

			$this->_redirect("/estimates/view/clientID/{$data['clientID']}/id/$id");
		}
	}

	public function addnoteAction()
	{
		$this->_helper->layout()->setLayout("modal");
		$ticket = new Cny_Model_Estimate();
		$this->view->id = $id = $this->_getParam('id',0);

		if( $this->getRequest()->isPost() ){
			$data['estimate_request_id'] = $id;
			$data['message'] = ($this->_getParam("Description",""));
			$data['created_user_id'] = $this->_user->ID;
			$data['type'] = 'User';
			$data['created'] = new Zend_Db_Expr("NOW()");
			$data['cc_list'] = str_replace(";",",",$this->_getParam("cc_list",""));

			$this->_db->insert("estimate_request_notes",$data);
			$note_id = $this->_db->lastInsertId();

			//upload attachment
			if ($_FILES['attachment']['name']) {
				$uploadpath = realpath('.').$this->_attachment_path;

				move_uploaded_file($_FILES['attachment']['tmp_name'], $uploadpath.'/'.$note_id."_".$_FILES['attachment']['name']);

				$this->_db->query("UPDATE estimate_request_notes SET attachment = '".$note_id."_".$_FILES['attachment']['name']."' WHERE id = $note_id");
			}

			$ticket->emailNote($note_id);

		}
	}

	public function updatenotesAction()
	{
		$id = $this->_getParam("estimate",0);
		$last_note = $this->_getParam("last_note",0);

		$sql = $this->_db->quoteInto("SELECT tn.*, CONCAT(u.FirstName, ' ', u.LastName) AS name, u.Roles FROM estimate_request_notes AS tn LEFT JOIN Users_BugTracker AS u ON tn.created_user_id = u.ID
				WHERE tn.estimate_request_id = ? AND tn.id > '$last_note' ORDER BY tn.created ASC",$id);
		$notes = $this->_db->fetchAssoc($sql);

		$items = array();
		foreach ($notes as $note) {
			$role_class = ($note['Roles'] == "administrator") ? "2" : "";

			$attachment = ($note['attachment'] && file_exists($_SERVER['DOCUMENT_ROOT'].$this->_attachment_path.$note['attachment']))?$this->_attachment_path.$note['attachment']:"";

			$items[] = array("id"=>$note['id'], "name"=>$note['name'], "created"=>date("n/j/y",strtotime($note['created'])), "time"=>date("g:ia",strtotime($note['created'])), "message"=>nl2br($note['message']), "attachment"=>$attachment, "role"=>$role_class);
		}

		$array['items'] = $items;

		echo Zend_Json_Encoder::encode($array);exit;
	}

	public function deletenoteAction()
	{
		$this->_helper->layout()->disableLayout();

		$id = $this->_getParam("note_id",0);

		$this->_db->delete("estimate_request_notes","id=$id");

		exit;
	}


	function init()
	{
		$bootstrap = $this->getInvokeArg('bootstrap'); // gets the boostrapper
		$resource = $bootstrap->getPluginResource('multidb'); //multi db support
		$this->view->db = $this->_db = $resource->getDefaultDb();

		$options = $bootstrap->getOptions();

		$this->view->placeholder('section')->set("estimates");

		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('portal'));
		if(!$auth->hasIdentity()){
			$auth->clearIdentity();
			$this->_redirect('/auth');
		}else{
			$this->view->user = $this->_user = $auth->getIdentity();
			$this->view->placeholder('logged_in')->set(true);
		}

		if ($this->_user->cyber_user == "yes") {
			$this->view->layout()->setLayout("cyber");
			$this->_clientID = $this->_getParam("clientID",0);
			$this->_userID = 0;
		}else {
			$this->_clientID = $this->_user->ClientID;
			$this->_userID = $this->_user->ID;
		}

	}
}
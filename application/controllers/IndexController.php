<?php
class IndexController extends Cny_Controller_LayoutAction
{
    public function indexAction()
	{
		//if cyber user go to cyber homepage
		if ($this->_user->cyber_user == "yes") {
			$this->_redirect("/cyber");
		}

		$sql = "SELECT T.id, CONCAT(U.FirstName, ' ', SUBSTRING(U.LastName,1,1),'.') AS UserName, summary, priority, status, DATE_FORMAT(modified, '%m/%d/%Y') AS modified,
				DATE_FORMAT(created, '%m/%d/%Y') AS created,
				DATE_FORMAT(duedate, '%m/%d/%Y') AS duedate , T.Sort
			 	FROM tickets T LEFT JOIN Users_BugTracker U ON  created_user_id = U.ID
			 	WHERE ( T.clientID =".$this->_user->ClientID." AND T.status <>'Approved & Closed')
			 	ORDER BY T.created DESC LIMIT 5";
		$tickets = $this->_db->fetchAssoc($sql);

		$sql = "SELECT DISTINCT T.id, CONCAT(U.FirstName, ' ', SUBSTRING(U.LastName,1,1),'.') AS UserName, summary, priority, status, DATE_FORMAT(T.modified, '%m/%d/%Y') AS modified,
				DATE_FORMAT(T.created, '%m/%d/%Y') AS created,
				DATE_FORMAT(duedate, '%m/%d/%Y') AS duedate , T.Sort
			 	FROM tickets T LEFT JOIN Users_BugTracker U ON  created_user_id = U.ID
			    LEFT JOIN ticket_notes AS tn ON T.id = tn.ticket_id
			 	WHERE ( T.clientID =".$this->_user->ClientID." AND T.status <>'Approved & Closed')
			 	ORDER BY tn.created DESC LIMIT 5";
		$ticket_notes = $this->_db->fetchAssoc($sql);

		$sql = "SELECT T.id, CONCAT(U.FirstName, ' ', SUBSTRING(U.LastName,1,1),'.') AS UserName, summary, status, DATE_FORMAT(modified, '%m/%d/%Y') AS modified,
				DATE_FORMAT(created, '%m/%d/%Y') AS created
			 	FROM estimate_requests T LEFT JOIN Users_BugTracker U ON  created_user_id = U.ID
			 	WHERE ( T.client_id =".$this->_user->ClientID." AND T.status <>'approved')
			 	ORDER BY T.created DESC LIMIT 5";
		$estimates = $this->_db->fetchAssoc($sql);

		$sql = "SELECT DISTINCT T.id, CONCAT(U.FirstName, ' ', SUBSTRING(U.LastName,1,1),'.') AS UserName, summary, status, DATE_FORMAT(T.modified, '%m/%d/%Y') AS modified,
				DATE_FORMAT(T.created, '%m/%d/%Y') AS created
			 	FROM estimate_requests T LEFT JOIN Users_BugTracker U ON  created_user_id = U.ID
			    LEFT JOIN estimate_request_notes AS tn ON T.id = tn.estimate_request_id
			 	WHERE ( T.client_id =".$this->_user->ClientID." AND T.status <>'approved')
			 	ORDER BY tn.created DESC LIMIT 5";
		$estimate_notes = $this->_db->fetchAssoc($sql);


		$sql = "SELECT c.*, p.title, p.client_id FROM concepts AS c LEFT JOIN concept_projects AS p ON p.id = c.concept_project_id
				WHERE p.client_id = ".$this->_user->ClientID." ORDER BY c.created DESC LIMIT 5";
		$concepts = $this->_db->fetchAssoc($sql);

		//create an array of items by date and type
		$recent = array();
		foreach ($tickets as $row) {
			$recent[strtotime($row['created']).".9"] = array('type' => 'ticket', 'data'=>$row);
		}
		foreach ($ticket_notes as $row) {
			$recent[strtotime($row['created']).".9"] = array('type' => 'ticket', 'data'=>$row);
		}
		foreach ($estimates as $row) {
			$recent[strtotime($row['created']).".6"] = array('type' => 'estimate', 'data'=>$row);
		}
		foreach ($estimate_notes as $row) {
			$recent[strtotime($row['created']).".6"] = array('type' => 'estimate', 'data'=>$row);
		}
		foreach ($concepts as $row) {
			$recent[strtotime($row['created']).".1"] = array('type' => 'concept', 'data'=>$row);
		}
		krsort($recent);
		$this->view->recent = array_slice($recent, 0, 8);
	}

	function init()
	{
		$bootstrap = $this->getInvokeArg('bootstrap'); // gets the boostrapper
		$resource = $bootstrap->getPluginResource('multidb'); //multi db support
		$this->_db = $resource->getDefaultDb();
		$this->_db_blog = $resource->getDb('blog');

		$this->view->placeholder('section')->set("home");

		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('portal'));
		if(!$auth->hasIdentity()){
			$auth->clearIdentity();
			$this->_redirect('/auth');
		}else{
			$this->view->user = $this->_user = $auth->getIdentity();
			$this->view->placeholder('logged_in')->set(true);

			if ($this->_user->cyber_user == "yes") {
				$this->view->layout()->setLayout("cyber");
			}
		}
	}
}

<?php
class CyberController extends Cny_Controller_LayoutAction
{
    public function indexAction()
	{
		// total projects open
		$sql = "SELECT COUNT(p.id) AS projects_open FROM Projects AS p WHERE p.ProjectStatusID!=4";
		$this->view->projects_open = $this->_db->fetchOne($sql);

		// total tickets open
		//$sql = "SELECT COUNT(*) FROM tickets WHERE status!='Approved & Closed'";
		//$this->view->tickets_open = $this->_db->fetchOne($sql);

		// total unassigned
		$sql = "SELECT COUNT(*) FROM tickets WHERE (assigned_user_id IS NULL OR assigned_user_id = 0) AND status != 'Approved & Closed'";
		$this->view->tickets_unassigned = $this->_db->fetchOne($sql);

		// total retainers
		$sql = "SELECT COUNT(*) FROM Retainers WHERE EndDate IS NULL OR EndDate >= NOW() GROUP BY ClientID";
		$this->view->retainer_count = $this->_db->fetchOne($sql);

		// upcoming calendar events
		$sql = "SELECT ce.id AS EventID, DATE_FORMAT(ce.start,'%Y-%m-%d') AS StartDateTime, DATE_FORMAT(ce.end,'%Y-%m-%d') AS EndDateTime, CONCAT(u.FirstName, ' ', u.LastName, ' - ', ce.title) AS Title, ce.description AS Description, ce.type AS CssClass, u.id AS UserID
			FROM calendar_entries AS ce LEFT JOIN Users AS u ON ce.user_id = u.ID WHERE u.Disabled = 'no' AND ce.public = 'y'
			AND ce.end > NOW() ORDER BY StartDateTime ASC LIMIT 25";
		$this->view->calendar_events = $this->_db->fetchAssoc($sql);

		// announcements
		$sql = "SELECT a.*, CONCAT(u.FirstName, ' ', u.LastName) AS name FROM announcements AS a LEFT JOIN Users AS u ON a.user_id = u.ID WHERE a.post_date >= '".date("Y-m-d",strtotime("today - 1 month"))."' ORDER BY a.post_date DESC";
		$this->view->announcements = $this->_db->fetchAssoc($sql);

		$this->view->messages = $this->_flashMessenger->getMessages();
	}

	public function chartAction()
	{
		$this->view->layout()->disableLayout();
		$type = $this->_getParam('type','');

		if ($type == "projects") {
			$sql = 'SELECT COUNT(p.id) AS statCount,ps.ProjectStatusName,p.ProjectStatusID,
					CASE ps.ProjectStatusName
						WHEN \'Open\' THEN FLOOR( ( p.ExpectedEndDate - '. strtotime('12:00 am') .' )  / 86400 )
						WHEN \'On Client\' THEN 10001
						WHEN \'Completed\' THEN 10002
						WHEN \'Closed\' THEN 10010
					END AS StatusRank
					FROM Projects AS p
					LEFT JOIN ProjectStatus ps ON p.ProjectStatusID = ps.id
					WHERE p.ProjectStatusID!=4
					GROUP BY StatusRank';

			$projects = $this->_db->fetchAssoc($sql);

			$stat_counts = array('pending'=>0,'tom'=>0,'now'=>0,'onclient'=>0);

			foreach ($projects as $p) {
				$cStatusRank = $p['StatusRank'];
				$scKey = false;
				$cProjectStatus = strtolower($p['ProjectStatusName']);

				if($cStatusRank < 1) {
					$scKey = 'now';
				} elseif( $cStatusRank >= 1 && $cStatusRank < 2 ){
					$scKey = 'tom';
				} else {
					$scKey = 'pending';
				}

				switch($cProjectStatus) {
					case 'completed':
						$scKey = false;
					break;
					case 'on client':
						$scKey = 'onclient';
					break;
					case 'closed':
						$scKey = false;
					break;
				}

				if($scKey) {
					$stat_counts[$scKey] += $p['statCount'];
				}
			}

			$this->view->stat_counts = $stat_counts;
			$this->render("chart_project");
		}elseif ($type == "tickets") {
			$sql = 'SELECT COUNT(id) AS count,status FROM tickets WHERE id GROUP BY status';
			$tickets = $this->_db->fetchAssoc($sql);

			$rt_counts = array('scheduled'=>0,'unopened'=>0,'on client'=>0);

			foreach ($tickets as $r) {
				$count = $r['count'];
				$status = $r['status'];

				switch($status) {
					case 'Ticket Opened':
						$rt_counts['unopened'] += $count;
					break;
					case 'In Progress':
						$rt_counts['scheduled'] += $count;
					break;
					case 'Attention Required':
						$rt_counts['on client'] += $count;
					break;
					case 'Completed':
						$rt_counts['on client'] += $count;
					break;
					case 'Comments Submitted':
					break;
				}
			}

			$this->view->rt_counts = $rt_counts;
			$this->render("chart_tickets");
		}elseif ($type == "retainers") {
			$retainer_over = 0;
			$retainer_under = 0;

			$last_month = date("m") - 1; $yr = date('Y');
			if($last_month==0){ $last_month = 12; $yr -= 1; }
			$last_month_start = (string)($last_month)."-01-".(string)($yr);

			$sql = 'SELECT r.id AS ID, r.ClientID AS ClientID, c.ClientName AS Owner, r.Hours AS TotalRetainer, r.Notes AS Notes FROM Retainers r LEFT JOIN Clients c ON r.ClientID = c.id WHERE r.Status=\'enabled\' ORDER BY c.ClientName';
			$result = $this->_db->fetchAssoc($sql);

			$_REQUEST['sort'] = 'PercentCompleted';

			if( $result ){
				$TABLE_GRID = array();

				$sql_Month_Start = " MONTH(FROM_UNIXTIME(pa. StartTime)) = MONTH(CURDATE())  AND  YEAR(FROM_UNIXTIME(pa. StartTime)) = YEAR(CURDATE()) ";
				$sql_Month_End = " MONTH(FROM_UNIXTIME(pa. EndTime)) = MONTH(CURDATE()) AND YEAR(FROM_UNIXTIME(pa. EndTime)) = YEAR(CURDATE()) ";

				foreach ($result as $row) {
					$row_id = $row['ID'];
					$TABLE_GRID[ $row_id ] = $row;
					$sql = '
						SELECT p.ClientID, IFNULL( ABS( SUM(pa.EndTime - pa.StartTime) ), 0 ) AS CurrentHours
						FROM Projects p LEFT JOIN ProjectActivity pa ON p.id = pa.ProjectID
						WHERE
							p.RetainerStatus = \'y\'
							AND '.
							$sql_Month_Start.'
							AND  '.
							$sql_Month_End.'
							AND
							p.ClientID = '.$TABLE_GRID[ $row_id ]['ClientID'].'
						GROUP BY p.ClientID';

					$tmp = $this->_db->fetchRow($sql);
					$TABLE_GRID[ $row_id ]['CurrentHours'] = $tmp['CurrentHours'];
					if($TABLE_GRID[ $row_id ]['TotalRetainer'] != 0)
						$TABLE_GRID[ $row_id ]['PercentCompleted'] = ceil( (( ($TABLE_GRID[ $row_id ]['CurrentHours']/60)/60) / $TABLE_GRID[ $row_id ]['TotalRetainer'] ) * 100 );
					else
						$TABLE_GRID[ $row_id ]['PercentCompleted'] = 0;
				}
			}else{
				//No Records Found
				echo 'Error :: '.$sql;
			}

			//CALCULATIONS
			foreach( $TABLE_GRID AS $key => $value )
				$sort_array[$key] = $value[$_REQUEST['sort']];

			switch($_REQUEST['sort']){
				case 'TotalRetainer':
				case 'CurrentHours':
				case 'PercentCompleted':
					arsort( $sort_array );
					break;
				default:
					asort( $sort_array );
			}

			foreach( $sort_array AS $key => $value ){
				$RETAINER = $TABLE_GRID[$key];

				if( $RETAINER['PercentCompleted'] > 100 ) {
					$retainer_over += 1;
				} else {
					$retainer_under += 1;
				}
			}

			$this->view->retainer_over = $retainer_over;
			$this->view->retainer_under = $retainer_under;
			$this->render("chart_retainers");
		}

	}

	public function announcementaddAction()
	{
		$this->_helper->layout()->setLayout("modal");

		if( $this->getRequest()->isPost() ){
			$data['message'] = $this->_getParam("Description","");
			$data['user_id'] = $this->_user->Cyberny_Users_ID;
			$data['post_date'] = new Zend_Db_Expr("NOW()");
			$data['created'] = new Zend_Db_Expr("NOW()");
			$data['modified'] = new Zend_Db_Expr("NOW()");

			$this->_db->insert("announcements",$data);

			$this->_redirect("/cyber");
		}
	}

	public function announcementeditAction()
	{
		$this->_helper->layout()->setLayout("modal");
		$id = $this->_getParam("id",0);

		$this->view->announcement = $this->_db->fetchRow($this->_db->quoteInto("SELECT * FROM announcements WHERE id = ?",$id));

		if( $this->getRequest()->isPost() ){
			$data['message'] = $this->_getParam("Description","");
			$data['modified'] = new Zend_Db_Expr("NOW()");

			$this->_db->update("announcements",$data,"id=$id");

			$this->_redirect("/cyber");
		}
	}

	public function announcementdeleteAction()
	{
		$this->_helper->layout()->disableLayout();

		$id = $this->_getParam("id",0);

		$this->_db->delete("announcements","id=$id");

		$this->_redirect("/cyber");
	}

	function init()
	{
		$bootstrap = $this->getInvokeArg('bootstrap'); // gets the boostrapper
		$resource = $bootstrap->getPluginResource('multidb'); //multi db support
		$this->_db = $resource->getDefaultDb();

		$this->view->placeholder('section')->set("home");
		$this->view->layout()->setLayout("cyber");

		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('portal'));
		if(!$auth->hasIdentity()){
			$auth->clearIdentity();
			$this->_redirect('/auth');
		}else{
			$this->view->user = $this->_user = $auth->getIdentity();
			$this->view->placeholder('logged_in')->set(true);

			if ($this->_user->cyber_user != "yes") {
				$auth->clearIdentity();
				$this->_redirect('/auth');
			}
		}
	}
}

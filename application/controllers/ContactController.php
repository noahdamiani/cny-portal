<?php
class ContactController extends Cny_Controller_LayoutAction
{

	public function indexAction()
	{
		define( 'newline', "\n" );

		$this->view->data = array();
		$this->view->data['name'] = $this->_user->FirstName." ".$this->_user->LastName;
		$this->view->data['email'] = $this->_user->Email;

		if( $this->getRequest()->isPost() ){
			$data = $this->_getParam('data',array());

			$client = $this->_db->fetchOne("SELECT ClientName FROM Clients WHERE id = {$this->_user->ClientID}");

			$mail_message = 'Sender Name: '.$data['name'].newline;
			$mail_message .= 'Client: '.$client.newline;
			$mail_message .= 'Email: '.$data['email'].newline;
			$mail_message .= 'Category: '.$data['category'].newline;
			$mail_message .= newline;
			$mail_message .= 'Questions:'.$data['question'].newline;
			$mail_message .= newline;

			$mail = new Zend_Mail();
			$mail->setBodyText($mail_message);
			$mail->setFrom('do-not-reply@cyber-ny.com', "Bug Tracker Contact Form");
			//$mail->setFrom('contact@cnydevzone3.com', "Bug Tracker Contact Form");
			$mail->addTo("contact@cyber-ny.com", "admin");
			$mail->setSubject("Cyber-NY Bug Tracker Contact Form");
			$mail->send();

			$this->_redirect("/contact/sent");
		}
	}

	public function sentAction()
	{}

	function init()
	{
		$bootstrap = $this->getInvokeArg('bootstrap'); // gets the boostrapper
		$resource = $bootstrap->getPluginResource('multidb'); //multi db support
		$this->_db = $resource->getDefaultDb();

		$this->view->placeholder('section')->set("contact");

		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('portal'));
		if(!$auth->hasIdentity()){
			$auth->clearIdentity();
			$this->_redirect('/auth');
		}else{
			$this->view->user = $this->_user = $auth->getIdentity();
			$this->view->placeholder('logged_in')->set(true);
		}

		$subSectionMenu = '';
		$this->view->placeholder("subSectionMenu")->set($subSectionMenu);
	}
}



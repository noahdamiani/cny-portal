<?php
class CronController extends Cny_Controller_LayoutAction
{

	public function pastdueAction()
	{
		define( 'newline', "<br/>" );

		//past due items
		$sql = "SELECT
					p.id, p.Title, p.EntryDate, p.StartDate, p.ExpectedEndDate, p.ActualEndDate, u.FirstName, u.LastName, u.Email, p.UserID, c.ClientName, p.Description,
				    DAYOFWEEK(FROM_UNIXTIME(p.ExpectedEndDate)) as WeekDay, timespan
				FROM
					Projects p LEFT JOIN Clients c ON p.ClientID = c.id
					LEFT JOIN Users u ON p.UserID = u.id
					left join ProjectStatus ps on p.ProjectStatusID = ps.id
				WHERE p.ExpectedEndDate <  ".strtotime("12:00am")."
				and ProjectStatusName = 'Open'
				ORDER BY u.UserName ASC";
		$results = $this->_db->fetchAssoc($sql);

		foreach ($results as $row) {
			$mail_message = date('l n/d/y').newline.newline;
			$mail_message .= ucfirst($row['UserName']).",".newline.newline;
			$mail_message .= "You have a past due project open in the project manager. Contact the person that assigned this project to set a new project date.  If this project is completed, close it out or place on client.  Make sure the hours are accurate and that it is set for retainer if this is a retainer project.".newline.newline;
			$mail_message .= "<strong>Project Entry:</strong>".newline;
			$mail_message .= "<a href='http://intranet.cyber-ny.com/projectmanager/project_details.php?id={$row['id']}&cmd=view'><span style='color:red;'>{$row['UserName']} - {$row['ClientName']} - {$row['Title']} - ".date('n/d/Y',$row['ExpectedEndDate'])."</span></a>".newline;

			$mail_message .= newline;
			$mail_message .= 'Thanks,'.newline;
			$mail_message .= 'Cyber-NY Project Manager'.newline;
			$mail_message .= '<span style="font-size:9px;color:#cccccc;">self aware since 11/09/09 17:18:00</span>'.newline;

			$mail = new Zend_Mail();
			$mail->setBodyHtml($mail_message);
			$mail->setFrom('mike@cyber-ny.com', 'Cyber-NY Project Manager');
			//$mail->setFrom('contact@cnydevzone3.com','Cyber-NY Project Manager');
			$mail->addTo($row['Email'],$row['FirstName']." ".$row['LastName']);
			$mail->setSubject( "Cyber-NY :: Past Due Project - ".date('l n/d/y'));
			$mail->send();
		}

	}

	function init()
	{
		$bootstrap = $this->getInvokeArg('bootstrap'); // gets the boostrapper
		$resource = $bootstrap->getPluginResource('multidb'); //multi db support
		$this->_db = $resource->getDefaultDb();
	}
}

<?php
class ClientsController extends Cny_Controller_LayoutAction
{
	public function indexAction()
	{
		$this->view->placeholder('sub_section')->set("clients");

		$search = new Zend_Session_Namespace('client_search');
    	$mask = "";
    	$mask = $this->view->mask = $this->_getParam("mask","");

		$page = $this->getRequest()->getParam('page',1);
		$this->view->dir = $dir = $this->_getParam('dir','ASC');
		$this->view->sort = $sort = $this->_getParam('sort','ClientName');

		$select = $this->_db->select();
		$select->from(array("c"=>"Clients"), "*");
		$select ->order(array("$sort $dir"));

		if ($mask) {
			$select->where("c.ClientName LIKE '%$mask%' OR c.client_code LIKE '%$mask' ");
		}

		$paginator = Zend_Paginator::factory($select);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(50);
		$this->view->clients = $paginator;
	}

	public function viewAction()
	{
		$this->view->placeholder('sub_section')->set("clients");
		$id = $this->_getParam("id",0);

		$sql = $this->_db->quoteInto("SELECT * FROM Clients WHERE id=?",$id);
		$this->view->client = $this->_db->fetchRow($sql);

		$sql = $this->_db->quoteInto("SELECT * FROM Retainers WHERE ClientID=? ORDER BY StartDate DESC",$id);
		$this->view->retainers = $this->_db->fetchAssoc($sql);

		$sql = $this->_db->quoteInto("SELECT cn.*, CONCAT(u.FirstName, ' ', u.LastName) AS name FROM ClientNotes AS cn LEFT JOIN Users AS u ON cn.UserID = u.ID WHERE cn.ClientID=?",$id);
		$this->view->notes = $this->_db->fetchAssoc($sql);

		$sql = $this->_db->quoteInto("SELECT u.* FROM Users_BugTracker AS u, users_client AS uc WHERE u.ID = uc.user_id AND uc.client_id=? ORDER BY u.LastName ASC",$id);
		$this->view->users = $this->_db->fetchAssoc($sql);

		$sql = $this->_db->quoteInto("SELECT tne.id, tne.role, CONCAT(u.FirstName, ' ', u.LastName) AS name FROM Tickets_Notification_Emails AS tne, Users AS u WHERE tne.userID = u.ID AND tne.clientID =?",$id);
		$this->view->staff = $this->_db->fetchAssoc($sql);
	}

	public function addAction()
	{
		$this->view->placeholder('sub_section')->set("clientsadd");

		if( $this->getRequest()->isPost() ){
			$data = $this->_getParam("data",array());
			$retainer = $this->_getParam("retainer",array());
			$portal = $this->_getParam("portal",array());

			$this->_db->insert("Clients",$data);
			$id = $this->_db->lastInsertId();
			$retainer['ClientID'] = $id;

			$this->_db->insert("Retainers",$retainer);

			if ($this->_getParam("createUser","") == "yes") {
				$portal['FirstName'] =  $data['ContactFirstName'];
				$portal['LastName'] =  $data['ContactLastName'];
				$portal['Phone'] = $data['TelephoneNumber'];
				$portal['Disabled'] = "no";
				$portal['Roles'] = "client";
				$portal['ClientID'] = $id;

				unset($portal['vUserPassword']);

				$this->_db->insert("Users_BugTracker",$portal);
			}

			$this->_redirect("/clients/view/id/$id");
		}
	}

	public function editAction()
	{
		$id = $this->_getParam("id",0);

		$sql = $this->_db->quoteInto("SELECT * FROM Clients WHERE id=?",$id);
		$this->view->client = $this->_db->fetchRow($sql);

		$sql = $this->_db->quoteInto("SELECT * FROM Retainers WHERE ClientID=? ORDER BY StartDate DESC",$id);
		$this->view->retainers = $this->_db->fetchAssoc($sql);

		if( $this->getRequest()->isPost() ){
			$data = $this->_getParam("data",array());
			$retainers = $this->_getParam("retainer",array());
			$retainer_new = $this->_getParam("retainer_new",array());

			$this->_db->update("Clients",$data,"id=$id");

			foreach ($retainers as $key => $retainer_data) {
				$this->_db->update("Retainers",$retainer_data,"ClientID=$id AND id=$key");
			}

			if ($retainer_new && ($retainer_new['Hours'] > 0 || (string)$retainer_new['Hours'] == '0')) {
				if ($retainer_new['EndDate'] == '0000-00-00' || !$retainer_new['EndDate'])
					$retainer_new['EndDate'] = new Zend_Db_Expr("NULL");

				$retainer_new['ClientID'] = $id;
				$retainer_new['Status'] = "enabled";
				$this->_db->insert("Retainers",$retainer_new);
				$new_retainer_id = $this->_db->lastInsertId();

				$endDate = date("Y-m-d",strtotime($retainer_new['StartDate']." - 1 day"));
				$this->_db->query("UPDATE Retainers SET EndDate = '$endDate', Status='disabled' WHERE (EndDate IS NULL OR EndDate = '0000-00-00') AND ClientID = '$id' AND id <> '$new_retainer_id'");
			}

			$this->_redirect("/clients/view/id/$id");
		}
	}

	public function deleteAction()
	{
		$id = $this->_getParam('id',0);

		$this->_db->delete("Clients","id=$id");
		$this->_db->delete("Retainers","ClientID=$id");

		$this->_redirect("/clients/");
	}

	public function addnoteAction()
	{
		$this->_helper->layout()->setLayout("blank");
		$this->view->id = $id = $this->_getParam('id',0);

		if( $this->getRequest()->isPost() ){
			$data['ClientID'] = $id;
			$data['Notes'] = $this->_getParam("Description","");
			$data['UserID'] = $this->_user->Cyberny_Users_ID;
			$data['EntryDate'] = new Zend_Db_Expr("NOW()");

			$this->_db->insert("ClientNotes",$data);
			$note_id = $this->_db->lastInsertId();
		}
	}

	public function updatenotesAction()
	{
		$this->_helper->layout()->disableLayout();

		$id = $this->_getParam("client",0);
		$last_note = $this->_getParam("last_note",0);

		$sql = $this->_db->quoteInto("SELECT cn.*, CONCAT(u.FirstName, ' ', u.LastName) AS name FROM ClientNotes AS cn LEFT JOIN Users AS u ON cn.UserID = u.ID
				WHERE cn.ClientID = ? AND cn.id > '$last_note' ORDER BY cn.EntryDate ASC",$id);
		$this->view->notes = $this->_db->fetchAssoc($sql);
	}

	public function deletenoteAction()
	{
		$this->_helper->layout()->disableLayout();

		$id = $this->_getParam("note_id",0);

		$this->_db->delete("ClientNotes","id=$id");

		exit;
	}

	public function adduserAction()
	{
		$this->_helper->layout()->setLayout("blank");
		$this->view->client_id = $client_id = $this->_getParam("client_id",0);

		$sql = "SELECT ID, CONCAT(FirstName,' ',LastName) FROM Users_BugTracker ORDER BY LastName ASC, FirstName ASC";
		$this->view->existing_users = $this->_db->fetchPairs($sql);

		if( $this->getRequest()->isPost() ){
			if ($this->_getParam('existing_user_id',0)) {
				$user_id = $this->_getParam('existing_user_id',0);
			}else {
				$data = $this->_getParam('data',array());
				//$data['ClientID'] = $client_id;
				$data['Roles'] = "client";

				if ($data['UserPassword'] != $data['vUserPassword'] || trim($data['UserPassword']) == '') {
					unset($data['UserPassword']);
				}
				unset($data['vUserPassword']);

				$this->_db->insert("Users_BugTracker",$data);
				$user_id = $this->_db->lastInsertId();
			}

			$this->_db->insert("users_client",array("user_id"=>$user_id,"client_id"=>$client_id));

			$this->_redirect("/clients/view/id/$client_id/#users");
		}
	}

	public function edituserAction()
	{
		$this->_helper->layout()->setLayout("blank");
		$this->view->client_id = $client_id = $this->_getParam("client_id",0);
		$id = $this->_getParam("id",0);

		$this->view->user = $this->_db->fetchRow($this->_db->quoteInto("SELECT * FROM Users_BugTracker WHERE ID=?",$id));

		if( $this->getRequest()->isPost() ){
			$data = $this->_getParam('data',array());

			if ($data['UserPassword'] != $data['vUserPassword'] || trim($data['UserPassword']) == '') {
				unset($data['UserPassword']);
			}
			unset($data['vUserPassword']);

			$this->_db->update("Users_BugTracker",$data,"ID=$id");

			$this->_redirect("/clients/view/id/$client_id/#users");
		}
	}

	public function viewuserAction()
	{
		$this->_helper->layout()->setLayout("blank");
		$this->view->client_id = $client_id = $this->_getParam("client_id",0);
		$id = $this->_getParam("id",0);

		$this->view->user = $this->_db->fetchRow($this->_db->quoteInto("SELECT * FROM Users_BugTracker WHERE ID=?",$id));
	}

	public function deleteuserAction()
	{
		$id = $this->_getParam("id",0);
		$client_id = $this->_getParam("client_id",0);

		$this->_db->delete("users_client","user_id=$id AND client_id=$client_id");

		$sql = "SELECT COUNT(*) FROM users_client WHERE user_id = $id";
		$count = $this->_db->fetchOne($sql);

		if ($count == 0)
			$this->_db->delete("Users_BugTracker","id=$id");

		$this->_redirect("/clients/view/id/$client_id/#users");
	}

	public function addstaffAction()
	{
		$this->_helper->layout()->setLayout("blank");
		$this->view->client_id = $client_id = $this->_getParam("client_id",0);

		$sql = "SELECT ID, CONCAT(FirstName, ' ', LastName) AS name FROM Users WHERE Disabled = 'no' ORDER BY LastName ASC";
		$this->view->staff = $this->_db->fetchPairs($sql);

		if( $this->getRequest()->isPost() ){
			$data = $this->_getParam('data',array());
			$data['clientID'] = $client_id;

			$this->_db->insert("Tickets_Notification_Emails",$data);

			$this->_redirect("/clients/view/id/$client_id/#staff");
		}
	}

	public function deletestaffAction()
	{
		$id = $this->_getParam("id",0);
		$client_id = $this->_getParam("client_id",0);

		$this->_db->delete("Tickets_Notification_Emails","id=$id");

		$this->_redirect("/clients/view/id/$client_id/#staff");
	}

	public function validateAction()
	{
		$type = $this->_getParam('type','');
		$data = $this->_getParam('data',array());
		if (!$data) $data = $this->_getParam('portal',array());

		if ($type == 'email') {
			$sql = $this->_db->quoteInto("SELECT id FROM Users_BugTracker WHERE Email = ?", $data['Email']);
			$result = $this->_db->fetchOne($sql);
		}elseif ($type == 'username') {
			$sql = $this->_db->quoteInto("SELECT id FROM Users_BugTracker WHERE UserName = ?", $data['username']);
			$result = $this->_db->fetchOne($sql);
		}

		if ($result) echo 'false';
		else echo 'true';
		exit();
	}

	/*
	function fixAction()
	{
		$sql = "SELECT * FROM Users_BugTracker";
		$users = $this->_db->fetchAssoc($sql);

		foreach ($users as $user) {
			$this->_db->insert("users_client",array("user_id"=>$user['ID'],"client_id"=>$user['ClientID']));
		}
		echo "done";exit;
	}
	*/

	function init()
	{
		$bootstrap = $this->getInvokeArg('bootstrap'); // gets the boostrapper
		$resource = $bootstrap->getPluginResource('multidb'); //multi db support
		$this->_db = $resource->getDefaultDb();

		$this->view->placeholder('section')->set("clients");
		$this->view->layout()->setLayout("cyber");

		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('portal'));
		if(!$auth->hasIdentity()){
			$auth->clearIdentity();
			$this->_redirect('/auth');
		}else{
			$this->view->user = $this->_user = $auth->getIdentity();
			$this->view->placeholder('logged_in')->set(true);

			if ($this->_user->cyber_user != "yes") {
				$auth->clearIdentity();
				$this->_redirect('/auth');
			}
		}

		$subSectionMenu = '<li id="subnav-viewclients"><a href="/clients/"><span class="subnav-size">View Clients</span></a></li>
							<li id="subnav-addclient"><a href="/clients/add"><span class="subnav-size">Add Client</span></a></li>
							<li id="subnav-retainers"><a href="/retainer"><span class="subnav-size">Retainer Tracker</span></a></li>
							';
		$this->view->placeholder("subSectionMenu")->set($subSectionMenu);
	}
}

function sec2hm ($sec, $padHours = false) {
	$hms = "";
	$hours = intval(intval($sec) / 3600);

	$hms .= ($padHours) ? str_pad($hours, 2, "0", STR_PAD_LEFT). 'h ' : $hours. 'h ';

	$minutes = intval(($sec / 60) % 60);

	$hms .= str_pad($minutes, 2, "0", STR_PAD_LEFT)."m";

	return $hms;
}

function sec2dec ($sec) {
	$hms = "";
	$hours = intval(intval($sec) / 3600);
	$minutes = intval(($sec / 60) % 60);

	$dec = round($hours + ($minutes/60) ,2);

	return $dec;
}

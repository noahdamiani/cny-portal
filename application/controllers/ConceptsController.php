<?php
//error_reporting(E_ALL);
//ini_set("display_errors", 1);

class ConceptsController extends Cny_Controller_LayoutAction
{

	public function indexAction()
	{
		if ($this->_user->cyber_user != "yes") {
			$this->_redirect("/concepts/list");
		}

		$this->view->placeholder('sub_section')->set("conceptsview");

		$search = new Zend_Session_Namespace('concepts_search');
    	$mask = "";

		$page = $this->getRequest()->getParam('page',1);
		$this->view->dir = $dir = $this->_getParam('dir','ASC');
		$this->view->sort = $sort = $this->_getParam('sort','ClientName');

		$select = $this->_db->select();
		$select->from(array("con"=>"concepts"), '');
		$select->joinLeft(array("p"=>"concept_projects"), "p.id = con.concept_project_id", '');
		$select->joinLeft(array("c"=>"Clients"), "c.id = p.client_id", array('id','ClientName'));
		$select->group("c.ClientName");
		$select ->order(array("$sort $dir"));

		$paginator = Zend_Paginator::factory($select);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(50);
		$this->view->clients = $paginator;
	}

	public function listAction()
	{
		$this->view->placeholder('sub_section')->set("concepts");

		$search = new Zend_Session_Namespace('concepts_search');
    	$mask = $relationship = "";

		$page = $this->getRequest()->getParam('page',1);
		$this->view->dir = $dir = $this->_getParam('dir','DESC');
		$this->view->sort = $sort = $this->_getParam('sort','p.created');

		$select = $this->_db->select();
		$select->from(array("p"=>"concept_projects"));
		$select->columns("(SELECT COUNT(*) FROM concepts WHERE concept_project_id=p.id) AS cnt");
		$select->where("p.client_id =".$this->_clientID);
		$select ->order(array("$sort $dir"));

		$paginator = Zend_Paginator::factory($select);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(50);
		$this->view->projects = $paginator;
	}

	public function deleteconceptsAction()
	{
		$id = $this->_getParam("id",0);

		$this->_db->delete("concept_projects","id='$id' AND client_id = '{$this->_clientID}'");

		$this->_redirect("/concepts/list/clientID/".$this->_clientID);
	}

	public function listconceptsAction()
	{
		$this->view->placeholder('sub_section')->set("concepts");
		$this->view->clientID = $this->_clientID;
		$id = $this->_getParam("id",0);

		$search = new Zend_Session_Namespace('concepts_search');
    	$mask = $relationship = "";

		$page = $this->getRequest()->getParam('page',1);
		$this->view->dir = $dir = $this->_getParam('dir','DESC');
		$this->view->sort = $sort = $this->_getParam('sort','c.created');

		$select = $this->_db->select();
		$select->from(array("c"=>"concepts"));
		$select->joinLeft(array("p"=>"concept_projects"),"p.id = c.concept_project_id",'title');
		$select->where("p.client_id =".$this->_clientID." AND p.id=$id");
		$select ->order(array("$sort $dir"));

		$paginator = Zend_Paginator::factory($select);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(50);
		$this->view->concepts = $paginator;
	}

	public function addAction()
	{
		if ($this->_user->cyber_user != "yes") {
			$this->_redirect("/concepts/list");
		}

		$this->view->placeholder('sub_section')->set("conceptadd");

		$sql = "SELECT id, ClientName FROM Clients WHERE Disabled = 'no' ORDER BY ClientName ASC";
		$this->view->clients = array(""=>"Select Client")+$this->_db->fetchPairs($sql);

		$sql = "SELECT id, width FROM concept_widths ";
		$this->view->widths = array("0"=>"None")+$this->_db->fetchPairs($sql);

		$this->view->names = array(""=>"Default Names","Home Page"=>"Home Page", "Interior Template"=>"Interior Template", "Member Home Page"=>"Member Home Page",
					"Store"=>"Store", "Shopping Cart"=>"Shopping Cart", "About Us"=>"About Us", "Contact Us"=>"Contact Us", "Login"=>"Login",
					"Checkout"=>"Checkout", "Gallery"=>"Gallery", "Banner"=>"Banner", "Email"=>"Email");

		$sql = "SELECT id, name FROM client_projects WHERE 1=1 ORDER BY name ASC";
		$this->view->client_projects = array(""=>"Select Project")+$this->_db->fetchPairs($sql);

		if( $this->getRequest()->isPost() ){
			$data = $this->_getParam("data",array());
			$project = $this->_getParam("project",array());

			if (!$data['name'] && trim($data['new_name']) != '') {
				$data['name'] = $data['new_name'];
			}
			unset($data['new_name']);

			if (!$data['concept_project_id'] && !$project['client_project_id'] && trim($project['title'] != '')) {
				$project['created'] = $project['modified'] = new Zend_Db_Expr("NOW()");

				$this->_db->insert("concept_projects",$project);
				$data['concept_project_id'] = $this->_db->lastInsertId();
			}

			if ($project['client_project_id'] && !$data['concept_project_id']) {
				$sql = "SELECT name FROM client_projects WHERE id = {$project['client_project_id']}";
				$title = $this->_db->fetchOne($sql);

				if ($title) {
					$sql = "SELECT id FROM concept_projects WHERE client_id = {$project['client_id']} AND title = '$title'";
					$existing_project_id = $this->_db->fetchOne($sql);
				}

				if ($existing_project_id) {
					$data['concept_project_id'] = $existing_project_id;
				}else {
					$project['created'] = $project['modified'] = new Zend_Db_Expr("NOW()");
					$project['title'] = $title;

					$this->_db->insert("concept_projects",$project);
					$data['concept_project_id'] = $this->_db->lastInsertId();
				}
			}

			$data['created'] = $data['modified'] = new Zend_Db_Expr("NOW()");

			$this->_db->insert("concepts",$data);
			$concept_id = $this->_db->lastInsertId();

			$this->_redirect("/concepts/upload/concept_id/$concept_id");
		}
	}

	public function editAction()
	{
		if ($this->_user->cyber_user != "yes") {
			$this->_redirect("/concepts/list");
		}

		$this->view->concept_id = $concept_id = $this->_getParam("concept_id",0);

		$sql = "SELECT id, ClientName FROM Clients WHERE Disabled = 'no' ORDER BY ClientName ASC";
		$this->view->clients = array(""=>"Select Client")+$this->_db->fetchPairs($sql);

		$sql = "SELECT id, width FROM concept_widths ";
		$this->view->widths = array("0"=>"None")+$this->_db->fetchPairs($sql);

		$this->view->names = array(""=>"Default Names","Home Page"=>"Home Page", "Interior Template"=>"Interior Template", "Member Home Page"=>"Member Home Page",
					"Store"=>"Store", "Shopping Cart"=>"Shopping Cart", "About Us"=>"About Us", "Contact Us"=>"Contact Us", "Login"=>"Login",
					"Checkout"=>"Checkout", "Gallery"=>"Gallery", "Banner"=>"Banner", "Email"=>"Email");

		$sql = $this->_db->quoteInto("SELECT * FROM concepts AS c, concept_projects AS p WHERE c.concept_project_id = p.id AND c.id = ?",$concept_id);
		$this->view->concept = $this->_db->fetchRow($sql);

		$sql = "SELECT id, title FROM concept_projects WHERE client_id = {$this->view->concept['client_id']}";
		$this->view->projects = array(""=>"")+$this->_db->fetchPairs($sql);

		$sql = "SELECT id, name FROM client_projects WHERE 1=1 ORDER BY name ASC";
		$this->view->client_projects = array(""=>"Select Project")+$this->_db->fetchPairs($sql);

		if( $this->getRequest()->isPost() ){
			$data = $this->_getParam("data",array());
			$project = $this->_getParam("project",array());

			if (!$data['name'] && trim($data['new_name']) != '') {
				$data['name'] = $data['new_name'];
			}
			unset($data['new_name']);

			if (!$data['concept_project_id'] && !$project['client_project_id'] && trim($project['title'] != '')) {
				$project['created'] = $project['modified'] = new Zend_Db_Expr("NOW()");

				$this->_db->insert("concept_projects",$project);
				$data['concept_project_id'] = $this->_db->lastInsertId();
			}

			if ($project['client_project_id'] && !$data['concept_project_id']) {
				$sql = "SELECT name FROM client_projects WHERE id = {$project['client_project_id']}";
				$title = $this->_db->fetchOne($sql);

				if ($title) {
					$sql = "SELECT id FROM concept_projects WHERE client_id = {$project['client_id']} AND title = '$title'";
					$existing_project_id = $this->_db->fetchOne($sql);
				}

				if ($existing_project_id) {
					$data['concept_project_id'] = $existing_project_id;
				}else {
					$project['created'] = $project['modified'] = new Zend_Db_Expr("NOW()");
					$project['title'] = $title;

					$this->_db->insert("concept_projects",$project);
					$data['concept_project_id'] = $this->_db->lastInsertId();
				}
			}

			$data['modified'] = new Zend_Db_Expr("NOW()");

			$this->_db->update("concepts",$data,"id=$concept_id");

			$this->_redirect("/concepts/view/concept_id/$concept_id");
		}
	}

	public function getprojectsAction()
	{
		$this->view->layout()->disableLayout();
		$client_id = $this->_getParam("client_id",0);

		$sql = $this->_db->quoteInto("SELECT id, title FROM concept_projects WHERE client_id = ? ORDER BY created DESC",$client_id);
		$projects = $this->view->projects = $this->_db->fetchPairs($sql);

		echo "<option value=''>Select Existing Project</option>";
		foreach ($projects as $id => $title) {
			echo "<option value='$id'>$title</option>";
		}
		exit;
	}

	public function uploadAction()
	{
		if ($this->_user->cyber_user != "yes") {
			$this->_redirect("/concepts/list");
		}

		$this->view->concept_id = $concept_id = $this->_getParam("concept_id",0);

		/*
		require_once realpath(".")."/aurigma/ImageUploader.class.php";

		if( $this->getRequest()->isPost() ){
			$this->view->layout()->disableLayout();

			require_once realpath(".").'/aurigma/UploadedFiles.php';

			$path = realpath(".").$this->_concept_path;

			//check if sub folder for concept_id exists
			if (!is_dir($path."/".$concept_id)) {
				mkdir($path."/".$concept_id);
			}

			$absThumbnailsPath = $absGalleryPath = realpath($path)."/$concept_id/";

			//Iterate through uploaded data and save the original file, thumbnail, and description.
			while(($file = UploadedFiles::fetchNext()) !== null) {
				$fileName = $file->getSourceFile()->getSafeFileName($absGalleryPath);
				$file->getSourceFile()->save($absGalleryPath . '/' . $fileName);

				$thumbFileName = $file->getThumbnail(1)->getSafeFileName($absThumbnailsPath);
				$file->getThumbnail(1)->save($absThumbnailsPath . '/' . $thumbFileName);

				$this->_db->insert("concept_images",array("concept_id"=>$concept_id,"src"=>$fileName,"created"=>new Zend_Db_Expr("NOW()")));
			}
		}
		*/

	}

	public function imageExistAction()
	{
		$this->view->layout()->disableLayout();

		$this->view->concept_id = $concept_id = $this->_getParam("concept_id",0);
		$path = realpath(".").$this->_concept_path;
		$targetFolder = realpath($path)."/$concept_id/";

		if (file_exists( $targetFolder . '/' . $_POST['filename'])) {
			echo 1;
		} else {
			echo 0;
		}
		exit;
	}

	public function imageSaveAction()
	{
		$this->view->layout()->disableLayout();

		$verifyToken = md5('cyberny' . $this->_getParam('timestamp',0));

		if (!empty($_FILES) && $_POST['token'] == $verifyToken) {
			$this->view->concept_id = $concept_id = $this->_getParam("concept_id",0);
			$path = realpath(".").$this->_concept_path;

			//check if sub folder for concept_id exists
			if (!is_dir($path."/".$concept_id)) {
				mkdir($path."/".$concept_id);
			}

			$absThumbnailsPath = $absGalleryPath = realpath($path)."/$concept_id/";

			$fileData = $_FILES['Filedata'];

			if ($fileData) {
				$tempFile = $fileData['tmp_name'];
				$fileName = $fileData['name'];
				$targetFile = $absGalleryPath . '/' . $fileName;

				// Validate the file type
				$fileTypes = array('jpg', 'jpeg', 'jpe', 'bmp', 'gif', 'png'); // Allowed file extensions
				$fileParts = pathinfo($fileData['name']);

				// Validate the filetype
				if (in_array(strtolower($fileParts['extension']), $fileTypes) && filesize($tempFile) > 0) {

					// Save the file
					move_uploaded_file($tempFile, $targetFile);

					//create 200x200 thumbnail
					$thumb_width = 200;
					$thumb_height = 200;
					$background = array(255,255,255,127);

					$imginfo = getimagesize($targetFile);
					switch( $imginfo['mime'] ){
						case 'image/gif':
							$original_canvas = imagecreatefromgif($targetFile);
							break;
						case 'image/jpeg':
						case 'image/pjpeg':
							$original_canvas = imagecreatefromjpeg($targetFile);
							break;
						case 'image/png':
							$original_canvas = imagecreatefrompng($targetFile);
							break;
						default:
							$original_canvas = imagecreatetruecolor(1,1);
					}

					$original_width = imagesx($original_canvas);
					$original_height = imagesy($original_canvas);
					$orig_ratio = $original_width/$original_height;

					$new_canvas = imagecreatetruecolor($thumb_width, $thumb_height);
					imagesavealpha($new_canvas, true);
					$new_width = $thumb_width;
					$new_height = $thumb_height;
					$new_x = 0;
					$new_y = 0;
					imagefill($new_canvas, 0, 0, imagecolorallocatealpha($new_canvas, $background[0], $background[1], $background[2], $background[3]));

					if ($new_width/$new_height > $orig_ratio) {
						$new_width = $new_height*$orig_ratio;
						$new_x = round( ( $thumb_width-$new_width )/2 );
					} else {
						$new_height = $new_width/$orig_ratio;
						$new_y = round( ( $thumb_height-$new_height )/2 );
					}

					// NOTE: vertically centering images
					imagecopyresampled($new_canvas, $original_canvas, $new_x, $new_y, 0, 0, $new_width, $new_height, $original_width, $original_height);

					//New version top-aligns image on new canvas
					//imagecopyresampled($new_canvas, $original_canvas, $new_x, 0, 0, 0, $new_width, $new_height, $original_width, $original_height);

					imagejpeg($new_canvas,$absThumbnailsPath.'/'.$fileName."_Thumbnail1.jpg");

					$this->_db->insert("concept_images",array("concept_id"=>$concept_id,"src"=>$fileName,"created"=>new Zend_Db_Expr("NOW()")));

					echo 1;
				} else {
					// The file type wasn't allowed
					echo 'Invalid file type.';
				}
			}
		}

		exit;
	}

	public function viewAction()
	{
		$this->view->concept_id = $concept_id = $this->_getParam("concept_id",0);

		$sql = $this->_db->quoteInto("SELECT c.*, p.title, p.client_id, Clients.ClientName, p.id AS project_id FROM concepts AS c, concept_projects AS p LEFT JOIN Clients ON p.client_id = Clients.id WHERE c.concept_project_id = p.id AND c.id = ?",$concept_id);
		$this->view->concept = $this->_db->fetchRow($sql);

		$sql = $this->_db->quoteInto("SELECT * FROM concept_images WHERE concept_id = ? ORDER BY zorder ASC",$concept_id);
		$this->view->images = $this->_db->fetchAssoc($sql);
	}

	public function deleteAction()
	{
		if ($this->_user->cyber_user != "yes") {
			$this->_redirect("/concepts/list");
		}

		$concept_id = $this->_getParam("concept_id",0);

		$this->_db->delete("concepts","id=$concept_id");

		$this->_redirect("/concepts/list/clientID/$this->_clientID");
	}

	public function deleteimageAction()
	{
		if ($this->_user->cyber_user != "yes") {
			$this->_redirect("/concepts/list");
		}

		$id = $this->_getParam("id",0);

		$sql = $this->_db->quoteInto("SELECT * FROM concept_images WHERE id = ? ",$id);
		$image = $this->_db->fetchRow($sql);

		$path = realpath(".").$this->_concept_path.$image['concept_id']."/";
		unlink($path.$image['src']);
		unlink($path.$image['src']."_Thumbnail1.jpg");

		$this->_db->delete("concept_images","id=$id");

		$this->_redirect("/concepts/view/concept_id/".$image['concept_id']);
	}

	public function notesAction()
	{
		$this->view->concept_id = $concept_id = $this->_getParam("concept_id",0);

		$sql = $this->_db->quoteInto("SELECT c.*, p.title, p.client_id, Clients.ClientName, p.id AS project_id FROM concepts AS c, concept_projects AS p LEFT JOIN Clients ON p.client_id = Clients.id WHERE c.concept_project_id = p.id AND c.id = ?",$concept_id);
		$this->view->concept = $this->_db->fetchRow($sql);

		$sql = $this->_db->quoteInto("SELECT c.*, (SELECT COUNT(*) FROM concept_likes WHERE concept_image_id = c.id) AS liked FROM concept_images AS c WHERE c.concept_id = ? ORDER BY c.zorder ASC",$concept_id);
		$this->view->images = $this->_db->fetchAssoc($sql);
	}

	public function addnoteAction()
	{
		$this->_helper->layout()->setLayout("modal");
		$this->view->id = $id = $this->_getParam('id',0);
		$this->view->concept_id = $concept_id = $this->_getParam('concept_id',0);

		/*
		$sql = $this->_db->quoteInto("SELECT tn.*, CONCAT(u.FirstName, ' ', u.LastName) AS name, u.Roles FROM concept_notes AS tn LEFT JOIN Users_BugTracker AS u ON tn.user_id = u.ID
								WHERE tn.concept_image_id = ? ORDER BY tn.created DESC",$id);
		$this->view->notes = $this->_db->fetchAssoc($sql);
		*/

		if( $this->getRequest()->isPost() ){
			$data['concept_image_id'] = $id;
			$data['note'] = $this->_getParam("Description","");
			$data['user_id'] = $this->_user->ID;
			$data['created'] = new Zend_Db_Expr("NOW()");

			$this->_db->insert("concept_notes",$data);
		}
	}

	public function addlikeAction()
	{
		$this->_helper->layout()->disableLayout();
		$this->view->id = $id = $this->_getParam('id',0);

		if( $this->getRequest()->isPost() ){
			$data['concept_image_id'] = $id;
			$data['note'] = "Image marked as LIKED";
			$data['user_id'] = $this->_user->ID;
			$data['created'] = new Zend_Db_Expr("NOW()");
			$this->_db->insert("concept_notes",$data);

			$like_data = $data;
			unset($like_data['note']);
			$this->_db->insert("concept_likes",$like_data);
		}

		exit;
	}

	public function addapprovalAction()
	{
		$this->_helper->layout()->disableLayout();
		$this->view->id = $id = $this->_getParam('id',0);

		if( $this->getRequest()->isPost() ){
			$data['concept_image_id'] = $id;
			$data['note'] = "Image marked as APPROVED";
			$data['user_id'] = $this->_user->ID;
			$data['created'] = new Zend_Db_Expr("NOW()");
			$this->_db->insert("concept_notes",$data);

			$this->_db->update("concept_images", array("approved"=>"y"), "id=$id");
		}

		exit;
	}

	public function updatenotesAction()
	{
		$this->_helper->layout()->disableLayout();

		$id = $this->_getParam("image_id",0);
		$last_note = $this->_getParam("last_note",0);

		$sql = $this->_db->quoteInto("SELECT tn.*, CONCAT(u.FirstName, ' ', u.LastName) AS name, u.Roles FROM concept_notes AS tn LEFT JOIN Users_BugTracker AS u ON tn.user_id = u.ID
				WHERE tn.concept_image_id = ? AND tn.id > '$last_note' ORDER BY tn.created ASC",$id);
		$this->view->notes = $this->_db->fetchAssoc($sql);
	}

	public function deletenoteAction()
	{
		$this->_helper->layout()->disableLayout();

		$id = $this->_getParam("note_id",0);

		$this->_db->delete("concept_notes","id=$id");

		exit;
	}

	public function reorderAction()
	{
		$list = $this->_getParam("list",array());

		$c=0;
		foreach ($list as $id) {
			if (is_numeric($id)) {
				$this->_db->update("concept_images",array("zorder"=>$c),"id=$id");
				$c++;
			}
		}

		exit;
	}

	function init()
	{
		$bootstrap = $this->getInvokeArg('bootstrap'); // gets the boostrapper
		$resource = $bootstrap->getPluginResource('multidb'); //multi db support
		$this->view->db = $this->_db = $resource->getDefaultDb();

		$options = $bootstrap->getOptions();
		$this->view->license = $options['key']['aurigma'];
		$this->view->concept_path = $this->_concept_path = $options['concept']['path'];

		$this->view->placeholder('section')->set("concepts");

		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('portal'));
		if(!$auth->hasIdentity()){
			$auth->clearIdentity();
			$this->_redirect('/auth');
		}else{
			$this->view->user = $this->_user = $auth->getIdentity();
			$this->view->placeholder('logged_in')->set(true);
		}

		if ($this->_user->cyber_user == "yes") {
			$this->view->layout()->setLayout("cyber");
			$this->_clientID = $this->_getParam("clientID",0);
			$this->_userID = 0;
		}else {
			$this->_clientID = $this->_user->ClientID;
			$this->_userID = $this->_user->ID;
		}

		$subSectionMenu = '<li id="subnav-conceptview"><a href="/concepts/"><span class="subnav-size">View Concepts</span></a></li>';
		if ($this->_user->cyber_user == "yes") {
			$subSectionMenu .= '<li id="subnav-conceptadd"><a href="/concepts/add"><span class="subnav-size">Add Concept</span></a></li>';
		}
		$this->view->placeholder("subSectionMenu")->set($subSectionMenu);
	}
}



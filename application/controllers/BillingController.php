<?php
class BillingController extends Cny_Controller_LayoutAction
{
    public function indexAction()
    {

    }
    public function invoicesAction()
    {

    }
    public function payAction()
    {

    }
    function init()
	{
		$bootstrap = $this->getInvokeArg('bootstrap'); // gets the boostrapper
		$resource = $bootstrap->getPluginResource('multidb'); //multi db support
		$this->view->db = $this->_db = $resource->getDefaultDb();

		$options = $bootstrap->getOptions();
		$this->view->license = $options['key']['aurigma'];
		$this->view->concept_path = $this->_concept_path = $options['concept']['path'];

		$this->view->placeholder('section')->set("concepts");

		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('portal'));
		if(!$auth->hasIdentity()){
			$auth->clearIdentity();
			$this->_redirect('/auth');
		}else{
			$this->view->user = $this->_user = $auth->getIdentity();
			$this->view->placeholder('logged_in')->set(true);
		}

		if ($this->_user->cyber_user == "yes") {
			$this->view->layout()->setLayout("cyber");
			$this->_clientID = $this->_getParam("clientID",0);
			$this->_userID = 0;
		}else {
			$this->_clientID = $this->_user->ClientID;
			$this->_userID = $this->_user->ID;
		}

		$subSectionMenu = '<li id="subnav-conceptview"><a href="/concepts/"><span class="subnav-size">View Concepts</span></a></li>';
		if ($this->_user->cyber_user == "yes") {
			$subSectionMenu .= '<li id="subnav-conceptadd"><a href="/concepts/add"><span class="subnav-size">Add Concept</span></a></li>';
		}
		$this->view->placeholder("subSectionMenu")->set($subSectionMenu);
	}
}
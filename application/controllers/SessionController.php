<?php
class SessionController extends Cny_Controller_LayoutAction
{

	public function statusAction()
	{
		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('portal'));
		if(!$auth->hasIdentity()){
			$auth->clearIdentity();
			echo "0";
		}else{
			echo "1";
		}

		exit;
	}

	function init()
	{
		$bootstrap = $this->getInvokeArg('bootstrap'); // gets the boostrapper
		$resource = $bootstrap->getPluginResource('multidb'); //multi db support
		$this->view->db = $this->_db = $resource->getDefaultDb();

	}
}
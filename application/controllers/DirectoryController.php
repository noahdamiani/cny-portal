<?php
class DirectoryController extends Cny_Controller_LayoutAction
{

	public function indexAction()
	{
		$this->view->placeholder('sub_section')->set("directory");

		$search = new Zend_Session_Namespace('directory_search');
    	$mask = "";
    	$mask = $this->view->mask = $this->_getParam("mask","");

		$page = $this->getRequest()->getParam('page',1);
		$this->view->dir = $dir = $this->_getParam('dir','ASC');
		$this->view->sort = $sort = $this->_getParam('sort','Last_Name');

		$select = $this->_db->select();
		$select->from(array("ud"=>"UserDirectory"), "");
		$select->joinLeft(array("d"=>"Directory"),"d.id = ud.directory_id","*");
		$select->where("ud.user_id = {$this->_user->Cyberny_Users_ID} ");
		$select ->order(array("$sort $dir"));

		if ($mask) {
			$select->where("d.first_name LIKE '%$mask%' OR d.last_name LIKE '%$mask%' OR d.company LIKE '%$mask%' ");
		}

		$paginator = Zend_Paginator::factory($select);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(50);
		$this->view->contacts = $paginator;

		$sql = "SELECT id, CONCAT(first_name, ' ', last_name) AS name FROM Directory ORDER BY last_name ASC";
		$this->view->directory = $this->_db->fetchPairs($sql);
	}

	public function adddirectoryAction()
	{
		if( $this->getRequest()->isPost() ){
			$data = $this->_getParam("data",array());
			$data['user_id'] = $this->_user->Cyberny_Users_ID;

			$this->_db->insert("UserDirectory",$data);

			$this->_redirect("/directory/");
		}
	}

	public function allAction()
	{
		$this->view->placeholder('sub_section')->set("directory");

		$search = new Zend_Session_Namespace('directory_search');
    	$mask = "";
    	$mask = $this->view->mask = $this->_getParam("mask","");

		$page = $this->getRequest()->getParam('page',1);
		$this->view->dir = $dir = $this->_getParam('dir','ASC');
		$this->view->sort = $sort = $this->_getParam('sort','Last_Name');

		$select = $this->_db->select();
		$select->from(array("d"=>"Directory"),"*");
		$select ->order(array("$sort $dir"));

		if ($mask) {
			$select->where("d.first_name LIKE '%$mask%' OR d.last_name LIKE '%$mask%' OR d.company LIKE '%$mask%' ");
		}

		$paginator = Zend_Paginator::factory($select);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(50);
		$this->view->contacts = $paginator;
	}

	public function addAction()
	{
		$this->view->placeholder('sub_section')->set("contactadd");

		if( $this->getRequest()->isPost() ){
			$data = $this->_getParam("data",array());
			$data['UserID'] = $this->_user->Cyberny_Users_ID;

			$this->_db->insert("Directory",$data);
			$id = $this->_db->lastInsertId();

			if ($id) {
				$contact['user_id'] = $this->_user->Cyberny_Users_ID;
				$contact['directory_id'] = $id;

				$this->_db->insert("UserDirectory",$contact);
			}

			$this->_redirect("/directory/");
		}
	}

	public function editAction()
	{
		$this->view->placeholder('sub_section')->set("contactedit");
		$id = $this->_getParam('id',0);

		$sql = $this->_db->quoteInto("SELECT * FROM Directory WHERE id = ?",$id);
		$this->view->contact = $this->_db->fetchRow($sql);

		if( $this->getRequest()->isPost() ){
			$data = $this->_getParam("data",array());

			$this->_db->update("Directory",$data,"id=$id");

			$this->_redirect("/directory/");
		}
	}

	public function viewAction()
	{
		$this->view->placeholder('sub_section')->set("contactview");
		$id = $this->_getParam('id',0);

		$sql = $this->_db->quoteInto("SELECT * FROM Directory WHERE id = ?",$id);
		$this->view->contact = $this->_db->fetchRow($sql);
	}

	public function deleteAction()
	{
		$id = $this->_getParam('id',0);

		$this->_db->delete("UserDirectory","directory_id=$id AND user_id = {$this->_user->Cyberny_Users_ID}");

		$this->_redirect("/directory/");
	}

	public function deletecontactAction()
	{
		$id = $this->_getParam('id',0);

		$this->_db->delete("Directory","id=$id");
		$this->_db->delete("UserDirectory","directory_id=$id");

		$this->_redirect("/directory/");
	}

	function init()
	{
		$bootstrap = $this->getInvokeArg('bootstrap'); // gets the boostrapper
		$resource = $bootstrap->getPluginResource('multidb'); //multi db support
		$this->_db = $resource->getDefaultDb();

		$this->view->placeholder('section')->set("directory");
		$this->view->layout()->setLayout("cyber");

		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('portal'));
		if(!$auth->hasIdentity()){
			$auth->clearIdentity();
			$this->_redirect('/auth');
		}else{
			$this->view->user = $this->_user = $auth->getIdentity();
			$this->view->placeholder('logged_in')->set(true);

			if ($this->_user->cyber_user != "yes") {
				$auth->clearIdentity();
				$this->_redirect('/auth');
			}
		}

		$subSectionMenu = '<li id="subnav-viewcontacts"><a href="/directory"><span class="subnav-size">My Contacts</span></a></li>
							<li id="subnav-allcontacts"><a href="/directory/all"><span class="subnav-size">All Contacts</span></a></li>
							<li id="subnav-addcontact"><a href="/directory/add"><span class="subnav-size">Add Contact</span></a></li>
							';
		$this->view->placeholder("subSectionMenu")->set($subSectionMenu);
	}
}



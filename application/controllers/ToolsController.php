<?php
class ToolsController extends Cny_Controller_LayoutAction
{
	public function indexAction()
	{
		//
	}

	public function sitemapAction()
	{
		$this->_sitemap = new Zend_Session_Namespace('sitemap');
		$this->_sitemap->setExpirationSeconds(1);
	}

	public function sitemapCreateAction()
	{
		$this->_sitemap = new Zend_Session_Namespace('sitemap');
		$sitemap = '';

		if( $this->getRequest()->isPost() ){
			$url = $this->_getParam("url","");

			if (stripos($url,"http://") === false && stripos($url,"https://") === false) {
	    		$url = trim("http://{$url}");
	    	}

			//Fetch the page using the CURL Library
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
			$store = curl_exec ($ch);
			curl_close ($ch);

			//Strip the Host name from the Url (used later)
			preg_match('@^(?:http://)?([^/]+)@i',$url, $matches);
			$host = $matches[1];

			//Take out the directory name of your url
			if(strrpos($url, "/") > 10)
				$root= substr($url,0,strrpos($url, "/"));
			else $root= $url;

			//Create an array to save urls
			$links=array();

			//Strip all links from the page
			$regexp = "<a\s[^>]*href=(\"??)([^\" >]*?)\\1[^>]*>(.*)<\/a>";
			preg_match_all("/$regexp/siU", $store, $matches, PREG_SET_ORDER);

			//Loop inside the links and rebuild the corresponding full urls.
			foreach ($matches as $val) {
				if(strpos($val[2],'#') === FALSE && strpos($val[2],'http://') === FALSE && strpos($val[2],'@') === FALSE) {
					if(strpos(trim($val[2]),'/') == 0 && strpos(trim($val[2]),'/') !== FALSE)
						$link='http://'.$host.trim($val[2]);
					else
						$link='http://'.$root.'/'.trim($val[2]);

					if(!in_array($link,$links)) $links[] = $link;
				}

			}

			$date=date('Y-m-d');

			//Print all results inside a textarea box:
			$sitemap .= '<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.google.com/schemas/sitemap/0.84" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.google.com/schemas/sitemap/0.84 http://www.google.com/schemas/sitemap/0.84/sitemap.xsd">';

			//Loop inside all links and add them to the sitemap using a default priority as 0.9 and a default changefreq as daily.
			foreach ($links as $val) {
				$sitemap .= "
<url>
	<loc>$val</loc>
	<lastmod>$date</lastmod>
	<changefreq>daily</changefreq>
	<priority>0.9</priority>
</url>
";
			}
			$sitemap .= '</urlset>';
			$this->_sitemap->code = $this->view->sitemap = $sitemap;


			//want to auto FTP the file but don't know full path to upload files to automatically
			/*
			$conn_id="ftp.yourserver.com";//Write in the format "ftp.servername.com"
			// make a connection to the ftp server
			$conn_id = ftp_connect ( $ftp_server );
			$ftp_user_name="username";
			$ftp_user_pass="password";
			// login with username and password
			$login_result = ftp_login ( $conn_id , $ftp_user_name , $ftp_user_pass );

			// check connection
			if ((! $conn_id ) || (! $login_result )) {
		        $this->view->error = "FTP connection has failed! <br/>";
		        $this->view->error .= "Attempted to connect to $ftp_server for user $ftp_user_name <br/>";
		    } else {
		        $destination_file="sitemap.xml";
				$source_file="sitemap.xml";

				// upload the file
				$upload = ftp_put ( $conn_id , $destination_file , $source_file , FTP_BINARY );

				// check upload status
				if (! $upload ) {
			        $this->view->error .= "FTP upload has failed! <br/>";
			    } else {
			        $this->view->error .= "Uploaded $source_file to $ftp_server as $destination_file <br/>";
			    }
		    }

			// close the FTP stream
			ftp_close ( $conn_id );
			*/
		}
	}

	public function sitemapDownloadAction()
	{
		$this->_sitemap = new Zend_Session_Namespace('sitemap');
		$this->_helper->layout()->disableLayout();

		header("Content-type: text/plain");
		header("Content-Disposition: attachment; filename=sitemap.xml");
		echo $this->_sitemap->code;
		exit;
	}

	public function sitemapSubmitAction()
	{
		if( $this->getRequest()->isPost() ){
			// Your SiteMap URL
			$sm_url = $this->_getParam("url","");
			if (stripos($sm_url,"http://") === false && stripos($sm_url,"https://") === false) {
	    		$sm_url = trim("http://{$sm_url}");
	    	}

			// Search Engine URLs
			$se_list[] = array("site" => "Google", "url" => "http://www.google.com/webmasters/sitemaps/ping?sitemap=");
			$se_list[] = array("site" => "Bing", "url" => "http://www.bing.com/webmaster/ping.aspx?siteMap=");
			$se_list[] = array("site" => "Yahoo", "url" => "http://search.yahooapis.com/SiteExplorerService/V1/updateNotification?appid=YahooDemo&url=");
			$se_list[] = array("site" => "Ask.com", "url" => "http://submissions.ask.com/ping?sitemap=");

			// Ping Them!
			$this->view->message = '';
			foreach($se_list as $i){
				$url_to_ping = $i['url'] . $sm_url;
				$data = file_get_contents($url_to_ping);

				if($data){
					$this->view->message .= "<h1>".$i['site']."</h1>".$data;
				}else{
					$this->view->message .= "Failed loading to ".$i['site']." ($url_to_ping)";
				}
				$this->view->message .= "<br/><br/>\r\n";
			}
		}
	}

	function init()
	{
		$bootstrap = $this->getInvokeArg('bootstrap'); // gets the boostrapper
		$resource = $bootstrap->getPluginResource('multidb'); //multi db support
		$this->_db = $resource->getDefaultDb();

		$this->view->placeholder('section')->set("tools");
		$this->view->layout()->setLayout("cyber");

		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('portal'));
		if(!$auth->hasIdentity()){
			$auth->clearIdentity();
			$this->_redirect('/auth');
		}else{
			$this->view->user = $this->_user = $auth->getIdentity();
			$this->view->placeholder('logged_in')->set(true);

			if ($this->_user->cyber_user != "yes") {
				$auth->clearIdentity();
				$this->_redirect('/auth');
			}
		}

		$subSectionMenu = '<li id="subnav-viewserver"><a href="/tools/sitemap"><span class="subnav-size">Generate Sitemap</span></a></li>
							<li id="subnav-viewserver"><a href="/tools/sitemap-submit"><span class="subnav-size">Submit Sitemap</span></a></li>
							';
		$this->view->placeholder("subSectionMenu")->set($subSectionMenu);
	}
}
?>
<?php
class ReportingController extends Cny_Controller_LayoutAction
{
	public function indexAction()
	{
		if ($this->_user->cyber_user != "yes") {
			$this->_redirect("/reporting/details");
		}

		$lastday = date('t');
		$this->view->from_date = $from_date = $this->_getParam("from_date",date("Y-m-01"));
		$this->view->to_date = $to_date = $this->_getParam("to_date",date("Y-m-$lastday"));

		$from_time = strtotime($this->view->from_date);
		$to_time = strtotime($this->view->to_date." 23:59:59");

		$this->view->placeholder('sub_section')->set("retainer");

		$search = new Zend_Session_Namespace('retainer_search');
    	$mask = "";

		$page = $this->getRequest()->getParam('page',1);
		$this->view->dir = $dir = $this->_getParam('dir','DESC');
		$this->view->sort = $sort = $this->_getParam('sort','percent');

		$select = $this->_db->select();
		$select->from(array("r"=>"Retainers"),
			array("id","ClientID","Hours",
				"currentHours"=>new Zend_Db_Expr("(SELECT ABS(SUM(pa.EndTime - pa.StartTime)) FROM Projects p LEFT JOIN ProjectActivity pa ON p.id = pa.ProjectID WHERE p.RetainerStatus = 'y' AND pa.StartTime >= {$from_time} AND pa.EndTime <= {$to_time} AND p.ClientID = r.ClientID GROUP BY p.ClientID)"),
				"percent"=>new Zend_Db_Expr("( ROUND( ( (SELECT ABS(SUM(pa.EndTime - pa.StartTime)) FROM Projects p LEFT JOIN ProjectActivity pa ON p.id = pa.ProjectID WHERE p.RetainerStatus = 'y' AND pa.StartTime >= {$from_time} AND pa.EndTime <= {$to_time} AND p.ClientID = r.ClientID GROUP BY p.ClientID) / (r.Hours*3600) ) * 100 ) )"))
			);
		$select->joinLeft(array("c"=>"Clients"), "r.ClientID = c.id",array("ClientName"));
		$select->where("r.EndDate IS NULL OR r.EndDate >= '{$this->view->to_date}' OR r.EndDate = '0000-00-00'");
		//$select->where("r.Hours > 0");
		//$select->where("r.Status = 'enabled'");
		$select ->order(array("$sort $dir"));

		$paginator = Zend_Paginator::factory($select);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(50);
		$this->view->retainers = $paginator;

		/* //for when we upgrade to PHP 5.3+
		$date1 = new DateTime($this->view->from_date);
		$date2 = new DateTime($this->view->to_date);
		$interval = $date1->diff($date2);
		$month_diff = $interval->y*12 + $interval->m;
		if ($interval->d) $month_diff++;
		*/

		$month_diff = abs(GetMonthsFromDate($to_time) - GetMonthsFromDate($from_time));
		if ($from_date != $to_date) $month_diff++;

		$this->view->client_retainer = array();
		foreach ($this->view->retainers as $k => $retainer) {
			$available = $total = 0;
			for ($c=0;$c<$month_diff;$c++) {
				$ret_time = strtotime("{$from_date} + $c month");
				$ret_to_time = strtotime("{$from_date} + $c month + 1 month - 1 sec");

				$due = $this->_db->fetchOne("SELECT Hours FROM Retainers WHERE ClientID = {$retainer['ClientID']} AND (StartDate <= '".date("Y-m-01",$ret_time)."' AND (EndDate IS NULL OR EndDate >= '".date("Y-m-01",$ret_time)."' OR EndDate = '0000-00-00' ) )");

				$sql = "SELECT SUM(pa.EndTime-pa.StartTime) FROM ProjectActivity AS pa, Projects AS p WHERE pa.ProjectID = p.id AND p.RetainerStatus='y' AND p.ClientID = {$retainer['ClientID']} AND pa.StartTime >= {$ret_time} AND pa.EndTime <= {$ret_to_time}";
				$used = $this->_db->fetchOne($sql);

				$available += ($due-(sec2dec($used, true)));
				$total += $due;
			}
			$this->view->client_retainer[$retainer['ClientID']]['available'] = $available;
			$this->view->client_retainer[$retainer['ClientID']]['due'] = $total;
		}
	}

	public function detailsAction()
	{
		$lastday = date('t');
		$this->view->from_date = $this->_getParam("from_date",date("Y-m-01"));
		$this->view->to_date = $this->_getParam("to_date",date("Y-m-$lastday"));

		$from_time = strtotime($this->view->from_date);
		$to_time = strtotime($this->view->to_date." 23:59:59");

		/* //for when we upgrade to PHP 5.3+
		$date1 = new DateTime($this->view->from_date);
		$date2 = new DateTime($this->view->to_date);
		$interval = $date1->diff($date2);
		$month_diff = $interval->y*12 + $interval->m;
		if ($interval->d) $month_diff++;
		*/

		$month_diff = abs(GetMonthsFromDate($to_time) - GetMonthsFromDate($from_time));
		if ($this->view->from_date != $this->view->to_date) $month_diff++;

		$this->view->placeholder('sub_section')->set("retainer");

		$search = new Zend_Session_Namespace('retainer_search');
		$this->view->mask = $mask = $this->_getParam("mask","");

		$page = $this->getRequest()->getParam('page',1);
		$this->view->dir = $dir = $this->_getParam('dir','ASC');
		$this->view->sort = $sort = $this->_getParam('sort','owner');

		$select = $this->_db->select();
		$select->from(array("r"=>"Retainers"),
			array("id","ClientID","Hours",
				"currentHours"=>new Zend_Db_Expr("(SELECT ABS(SUM(pa.EndTime - pa.StartTime)) FROM Projects p LEFT JOIN ProjectActivity pa ON p.id = pa.ProjectID WHERE p.RetainerStatus = 'y' AND pa.StartTime >= {$from_time} AND pa.EndTime <= {$to_time} AND p.ClientID = r.ClientID GROUP BY p.ClientID)"),
				"percent"=>new Zend_Db_Expr("( ROUND( ( (SELECT ABS(SUM(pa.EndTime - pa.StartTime)) FROM Projects p LEFT JOIN ProjectActivity pa ON p.id = pa.ProjectID WHERE p.RetainerStatus = 'y' AND pa.StartTime >= {$from_time} AND pa.EndTime <= {$to_time} AND p.ClientID = r.ClientID GROUP BY p.ClientID) / (r.Hours*3600) ) * 100 ) )"))
			);
		$select->joinLeft(array("c"=>"Clients"), "r.ClientID = c.id",array("ClientName"));
		$select->where("r.ClientID = {$this->_clientID}");

		$this->view->client = $this->_db->fetchRow($select->__toString());

		$this->view->current_retainer = $this->_db->fetchOne("SELECT Hours FROM Retainers WHERE ClientID = {$this->_clientID} AND StartDate <= DATE(NOW()) AND (EndDate >= DATE(NOW()) OR EndDate IS NULL) ");
		$this->view->retainer_due = $this->_db->fetchOne("SELECT SUM(Hours) FROM Retainers WHERE ClientID = {$this->_clientID} AND (StartDate >= '".date("Y-m-01",$from_time)."' OR (StartDate <= '".date("Y-m-01",$from_time)."' AND (EndDate IS NULL OR EndDate >= '".date("Y-m-01",$from_time)."' ) ) )");
		$this->view->retainer_due = 0;
		for ($c=0;$c<$month_diff;$c++) {
			$ret_time = strtotime("{$this->view->from_date} + $c month");
			$this->view->retainer_due += $this->_db->fetchOne("SELECT Hours FROM Retainers WHERE ClientID = {$this->_clientID} AND (StartDate <= '".date("Y-m-01",$ret_time)."' AND (EndDate IS NULL OR EndDate >= '".date("Y-m-01",$ret_time)."' OR EndDate = '0000-00-00' ) )");
		}

		$select = $this->_db->select();
		$select->from(array("r"=>"Retainers"),
			array(
				"currentHours"=>new Zend_Db_Expr("(SELECT ABS(SUM(pa.EndTime - pa.StartTime)) FROM ProjectActivity pa WHERE pa.StartTime >= {$from_time} AND pa.EndTime <= {$to_time} AND pa.ProjectID = p.id GROUP BY pa.ProjectID)"))
			);
		$select->joinLeft(array("p"=>"Projects"), "r.ClientID = p.ClientID",array("Title", "id", "ActualEndDate", "job_code"));
		$select->joinLeft(array("u"=>"Users"), "p.UserID = u.ID",array("owner"=>new Zend_Db_Expr("CONCAT(u.FirstName, ' ', u.LastName)")));
		$select->joinLeft(array("ps"=>"ProjectStatus"), "p.ProjectStatusID = ps.id",array("ProjectStatusName"));
		$select->joinLeft(array("pa"=>"ProjectActivity"), "p.id = pa.ProjectID",array());
		$select->where("p.RetainerStatus='y' AND r.ClientId = {$this->_clientID} AND pa.StartTime >= {$from_time} AND pa.EndTime <= {$to_time}");
		if ($mask) $select->where("p.Title LIKE '%$mask%' OR p.job_code LIKE '%$mask%'");
		$select->group(array("u.ID","p.id"));
		$select->order(array("$sort $dir"));

		$paginator = Zend_Paginator::factory($select);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(50);
		$this->view->tickets = $paginator;

		//in order to get all the tickets and add up the hours
		$this->view->ticket_total = 0;
		$stmt = $this->_db->query($select);
		$all_tickets = $stmt->fetchAll();
		foreach ($all_tickets as $t) {
			$this->view->ticket_total += $t['currentHours'];
		}

		if($this->_getParam("export","")) {
			$this->_helper->layout()->disableLayout();

			$stmt = $this->_db->query($select);
			$this->view->tickets = $stmt->fetchAll();

			if ($this->_user->cyber_user == "yes") {
				$this->render('export');
			}else {
				$this->render('clientexport');
			}
		}else {
			if ($this->_user->cyber_user != "yes") {
				$this->render('view');
			}
		}
	}

	public function reportAction() {
		$lastday = date('t');
		$this->view->from_date = $this->_getParam("from_date",date("Y-m-01"));
		$this->view->to_date = $this->_getParam("to_date",date("Y-m-$lastday"));

		$this->view->placeholder('sub_section')->set("retainer");

		$search = new Zend_Session_Namespace('retainer_report_search');
		$this->view->mask = $mask = $this->_getParam("mask","");

		$page = $this->getRequest()->getParam('page',1);
		$this->view->dir = $dir = $this->_getParam('dir','ASC');
		$this->view->sort = $sort = $this->_getParam('sort','owner');

		$select = $this->_db->select();
		/*
		$select->from(array("r"=>"Retainers"),
			array("id","ClientID","Hours",
				"currentHours"=>new Zend_Db_Expr("(SELECT ABS(SUM(pa.EndTime - pa.StartTime)) FROM Projects p LEFT JOIN ProjectActivity pa ON p.id = pa.ProjectID WHERE p.RetainerStatus = 'y' AND pa.StartTime >= {$from_time} AND pa.EndTime <= {$to_time} AND p.ClientID = r.ClientID GROUP BY p.ClientID)"),
				"percent"=>new Zend_Db_Expr("( ROUND( ( (SELECT ABS(SUM(pa.EndTime - pa.StartTime)) FROM Projects p LEFT JOIN ProjectActivity pa ON p.id = pa.ProjectID WHERE p.RetainerStatus = 'y' AND pa.StartTime >= {$from_time} AND pa.EndTime <= {$to_time} AND p.ClientID = r.ClientID GROUP BY p.ClientID) / (r.Hours*3600) ) * 100 ) )"))
		);
		$select->joinLeft(array("c"=>"Clients"), "r.ClientID = c.id",array("ClientName"));
		$select->where("r.ClientID = {$this->_clientID}");

		$this->view->client = $this->_db->fetchRow($select->__toString());
		*/

		$this->view->client = $this->_db->fetchRow("SELECT * FROM Clients WHERE id = '{$this->_clientID}'");

		$select->from(array("r"=>"Retainers"));
		$select->where("r.ClientID = {$this->_clientID}");
		$select->where("r.StartDate <= ?",$this->view->from_date);
		$select->where("(r.EndDate <= ? OR r.EndDate IS NULL)",$this->view->to_date);
		$select->order("r.StartDate ASC");

		$sql = "SELECT * FROM Retainers WHERE ClientID = {$this->_clientID} ORDER BY StartDate ASC";
		$retainers = $this->_db->fetchAssoc($sql);

		$this->view->retainers = array(); $this->view->tickets = array();
		foreach ($retainers as $key => $retainer) {
			$this->view->retainers[$key] = $retainer;
			$this->view->retainers[$key]['tickets'] = array();

			if (!$retainer['EndDate']) $retainer['EndDate'] = date("Y-m-d");

			$start_date = (strtotime($retainer['StartDate']) > strtotime($this->view->from_date)?$retainer['StartDate']:$this->view->from_date);
			$end_date = (strtotime($retainer['EndDate']) < strtotime($this->view->to_date)?$retainer['EndDate']:$this->view->to_date);

			for ($i = GetMonthsFromDate($start_date); $i <= GetMonthsFromDate($end_date); $i++) {
				$dates = GetDatesFromMonths($i);

				$from_time = strtotime($dates['start']);
				$to_time = strtotime($dates['end']." 23:59:59");

				$select = $this->_db->select();
				$select->from(array("r"=>"Retainers"),
					array(
						"currentHours"=>new Zend_Db_Expr("(SELECT ABS(SUM(pa.EndTime - pa.StartTime)) FROM ProjectActivity pa WHERE pa.StartTime >= {$from_time} AND pa.EndTime <= {$to_time} AND pa.ProjectID = p.id GROUP BY pa.ProjectID)"))
				);
				$select->joinLeft(array("p"=>"Projects"), "r.ClientID = p.ClientID",array("Title", "id", "ActualEndDate", "job_code"));
				$select->joinLeft(array("u"=>"Users"), "p.UserID = u.ID",array("owner"=>new Zend_Db_Expr("CONCAT(u.FirstName, ' ', u.LastName)")));
				$select->joinLeft(array("ps"=>"ProjectStatus"), "p.ProjectStatusID = ps.id",array("ProjectStatusName"));
				$select->joinLeft(array("pa"=>"ProjectActivity"), "p.id = pa.ProjectID",array());
				$select->where("p.RetainerStatus='y' AND r.ClientId = {$this->_clientID} AND pa.StartTime >= {$from_time} AND pa.EndTime <= {$to_time}");
				if ($mask) $select->where("p.Title LIKE '%$mask%' OR p.job_code LIKE '%$mask%'");
				$select->group(array("u.ID","p.id"));
				$select->order(array("$sort $dir"));

				$tickets = $this->_db->fetchAssoc($select->__toString());

				$this->view->retainers[$key]['tickets'][$dates['start']] = $tickets;

				$this->view->tickets = array_merge($this->view->tickets,$tickets);
			}

		}

		if($this->_getParam("export","")) {
			$this->_helper->layout()->disableLayout();

			//$stmt = $this->_db->query($select);
			//$this->view->tickets = $stmt->fetchAll();

			if ($this->_user->cyber_user == "yes") {
				$this->render('export');
			}else {
				$this->render('clientexport');
			}
		}
/*
		$paginator = Zend_Paginator::factory($select);
		$paginator->setCurrentPageNumber($page);
		$paginator->setItemCountPerPage(50);
		$this->view->tickets = $paginator;

		if($this->_getParam("export","")) {
			$this->_helper->layout()->disableLayout();

			$stmt = $this->_db->query($select);
			$this->view->tickets = $stmt->fetchAll();

			if ($this->_user->cyber_user == "yes") {
				$this->render('export');
			}else {
				$this->render('clientexport');
			}
		}else {
			if ($this->_user->cyber_user != "yes") {
				$this->render('view');
			}
		}
*/
	}

	public function ytdAction()
	{
		$this->view->placeholder('sub_section')->set("retainer");

		$lastday = date('t');
		$this->view->from_date = $from_date = $this->_getParam("from_date",date("Y-01-01"));
		$this->view->to_date = $to_date = $this->_getParam("to_date",date("Y-m-$lastday"));

		$this->view->client = $this->_db->fetchRow("SELECT * FROM Clients WHERE id = '{$this->_clientID}'");

		$from_time = strtotime($from_date);
		$to_time = strtotime($to_date." 23:59:59");

		/* //for when we upgrade to PHP 5.3+
		$date1 = new DateTime($this->view->from_date);
		$date2 = new DateTime($this->view->to_date);
		$interval = $date1->diff($date2);
		$month_diff = $interval->y*12 + $interval->m;
		if ($interval->d) $month_diff++;
		*/

		$month_diff = abs(GetMonthsFromDate($to_time) - GetMonthsFromDate($from_time));
		if ($from_date != $to_date) $month_diff++;

		$this->view->retainer_due = $this->view->retainer_use = array();
		for ($c=0;$c<$month_diff;$c++) {
			$ret_time = strtotime("{$from_date} + $c month");
			$ret_to_time = strtotime("{$from_date} + $c month + 1 month - 1 sec");

			$this->view->retainer_due[date("Ym",$ret_time)] = $this->_db->fetchOne("SELECT Hours FROM Retainers WHERE ClientID = {$this->_clientID} AND (StartDate <= '".date("Y-m-01",$ret_time)."' AND (EndDate IS NULL OR EndDate >= '".date("Y-m-01",$ret_time)."' OR EndDate = '0000-00-00' ) )");

			$sql = "SELECT SUM(pa.EndTime-pa.StartTime) FROM ProjectActivity AS pa, Projects AS p WHERE pa.ProjectID = p.id AND p.RetainerStatus='y' AND p.ClientID = {$this->_clientID} AND pa.StartTime >= {$ret_time} AND pa.EndTime <= {$ret_to_time}";
			$this->view->retainer_use[date("Ym",$ret_time)] = $this->_db->fetchOne($sql);
		}

		if($this->_getParam("export","")) {
			$this->_helper->layout()->disableLayout();
			$this->render('ytdexport');
		}

	}

	public function donutAction()
	{
		$lastday = date('t');
		$this->view->from_date = $this->_getParam("from_date",date("Y-m-01"));
		$this->view->to_date = $this->_getParam("to_date",date("Y-m-$lastday"));

		$from_time = strtotime($this->view->from_date);
		$to_time = strtotime($this->view->to_date." 23:59:59");

		/* //for when we upgrade to PHP 5.3+
		$date1 = new DateTime($this->view->from_date);
		$date2 = new DateTime($this->view->to_date);
		$interval = $date1->diff($date2);
		$month_diff = $interval->y*12 + $interval->m;
		if ($interval->d) $month_diff++;
		*/

		$month_diff = abs(GetMonthsFromDate($to_time) - GetMonthsFromDate($from_time));
		if ($this->view->from_date != $this->view->to_date) $month_diff++;

		$this->view->placeholder('sub_section')->set("retainer");

		$select = $this->_db->select();
		$select->from(array("r"=>"Retainers"),
			array("id","ClientID","Hours",
				"currentHours"=>new Zend_Db_Expr("(SELECT ABS(SUM(pa.EndTime - pa.StartTime)) FROM Projects p LEFT JOIN ProjectActivity pa ON p.id = pa.ProjectID WHERE p.RetainerStatus = 'y' AND pa.StartTime >= {$from_time} AND pa.EndTime <= {$to_time} AND p.ClientID = r.ClientID GROUP BY p.ClientID)"),
				"percent"=>new Zend_Db_Expr("( ROUND( ( (SELECT ABS(SUM(pa.EndTime - pa.StartTime)) FROM Projects p LEFT JOIN ProjectActivity pa ON p.id = pa.ProjectID WHERE p.RetainerStatus = 'y' AND pa.StartTime >= {$from_time} AND pa.EndTime <= {$to_time} AND p.ClientID = r.ClientID GROUP BY p.ClientID) / (r.Hours*3600) ) * 100 ) )"))
		);
		$select->joinLeft(array("c"=>"Clients"), "r.ClientID = c.id",array("ClientName"));
		$select->where("r.ClientID = {$this->_clientID}");

		$this->view->client = $this->_db->fetchRow($select->__toString());

		$this->view->current_retainer = $this->_db->fetchOne("SELECT Hours FROM Retainers WHERE ClientID = {$this->_clientID} AND StartDate <= DATE(NOW()) AND (EndDate >= DATE(NOW()) OR EndDate IS NULL) ");
		$this->view->retainer_due = $this->_db->fetchOne("SELECT SUM(Hours) FROM Retainers WHERE ClientID = {$this->_clientID} AND (StartDate >= '".date("Y-m-01",$from_time)."' OR (StartDate <= '".date("Y-m-01",$from_time)."' AND (EndDate IS NULL OR EndDate >= '".date("Y-m-01",$from_time)."' ) ) )");
		$this->view->retainer_due = 0;
		for ($c=0;$c<$month_diff;$c++) {
			$ret_time = strtotime("{$this->view->from_date} + $c month");
			$this->view->retainer_due += $this->_db->fetchOne("SELECT Hours FROM Retainers WHERE ClientID = {$this->_clientID} AND (StartDate <= '".date("Y-m-01",$ret_time)."' AND (EndDate IS NULL OR EndDate >= '".date("Y-m-01",$ret_time)."' OR EndDate = '0000-00-00' ) )");
		}

		$select = $this->_db->select();
		$select->from(array("r"=>"Retainers"),
			array(
				"currentHours"=>new Zend_Db_Expr("(SELECT ABS(SUM(pa.EndTime - pa.StartTime)) FROM ProjectActivity pa WHERE pa.StartTime >= {$from_time} AND pa.EndTime <= {$to_time} AND pa.ProjectID = p.id GROUP BY pa.ProjectID)"))
		);
		$select->joinLeft(array("p"=>"Projects"), "r.ClientID = p.ClientID",array("Title", "id", "ActualEndDate", "job_code"));
		$select->joinLeft(array("u"=>"Users"), "p.UserID = u.ID",array("owner"=>new Zend_Db_Expr("CONCAT(u.FirstName, ' ', u.LastName)")));
		$select->joinLeft(array("ps"=>"ProjectStatus"), "p.ProjectStatusID = ps.id",array("ProjectStatusName"));
		$select->joinLeft(array("pa"=>"ProjectActivity"), "p.id = pa.ProjectID",array());
		$select->where("p.RetainerStatus='y' AND r.ClientId = {$this->_clientID} AND pa.StartTime >= {$from_time} AND pa.EndTime <= {$to_time}");
		$select->group(array("u.ID","p.id"));

		//in order to get all the tickets and add up the hours
		$this->view->ticket_total = 0;
		$stmt = $this->_db->query($select);
		$all_tickets = $stmt->fetchAll();
		foreach ($all_tickets as $t) {
			$this->view->ticket_total += $t['currentHours'];
		}


	}

	function init()
	{
		$bootstrap = $this->getInvokeArg('bootstrap'); // gets the boostrapper
		$resource = $bootstrap->getPluginResource('multidb'); //multi db support
		$this->_db = $resource->getDefaultDb();

		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('portal'));
		if(!$auth->hasIdentity()){
			$auth->clearIdentity();
			$this->_redirect('/auth');
		}else{
			$this->view->user = $this->_user = $auth->getIdentity();
			$this->view->placeholder('logged_in')->set(true);
		}

		if ($this->_user->cyber_user == "yes") {
			$this->view->layout()->setLayout("cyber");
			$this->_clientID = $this->_getParam("clientID",0);
			$this->_userID = 0;
			$this->view->placeholder('section')->set("clients");
		}else {
			$this->_clientID = $this->_user->ClientID;
			$this->_userID = $this->_user->ID;
			$this->view->placeholder('section')->set("retainer");
		}
		$this->view->clientID = $this->_clientID;

		$subSectionMenu = '<li id="subnav-retainers"><a href="/retainer"><span class="subnav-size">Retainer Tracker</span></a></li>
							';
		$this->view->placeholder("subSectionMenu")->set($subSectionMenu);
	}
}

function sec2hm ($sec, $padHours = false) {
	$hms = "";
	$hours = intval(intval($sec) / 3600);

	$hms .= ($padHours) ? str_pad($hours, 2, "0", STR_PAD_LEFT). 'h ' : $hours. 'h ';

	$minutes = intval(($sec / 60) % 60);

	$hms .= str_pad($minutes, 2, "0", STR_PAD_LEFT)."m";

	return $hms;
}

function sec2dec ($sec) {
	$hms = "";
	$hours = intval(intval($sec) / 3600);
	$minutes = intval(($sec / 60) % 60);

	$dec = round($hours + ($minutes/60) ,2);

	return $dec;
}

function dec2hm ($dec, $padHours = false) {
	$hms = "";
	$hours = intval($dec);
	$minutes = intval(($dec - $hours) * 60);

	$hms .= ($padHours) ? str_pad($hours, 2, "0", STR_PAD_LEFT). 'h ' : $hours. 'h ';
	$hms .= str_pad($minutes, 2, "0", STR_PAD_LEFT)."m";

	return $hms;
}

function GetMonthsFromDate($myDate) {
	if (!is_int($myDate))$myDate=strtotime($myDate);
	$year = (int) date('Y',$myDate);
	$months = (int) date('m', $myDate);
	$dateAsMonths = 12*$year + $months;
	return $dateAsMonths;
}

function GetDatesFromMonths($months) {
	$years = floor($months / 12);
	$month = $months % 12;

	if ($month == 0) { $years--; $month = 12; }

	$start = "$years/$month/01";
	$end = "$years/$month/".date("t",strtotime($start)); //makes a date like 2009/12/01
	return array("start"=>$start,"end"=>$end);
}
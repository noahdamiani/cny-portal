<?php
class CalendarController extends Cny_Controller_LayoutAction
{
    public function indexAction()
	{
		$this->view->vacation = $this->view->meeting = $this->view->general = 0;
		$ym = date('Y-m');

		//lets calculate number of days used as vacation, general, meeting
		//get all events this year for this user
		$year = date("Y");
		$sql = "SELECT ce.id, ce.start, ce.end, DATEDIFF(ce.end, ce.start) AS day_diff, ce.type
				FROM calendar_entries AS ce LEFT JOIN Users AS u ON ce.user_id = u.ID WHERE u.Disabled = 'no' AND ce.public = 'y'
				AND ce.start LIKE '$year-%' AND u.id = {$this->_user->Cyberny_Users_ID}";
		$events = $this->_db->fetchAssoc($sql);

		DEFINE ('ONEDAY', 60*60*24);

		// ESTABLISH THE HOLIDAYS
		$holidays = array
		(
			//New Years
			(date("w",strtotime("January 1"))==0)?"January 2":"January 1",
			//MLK Day
			date("F d",strtotime("2 weeks monday",mktime(0,0,0,1,1,date("Y")))),
			//Memorial Day
			date("F d",strtotime("last monday",mktime(0,0,0,6,1,date("Y")))),
			//July 4
			(date("w",strtotime("July 4"))==0)?"July 5":(date("w",strtotime("July 4"))==6)?"July 3":"July 4",
			//Labor Day
			date("F d",strtotime("monday",mktime(0,0,0,9,1,date("Y")))),
			//Thanksgiving
			date("F d",strtotime("3 weeks thursday",mktime(0,0,0,11,1,date("Y")))),
			date("F d",strtotime("3 weeks thursday",mktime(0,0,0,11,1,date("Y"))+ONEDAY)),
			//Christmas
			(date("w",strtotime("December 25"))==0)?"December 26":(date("w",strtotime("December 25"))==6)?"December 24":"December 25",
			//Check if New Years celebrated in this calendar year
			(date("w",strtotime("January 1 ".date("Y")+1))==6)?"December 31":""
		);

		foreach ($events as $e) {
			$start = strtotime($e['start']);
			$end = strtotime($e['end']);

			// CONVERT HOLIDAYS TO ISO DATES
			foreach ($holidays as $x => $holiday) {
				$holidays[$x] = date('Y-m-d', strtotime($holiday));
			}

			// MAKE AN ARRAY OF DATES
			$workdays = array();

			// ITERATE OVER THE DAYS
			$start = $start - ONEDAY;
			while ($start < $end) {
				$start = $start + ONEDAY;
				// ELIMINATE WEEKENDS - SAT AND SUN
				$weekday = date('D', $start);
				if (substr($weekday,0,1) == 'S') continue;
				// ELIMINATE HOLIDAYS
				$iso_date = date('Y-m-d', $start);
				if (in_array($iso_date, $holidays)) continue;
				$workdays[] = $iso_date;
			}

			$number_of_workdays = number_format(count($workdays));

			if ($e['type'] == "vacation") $this->view->vacation += $number_of_workdays;
			if ($e['type'] == "meeting") $this->view->meeting += $number_of_workdays;
			if ($e['type'] == "general") $this->view->general += $number_of_workdays;
		}

	}

	public function saveAction()
	{
		$action = $this->_getParam("act","");
		$id = $this->_getParam("id",0);

		$data = array();
		$data['title'] = $this->_getParam("title","");
		$data['start'] = date('Y-m-d',strtotime($this->_getParam("start","")));
		$data['end'] = date('Y-m-d',strtotime($this->_getParam("end","")));
		$data['description'] = $this->_getParam("description","");
		$data['user_id'] = $this->_user->Cyberny_Users_ID;
		$data['type'] = $this->_getParam("type","");
		$data['public'] = 'y';
		$data['modified'] = new Zend_Db_Expr("NOW()");

		if ($id && $action == 'save') {
			unset($data['user_id']);
			$this->_db->update("calendar_entries",$data,"id=$id");
		}
		elseif ($id && $action == 'delete') {
			$this->_db->delete("calendar_entries","id=$id");
		}
		elseif ($action == 'add'){
			$data['created'] = new Zend_Db_Expr("NOW()");
			$this->_db->insert("calendar_entries",$data);
		}

		echo "1";exit;
	}

	public function eventsAction()
	{
		$date = $this->_getParam("date",date("D F d Y"));
		$explode = explode(" ",$date);
		$ym = date('Y-m',strtotime($explode[1]." ".$explode[2]." ".$explode[3]));

		//get events
		$sql = "SELECT ce.id AS EventID, DATE_FORMAT(ce.start,'%Y-%m-%d') AS StartDateTime, DATE_FORMAT(ce.end,'%Y-%m-%d') AS EndDateTime, CONCAT(u.FirstName, ' ', u.LastName, ' - ', ce.title) AS Title, ce.description AS Description, ce.type AS CssClass, u.id AS UserID
				FROM calendar_entries AS ce LEFT JOIN Users AS u ON ce.user_id = u.ID WHERE u.Disabled = 'no' AND ce.public = 'y'
				AND ((ce.start LIKE '$ym%' OR ce.end LIKE '$ym%'))";
		$events = $this->_db->fetchAssoc($sql);

		$array = array();
		foreach ($events as $e) {
			$array[] = array("id"=>$e["EventID"], "start"=>strtotime($e["StartDateTime"]), "end"=>strtotime($e["EndDateTime"]), "title"=>$e["Title"], "description"=>($e["Description"])?$e["Description"]:$e["Title"], "className"=>$e["CssClass"], "UserID"=>$e["UserID"]);
		}

		echo Zend_Json_Encoder::encode($array);
		exit;
	}

	public function editAction()
	{
		$this->_helper->layout()->setLayout("blank");

		$id = $this->_getParam("id",0);

		// Javascript doesn't pass the date clicked on. Only the first day of the month
		$date = $this->_getParam("date","");
		$this->view->explode = $explode = explode(" ",$date);
		$this->view->date = $date = date('Y-m-d',strtotime($explode[1]." ".$explode[2]." ".$explode[3]));

		$sql = $this->_db->quoteInto("SELECT * FROM calendar_entries WHERE id = ?",$id);
		$this->view->event = $this->_db->fetchRow($sql);
	}

	public function addAction()
	{
		$this->_helper->layout()->setLayout("blank");

		$date = $this->_getParam("date","");
		$this->view->explode = $explode = explode(" ",$date);
		$this->view->date = $date = date('m/d/Y',strtotime($explode[1]." ".$explode[2]." ".$explode[3]));
	}

	public function icalAction()
	{
		$this->_helper->layout()->setLayout("blank");
		$id = $this->_getParam("id",0);

		$sql = $this->_db->quoteInto("SELECT * FROM calendar_entries WHERE id = ?",$id);
		$this->view->event = $this->_db->fetchRow($sql);
	}

	function init()
	{
		$bootstrap = $this->getInvokeArg('bootstrap'); // gets the boostrapper
		$resource = $bootstrap->getPluginResource('multidb'); //multi db support
		$this->_db = $resource->getDefaultDb();

		$this->view->placeholder('section')->set("calendar");
		$this->view->layout()->setLayout("cyber");

		$this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
		$auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('portal'));
		if(!$auth->hasIdentity()){
			$auth->clearIdentity();
			$this->_redirect('/auth');
		}else{
			$this->view->user = $this->_user = $auth->getIdentity();
			$this->view->placeholder('logged_in')->set(true);

			if ($this->_user->cyber_user != "yes") {
				$auth->clearIdentity();
				$this->_redirect('/auth');
			}
		}

		$subSectionMenu = '<li id="subnav-viewcalendar"><a href="/calendar"><span class="subnav-size">View Calendar</span></a></li>
							';
		$this->view->placeholder("subSectionMenu")->set($subSectionMenu);
	}
}
